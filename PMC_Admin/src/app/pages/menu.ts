export let MENU_ITEM = [
    {
        path: 'index',
        title: 'Dashboard',
        icon: 'dashboard'
    },
   
    {
        path: 'engineer',
        title: 'Engineers',
        icon: 'user'
    },

    {
        path: 'zones',
        title: 'Zones',
        icon: 'table'
    },

    {
        path: 'wards',
        title: 'Wards',
        icon: 'table'
    },

    {
        path: 'issues',
        title: 'Issues',
        icon: 'pencil'
    },

    {
        path: 'observations-list',
        title: 'Notifications',
        icon: 'bell'
    },

    // {
    //     path: 'profile',
    //     title: 'User Profile',
    //     icon: 'user'
    // },

    
   
];
