import { Component, OnInit } from '@angular/core';
import { ChartsService } from '../charts/components/echarts/charts.service';
import { EChartOption } from 'echarts';
import { HttpClient } from '@angular/common/http';
import { Location } from '@angular/common';
import { Http, Response } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
declare var $: any;
import { IMyDrpOptions } from 'mydaterangepicker';
import * as moment from 'moment';
import 'moment/locale/pt-br';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  providers: [ChartsService]
})
export class IndexComponent implements OnInit {
  showloading: boolean = false;

  public AnimationBarOption;

  public viewStore: string;
  wards ;
  mydaterange;
  downloadParam = "";

  server_url: string = environment.devServer_url;
  tokens: any = localStorage.getItem('token');
  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'JWT ' + this.tokens
    })
  };


  myDateRangePickerOptions: IMyDrpOptions = {
    dateFormat:'mmm.dd.yyyy',
    sunHighlight: true,
    height: '40px',
    width: '200%',
    inline: false,
    alignSelectorRight: false,
    indicateInvalidDateRange: true
  };

  constructor(private _router: Router, private route: ActivatedRoute, private http: HttpClient, private location: Location, private _chartsService: ChartsService) { }
  showListTable(showValue) {
    // 
    switch (showValue) {
      case 1:
        this.downloadParam = "";
        this.tableTotalIssues = true;
        this.tableResolvedIssues = false;
        this.tablePendingIssues = false;
        this.tableExcalatedIssues = false;
        document.getElementById("total_issue_id").style.border = "2px solid #465294";
        document.getElementById("resolved_issue_id").style.border = "2px solid #e4e3e3";
        document.getElementById("pending_issue_id").style.border = "2px solid #e4e3e3";
        document.getElementById("excalated_issue_id").style.border = "2px solid #e4e3e3";
        break;
      case 2:
        this.downloadParam = "status=3";  
        this.tableTotalIssues = false;
        this.tableResolvedIssues = true;
        this.tablePendingIssues = false;
        this.tableExcalatedIssues = false;
        document.getElementById("total_issue_id").style.border = "2px solid #e4e3e3";
        document.getElementById("resolved_issue_id").style.border = "2px solid #465294";
        document.getElementById("pending_issue_id").style.border = "2px solid #e4e3e3";
        document.getElementById("excalated_issue_id").style.border = "2px solid #e4e3e3";
        break;
      case 3:
        this.downloadParam = "status=1";  
        this.tableTotalIssues = false;
        this.tableResolvedIssues = false;
        this.tablePendingIssues = true;
        this.tableExcalatedIssues = false;
        document.getElementById("total_issue_id").style.border = "2px solid #e4e3e3";
        document.getElementById("resolved_issue_id").style.border = "2px solid #e4e3e3";
        document.getElementById("pending_issue_id").style.border = "2px solid #465294";
        document.getElementById("excalated_issue_id").style.border = "2px solid #e4e3e3";
        break;
      case 4:
        this.downloadParam = "escalated=1";
        this.tableTotalIssues = false;
        this.tableResolvedIssues = false;
        this.tablePendingIssues = false;
        this.tableExcalatedIssues = true;
        document.getElementById("total_issue_id").style.border = "2px solid #e4e3e3";
        document.getElementById("resolved_issue_id").style.border = "2px solid #e4e3e3";
        document.getElementById("pending_issue_id").style.border = "2px solid #e4e3e3";
        document.getElementById("excalated_issue_id").style.border = "2px solid #465294";
        break;
      default:
        this.downloadParam = "";
        this.tableTotalIssues = true;
        this.tableResolvedIssues = false;
        this.tablePendingIssues = false;
        this.tableExcalatedIssues = false;
        document.getElementById("total_issue_id").style.border = "2px solid #e4e3e3";
        document.getElementById("resolved_issue_id").style.border = "2px solid #e4e3e3";
        document.getElementById("pending_issue_id").style.border = "2px solid #e4e3e3";
        document.getElementById("excalated_issue_id").style.border = "2px solid #e4e3e3";
    }
    // if(showValue==1){
    //   this.tableTotalIssues=true;
    //   this.tableResolvedIssues = false;
    //   this.tablePendingIssues = false;
    //   this.tableExcalatedIssues = false;
    // }

    // if(showValue==2){
    //   this.tableTotalIssues=false;
    //   this.tableResolvedIssues = true;
    //   this.tablePendingIssues = false;
    //   this.tableExcalatedIssues = false;
    // }

    // if(showValue==3){
    //   this.tableTotalIssues=false;
    //   this.tableResolvedIssues = false;
    //   this.tablePendingIssues = true;
    //   this.tableExcalatedIssues = false;

    // }

    // if(showValue==4){
    //   this.tableTotalIssues=false;
    //   this.tableResolvedIssues = false;
    //   this.tablePendingIssues = false;
    //   this.tableExcalatedIssues = true;

    // }
  }
  selectedId: any = [];

  



  tableTotalIssues;
  tableResolvedIssues;
  tablePendingIssues;
  tableExcalatedIssues;
  filterBodyStorage;
  DashboardResolvedData;
  DashboardResolvedDataCount;
  zonename = [];
  getDashboardResolvedData(status) {

    // 
    this.filterBodyStorage = JSON.parse(localStorage.getItem('filterbody'));
    if(this.filterBodyStorage){
      // this.DashboardpageNo = pageNo;
this.filterData(this.filterBodyStorage,1);

    }else{
    // 
    localStorage.removeItem('filterbody');
    // this.id = this.route.snapshot.paramMap.get('id');
    // this.spinnerService.show();
    let localUrl = "ticket/?status=" + status;
    //let localUrl = "brand/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.DashboardResolvedData = res['results'];
          this.DashboardResolvedDataCount = res['count'];

          this.DashboardResolvedData.forEach(obj => {
            // for(let i = 0; i<obj.zones.length; i++){
            let localUrl = "user/" + obj.user;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;

            this.http.get(url, this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz = res;
                  obj.first_name = xyz["first_name"];
                  obj.last_name = xyz["last_name"];
                  obj.mobile_no = xyz["mobile_no"];
                  //this.zonename.push(obj.zonename);
                  // this.spinnerService.hide();
                },
              //err => { alert("Error occoured"); this.spinnerService.hide(); }
            );
            // }
          });
          this.DashboardResolvedData.forEach(obj => {
            // for(let i = 0; i<obj.zones.length; i++){
            let localUrl = "user/" + obj.engineer;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;

            this.http.get(url, this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz = res;
                  obj.engineer_first_name = xyz["first_name"];
                  obj.engineer_last_name = xyz["last_name"];
                  obj.engineer_mobile_no = xyz["mobile_no"];
                  //this.zonename.push(obj.zonename);
                  // this.spinnerService.hide();
                },
              //err => { alert("Error occoured"); this.spinnerService.hide(); }
            );
            // }
          });

          this.DashboardResolvedData.forEach(objjj => {
            // for(let i = 0; i<objj.wards.length; i++){
            let localUrl = "ticketimage/?ticket="+objjj.id;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;

            this.http.get(url,this.httpOptions)
              .subscribe(
                (res: Response) => {
                  // var xyzzz=res['results'];
                  objjj.images=res['results'];
                  // this.spinnerService.hide();
                },
                //err => { alert("Error occoured"); this.spinnerService.hide(); }
              );
            // }
        });

          //     this.UserData.forEach(objj => {
          //       for(let i = 0; i<objj.wards.length; i++){
          //       let localUrl = "ward/"+objj.wards[i];
          //       //let localUrl = "brand/";
          //       let url = this.server_url + localUrl;

          //       this.http.get(url,this.httpOptions)
          //         .subscribe(
          //           (res: Response) => {
          //             var xyz=res;
          //             objj.wardname=xyz["name"];
          //             // this.spinnerService.hide();
          //           },
          //           //err => { alert("Error occoured"); this.spinnerService.hide(); }
          //         );
          //       }
          //   });
          // this.spinnerService.hide();
        },
        err => {
          alert("Error occoured");
          // this.spinnerService.hide();
        }
      );
    }
  }

  DashboardData;
  DashboardDataCount;
  
  getDashboardAllData() {

    this.filterBodyStorage = JSON.parse(localStorage.getItem('filterbody'));
    if(this.filterBodyStorage){
      // this.DashboardpageNo = pageNo;
this.filterData(this.filterBodyStorage,1);

    }else{
    
    localStorage.removeItem('filterbody');
  //  
    // this.id = this.route.snapshot.paramMap.get('id');
    // this.spinnerService.show();
    let localUrl = "ticket/";
    //let localUrl = "brand/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.DashboardData = res['results'];
          this.DashboardDataCount = res['count'];

          this.DashboardData.forEach(obj => {
            // for(let i = 0; i<obj.zones.length; i++){
            let localUrl = "user/" + obj.user;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;

            this.http.get(url, this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz = res;
                  obj.first_name = xyz["first_name"];
                  obj.last_name = xyz["last_name"];
                  obj.mobile_no = xyz["mobile_no"];
                  //this.zonename.push(obj.zonename);
                  // this.spinnerService.hide();
                },
              //err => { alert("Error occoured"); this.spinnerService.hide(); }
            );
            // }
          });
          this.DashboardData.forEach(obj => {
            // for(let i = 0; i<obj.zones.length; i++){
            let localUrl = "user/" + obj.engineer;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;

            this.http.get(url, this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz = res;
                  obj.engineer_first_name = xyz["first_name"];
                  obj.engineer_last_name = xyz["last_name"];
                  obj.engineer_mobile_no = xyz["mobile_no"];
                  //this.zonename.push(obj.zonename);
                  // this.spinnerService.hide();
                },
              //err => { alert("Error occoured"); this.spinnerService.hide(); }
            );
            // }
          });

              this.DashboardData.forEach(objjj => {
                // for(let i = 0; i<objj.wards.length; i++){
                let localUrl = "ticketimage/?ticket="+objjj.id;
                //let localUrl = "brand/";
                let url = this.server_url + localUrl;

                this.http.get(url,this.httpOptions)
                  .subscribe(
                    (res: Response) => {
                     // var xyzzz=res['results'];
                      objjj.images=res['results'];
                      // this.spinnerService.hide();
                    },
                    //err => { alert("Error occoured"); this.spinnerService.hide(); }
                  );
                // }
            });
          // this.spinnerService.hide();
        },
        err => {
          alert("Error occoured");
          // this.spinnerService.hide();
        }
      );
    }
  }


  DashboardPendingData;
  DashboardPendingDataCount;
  // zonename=[];
  getDashboardPendingData(status) {
    // 
    this.filterBodyStorage = JSON.parse(localStorage.getItem('filterbody'));
    if(this.filterBodyStorage){
      // this.DashboardpageNo = pageNo;
this.filterData(this.filterBodyStorage,1);

    }else{

    localStorage.removeItem('filterbody');
    // 
    // this.id = this.route.snapshot.paramMap.get('id');
    // this.spinnerService.show();
    let localUrl = "ticket/?status=" + status;
    //let localUrl = "brand/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.DashboardPendingData = res['results'];
          this.DashboardPendingDataCount = res['count'];

          this.DashboardPendingData.forEach(obj => {
            // for(let i = 0; i<obj.zones.length; i++){
            let localUrl = "user/" + obj.user;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;

            this.http.get(url, this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz = res;
                  obj.first_name = xyz["first_name"];
                  obj.last_name = xyz["last_name"];
                  obj.mobile_no = xyz["mobile_no"];
                  //this.zonename.push(obj.zonename);
                  // this.spinnerService.hide();
                },
              //err => { alert("Error occoured"); this.spinnerService.hide(); }
            );
            // }
          });
          this.DashboardPendingData.forEach(obj => {
            // for(let i = 0; i<obj.zones.length; i++){
            let localUrl = "user/" + obj.engineer;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;

            this.http.get(url, this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz = res;
                  obj.engineer_first_name = xyz["first_name"];
                  obj.engineer_last_name = xyz["last_name"];
                  obj.engineer_mobile_no = xyz["mobile_no"];
                  //this.zonename.push(obj.zonename);
                  // this.spinnerService.hide();
                },
              //err => { alert("Error occoured"); this.spinnerService.hide(); }
            );
            // }
          });

        //   this.DashboardPendingData.forEach(objjj => {
        //     // for(let i = 0; i<objj.wards.length; i++){
        //     let localUrl = "ticketimage/?ticket="+objjj.id;
        //     //let localUrl = "brand/";
        //     let url = this.server_url + localUrl;

        //     this.http.get(url,this.httpOptions)
        //       .subscribe(
        //         (res: Response) => {
        //           var xyzzz=res['results'];
        //           objjj.images=xyzzz[0]["image"];
        //           // this.spinnerService.hide();
        //         },
        //         //err => { alert("Error occoured"); this.spinnerService.hide(); }
        //       );
        //     // }
        // });
          // this.spinnerService.hide();
        },
        err => {
          alert("Error occoured");
          // this.spinnerService.hide();
        }
      );
    }
  }

  DashboardEscalatedData;
  DashboardEscalatedDataCount;
  // zonename=[];
  getDashboardEscalatedData() {

    // 
    this.filterBodyStorage = JSON.parse(localStorage.getItem('filterbody'));
    if(this.filterBodyStorage){
      // this.DashboardpageNo = pageNo;
this.filterData(this.filterBodyStorage,1);

    }else{

    localStorage.removeItem('filterbody');
    // 
    // this.id = this.route.snapshot.paramMap.get('id');
    // this.spinnerService.show();
    let localUrl = "ticket/?escalated=1";
    //let localUrl = "brand/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.DashboardEscalatedData = res['results'];
          this.DashboardEscalatedDataCount = res['count'];

          this.DashboardEscalatedData.forEach(obj => {
            // for(let i = 0; i<obj.zones.length; i++){
            let localUrl = "user/" + obj.user;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;

            this.http.get(url, this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz = res;
                  obj.first_name = xyz["first_name"];
                  obj.last_name = xyz["last_name"];
                  obj.mobile_no = xyz["mobile_no"];
                  //this.zonename.push(obj.zonename);
                  // this.spinnerService.hide();
                },
              //err => { alert("Error occoured"); this.spinnerService.hide(); }
            );
            // }
          });
          this.DashboardEscalatedData.forEach(obj => {
            // for(let i = 0; i<obj.zones.length; i++){
            let localUrl = "user/" + obj.engineer;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;

            this.http.get(url, this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz = res;
                  obj.engineer_first_name = xyz["first_name"];
                  obj.engineer_last_name = xyz["last_name"];
                  obj.engineer_mobile_no = xyz["mobile_no"];
                  //this.zonename.push(obj.zonename);
                  // this.spinnerService.hide();
                },
              //err => { alert("Error occoured"); this.spinnerService.hide(); }
            );
            // }
          });
          this.DashboardEscalatedData.forEach(objjj => {
            // for(let i = 0; i<objj.wards.length; i++){
            let localUrl = "ticketimage/?ticket="+objjj.id;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;

            this.http.get(url,this.httpOptions)
              .subscribe(
                (res: Response) => {
                  // var xyzzz=res['results'];
                  objjj.images=res['results'];
                  // this.spinnerService.hide();
                },
                //err => { alert("Error occoured"); this.spinnerService.hide(); }
              );
            // }
        });

          //     this.UserData.forEach(objj => {
          //       for(let i = 0; i<objj.wards.length; i++){
          //       let localUrl = "ward/"+objj.wards[i];
          //       //let localUrl = "brand/";
          //       let url = this.server_url + localUrl;

          //       this.http.get(url,this.httpOptions)
          //         .subscribe(
          //           (res: Response) => {
          //             var xyz=res;
          //             objj.wardname=xyz["name"];
          //             // this.spinnerService.hide();
          //           },
          //           //err => { alert("Error occoured"); this.spinnerService.hide(); }
          //         );
          //       }
          //   });
          // this.spinnerService.hide();
        },
        err => {
          alert("Error occoured");
          // this.spinnerService.hide();
        }
      );
    }
  }

  filterDatapagination(filterBody): void {
    // ;
    
   // localStorage.setItem('filterBody', JSON.stringify(this.filterBody));
    //filterBody.ref_vehicle_project_id = project_Id;
    //filterBody.ref_shop_id = this.ReportService.getShopId();
    //this.exportFilterBody = filterBody;
    //this.ReportService.setFilterData(this.exportFilterBody);
//this.dashboardpageNo=1;
let localUrl = "ticket/?page=" + this.DashboardpageNo+"&";
    if (filterBody.wards) {
      localUrl = localUrl + "ward=" +filterBody.wards + "&"
    }
    if (filterBody.jrengineer) {
      localUrl = localUrl + "engineer=" +filterBody.jrengineer + "&"
    }
    if (filterBody.fromDate && filterBody.toDate) {
      localUrl = localUrl +"from="+filterBody.fromDate+"&"+"to="+filterBody.toDate;
    }
    // let localUrl = "stat/brand/?user_code="+filterBody.searchShopcode
    let url = this.server_url + localUrl;
    // ticket/?zone=3&ward=5&ticket_type=1

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {

          this.DashboardData = res['results'];
          this.DashboardDataCount = res['count'];

          this.DashboardData.forEach(obj => {
            // for(let i = 0; i<obj.zones.length; i++){
            let localUrl = "user/" + obj.user;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;

            this.http.get(url, this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz = res;
                  obj.first_name = xyz["first_name"];
                  obj.last_name = xyz["last_name"];
                  obj.mobile_no = xyz["mobile_no"];
                  //this.zonename.push(obj.zonename);
                  // this.spinnerService.hide();
                },
              //err => { alert("Error occoured"); this.spinnerService.hide(); }
            );
            // }
          });
          this.DashboardData.forEach(obj => {
            // for(let i = 0; i<obj.zones.length; i++){
            let localUrl = "user/" + obj.engineer;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;

            this.http.get(url, this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz = res;
                  obj.engineer_first_name = xyz["first_name"];
                  obj.engineer_last_name = xyz["last_name"];
                  obj.engineer_mobile_no = xyz["mobile_no"];
                  //this.zonename.push(obj.zonename);
                  // this.spinnerService.hide();
                },
              //err => { alert("Error occoured"); this.spinnerService.hide(); }
            );
            // }
          });
          this.DashboardData.forEach(objjj => {
            // for(let i = 0; i<objj.wards.length; i++){
            let localUrl = "ticketimage/?ticket="+objjj.id;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;

            this.http.get(url,this.httpOptions)
              .subscribe(
                (res: Response) => {
                  // var xyzzz=res['results'];
                  objjj.images=res['results'];
                  // this.spinnerService.hide();
                },
                //err => { alert("Error occoured"); this.spinnerService.hide(); }
              );
            // }
        });

          // this.DataobjectTopbrands = res;
          //return this.DataobjectTopbrands;
          //this.totalcustomer = res['data']['surveysCount'][0]['totalCustomer'];
          //this.totalissues = this.DataobjectTopissues.graphData.length;

          // this.barChartLabelsDelivery=[];
          // this.dataArray=[];
          // this.pphDelivery=[];

          // for(let i=0; i< this.DataobjectTopissues.graphData.length; i++){
          //  this.barChartLabelsDelivery.push(this.DataobjectTopissues.graphData[i]['master_problem_sub_categories_name']);



          //  for(let j=0; j< this.DataobjectTopissues.surveysCount.length; j++){
          //      this.pphDelivery = (this.DataobjectTopissues.graphData[i]['customerReportedCnt']/this.DataobjectTopissues.surveysCount[j]['totalCustomer'])*100;
          //      this.pphDelivery = this.pphDelivery.toFixed(1);

          //      this.dataArray.push(this.pphDelivery);
          //     }

          //   }
          // for(let k =0; k<this.DataobjectTopissues.graphData.length; k++){
          //   this.totalissues += this.DataobjectTopissues.graphData[k].customerReportedCnt;

          //  }
          //  this.totalpph = ((this.totalissues/this.totalcustomer)*100).toFixed(1);


          // this.getbargraph();

          //  this.spinnerService.hide(); 
        },
        err => {
          // this.spinnerService.hide(); 
          alert("Error occoured");
        }
      );

    // } else{
    //  this.spinnerService.hide();
    //  alert("Please select Project Name");

    // }// if else form.value close

  }

  filterDatapagination2(filterBody): void {
    // ;
    
   // localStorage.setItem('filterBody', JSON.stringify(this.filterBody));
    //filterBody.ref_vehicle_project_id = project_Id;
    //filterBody.ref_shop_id = this.ReportService.getShopId();
    //this.exportFilterBody = filterBody;
    //this.ReportService.setFilterData(this.exportFilterBody);
//this.dashboardpageNo=1;
let localUrl = "ticket/?page=" + this.DashboardpageNo2+"&status=3&";
// let localUrl = "ticket/?status=3&"
if (filterBody.wards) {
  localUrl = localUrl + "ward=" +filterBody.wards + "&"
}
if (filterBody.jrengineer) {
  localUrl = localUrl + "engineer=" +filterBody.jrengineer + "&"
}
if (filterBody.fromDate && filterBody.toDate) {
  localUrl = localUrl +"from="+filterBody.fromDate+"&"+"to="+filterBody.toDate;
}
// let localUrl = "stat/brand/?user_code="+filterBody.searchShopcode
let url = this.server_url + localUrl;
// ticket/?zone=3&ward=5&ticket_type=1

this.http.get(url, this.httpOptions)
  .subscribe(
    (res: Response) => {

      this.DashboardResolvedData = res['results'];
      this.DashboardResolvedDataCount = res['count'];

      this.DashboardResolvedData.forEach(obj => {
        // for(let i = 0; i<obj.zones.length; i++){
        let localUrl = "user/" + obj.user;
        //let localUrl = "brand/";
        let url = this.server_url + localUrl;

        this.http.get(url, this.httpOptions)
          .subscribe(
            (res: Response) => {
              var xyz = res;
              obj.first_name = xyz["first_name"];
              obj.last_name = xyz["last_name"];
              obj.mobile_no = xyz["mobile_no"];
              //this.zonename.push(obj.zonename);
              // this.spinnerService.hide();
            },
          //err => { alert("Error occoured"); this.spinnerService.hide(); }
        );
        // }
      });
      this.DashboardResolvedData.forEach(obj => {
        // for(let i = 0; i<obj.zones.length; i++){
        let localUrl = "user/" + obj.engineer;
        //let localUrl = "brand/";
        let url = this.server_url + localUrl;

        this.http.get(url, this.httpOptions)
          .subscribe(
            (res: Response) => {
              var xyz = res;
              obj.engineer_first_name = xyz["first_name"];
              obj.engineer_last_name = xyz["last_name"];
              obj.engineer_mobile_no = xyz["mobile_no"];
              //this.zonename.push(obj.zonename);
              // this.spinnerService.hide();
            },
          //err => { alert("Error occoured"); this.spinnerService.hide(); }
        );
        // }
      });

      this.DashboardResolvedData.forEach(objjj => {
        // for(let i = 0; i<objj.wards.length; i++){
        let localUrl = "ticketimage/?ticket="+objjj.id;
        //let localUrl = "brand/";
        let url = this.server_url + localUrl;

        this.http.get(url,this.httpOptions)
          .subscribe(
            (res: Response) => {
              // var xyzzz=res['results'];
              objjj.images=res['results'];
              // this.spinnerService.hide();
            },
            //err => { alert("Error occoured"); this.spinnerService.hide(); }
          );
        // }
    });
      // this.getbargraph();

      //  this.spinnerService.hide(); 
    },
    err => {
      // this.spinnerService.hide(); 
      alert("Error occoured");
    }
  );

    // } else{
    //  this.spinnerService.hide();
    //  alert("Please select Project Name");

    // }// if else form.value close

  }

  filterDatapagination3(filterBody): void {
    // ;
    
   // localStorage.setItem('filterBody', JSON.stringify(this.filterBody));
    //filterBody.ref_vehicle_project_id = project_Id;
    //filterBody.ref_shop_id = this.ReportService.getShopId();
    //this.exportFilterBody = filterBody;
    //this.ReportService.setFilterData(this.exportFilterBody);
//this.dashboardpageNo=1;
let localUrl = "ticket/?page=" + this.DashboardpageNo2+"&status=1&";
      if (filterBody.wards) {
        localUrl = localUrl + "ward=" +filterBody.wards + "&"
      }
      if (filterBody.jrengineer) {
        localUrl = localUrl + "engineer=" +filterBody.jrengineer + "&"
      }
      if (filterBody.fromDate && filterBody.toDate) {
        localUrl = localUrl +"from="+filterBody.fromDate+"&"+"to="+filterBody.toDate;
      }
      // let localUrl = "stat/brand/?user_code="+filterBody.searchShopcode
      let url = this.server_url + localUrl;
      // ticket/?zone=3&ward=5&ticket_type=1
  
      this.http.get(url, this.httpOptions)
        .subscribe(
          (res: Response) => {
  
            this.DashboardPendingData = res['results'];
            this.DashboardPendingDataCount = res['count'];
  
            this.DashboardPendingData.forEach(obj => {
              // for(let i = 0; i<obj.zones.length; i++){
              let localUrl = "user/" + obj.user;
              //let localUrl = "brand/";
              let url = this.server_url + localUrl;
  
              this.http.get(url, this.httpOptions)
                .subscribe(
                  (res: Response) => {
                    var xyz = res;
                    obj.first_name = xyz["first_name"];
                    obj.last_name = xyz["last_name"];
                    obj.mobile_no = xyz["mobile_no"];
                    //this.zonename.push(obj.zonename);
                    // this.spinnerService.hide();
                  },
                //err => { alert("Error occoured"); this.spinnerService.hide(); }
              );
              // }
            });
            this.DashboardPendingData.forEach(obj => {
              // for(let i = 0; i<obj.zones.length; i++){
              let localUrl = "user/" + obj.engineer;
              //let localUrl = "brand/";
              let url = this.server_url + localUrl;
  
              this.http.get(url, this.httpOptions)
                .subscribe(
                  (res: Response) => {
                    var xyz = res;
                    obj.engineer_first_name = xyz["first_name"];
                    obj.engineer_last_name = xyz["last_name"];
                    obj.engineer_mobile_no = xyz["mobile_no"];
                    //this.zonename.push(obj.zonename);
                    // this.spinnerService.hide();
                  },
                //err => { alert("Error occoured"); this.spinnerService.hide(); }
              );
              // }
            });

            this.DashboardPendingData.forEach(objjj => {
              // for(let i = 0; i<objj.wards.length; i++){
              let localUrl = "ticketimage/?ticket="+objjj.id;
              //let localUrl = "brand/";
              let url = this.server_url + localUrl;
      
              this.http.get(url,this.httpOptions)
                .subscribe(
                  (res: Response) => {
                    // var xyzzz=res['results'];
                    objjj.images=res['results'];
                    // this.spinnerService.hide();
                  },
                  //err => { alert("Error occoured"); this.spinnerService.hide(); }
                );
              // }
          });
            // this.getbargraph();
  
            //  this.spinnerService.hide(); 
          },
          err => {
            // this.spinnerService.hide(); 
            alert("Error occoured");
          }
        );

    // } else{
    //  this.spinnerService.hide();
    //  alert("Please select Project Name");

    // }// if else form.value close

  }

  filterDatapagination4(filterBody): void {
    // ;
    
   // localStorage.setItem('filterBody', JSON.stringify(this.filterBody));
    //filterBody.ref_vehicle_project_id = project_Id;
    //filterBody.ref_shop_id = this.ReportService.getShopId();
    //this.exportFilterBody = filterBody;
    //this.ReportService.setFilterData(this.exportFilterBody);
//this.dashboardpageNo=1;
let localUrl = "ticket/?page=" + this.DashboardpageNo2+"&escalated=1&";
// let localUrl = "ticket/?escalated=1&"
if (filterBody.wards) {
  localUrl = localUrl + "ward=" +filterBody.wards + "&"
}
if (filterBody.jrengineer) {
  localUrl = localUrl + "engineer=" +filterBody.jrengineer + "&"
}
if (filterBody.fromDate && filterBody.toDate) {
  localUrl = localUrl +"from="+filterBody.fromDate+"&"+"to="+filterBody.toDate;
}
// let localUrl = "stat/brand/?user_code="+filterBody.searchShopcode
let url = this.server_url + localUrl;
// ticket/?zone=3&ward=5&ticket_type=1

this.http.get(url, this.httpOptions)
  .subscribe(
    (res: Response) => {

      this.DashboardEscalatedData = res['results'];
    this.DashboardEscalatedDataCount = res['count'];

      this.DashboardEscalatedData.forEach(obj => {
        // for(let i = 0; i<obj.zones.length; i++){
        let localUrl = "user/" + obj.user;
        //let localUrl = "brand/";
        let url = this.server_url + localUrl;

        this.http.get(url, this.httpOptions)
          .subscribe(
            (res: Response) => {
              var xyz = res;
              obj.first_name = xyz["first_name"];
              obj.last_name = xyz["last_name"];
              obj.mobile_no = xyz["mobile_no"];
              //this.zonename.push(obj.zonename);
              // this.spinnerService.hide();
            },
          //err => { alert("Error occoured"); this.spinnerService.hide(); }
        );
        // }
      });
      this.DashboardEscalatedData.forEach(obj => {
        // for(let i = 0; i<obj.zones.length; i++){
        let localUrl = "user/" + obj.engineer;
        //let localUrl = "brand/";
        let url = this.server_url + localUrl;

        this.http.get(url, this.httpOptions)
          .subscribe(
            (res: Response) => {
              var xyz = res;
              obj.engineer_first_name = xyz["first_name"];
              obj.engineer_last_name = xyz["last_name"];
              obj.engineer_mobile_no = xyz["mobile_no"];
              //this.zonename.push(obj.zonename);
              // this.spinnerService.hide();
            },
          //err => { alert("Error occoured"); this.spinnerService.hide(); }
        );
        // }
      });

      this.DashboardEscalatedData.forEach(objjj => {
        // for(let i = 0; i<objj.wards.length; i++){
        let localUrl = "ticketimage/?ticket="+objjj.id;
        //let localUrl = "brand/";
        let url = this.server_url + localUrl;

        this.http.get(url,this.httpOptions)
          .subscribe(
            (res: Response) => {
              // var xyzzz=res['results'];
              objjj.images=res['results'];
              // this.spinnerService.hide();
            },
            //err => { alert("Error occoured"); this.spinnerService.hide(); }
          );
        // }
    });
      // this.getbargraph();

      //  this.spinnerService.hide(); 
    },
    err => {
      // this.spinnerService.hide(); 
      alert("Error occoured");
    }
  );

    // } else{
    //  this.spinnerService.hide();
    //  alert("Please select Project Name");

    // }// if else form.value close

  }

  DashboardpageNo: number = 1;
  pageChange(pageNo: any) {
    // 
    // this.selectedId;
    this.filterBodyPagination = JSON.parse(localStorage.getItem('filterbody'));
    if(this.filterBodyPagination){
      this.DashboardpageNo = pageNo;
this.filterDatapagination(this.filterBodyPagination);

    }else{
    this.DashboardpageNo = pageNo;
    console.log("pageNo: " + this.DashboardpageNo);

    // this.spinnerService.show();
    let localUrl = "ticket/?page=" + this.DashboardpageNo;
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.DashboardData = res['results'];
          this.DashboardDataCount = res['count'];

          this.DashboardData.forEach(obj => {
            // for(let i = 0; i<obj.zones.length; i++){
            let localUrl = "user/" + obj.user;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;

            this.http.get(url, this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz = res;
                  obj.first_name = xyz["first_name"];
                  obj.last_name = xyz["last_name"];
                  obj.mobile_no = xyz["mobile_no"];
                  //this.zonename.push(obj.zonename);
                  // this.spinnerService.hide();
                },
              //err => { alert("Error occoured"); this.spinnerService.hide(); }
            );
            // }
          });
          this.DashboardData.forEach(obj => {
            // for(let i = 0; i<obj.zones.length; i++){
            let localUrl = "user/" + obj.engineer;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;

            this.http.get(url, this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz = res;
                  obj.engineer_first_name = xyz["first_name"];
                  obj.engineer_last_name = xyz["last_name"];
                  obj.engineer_mobile_no = xyz["mobile_no"];
                  //this.zonename.push(obj.zonename);
                  // this.spinnerService.hide();
                },
              //err => { alert("Error occoured"); this.spinnerService.hide(); }
            );
            // }
          });

          this.DashboardData.forEach(objjj => {
            // for(let i = 0; i<objj.wards.length; i++){
            let localUrl = "ticketimage/?ticket="+objjj.id;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;

            this.http.get(url,this.httpOptions)
              .subscribe(
                (res: Response) => {
                  // var xyzzz=res['results'];
                  objjj.images=res['results'];
                  // this.spinnerService.hide();
                },
                //err => { alert("Error occoured"); this.spinnerService.hide(); }
              );
            // }
        });

          //     this.UserData.forEach(objj => {
          //       for(let i = 0; i<objj.wards.length; i++){
          //       let localUrl = "ward/"+objj.wards[i];
          //       //let localUrl = "brand/";
          //       let url = this.server_url + localUrl;

          //       this.http.get(url,this.httpOptions)
          //         .subscribe(
          //           (res: Response) => {
          //             var xyz=res;
          //             objj.wardname=xyz["name"];
          //             // this.spinnerService.hide();
          //           },
          //           //err => { alert("Error occoured"); this.spinnerService.hide(); }
          //         );
          //       }
          //   });
          // this.spinnerService.hide();
        },
    );
  }
  }

  DashboardpageNo2: number = 1;
  pageChange2(pageNo: any) {
    // 
    // this.selectedId;
    this.filterBodyPagination = JSON.parse(localStorage.getItem('filterbody'));
    if(this.filterBodyPagination){
      this.DashboardpageNo2 = pageNo;
this.filterDatapagination2(this.filterBodyPagination);

    }else{
    this.DashboardpageNo2 = pageNo;
    console.log("pageNo: " + this.DashboardpageNo2);

    // this.spinnerService.show();
    let localUrl = "ticket/?page=" + this.DashboardpageNo2 + "&status=3";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.DashboardResolvedData = res['results'];
          this.DashboardResolvedDataCount = res['count'];

          this.DashboardResolvedData.forEach(obj => {
            // for(let i = 0; i<obj.zones.length; i++){
            let localUrl = "user/" + obj.user;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;

            this.http.get(url, this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz = res;
                  obj.first_name = xyz["first_name"];
                  obj.last_name = xyz["last_name"];
                  obj.mobile_no = xyz["mobile_no"];
                  //this.zonename.push(obj.zonename);
                  // this.spinnerService.hide();
                },
              //err => { alert("Error occoured"); this.spinnerService.hide(); }
            );
            // }
          });
          this.DashboardResolvedData.forEach(obj => {
            // for(let i = 0; i<obj.zones.length; i++){
            let localUrl = "user/" + obj.engineer;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;

            this.http.get(url, this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz = res;
                  obj.engineer_first_name = xyz["first_name"];
                  obj.engineer_last_name = xyz["last_name"];
                  obj.engineer_mobile_no = xyz["mobile_no"];
                  //this.zonename.push(obj.zonename);
                  // this.spinnerService.hide();
                },
              //err => { alert("Error occoured"); this.spinnerService.hide(); }
            );
            // }
          });

          this.DashboardResolvedData.forEach(objjj => {
            // for(let i = 0; i<objj.wards.length; i++){
            let localUrl = "ticketimage/?ticket="+objjj.id;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;

            this.http.get(url,this.httpOptions)
              .subscribe(
                (res: Response) => {
                  // var xyzzz=res['results'];
                  objjj.images=res['results'];
                  // this.spinnerService.hide();
                },
                //err => { alert("Error occoured"); this.spinnerService.hide(); }
              );
            // }
        });

          //     this.UserData.forEach(objj => {
          //       for(let i = 0; i<objj.wards.length; i++){
          //       let localUrl = "ward/"+objj.wards[i];
          //       //let localUrl = "brand/";
          //       let url = this.server_url + localUrl;

          //       this.http.get(url,this.httpOptions)
          //         .subscribe(
          //           (res: Response) => {
          //             var xyz=res;
          //             objj.wardname=xyz["name"];
          //             // this.spinnerService.hide();
          //           },
          //           //err => { alert("Error occoured"); this.spinnerService.hide(); }
          //         );
          //       }
          //   });
          // this.spinnerService.hide();
        },
        err => {
          alert("Error occoured");
          // this.spinnerService.hide();
        }
      );
    }
  }

  DashboardpageNo3: number = 1;
  pageChange3(pageNo: any) {
    // 
    // this.selectedId;

    this.filterBodyPagination = JSON.parse(localStorage.getItem('filterbody'));
    if(this.filterBodyPagination){
      this.DashboardpageNo3 = pageNo;
this.filterDatapagination3(this.filterBodyPagination);

    }else{
    this.DashboardpageNo3 = pageNo;
    console.log("pageNo: " + this.DashboardpageNo3);

    // this.spinnerService.show();
    let localUrl = "ticket/?page=" + this.DashboardpageNo3 + "&status=1";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.DashboardPendingData = res['results'];
          this.DashboardPendingDataCount = res['count'];

          this.DashboardPendingData.forEach(obj => {
            // for(let i = 0; i<obj.zones.length; i++){
            let localUrl = "user/" + obj.user;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;

            this.http.get(url, this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz = res;
                  obj.first_name = xyz["first_name"];
                  obj.last_name = xyz["last_name"];
                  obj.mobile_no = xyz["mobile_no"];
                  //this.zonename.push(obj.zonename);
                  // this.spinnerService.hide();
                },
              //err => { alert("Error occoured"); this.spinnerService.hide(); }
            );
            // }
          });
          this.DashboardPendingData.forEach(obj => {
            // for(let i = 0; i<obj.zones.length; i++){
            let localUrl = "user/" + obj.engineer;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;

            this.http.get(url, this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz = res;
                  obj.engineer_first_name = xyz["first_name"];
                  obj.engineer_last_name = xyz["last_name"];
                  obj.engineer_mobile_no = xyz["mobile_no"];
                  //this.zonename.push(obj.zonename);
                  // this.spinnerService.hide();
                },
              //err => { alert("Error occoured"); this.spinnerService.hide(); }
            );
            // }
          });

          this.DashboardPendingData.forEach(objjj => {
            // for(let i = 0; i<objj.wards.length; i++){
            let localUrl = "ticketimage/?ticket="+objjj.id;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;

            this.http.get(url,this.httpOptions)
              .subscribe(
                (res: Response) => {
                  // var xyzzz=res['results'];
                  objjj.images=res['results'];
                  // this.spinnerService.hide();
                },
                //err => { alert("Error occoured"); this.spinnerService.hide(); }
              );
            // }
        });
        //   this.DashboardPendingData.forEach(objjj => {
        //     // for(let i = 0; i<objj.wards.length; i++){
        //     let localUrl = "ticketimage/?ticket="+objjj.id;
        //     //let localUrl = "brand/";
        //     let url = this.server_url + localUrl;

        //     this.http.get(url,this.httpOptions)
        //       .subscribe(
        //         (res: Response) => {
        //           var xyzzz=res['results'];
        //           objjj.images=xyzzz[0]["image"];
        //           // this.spinnerService.hide();
        //         },
        //         //err => { alert("Error occoured"); this.spinnerService.hide(); }
        //       );
        //     // }
        // });

          //     this.UserData.forEach(objj => {
          //       for(let i = 0; i<objj.wards.length; i++){
          //       let localUrl = "ward/"+objj.wards[i];
          //       //let localUrl = "brand/";
          //       let url = this.server_url + localUrl;

          //       this.http.get(url,this.httpOptions)
          //         .subscribe(
          //           (res: Response) => {
          //             var xyz=res;
          //             objj.wardname=xyz["name"];
          //             // this.spinnerService.hide();
          //           },
          //           //err => { alert("Error occoured"); this.spinnerService.hide(); }
          //         );
          //       }
          //   });
          // this.spinnerService.hide();
        },
        err => {
          alert("Error occoured");
          // this.spinnerService.hide();
        }
      );
  }
  }
  DashboardpageNo4: number = 1;
  pageChange4(pageNo: any) {
    // 
    // this.selectedId;
    this.filterBodyPagination = JSON.parse(localStorage.getItem('filterbody'));
    if(this.filterBodyPagination){
      this.DashboardpageNo4 = pageNo;
this.filterDatapagination4(this.filterBodyPagination);

    }else{
    this.DashboardpageNo4 = pageNo;
    console.log("pageNo: " + this.DashboardpageNo4);

    // this.spinnerService.show();
    let localUrl = "ticket/?page=" + this.DashboardpageNo4 + "?escalated=1";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.DashboardEscalatedData = res['results'];
          this.DashboardEscalatedDataCount = res['count'];

          this.DashboardEscalatedData.forEach(obj => {
            // for(let i = 0; i<obj.zones.length; i++){
            let localUrl = "user/" + obj.user;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;

            this.http.get(url, this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz = res;
                  obj.first_name = xyz["first_name"];
                  obj.last_name = xyz["last_name"];
                  obj.mobile_no = xyz["mobile_no"];
                  //this.zonename.push(obj.zonename);
                  // this.spinnerService.hide();
                },
              //err => { alert("Error occoured"); this.spinnerService.hide(); }
            );
            // }
          });
          this.DashboardEscalatedData.forEach(obj => {
            // for(let i = 0; i<obj.zones.length; i++){
            let localUrl = "user/" + obj.engineer;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;

            this.http.get(url, this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz = res;
                  obj.engineer_first_name = xyz["first_name"];
                  obj.engineer_last_name = xyz["last_name"];
                  obj.engineer_mobile_no = xyz["mobile_no"];
                  //this.zonename.push(obj.zonename);
                  // this.spinnerService.hide();
                },
              //err => { alert("Error occoured"); this.spinnerService.hide(); }
            );
            // }
          });

          this.DashboardEscalatedData.forEach(objjj => {
            // for(let i = 0; i<objj.wards.length; i++){
            let localUrl = "ticketimage/?ticket="+objjj.id;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;

            this.http.get(url,this.httpOptions)
              .subscribe(
                (res: Response) => {
                  // var xyzzz=res['results'];
                  objjj.images=res['results'];
                  // this.spinnerService.hide();
                },
                //err => { alert("Error occoured"); this.spinnerService.hide(); }
              );
            // }
        });

          //     this.UserData.forEach(objj => {
          //       for(let i = 0; i<objj.wards.length; i++){
          //       let localUrl = "ward/"+objj.wards[i];
          //       //let localUrl = "brand/";
          //       let url = this.server_url + localUrl;

          //       this.http.get(url,this.httpOptions)
          //         .subscribe(
          //           (res: Response) => {
          //             var xyz=res;
          //             objj.wardname=xyz["name"];
          //             // this.spinnerService.hide();
          //           },
          //           //err => { alert("Error occoured"); this.spinnerService.hide(); }
          //         );
          //       }
          //   });
          // this.spinnerService.hide();
        },
        err => {
          alert("Error occoured");
          // this.spinnerService.hide();
        }
      );
  }
}

  WardData = [];
  WardDataCount;
  getWardData(userpageNo) {
    // 
    // this.id = this.route.snapshot.paramMap.get('id');
    // this.spinnerService.show();
    let localUrl = "ward/?page=" + userpageNo;
    //let localUrl = "brand/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          // this.WardData = res['results'];
          this.WardDataCount = res['count'];

          this.WardData = this.WardData.concat(res['results']);
          //this.allcategoryDataDropdown.push(this.allcategoryData);
          if (this.WardData.length < this.WardDataCount) {
            this.getWardData(userpageNo + 1);
          }
          //   this.WardData.forEach(obj => {

          //     let localUrl = "zone/"+obj.zone+ "/";
          //     //let localUrl = "brand/";
          //     let url = this.server_url + localUrl;

          //     this.http.get(url,this.httpOptions)
          //       .subscribe(
          //         (res: Response) => {
          //           var xyz=res;
          //           obj.Zonename=xyz["name"];
          //           // this.spinnerService.hide();
          //         },
          //         //err => { alert("Error occoured"); this.spinnerService.hide(); }
          //       );

          // });
          // this.spinnerService.hide();
        },
        err => {
          alert("Error occoured");
          // this.spinnerService.hide();
        }
      );
  }

  getDashboardDataByWard(id) {
    // 
    // this.id = this.route.snapshot.paramMap.get('id');
    // this.spinnerService.show();
    let localUrl = "ticket/";
    //let localUrl = "brand/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.DashboardData = res['results'].filter(user => user.ward == id);
          this.DashboardDataCount = res['count'];

          this.DashboardData.forEach(obj => {
            // for(let i = 0; i<obj.zones.length; i++){
            let localUrl = "user/" + obj.user;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;

            this.http.get(url, this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz = res;
                  obj.first_name = xyz["first_name"];
                  obj.last_name = xyz["last_name"];
                  obj.mobile_no = xyz["mobile_no"];
                  //this.zonename.push(obj.zonename);
                  // this.spinnerService.hide();
                },
              //err => { alert("Error occoured"); this.spinnerService.hide(); }
            );
            // }
          });

          //     this.UserData.forEach(objj => {
          //       for(let i = 0; i<objj.wards.length; i++){
          //       let localUrl = "ward/"+objj.wards[i];
          //       //let localUrl = "brand/";
          //       let url = this.server_url + localUrl;

          //       this.http.get(url,this.httpOptions)
          //         .subscribe(
          //           (res: Response) => {
          //             var xyz=res;
          //             objj.wardname=xyz["name"];
          //             // this.spinnerService.hide();
          //           },
          //           //err => { alert("Error occoured"); this.spinnerService.hide(); }
          //         );
          //       }
          //   });
          // this.spinnerService.hide();
        },
        err => {
          alert("Error occoured");
          // this.spinnerService.hide();
        }
      );
  }

  UserData = [];
  UserDataCount;
  //   zonename=[];
  getUserJrData(userpageNo) {
    // 
    // this.id = this.route.snapshot.paramMap.get('id');
    // this.spinnerService.show();
    let localUrl = "user/?user_type=2&user_type=3&page=" + userpageNo;
    //let localUrl = "brand/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          // this.UserData = res['results'];
          this.UserDataCount = res['count'];

          //             this.UserDataSrCount = res['count'];
          var xyz = res;
          this.UserData = this.UserData.concat(res['results']);
          //this.allcategoryDataDropdown.push(this.allcategoryData);
          if (this.UserData.length < this.UserDataCount) {
            this.getUserJrData(userpageNo + 1);
          }
          // this.spinnerService.hide();
        },
        err => {
          alert("Error occoured");
          // this.spinnerService.hide();
        }
      );
  }

  UserDataSr = [];
  UserDataSrCount;
  getUserSrData(userpageNo) {
    // 
    // this.id = this.route.snapshot.paramMap.get('id');
    // this.spinnerService.show();
    let localUrl = "user/?page=" + userpageNo + "&user_type=2";
    //let localUrl = "brand/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          // this.UserDataSr = res['results'];
          // this.UserDataSrCount = res['count'];


          this.UserDataSrCount = res['count'];
          var xyz = res;
          this.UserDataSr = this.UserDataSr.concat(res['results']);
          //this.allcategoryDataDropdown.push(this.allcategoryData);
          if (this.UserDataSr.length < this.UserDataSrCount) {
            this.getUserSrData(userpageNo + 1);
          }
          // this.spinnerService.hide();
        },
        err => {
          alert("Error occoured");
          // this.spinnerService.hide();
        }
      );
  }

  filterBodyPagination;

  filterData(form: any,tab): void {
    //  
    // this.spinnerService.show();
    console.log('you submitted value:', form.value);

    let filterBody: any = {};

    if (form.value.wards != null) {
      filterBody.wards = form.value.wards;
    } else { filterBody.wards = null }

    if (form.value.jrengineer != null) {
      filterBody.jrengineer = form.value.jrengineer;
    } else { filterBody.jrengineer = null }

    // if(form.value.srengineer != null){
    //   filterBody.srengineer = form.value.srengineer;
    // }else{filterBody.srengineer = null}

    // if(form.value.searchShopcode != null){
    //   filterBody.searchShopcode = form.value.searchShopcode;
    // }else{filterBody.searchShopcode = null}


    if (form.value.mydaterange != null) {
      var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
      // this.filterBody.fromDate = (new Date(form.value.mydaterange.beginJsDate.setHours(0,0,0,0) - tzoffset)).toISOString();
      // this.filterBody.toDate = (new Date(form.value.mydaterange.endJsDate.setHours(23,59,59,999) - tzoffset)).toISOString();
      filterBody.fromDate = (new Date(form.value.mydaterange.beginJsDate.setHours(0, 0, 0, 0) - tzoffset)).toISOString();
      filterBody.fromDate = moment(filterBody.fromDate).unix();
      // filterBody.fromDate = new Date(filterBody.fromDate).getTime();
      // filterBody.fromDate = filterBody.fromDate.trim();
      filterBody.toDate = (new Date(form.value.mydaterange.endJsDate.setHours(23, 59, 59, 999) - tzoffset)).toISOString();
      filterBody.toDate = moment(filterBody.toDate).unix();
      // filterBody.toDate = new Date(filterBody.toDate).getTime();
      // filterBody.toDate = filterBody.toDate.trim();
    } else { filterBody.fromDate = null; filterBody.toDate = null; }

    localStorage.setItem('filterbody', JSON.stringify(filterBody));
    // 
if(tab==1){
    // let localUrl = "ticket/?wards="+filterBody.wards+"&engineer="+filterBody.jrengineer+"&from="+filterBody.fromDate+"&to="+filterBody.toDate;
    let localUrl = "ticket/?"
    if (filterBody.wards) {
      localUrl = localUrl + "ward=" +filterBody.wards + "&"
    }
    if (filterBody.jrengineer) {
      localUrl = localUrl + "engineer=" +filterBody.jrengineer + "&"
    }
    if (filterBody.fromDate && filterBody.toDate) {
      localUrl = localUrl +"from="+filterBody.fromDate+"&"+"to="+filterBody.toDate;
    }
    // let localUrl = "stat/brand/?user_code="+filterBody.searchShopcode
    let url = this.server_url + localUrl;
    // ticket/?zone=3&ward=5&ticket_type=1

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {

          this.DashboardData = res['results'];
          this.DashboardDataCount = res['count'];

          this.DashboardData.forEach(obj => {
            // for(let i = 0; i<obj.zones.length; i++){
            let localUrl = "user/" + obj.user;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;

            this.http.get(url, this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz = res;
                  obj.first_name = xyz["first_name"];
                  obj.last_name = xyz["last_name"];
                  obj.mobile_no = xyz["mobile_no"];
                  //this.zonename.push(obj.zonename);
                  // this.spinnerService.hide();
                },
              //err => { alert("Error occoured"); this.spinnerService.hide(); }
            );
            // }
          });
          this.DashboardData.forEach(obj => {
            // for(let i = 0; i<obj.zones.length; i++){
            let localUrl = "user/" + obj.engineer;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;

            this.http.get(url, this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz = res;
                  obj.engineer_first_name = xyz["first_name"];
                  obj.engineer_last_name = xyz["last_name"];
                  obj.engineer_mobile_no = xyz["mobile_no"];
                  //this.zonename.push(obj.zonename);
                  // this.spinnerService.hide();
                },
              //err => { alert("Error occoured"); this.spinnerService.hide(); }
            );
            // }
          });
          this.DashboardData.forEach(objjj => {
            // for(let i = 0; i<objj.wards.length; i++){
            let localUrl = "ticketimage/?ticket="+objjj.id;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;
    
            this.http.get(url,this.httpOptions)
              .subscribe(
                (res: Response) => {
                  // var xyzzz=res['results'];
                  objjj.images=res['results'];
                  // this.spinnerService.hide();
                },
                //err => { alert("Error occoured"); this.spinnerService.hide(); }
              );
            // }
        });

          // this.DataobjectTopbrands = res;
          //return this.DataobjectTopbrands;
          //this.totalcustomer = res['data']['surveysCount'][0]['totalCustomer'];
          //this.totalissues = this.DataobjectTopissues.graphData.length;

          // this.barChartLabelsDelivery=[];
          // this.dataArray=[];
          // this.pphDelivery=[];

          // for(let i=0; i< this.DataobjectTopissues.graphData.length; i++){
          //  this.barChartLabelsDelivery.push(this.DataobjectTopissues.graphData[i]['master_problem_sub_categories_name']);



          //  for(let j=0; j< this.DataobjectTopissues.surveysCount.length; j++){
          //      this.pphDelivery = (this.DataobjectTopissues.graphData[i]['customerReportedCnt']/this.DataobjectTopissues.surveysCount[j]['totalCustomer'])*100;
          //      this.pphDelivery = this.pphDelivery.toFixed(1);

          //      this.dataArray.push(this.pphDelivery);
          //     }

          //   }
          // for(let k =0; k<this.DataobjectTopissues.graphData.length; k++){
          //   this.totalissues += this.DataobjectTopissues.graphData[k].customerReportedCnt;

          //  }
          //  this.totalpph = ((this.totalissues/this.totalcustomer)*100).toFixed(1);


          // this.getbargraph();

          //  this.spinnerService.hide(); 
        },
        err => {
          // this.spinnerService.hide(); 
          // alert("Error occoured");
        }
      );
    }

    if(tab==1){
      let localUrl = "ticket/?status=3&"
      if (filterBody.wards) {
        localUrl = localUrl + "ward=" +filterBody.wards + "&"
      }
      if (filterBody.jrengineer) {
        localUrl = localUrl + "engineer=" +filterBody.jrengineer + "&"
      }
      if (filterBody.fromDate && filterBody.toDate) {
        localUrl = localUrl +"from="+filterBody.fromDate+"&"+"to="+filterBody.toDate;
      }
      // let localUrl = "stat/brand/?user_code="+filterBody.searchShopcode
      let url = this.server_url + localUrl;
      // ticket/?zone=3&ward=5&ticket_type=1
  
      this.http.get(url, this.httpOptions)
        .subscribe(
          (res: Response) => {
  
            this.DashboardResolvedData = res['results'];
            this.DashboardResolvedDataCount = res['count'];
  
            this.DashboardResolvedData.forEach(obj => {
              // for(let i = 0; i<obj.zones.length; i++){
              let localUrl = "user/" + obj.user;
              //let localUrl = "brand/";
              let url = this.server_url + localUrl;
  
              this.http.get(url, this.httpOptions)
                .subscribe(
                  (res: Response) => {
                    var xyz = res;
                    obj.first_name = xyz["first_name"];
                    obj.last_name = xyz["last_name"];
                    obj.mobile_no = xyz["mobile_no"];
                    //this.zonename.push(obj.zonename);
                    // this.spinnerService.hide();
                  },
                //err => { alert("Error occoured"); this.spinnerService.hide(); }
              );
              // }
            });
            this.DashboardResolvedData.forEach(obj => {
              // for(let i = 0; i<obj.zones.length; i++){
              let localUrl = "user/" + obj.engineer;
              //let localUrl = "brand/";
              let url = this.server_url + localUrl;
  
              this.http.get(url, this.httpOptions)
                .subscribe(
                  (res: Response) => {
                    var xyz = res;
                    obj.engineer_first_name = xyz["first_name"];
                    obj.engineer_last_name = xyz["last_name"];
                    obj.engineer_mobile_no = xyz["mobile_no"];
                    //this.zonename.push(obj.zonename);
                    // this.spinnerService.hide();
                  },
                //err => { alert("Error occoured"); this.spinnerService.hide(); }
              );
              // }
            });

            this.DashboardResolvedData.forEach(objjj => {
              // for(let i = 0; i<objj.wards.length; i++){
              let localUrl = "ticketimage/?ticket="+objjj.id;
              //let localUrl = "brand/";
              let url = this.server_url + localUrl;
      
              this.http.get(url,this.httpOptions)
                .subscribe(
                  (res: Response) => {
                    // var xyzzz=res['results'];
                    objjj.images=res['results'];
                    // this.spinnerService.hide();
                  },
                  //err => { alert("Error occoured"); this.spinnerService.hide(); }
                );
              // }
          });
            // this.getbargraph();
  
            //  this.spinnerService.hide(); 
          },
          err => {
            // this.spinnerService.hide(); 
            alert("Error occoured");
          }
        );

    }

    if(tab==1){
      let localUrl = "ticket/?status=1&"
      if (filterBody.wards) {
        localUrl = localUrl + "ward=" +filterBody.wards + "&"
      }
      if (filterBody.jrengineer) {
        localUrl = localUrl + "engineer=" +filterBody.jrengineer + "&"
      }
      if (filterBody.fromDate && filterBody.toDate) {
        localUrl = localUrl +"from="+filterBody.fromDate+"&"+"to="+filterBody.toDate;
      }
      // let localUrl = "stat/brand/?user_code="+filterBody.searchShopcode
      let url = this.server_url + localUrl;
      // ticket/?zone=3&ward=5&ticket_type=1
  
      this.http.get(url, this.httpOptions)
        .subscribe(
          (res: Response) => {
  
            this.DashboardPendingData = res['results'];
            this.DashboardPendingDataCount = res['count'];
  
            this.DashboardPendingData.forEach(obj => {
              // for(let i = 0; i<obj.zones.length; i++){
              let localUrl = "user/" + obj.user;
              //let localUrl = "brand/";
              let url = this.server_url + localUrl;
  
              this.http.get(url, this.httpOptions)
                .subscribe(
                  (res: Response) => {
                    var xyz = res;
                    obj.first_name = xyz["first_name"];
                    obj.last_name = xyz["last_name"];
                    obj.mobile_no = xyz["mobile_no"];
                    //this.zonename.push(obj.zonename);
                    // this.spinnerService.hide();
                  },
                //err => { alert("Error occoured"); this.spinnerService.hide(); }
              );
              // }
            });
            this.DashboardPendingData.forEach(obj => {
              // for(let i = 0; i<obj.zones.length; i++){
              let localUrl = "user/" + obj.engineer;
              //let localUrl = "brand/";
              let url = this.server_url + localUrl;
  
              this.http.get(url, this.httpOptions)
                .subscribe(
                  (res: Response) => {
                    var xyz = res;
                    obj.engineer_first_name = xyz["first_name"];
                    obj.engineer_last_name = xyz["last_name"];
                    obj.engineer_mobile_no = xyz["mobile_no"];
                    //this.zonename.push(obj.zonename);
                    // this.spinnerService.hide();
                  },
                //err => { alert("Error occoured"); this.spinnerService.hide(); }
              );
              // }
            });

            this.DashboardPendingData.forEach(objjj => {
              // for(let i = 0; i<objj.wards.length; i++){
              let localUrl = "ticketimage/?ticket="+objjj.id;
              //let localUrl = "brand/";
              let url = this.server_url + localUrl;
      
              this.http.get(url,this.httpOptions)
                .subscribe(
                  (res: Response) => {
                    // var xyzzz=res['results'];
                    objjj.images=res['results'];
                    // this.spinnerService.hide();
                  },
                  //err => { alert("Error occoured"); this.spinnerService.hide(); }
                );
              // }
          });
            // this.getbargraph();
  
            //  this.spinnerService.hide(); 
          },
          err => {
            // this.spinnerService.hide(); 
            alert("Error occoured");
          }
        );

    }

    if(tab==1){
      let localUrl = "ticket/?escalated=1&"
      if (filterBody.wards) {
        localUrl = localUrl + "ward=" +filterBody.wards + "&"
      }
      if (filterBody.jrengineer) {
        localUrl = localUrl + "engineer=" +filterBody.jrengineer + "&"
      }
      if (filterBody.fromDate && filterBody.toDate) {
        localUrl = localUrl +"from="+filterBody.fromDate+"&"+"to="+filterBody.toDate;
      }
      // let localUrl = "stat/brand/?user_code="+filterBody.searchShopcode
      let url = this.server_url + localUrl;
      // ticket/?zone=3&ward=5&ticket_type=1
  
      this.http.get(url, this.httpOptions)
        .subscribe(
          (res: Response) => {
  
            this.DashboardEscalatedData = res['results'];
          this.DashboardEscalatedDataCount = res['count'];
  
            this.DashboardEscalatedData.forEach(obj => {
              // for(let i = 0; i<obj.zones.length; i++){
              let localUrl = "user/" + obj.user;
              //let localUrl = "brand/";
              let url = this.server_url + localUrl;
  
              this.http.get(url, this.httpOptions)
                .subscribe(
                  (res: Response) => {
                    var xyz = res;
                    obj.first_name = xyz["first_name"];
                    obj.last_name = xyz["last_name"];
                    obj.mobile_no = xyz["mobile_no"];
                    //this.zonename.push(obj.zonename);
                    // this.spinnerService.hide();
                  },
                //err => { alert("Error occoured"); this.spinnerService.hide(); }
              );
              // }
            });
            this.DashboardEscalatedData.forEach(obj => {
              // for(let i = 0; i<obj.zones.length; i++){
              let localUrl = "user/" + obj.engineer;
              //let localUrl = "brand/";
              let url = this.server_url + localUrl;
  
              this.http.get(url, this.httpOptions)
                .subscribe(
                  (res: Response) => {
                    var xyz = res;
                    obj.engineer_first_name = xyz["first_name"];
                    obj.engineer_last_name = xyz["last_name"];
                    obj.engineer_mobile_no = xyz["mobile_no"];
                    //this.zonename.push(obj.zonename);
                    // this.spinnerService.hide();
                  },
                //err => { alert("Error occoured"); this.spinnerService.hide(); }
              );
              // }
            });
            this.DashboardEscalatedData.forEach(objjj => {
              // for(let i = 0; i<objj.wards.length; i++){
              let localUrl = "ticketimage/?ticket="+objjj.id;
              //let localUrl = "brand/";
              let url = this.server_url + localUrl;
      
              this.http.get(url,this.httpOptions)
                .subscribe(
                  (res: Response) => {
                    // var xyzzz=res['results'];
                    objjj.images=res['results'];
                    // this.spinnerService.hide();
                  },
                  //err => { alert("Error occoured"); this.spinnerService.hide(); }
                );
              // }
          });
            // this.getbargraph();
  
            //  this.spinnerService.hide(); 
          },
          err => {
            // this.spinnerService.hide(); 
            alert("Error occoured");
          }
        );

    }
    // this.getbargraph();
    // } else{
    //   this.spinnerService.hide();
    //   alert("Please select One");

    // }// if else form.value close
    //  this.spinnerService.hide();
  }
  beforeUpdateEngineerObj
  assignEngineer(id, engineer) {
    this.beforeUpdateEngineerObj = {
      "engineer": engineer
    };

    let localUrl = "ticket/" + id + "/";
    let url = this.server_url + localUrl;

    // ;
    this.http.patch(url, this.beforeUpdateEngineerObj, this.httpOptions).subscribe(
      (res) => {
        if (res) {
          alert("You have Assigned successfully..");
          // this.viewUser = '';
          this.getDashboardPendingData(1);
        }
        else {
          alert("error Occured");
          //  this.viewUser = ''; 
          this.getDashboardPendingData(1);
        }
      });

  }
  beforeUpdateEngineerwardObj;
  WardDataEng
  ChangeWard(id, ward) {
    // 
    this.WardDataEng=[];
    let localUrll = "ward/"+ward;
    //let localUrl = "brand/";
    let urll = this.server_url + localUrll;

    this.http.get(urll, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.WardDataEng = res;
       

    this.beforeUpdateEngineerwardObj = {
      "ward": ward,
      // "engineer": this.WardDataEng.users[0],
      "zone" : this.WardDataEng.zone
    };

    let localUrl = "ticket/" + id + "/";
    let url = this.server_url + localUrl;

    // ;
    this.http.patch(url, this.beforeUpdateEngineerwardObj, this.httpOptions).subscribe(
      (res) => {
        if (res) {
          alert("You have Changed successfully..");
          // this.viewUser = '';
          this.getDashboardPendingData(1);
        }
        else {
          alert("error Occured");
          //  this.viewUser = ''; 
          this.getDashboardPendingData(1);
        }
      });
    },
       
  );

  }

  DataobjectGraph;
  barChartLabels:any=[];
  dataArrayTotal=[];
  dataArrayPending=[];
  dataArrayResolved=[];
  dataArrayEscalated=[];
  DataobjectGraphall:any = []

  datacount;
  getGraphData(form: any,tabb){
    this.barChartLabels=[];
    this.dataArrayTotal=[];
  this.dataArrayPending=[];
  this.dataArrayResolved=[];
  this.dataArrayEscalated=[];
  this.DataobjectGraphall = [];
  this.DataobjectGraph = [];
      // 
      // this.spinnerService.show();
      console.log('you submitted value:', form.value);
  
      let filterBody: any = {};
  
      if (form.value.wards != null) {
        filterBody.wards = form.value.wards;
      } else { filterBody.wards = null }
  
      if (form.value.jrengineer != null) {
        filterBody.jrengineer = form.value.jrengineer;
      } else { filterBody.jrengineer = null }
  
      // if(form.value.srengineer != null){
      //   filterBody.srengineer = form.value.srengineer;
      // }else{filterBody.srengineer = null}
  
      // if(form.value.searchShopcode != null){
      //   filterBody.searchShopcode = form.value.searchShopcode;
      // }else{filterBody.searchShopcode = null}
  
  
      if (form.value.mydaterange != null) {
        var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
        // this.filterBody.fromDate = (new Date(form.value.mydaterange.beginJsDate.setHours(0,0,0,0) - tzoffset)).toISOString();
        // this.filterBody.toDate = (new Date(form.value.mydaterange.endJsDate.setHours(23,59,59,999) - tzoffset)).toISOString();
        filterBody.fromDate = (new Date(form.value.mydaterange.beginJsDate));
        filterBody.fromDate = moment(filterBody.fromDate).unix();
        // filterBody.fromDate = new Date(filterBody.fromDate).getTime();
        // filterBody.fromDate = filterBody.fromDate.trim();
        filterBody.toDate = (new Date(form.value.mydaterange.endJsDate));
        filterBody.toDate = moment(filterBody.toDate).unix();
        // filterBody.toDate = new Date(filterBody.toDate).getTime();
        // filterBody.toDate = filterBody.toDate.trim();
      } else { filterBody.fromDate = null; filterBody.toDate = null; }
      // 
  
      // let localUrl = "ticket/?wards="+filterBody.wards+"&engineer="+filterBody.jrengineer+"&from="+filterBody.fromDate+"&to="+filterBody.toDate;
      if(tabb==1){
      let localUrl = "stat/ticket/?"
      if (filterBody.wards) {
        localUrl = localUrl + "ward=" +filterBody.wards + "&"
      }
      if (filterBody.jrengineer) {
        localUrl = localUrl + "engineer=" +filterBody.jrengineer + "&"
      }
      if (filterBody.fromDate && filterBody.toDate) {
        localUrl = localUrl +"from="+filterBody.fromDate+"&"+"to="+filterBody.toDate;
      }
    
  //  this.spinnerService.show();
    // if(request.hasOwnProperty('master_vehicle_id') && request.master_vehicle_id.length==1){
      // let project_IDDataa = {"delivery_Date":true};
     
    // let localUrl = "stat/ticket/";
    let url = this.server_url+localUrl;
  this.http.get(url, this.httpOptions)
  .subscribe(
       (res:Response) => {
      this.DataobjectGraph = res;
      // var total = 0;
      // for(let i=0; i< this.DataobjectGraph.length; i++){
      //   if(this.barChartLabels.indexOf(this.DataobjectGraph[i]['day']) !== -1) {
      //     // this.barChartLabels.push(this.DataobjectGraph[i]['day']);
      //       total += this.DataobjectGraph[i]['count'];
      //       // var sum =this.dataArrayTotal.reduce((a, b) => a + b, 0);
      //       this.datacount = total;
      //       // this.datacount = this.datacount.reduce((a, b) => a + b, 0);
      //       // this.pphDelivery = this.pphDelivery.toFixed(1);
      //       this.dataArrayTotal=this.datacount;
      //     }else{
      //       this.barChartLabels.push(this.DataobjectGraph[i]['day']);
      //       // +"-"+this.DataobjectGraph[i]['year']
      //       this.datacount=this.DataobjectGraph[i]['count'];
      //       this.dataArrayTotal.push(this.datacount);
      //     }
         
      //     // }
      // } 

      //var DataobjectGraphall = {DataobjectGraphall};
      // var total = 0;
      for(let i=0; i< this.DataobjectGraph.length; i++){
          var day =this.DataobjectGraph[i]['day'];
          if(this.DataobjectGraphall[day]){
              // this.barChartLabels = this.DataobjectGraphall[day];
              this.DataobjectGraphall[day] = this.DataobjectGraphall[day] + this.DataobjectGraph[i]['count'];
              // this.dataArrayTotal=this.DataobjectGraphall[day];
              // this.barChartLabels=day;
          }else{
            this.DataobjectGraphall[day] = this.DataobjectGraph[i]['count'];
            // this.dataArrayTotal= Object.values(this.DataobjectGraphall);
            // this.barChartLabels[day] = this.DataobjectGraphall[day];
            // var split = this.DataobjectGraphall.split(":");
           //   Object.keys(this.DataobjectGraphall).forEach(key => {
        //     if (this.DataobjectGraphall[key].index === day) {

        //       this.barChartLabels.push(this.DataobjectGraphall[key].index);
        //    this.dataArrayTotal.push(this.DataobjectGraphall[key]);

        //         console.log("Found.");
        //     }
        // });
            
          }
         
          console.log(console.log(Object.keys(this.DataobjectGraphall)));
      }
      
      for(var key in this.DataobjectGraphall) {
        this.barChartLabels=Object.keys(this.DataobjectGraphall);
        this.dataArrayTotal = Object.values(this.DataobjectGraphall);
    }

      this.getbargraph();
      // this.spinnerService.hide();
      // 
     },
     err => {alert( "Error occoured"); 
    //  this.spinnerService.hide();
    }
     );
    }

    if(tabb==1){
      // this.DataobjectGraphall=[];
      let localUrl = "stat/ticket/?status=3&"
      if (filterBody.wards) {
        localUrl = localUrl + "ward=" +filterBody.wards + "&"
      }
      if (filterBody.jrengineer) {
        localUrl = localUrl + "engineer=" +filterBody.jrengineer + "&"
      }
      if (filterBody.fromDate && filterBody.toDate) {
        localUrl = localUrl +"from="+filterBody.fromDate+"&"+"to="+filterBody.toDate;
      }
   
  //  this.spinnerService.show();
    // if(request.hasOwnProperty('master_vehicle_id') && request.master_vehicle_id.length==1){
      // let project_IDDataa = {"delivery_Date":true};
    
    let url = this.server_url+localUrl;
  this.http.get(url, this.httpOptions)
  .subscribe(
       (res:Response) => {
      this.DataobjectGraph = res;
var total=0;

for(var i in this.DataobjectGraphall) if (Object.hasOwnProperty.call(this.DataobjectGraphall, i)) {
  this.DataobjectGraphall[i] = 0;
};
      for(let n=0; n< this.DataobjectGraph.length; n++){
        // var Monthenum = [ undefined,'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun','Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec' ];
        // if((this.DataobjectGraph[i]['month']) === Monthenum.indexOf(Monthenum[this.barChartLabels.length])){
          // this.barChartLabels=Monthenum[this.DataobjectGraph[i]['month']];
          // total+= this.DataobjectGraph[i]['status__count'];
          // this.datacount = total;
          // // this.datacount = this.datacount.reduce((a, b) => a + b, 0);
          // // this.pphDelivery = this.pphDelivery.toFixed(1);
          // this.dataArrayResolved=this.datacount;
        // }else{
        //   if(this.barChartLabels.indexOf(this.DataobjectGraph[i]['day']) !== -1) {
        //   //this.barChartLabels.push(this.DataobjectGraph[i]['day']);
        //   this.datacount=this.DataobjectGraph[i]['count'];
        //   this.dataArrayResolved=this.datacount;
        //  }else{
        //   this.barChartLabels.push(this.DataobjectGraph[i]['day']);
        //   this.datacount=this.DataobjectGraph[i]['count'];
        //   this.dataArrayResolved.push(this.datacount);
        //  }
          // }
          // }
          var day =this.DataobjectGraph[n]['day'];
          
          // for(let l=0; l< this.barChartLabels.length; l++){
            // if(this.barChartLabels[s] != day){
            //   this.DataobjectGraphall[this.barChartLabels[s]]= 0;
            // }
            

          //   if(this.DataobjectGraphall.hasOwnProperty(this.barChartLabels[l])){
          //     // this.barChartLabels = this.DataobjectGraphall[day];

          //     this.DataobjectGraphall[this.barChartLabels[l]]= 0;
          //     // this.DataobjectGraphall[day] = this.DataobjectGraphall[day] ? this.DataobjectGraph[o]['count'] : 0;
          //     // this.DataobjectGraphall[day] = this.DataobjectGraph[o]['count'];
          //     // this.dataArrayTotal=this.DataobjectGraphall[day];
          //     // this.barChartLabels=day;
          //     //  break
          // }
          // break
          // else{
          //   this.DataobjectGraphall[this.barChartLabels[l]] = 0;
            
            // this.dataArrayTotal= Object.values(this.DataobjectGraphall);
            // this.barChartLabels[day] = this.DataobjectGraphall[day];
            // var split = this.DataobjectGraphall.split(":");
           //   Object.keys(this.DataobjectGraphall).forEach(key => {
        //     if (this.DataobjectGraphall[key].index === day) {

        //       this.barChartLabels.push(this.DataobjectGraphall[key].index);
        //    this.dataArrayTotal.push(this.DataobjectGraphall[key]);

        //         console.log("Found.");
        //     }
        // });
            
          // }
         
          // console.log(console.log(Object.keys(this.DataobjectGraphall)));
          
        // }
        if(this.DataobjectGraphall[day]){
          // this.barChartLabels = this.DataobjectGraphall[day];
          this.DataobjectGraphall[day] = this.DataobjectGraphall[day] + this.DataobjectGraph[n]['count'];
          // this.dataArrayTotal=this.DataobjectGraphall[day];
          // this.barChartLabels=day;
      }else{
        this.DataobjectGraphall[day] = this.DataobjectGraph[n]['count'];
      }
        // this.DataobjectGraphall[day] = this.DataobjectGraph[n]['count'];
        }
     
      
      for(var key in this.DataobjectGraphall) {
        this.barChartLabels=Object.keys(this.DataobjectGraphall);
        this.dataArrayResolved = Object.values(this.DataobjectGraphall);
    }
      

      this.getbargraph();
      // this.spinnerService.hide();
      // 
     },
     err => {alert( "Error occoured"); 
    //  this.spinnerService.hide();
    }
     );
    }

    if(tabb==1){
      
      let localUrl = "stat/ticket/?status=1&"
      if (filterBody.wards) {
        localUrl = localUrl + "ward=" +filterBody.wards + "&"
      }
      if (filterBody.jrengineer) {
        localUrl = localUrl + "engineer=" +filterBody.jrengineer + "&"
      }
      if (filterBody.fromDate && filterBody.toDate) {
        localUrl = localUrl +"from="+filterBody.fromDate+"&"+"to="+filterBody.toDate;
      }
    
  //  this.spinnerService.show();
    // if(request.hasOwnProperty('master_vehicle_id') && request.master_vehicle_id.length==1){
      // let project_IDDataa = {"delivery_Date":true};
     
    // let localUrl = "stat/ticket/";
    let url = this.server_url+localUrl;
  this.http.get(url, this.httpOptions)
  .subscribe(
       (res:Response) => {
      this.DataobjectGraph = res;
var total=0;
for(var i in this.DataobjectGraphall) if (Object.hasOwnProperty.call(this.DataobjectGraphall, i)) {
  this.DataobjectGraphall[i] = 0;
};
      for(let o=0; o< this.DataobjectGraph.length; o++){
        // var Monthenum = [ undefined,'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun','Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec' ];
        // if((this.DataobjectGraph[i]['month']) === Monthenum.indexOf(Monthenum[this.barChartLabels.length])){
          // this.barChartLabels=Monthenum[this.DataobjectGraph[i]['month']];
          // total+= this.DataobjectGraph[i]['status__count'];
          // this.datacount = total;
          // // this.datacount = this.datacount.reduce((a, b) => a + b, 0);
          // // this.pphDelivery = this.pphDelivery.toFixed(1);
          // this.dataArrayPending=this.datacount;
        // }else{
          // if(this.barChartLabels.indexOf(this.DataobjectGraph[i]['day']) !== -1) {
          //   //this.barChartLabels.push(this.DataobjectGraph[i]['day']);
          //   this.datacount=this.DataobjectGraph[i]['count'];
          //   this.dataArrayPending=this.datacount;
          //  }else{
          //   this.barChartLabels.push(this.DataobjectGraph[i]['day']);
          //   this.datacount=this.DataobjectGraph[i]['count'];
          //   this.dataArrayPending.push(this.datacount);
          //  }

          var day =this.DataobjectGraph[o]['day'];
          
        //   for(let q=0; q< this.barChartLabels.length; q++){
        //     // if(this.barChartLabels[s] != day){
        //     //   this.DataobjectGraphall[this.barChartLabels[s]]= 0;
        //     // }
          

         
        //       // if(this.barChartLabels[s] != day){
        //       //   this.DataobjectGraphall[this.barChartLabels[s]]= 0;
        //       // }
            
    
        //       if(this.DataobjectGraphall.hasOwnProperty(this.barChartLabels[q])){
        //         // this.barChartLabels = this.DataobjectGraphall[day];
        //         this.DataobjectGraphall[this.barChartLabels[q]]= 0;
        //         // this.DataobjectGraphall[day] = this.DataobjectGraphall[day] ? this.DataobjectGraph[o]['count'] : 0;
        //         // this.DataobjectGraphall[day] = this.DataobjectGraph[o]['count'];
        //         // this.dataArrayTotal=this.DataobjectGraphall[day];
        //         // this.barChartLabels=day;
        //         //  break
        //     }
        //     break
          
         
        //   // else{
        //     // this.DataobjectGraphall[this.barChartLabels[q]] = 0;
        //     // this.dataArrayTotal= Object.values(this.DataobjectGraphall);
        //     // this.barChartLabels[day] = this.DataobjectGraphall[day];
        //     // var split = this.DataobjectGraphall.split(":");
        //    //   Object.keys(this.DataobjectGraphall).forEach(key => {
        // //     if (this.DataobjectGraphall[key].index === day) {

        // //       this.barChartLabels.push(this.DataobjectGraphall[key].index);
        // //    this.dataArrayTotal.push(this.DataobjectGraphall[key]);

        // //         console.log("Found.");
        // //     }
        // // });
            
        //   // }
         
        //   // console.log(console.log(Object.keys(this.DataobjectGraphall)));
          
        // }
        if(this.DataobjectGraphall[day]){
          // this.barChartLabels = this.DataobjectGraphall[day];
          this.DataobjectGraphall[day] = this.DataobjectGraphall[day] + this.DataobjectGraph[o]['count'];
          // this.dataArrayTotal=this.DataobjectGraphall[day];
          // this.barChartLabels=day;
      }else{
        this.DataobjectGraphall[day] = this.DataobjectGraph[o]['count'];
      }
        }
     
      
      for(var key in this.DataobjectGraphall) {
        this.barChartLabels=Object.keys(this.DataobjectGraphall);
        this.dataArrayPending = Object.values(this.DataobjectGraphall);
    }
      

      this.getbargraph();
      // this.spinnerService.hide();
      // 
     },
     err => {alert( "Error occoured"); 
    //  this.spinnerService.hide();
    }
     );
    }

    if(tabb==1){
      let localUrl = "stat/ticket/?escalated=1&"
      if (filterBody.wards) {
        localUrl = localUrl + "ward=" +filterBody.wards + "&"
      }
      if (filterBody.jrengineer) {
        localUrl = localUrl + "engineer=" +filterBody.jrengineer + "&"
      }
      if (filterBody.fromDate && filterBody.toDate) {
        localUrl = localUrl +"from="+filterBody.fromDate+"&"+"to="+filterBody.toDate;
      }
    
  //  this.spinnerService.show();
    // if(request.hasOwnProperty('master_vehicle_id') && request.master_vehicle_id.length==1){
      // let project_IDDataa = {"delivery_Date":true};
     
    // let localUrl = "stat/ticket/";
    let url = this.server_url+localUrl;
  this.http.get(url, this.httpOptions)
  .subscribe(
       (res:Response) => {
      this.DataobjectGraph = res;
var total=0;
for(var i in this.DataobjectGraphall) if (Object.hasOwnProperty.call(this.DataobjectGraphall, i)) {
  this.DataobjectGraphall[i] = 0;
};
      for(let i=0; i< this.DataobjectGraph.length; i++){
        // var Monthenum = [ undefined,'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun','Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec' ];
        // if((this.DataobjectGraph[i]['month']) === Monthenum.indexOf(Monthenum[this.barChartLabels.length])){
          // this.barChartLabels=Monthenum[this.DataobjectGraph[i]['month']];
          // total+= this.DataobjectGraph[i]['status__count'];
          // this.datacount = total;
          // // this.datacount = this.datacount.reduce((a, b) => a + b, 0);
          // // this.pphDelivery = this.pphDelivery.toFixed(1);
          // this.dataArrayEscalated=this.datacount;
        // }else{
        // }
        // if(this.barChartLabels.indexOf(this.DataobjectGraph[i]['day']) !== -1) {
        //   //this.barChartLabels.push(this.DataobjectGraph[i]['day']);
        //   this.datacount=this.DataobjectGraph[i]['count'];
        //   this.dataArrayEscalated=this.datacount;
        //  }else{
        //   this.barChartLabels.push(this.DataobjectGraph[i]['day']);
        //   this.datacount=this.DataobjectGraph[i]['count'];
        //   this.dataArrayEscalated.push(this.datacount);
        //  }

        var day =this.DataobjectGraph[i]['day'];
          
      //    for(let s=0; s< this.barChartLabels.length; s++){
      //     // if(this.barChartLabels[s] != day){
      //     //   this.DataobjectGraphall[this.barChartLabels[s]]= 0;
      //     // }
        

      //     if(this.DataobjectGraphall.hasOwnProperty(this.barChartLabels[s])){
      //       // this.barChartLabels = this.DataobjectGraphall[day];
      //       this.DataobjectGraphall[this.barChartLabels[s]]= 0;
      //       // this.DataobjectGraphall[day] = this.DataobjectGraphall[day] ? this.DataobjectGraph[o]['count'] : 0;
      //       // this.DataobjectGraphall[day] = this.DataobjectGraph[o]['count'];
      //       // this.dataArrayTotal=this.DataobjectGraphall[day];
      //       // this.barChartLabels=day;
      //       // break
      //   }
      //   break
        
      //   // else{
      //   //   this.DataobjectGraphall[day] = 0;
      //     // this.dataArrayTotal= Object.values(this.DataobjectGraphall);
      //     // this.barChartLabels[day] = this.DataobjectGraphall[day];
      //     // var split = this.DataobjectGraphall.split(":");
      //    //   Object.keys(this.DataobjectGraphall).forEach(key => {
      // //     if (this.DataobjectGraphall[key].index === day) {

      // //       this.barChartLabels.push(this.DataobjectGraphall[key].index);
      // //    this.dataArrayTotal.push(this.DataobjectGraphall[key]);

      // //         console.log("Found.");
      // //     }
      // // });
          
      //   // }
       
      //   // console.log(console.log(Object.keys(this.DataobjectGraphall)));
        
      //  }

      if(this.DataobjectGraphall[day]){
        // this.barChartLabels = this.DataobjectGraphall[day];
        this.DataobjectGraphall[day] = this.DataobjectGraphall[day] + this.DataobjectGraph[i]['count'];
        // this.dataArrayTotal=this.DataobjectGraphall[day];
        // this.barChartLabels=day;
    }else{
      this.DataobjectGraphall[day] = this.DataobjectGraph[i]['count'];
    }
      //  this.DataobjectGraphall[day] = this.DataobjectGraph[i]['count'];
      }
   
    
    for(var key in this.DataobjectGraphall) {
      this.barChartLabels=Object.keys(this.DataobjectGraphall);
      this.dataArrayEscalated = Object.values(this.DataobjectGraphall);
  }
  
  //   
  //   for(var key in this.DataobjectGraphall) {
  //     this.barChartLabels=Object.keys(this.DataobjectGraphall);
  //     this.dataArrayEscalated = Object.values(this.DataobjectGraphall);
  // }
          
        
       

      this.getbargraph();
      // this.spinnerService.hide();
      // 
     },
     err => {alert( "Error occoured"); 
    //  this.spinnerService.hide();
    }
     );
    }
    // } else{}
  }

 

  public EChartOption: any = {};
  getbargraph() {
    // 
    this.EChartOption = {
      color: ['#003366', '#006699', '#4cabce', '#e5323e'],
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow'
        }
      },

      title: {

        // text: 'In Year 2018'
      },
      legend: {

        data: ['Total Issues','Resolved Issues','Pending Issues','Escalated Issues']

      },
      toolbox: {
        show: true,
        orient: 'vertical',
        left: 'right',
        top: 'center',
          feature: {
            mark: {show: true},
            // dataView: {show: true, readOnly: false},
            // magicType: {show: true, type: ['line', 'bar', 'stack', 'tiled']},
            // restore: {show: true},
            // saveAsImage: {show: true}
        }
      },
      calculable: true,
      xAxis: [
        {
          type: 'category',
          data: this.barChartLabels
        }
      ],
      yAxis: [
        {
          type: 'value'
        }
      ],
      series: [
        {
          name: 'Total Issues',
          type: 'bar',
          barWidth: 30,    
          itemStyle: {
            normal: {                 
                // borderRadius: 5,
                label : {
                    show : true,
                    textStyle : {
                        fontSize : '20',
                        fontWeight : 'bold',
                        color: 'white'
                    }
                }
            }
        },
          data: this.dataArrayTotal
        },
        {
          name: 'Resolved Issues',
         type: 'bar',
          barWidth: 30,    
          itemStyle: {
            normal: {                  
                // borderRadius: 5,
                label : {
                    show : true,
                    textStyle : {
                        fontSize : '20',
                        fontWeight : 'bold',
                        color: 'white'
                    }
                }
            }
        },
          data: this.dataArrayResolved
        },
        {
          name: 'Pending Issues',
         type: 'bar',
          barWidth: 30,    
          itemStyle: {
            normal: {                  
                // borderRadius: 5,
                label : {
                    show : true,
                    textStyle : {
                        fontSize : '20',
                        fontWeight : 'bold',
                        color: 'white'
                    }
                }
            }
        },
          data: this.dataArrayPending
        },
        {
          name: 'Escalated Issues',
         type: 'bar',
          barWidth: 30,    
          itemStyle: {
            normal: {                  
                // borderRadius: 5,
                label : {
                    show : true,
                    textStyle : {
                        fontSize : '20',
                        fontWeight : 'bold',
                        color: 'white'
                    }
                }
            }
        },
          data: this.dataArrayEscalated
        },
        // {
        //   name: 'Pending Issues',
        //   type: 'bar',

        //   data: [150, 232, 201, 154, 190, 200, 50, 10, 10, 30, 30]
        // },
        // {
        //   name: 'Escalated Issues',
        //   type: 'bar',

        //   data: [98, 77, 101, 99, 40, 40, 10, 10, 10, 30, 20]
        // }
      ]
    }

  }

  getResolvedbargraph() {
    this.EChartOption = {
      color: ['#003366', '#006699', '#4cabce', '#e5323e'],
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow'
        }
      },

      title: {

        // text: 'In Year 2018'
      },
      legend: {

        data: ['Resolved Issues']
      },
      toolbox: {
        show: true,
        orient: 'vertical',
        left: 'right',
        top: 'center',
        //   feature: {
        //     mark: {show: true},
        //     dataView: {show: true, readOnly: false},
        //     magicType: {show: true, type: ['line', 'bar', 'stack', 'tiled']},
        //     restore: {show: true},
        //     saveAsImage: {show: true}
        // }
      },
      calculable: true,
      xAxis: [
        {
          type: 'category',
          axisTick: { show: false },
          data: [this.barChartLabels]
        }
      ],
      yAxis: [
        {
          type: 'value'
        }
      ],
      series: [
        {
          name: 'Resolved Issues',
          type: 'bar',
          barWidth: 40,    
          itemStyle: {
            normal: {                  
                // borderRadius: 5,
                label : {
                    show : true,
                    textStyle : {
                        fontSize : '20',
                        fontWeight : 'bold',
                        color: 'white'
                    }
                }
            }
        },

          data: [this.dataArrayResolved]
        },
        // {
        //   name: 'Resolved Issues',
        //   type: 'bar',

        //   data: [220, 182, 191, 234, 290, 60, 300, 100, 30, 60, 150]
        // },
        // {
        //   name: 'Pending Issues',
        //   type: 'bar',

        //   data: [150, 232, 201, 154, 190, 200, 50, 10, 10, 30, 30]
        // },
        // {
        //   name: 'Escalated Issues',
        //   type: 'bar',

        //   data: [98, 77, 101, 99, 40, 40, 10, 10, 10, 30, 20]
        // }
      ]
    }

  }

  getPendingbargraph() {
    this.EChartOption = {
      color: ['#003366', '#006699', '#4cabce', '#e5323e'],
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow'
        }
      },

      title: {

        // text: 'In Year 2018'
      },
      legend: {

        data: ['Pending Issues']
      },
      toolbox: {
        show: true,
        orient: 'vertical',
        left: 'right',
        top: 'center',
        //   feature: {
        //     mark: {show: true},
        //     dataView: {show: true, readOnly: false},
        //     magicType: {show: true, type: ['line', 'bar', 'stack', 'tiled']},
        //     restore: {show: true},
        //     saveAsImage: {show: true}
        // }
      },
      calculable: true,
      xAxis: [
        {
          type: 'category',
          axisTick: { show: false },
          data: [this.barChartLabels]
        }
      ],
      yAxis: [
        {
          type: 'value'
        }
      ],
      series: [
        {
          name: 'Pending Issues',
          type: 'bar',
          barWidth: 40,    
          itemStyle: {
            normal: {                  
                // borderRadius: 5,
                label : {
                    show : true,
                    textStyle : {
                        fontSize : '20',
                        fontWeight : 'bold',
                        color: 'white'
                    }
                }
            }
        },

          data: [this.dataArrayPending]
        },
        // {
        //   name: 'Resolved Issues',
        //   type: 'bar',

        //   data: [220, 182, 191, 234, 290, 60, 300, 100, 30, 60, 150]
        // },
        // {
        //   name: 'Pending Issues',
        //   type: 'bar',

        //   data: [150, 232, 201, 154, 190, 200, 50, 10, 10, 30, 30]
        // },
        // {
        //   name: 'Escalated Issues',
        //   type: 'bar',

        //   data: [98, 77, 101, 99, 40, 40, 10, 10, 10, 30, 20]
        // }
      ]
    }

  }

  getEscalatedbargraph() {
    this.EChartOption = {
      color: ['#003366', '#006699', '#4cabce', '#e5323e'],
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow'
        }
      },

      title: {

        // text: 'In Year 2018'
      },
      legend: {

        data: ['Escalated Issues']
      },
      toolbox: {
        show: true,
        orient: 'vertical',
        left: 'right',
        top: 'center',
        //   feature: {
        //     mark: {show: true},
        //     dataView: {show: true, readOnly: false},
        //     magicType: {show: true, type: ['line', 'bar', 'stack', 'tiled']},
        //     restore: {show: true},
        //     saveAsImage: {show: true}
        // }
      },
      calculable: true,
      xAxis: [
        {
          type: 'category',
          axisTick: { show: false },
          data: [this.barChartLabels]
        }
      ],
      yAxis: [
        {
          type: 'value'
        }
      ],
      series: [
        {
          name: 'Escalated Issues',
          type: 'bar',
          barWidth: 40,    
          itemStyle: {
            normal: {                  
                // borderRadius: 5,
                label : {
                    show : true,
                    textStyle : {
                        fontSize : '20',
                        fontWeight : 'bold',
                        color: 'white'
                    }
                }
            }
        },

          data: [this.dataArrayEscalated]
        },
        // {
        //   name: 'Resolved Issues',
        //   type: 'bar',

        //   data: [220, 182, 191, 234, 290, 60, 300, 100, 30, 60, 150]
        // },
        // {
        //   name: 'Pending Issues',
        //   type: 'bar',

        //   data: [150, 232, 201, 154, 190, 200, 50, 10, 10, 30, 30]
        // },
        // {
        //   name: 'Escalated Issues',
        //   type: 'bar',

        //   data: [98, 77, 101, 99, 40, 40, 10, 10, 10, 30, 20]
        // }
      ]
    }

  }

  DownloadXlsx(status) {
    // this.spinnerService.show();
    // 
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'JWT '+ this.tokens
      })
    };
    // let FilterBodyForLink = this.ReportService.getFilterData();
    let localUrl = "download/ticket/?status="+status;
    let url = this.server_url + localUrl;
    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
// if(res){
          var blob = new Blob([res], { type: 'text/csv'});
          var objectUrl = window.URL.createObjectURL(blob);
          //window.open(objectUrl);
          window.open(objectUrl);
// }
          // this.spinnerService.hide();
          // window.open(url);

        },
       
      );

    
  }

  DownloadXlsxAll() {
    // this.spinnerService.show();
//     
//     let httpOptions = {
//       headers: new HttpHeaders({
//         'Authorization': 'JWT '+ this.tokens
//       })
//     };
//     // let FilterBodyForLink = this.ReportService.getFilterData();
//     let localUrl = "download/ticket/";
//     let url = this.server_url + localUrl;
//     this.http.get(url, this.httpOptions)
//       .subscribe(
//         (res: Response) => {
// // if(res){
//           var blob = new Blob([res], { type: 'text/csv'});
//           var objectUrl = window.URL.createObjectURL(blob);
          //window.open(objectUrl);
          let param = this.downloadParam;
          if (this.wards){
            param = param + "&ward="+this.wards;
          }
          if (this.mydaterange){
            param = param + "&from="+this.mydaterange.beginEpoc + "&to="+this.mydaterange.endEpoc
          }

          window.open("http://139.59.55.102/api/download/ticket/?"+param);
// }
          // this.spinnerService.hide();
          // window.open(url);

      //   },
       
      // );

    
  }

  DownloadXlsxEscalated() {
    // this.spinnerService.show();
    // 
    let httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'JWT '+ this.tokens
      })
    };
    // let FilterBodyForLink = this.ReportService.getFilterData();
    let localUrl = "download/ticket/?escalated=1";
    let url = this.server_url + localUrl;
    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
// if(res){
          var blob = new Blob([res], { type: 'text/csv'});
          var objectUrl = window.URL.createObjectURL(blob);
          //window.open(objectUrl);
          window.open(objectUrl);
// }
          // this.spinnerService.hide();
          // window.open(url);

        },
       
      );

    
  }

  resetFilter(){
    // 
    this.DataobjectGraph=[];
    this.barChartLabels=[];
    this.dataArrayTotal=[];
    this.dataArrayResolved=[];
    this.dataArrayPending=[];
    this.dataArrayEscalated=[];

    this.datacount=[];
    localStorage.removeItem('filterbody');
    this.filterBodyStorage=[];
    this.getDashboardAllData();
    this.getDashboardEscalatedData();
    this.getDashboardPendingData(1)
    this.getDashboardResolvedData(3);
    // this.ngOnInit();
   
  }

  dataRefresher: any;

  refreshData(){
    this.dataRefresher =
      setInterval(() => {
        this.getDashboardAllData();
        this.getDashboardEscalatedData();
    this.getDashboardPendingData(1)
    this.getDashboardResolvedData(3);
        //Passing the false flag would prevent page reset to 1 and hinder user interaction
      }, 30000);  
  }
  interval:any;
  ngOnInit() {
    localStorage.removeItem('filterbody');
    this.filterBodyStorage=[]
    this.AnimationBarOption = this._chartsService.getAnimationBarOption();
    this.tableTotalIssues = true;
    this.tableResolvedIssues = false;
    this.tablePendingIssues = false;
    this.tableExcalatedIssues = false;

    this.getDashboardAllData();
    this.getWardData(1);
    this.getUserJrData(1);
    this.getUserSrData(1);
    this.getDashboardEscalatedData();
    this.getDashboardPendingData(1)
    this.getDashboardResolvedData(3);
    // this.getGraphData();
    this.interval = setInterval(() => { 
      this.getDashboardAllData();
      // this.getWardData(1);
      // this.getUserJrData(1);
      // this.getUserSrData(1);
      this.getDashboardEscalatedData();
      this.getDashboardPendingData(1)
      this.getDashboardResolvedData(3);
    }, 60000*5);
    
// localStorage.clear();
// this.refreshData()


  }
}
