import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexComponent } from './index.component';
import { routing } from './index.routing';
import { SharedModule } from '../../shared/shared.module';
import { NgxEchartsModule } from 'ngx-echarts';
import {NgxPaginationModule} from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FormsModule } from '@angular/forms';
import {ModalModule} from "ngx-modal";
import { MyDateRangePickerModule } from 'mydaterangepicker';


@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        NgxEchartsModule,
        routing,
        NgxPaginationModule,
        Ng2SearchPipeModule,
        FormsModule,
        ModalModule,
        MyDateRangePickerModule
    ],
    declarations: [
        IndexComponent
    ]
})
export class IndexModule { }
