import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WardsComponent } from './wards.component';
import { WardsRoutingModule } from './wards-routing.module';
import {ModalModule} from "ngx-modal";
import { FormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {NgxPaginationModule} from 'ngx-pagination';

@NgModule({
  imports: [
    CommonModule,
    ModalModule,
    WardsRoutingModule,
    FormsModule,
    Ng2SearchPipeModule,
    NgxPaginationModule
  ],
  declarations: [WardsComponent]
})
export class WardsModule { }
