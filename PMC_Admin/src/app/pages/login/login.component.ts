import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';

import { Router } from '@angular/router';
// import { UserService } from '../user.service';
import { environment } from '../../../environments/environment';
import { NgxLoadingModule } from 'ngx-loading';
declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loading = false;


  public href: string = "";
  public loginObject: any = {username: '', password:''};
  public loginObjjson= {};
  public jsonloginbody :any;
  public userName: string = "";
  public userRoleid: number;
  constructor(private router:Router, private http:HttpClient) { }
  server_url : string = environment.devServer_url;
  
  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  public loginUser(e): void {
    // this.router.navigate(['./dashboard']);
    // this.spinnerService.show();
    this.loading = true;
  	e.preventDefault();
    console.log(e);
    var username = e.target.elements[0].value;
    var password = e.target.elements[1].value;
    localStorage.setItem('currentUser', username);
    
    // if(username  ==='admin' && password === '123456'){
    //   //console.log('--inside logiin ---',this.login);
    //   this.user.setUserLoggedIn();
    //      this.user.setUserLoggedInName(this.userName);
    //   this.router.navigate(['/dashboard']);
    //   localStorage.setItem('currentUser', JSON.stringify(this.userName));
    //   //this.loginData =this.login.psw;
    //   this.spinnerService.hide();
    // }
    // else{
    //   this.router.navigate(['']);
    //  alert("invalid Credentials");
    //  this.spinnerService.hide();
    // }
  
    
     const addloginObject = {username: username, password: password}
    
let localUrl = "login/";
let url = this.server_url+localUrl;
;

this.http.post(url, JSON.stringify(addloginObject), this.httpOptions)
.subscribe(
    (res) => {
      this.loading = false;
      // this.spinnerService.hide();
          // this.userName = res['data']['auditor_name'];
          // this.userRoleid = res['data']['user_role_id'];
          // let currentUserid = res['data']['auditor_id'];
          let token = res['token'];
          // this.user.setUserLoggedIn();
          // this.user.setUserLoggedInName(this.userName);
          // localStorage.setItem('currentUser', JSON.stringify(this.userName));
        localStorage.setItem('userRoleid', JSON.stringify(this.userRoleid));
       localStorage.setItem('token', token);
          
          // this.router.navigate(['dashboard']);
     this.router.navigate(['pages/index']);
          //console.log("userNameLogin"+this.userName);
          // this.spinnerService.hide();
          // alert("success");
          this.loading = false;
    },
    err => {
      // this.spinnerService.hide();
      this.loading = false;
     
      
        alert("Invalid username or password");
      
  }
    );

  }

  ngOnInit() {
    this.href = this.router.url;
    localStorage.removeItem('currentUser');
    localStorage.removeItem('token');
    localStorage.clear();
  }

}
