import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Location } from '@angular/common';
import { Http, Response } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';


import { Router, ActivatedRoute, Params } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-zones',
  templateUrl: './zones.component.html',
  styleUrls: ['./zones.component.scss']
})
export class ZonesComponent implements OnInit {

  public viewStore: string;

  server_url: string = environment.devServer_url;
  tokens: any = localStorage.getItem('token');
  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'JWT ' + this.tokens
    })
  };

  constructor(private _router: Router, private route: ActivatedRoute, private http: HttpClient, private location: Location) { }


  public addZone(e): void {
    e.preventDefault();
    ;

    let name = e.target.elements[1].value;
    let description = e.target.elements[3].value;
    // let category = e.target.elements[2].value;

    const addZoneObject = {
      name: name,
      description: description,
      //  category : category,
    }
    ;
    let localUrl = "zone/";
    let url = this.server_url + localUrl;
    this.http.post(url, JSON.stringify(addZoneObject), this.httpOptions)
      .subscribe(
        (res) => {
          if (res) {
            alert("You added new Zone successfully..");
            this.viewStore = '';
            //  this.getBrandData();
          } else {
            alert("Zone NOT Added");
          }
        },
        err => {
          alert("Error occoured");
          // this.viewAuditor ='';
        }
      );

  }

  userFilter: any = { name: '' };
  id: any;
  ZoneData;
  ZoneDataCount;
  getZoneData() {
    
    // this.id = this.route.snapshot.paramMap.get('id');
    // this.spinnerService.show();
    let localUrl = "zone/";
    //let localUrl = "brand/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.ZoneData = res['results'];
          this.ZoneDataCount = res['count'];

          //   this.BrandData.forEach(obj => {

          //     let localUrl = "category/"+obj.category;
          //     //let localUrl = "brand/";
          //     let url = this.server_url + localUrl;

          //     this.http.get(url,this.httpOptions)
          //       .subscribe(
          //         (res: Response) => {
          //           var xyz=res;
          //           obj.catname=xyz["name"];
          //           // this.spinnerService.hide();
          //         },
          //         //err => { alert("Error occoured"); this.spinnerService.hide(); }
          //       );

          // });
          // this.spinnerService.hide();
        },
        err => {
          alert("Error occoured");
          // this.spinnerService.hide();
        }
      );
  }


  updateZone(beforeUpdateZoneObj) {
    
    this.beforeUpdateZoneObj = {
      "name": beforeUpdateZoneObj.name,
      "description": beforeUpdateZoneObj.description
    };
    let localUrl = "zone/" + beforeUpdateZoneObj.id + "/";
    let url = this.server_url + localUrl;

    this.http.put(url, JSON.stringify(this.beforeUpdateZoneObj), this.httpOptions).subscribe(
      (res) => {
        if (res) {
          this.beforeUpdateZoneObj = {
            "id": res['id'],
            "name": res['name'],
            "description": res['description'],
          };
          alert("Successfully Updated");
          this.viewStore = '';
          this.getZoneData();
        } else { alert("error Occured") }
      },
    );


  } // Update Auditor functionality close
  beforeUpdateZoneObj;
  // Editing Auditor function 
  editZone(ZoneData) {

    if (confirm("You want to edit Zone ?")) {
      ;
      this.viewStore = 'editStore';
      let localUrl = "zone/" + ZoneData.id + "/";
      let url = this.server_url + localUrl;
      const checkcatObject = { id: ZoneData.id }

      this.http.patch(url, JSON.stringify(checkcatObject), this.httpOptions).subscribe(
        (res) => {
          if (res)
            this.beforeUpdateZoneObj = {
              "id": res['id'],
              "name": res['name'],
              "description": res['description'],
              

            };

        }, err => { alert("You made no change"); }
      );
    } else { }

  }

  deleteZone(ZoneData) {
    if (confirm("You want to Delete Zone?")) {
      ;
      let localUrl = "zone/" + ZoneData.id + "/";
      let url = this.server_url + localUrl;

      this.http.delete(url, this.httpOptions).subscribe(
        (res) => {
          alert("You Deleted Zone");
          //this.viewStore="";
          this.getZoneData();
        }, err => { alert("You made no change"); }
      );
    } else { alert("error occured"); }

  }

  zonepageNo: number = 1;
  pageChange(pageNo: any) {
    // this.zoneName=[];
    
    this.zonepageNo = pageNo;
    console.log("pageNo: " + this.zonepageNo);

    // this.spinnerService.show();
    let localUrl = "zone/?page="+this.zonepageNo;
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.ZoneData = res['results'];
          this.ZoneDataCount = res['count'];

          //   this.BrandData.forEach(obj => {

          //     let localUrl = "category/"+obj.category;
          //     //let localUrl = "brand/";
          //     let url = this.server_url + localUrl;

          //     this.http.get(url,this.httpOptions)
          //       .subscribe(
          //         (res: Response) => {
          //           var xyz=res;
          //           obj.catname=xyz["name"];
          //           // this.spinnerService.hide();
          //         },
          //         //err => { alert("Error occoured"); this.spinnerService.hide(); }
          //       );

          // });
          // this.spinnerService.hide();
        },
        err => {
          alert("Error occoured");
          // this.spinnerService.hide();
        }
      );
  }

  ngOnInit() {
    this.getZoneData();
    this.viewStore = 'addStore';

  }

}
