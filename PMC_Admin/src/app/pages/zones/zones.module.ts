import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ZonesRoutingModule } from './zones-routing.module';
import { ZonesComponent } from './zones.component';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { FormsModule } from '@angular/forms';
import {ModalModule} from "ngx-modal";
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {NgxPaginationModule} from 'ngx-pagination';

@NgModule({
  imports: [
    CommonModule,
    ZonesRoutingModule,
    FilterPipeModule,
    FormsModule,
    ModalModule,
    Ng2SearchPipeModule,
    NgxPaginationModule
  ],
  declarations: [ZonesComponent]
})
export class ZonesModule { }
