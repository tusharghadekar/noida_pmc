import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';
import { LoginComponent } from './login/login.component';

export const childRoutes: Routes = [
    {
        path: 'login',
        component: LoginComponent,
        
    },
    
    {
        path: 'pages',
        component: PagesComponent,
        children: [
            { path: '', redirectTo: 'login', pathMatch: 'full' },
            { path: 'index', loadChildren: './index/index.module#IndexModule' },
            { path: 'issues', loadChildren: './issues/issues.module#IssuesModule' },
            { path: 'engineer', loadChildren: './engineer/engineer.module#EngineerModule' },
            { path: 'wards', loadChildren: './wards/wards.module#WardsModule' },
            { path: 'issues-list', loadChildren: './issues-list/issues-list.module#IssuesListModule' },
            { path: 'observations-list', loadChildren: './observations-list/observations-list.module#ObservationsListModule' },
            { path: 'editor', loadChildren: './editor/editor.module#EditorModule' },
            { path: 'icon', loadChildren: './icon/icon.module#IconModule' },
            { path: 'profile', loadChildren: './profile/profile.module#ProfileModule' },
            { path: 'form', loadChildren: './form/form.module#FormModule' },
            { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
            { path: 'ui', loadChildren: './ui/ui.module#UIModule' },
            { path: 'table', loadChildren: './table/table.module#TableModule' },
            { path: 'menu-levels', loadChildren: './menu-levels/menu-levels.module#MenuLevelsModule' },
            { path: 'zones', loadChildren: './zones/zones.module#ZonesModule' },

        ]
    }
];

export const routing = RouterModule.forChild(childRoutes);
