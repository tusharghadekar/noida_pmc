import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Location } from '@angular/common';
import { Http, Response } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';


import { Router, ActivatedRoute, Params } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-observations-list',
  templateUrl: './observations-list.component.html',
  styleUrls: ['./observations-list.component.scss']
})
export class ObservationsListComponent implements OnInit {

  server_url: string = environment.devServer_url;
  tokens: any = localStorage.getItem('token');
  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'JWT ' + this.tokens
    })
  };

  constructor(private _router: Router, private route: ActivatedRoute, private http: HttpClient, private location: Location) { }



  NotificationData;
  NotificationDataCount;
// zonename=[];
getNotificationData() {
  
  // this.id = this.route.snapshot.paramMap.get('id');
  // this.spinnerService.show();
  let localUrl = "notification/";
  //let localUrl = "brand/";
  let url = this.server_url + localUrl;

  this.http.get(url, this.httpOptions)
    .subscribe(
      (res: Response) => {
        this.NotificationData = res['results'];
        this.NotificationDataCount = res['count'];

          this.NotificationData.forEach(obj => {
// for(let i = 0; i<obj.zones.length; i++){
            let localUrl = "user/"+obj.user;
            //let localUrl = "brand/";
            let url = this.server_url + localUrl;

            this.http.get(url,this.httpOptions)
              .subscribe(
                (res: Response) => {
                  var xyz=res;
                  obj.first_name = xyz["first_name"];
                  obj.last_name = xyz["last_name"];
                  obj.mobile_no = xyz["mobile_no"];
                  //this.zonename.push(obj.zonename);
                  // this.spinnerService.hide();
                },
                //err => { alert("Error occoured"); this.spinnerService.hide(); }
              );
            // }
        });

    //     this.UserData.forEach(objj => {
    //       for(let i = 0; i<objj.wards.length; i++){
    //       let localUrl = "ward/"+objj.wards[i];
    //       //let localUrl = "brand/";
    //       let url = this.server_url + localUrl;

    //       this.http.get(url,this.httpOptions)
    //         .subscribe(
    //           (res: Response) => {
    //             var xyz=res;
    //             objj.wardname=xyz["name"];
    //             // this.spinnerService.hide();
    //           },
    //           //err => { alert("Error occoured"); this.spinnerService.hide(); }
    //         );
    //       }
    //   });
        // this.spinnerService.hide();
      },
      err => {
        alert("Error occoured");
        // this.spinnerService.hide();
      }
    );
}

issuepageNo: number = 1;
  pageChange(pageNo: any) {
    // this.zoneName=[];
    
    this.issuepageNo = pageNo;
    console.log("pageNo: " + this.issuepageNo);
//     let localUrl = "notification/?page="+this.issuepageNo;
//     //let localUrl = "brand/";
//     let url = this.server_url + localUrl;
//     // this.spinnerService.show();
//     // let localUrl = "user/?page="+this.userpageNo;
//     // let url = this.server_url + localUrl;

//     this.http.get(url, this.httpOptions)
//     .subscribe(
//       (res: Response) => {
//         this.NotificationData = res['results'];
//         this.NotificationDataCount = res['count'];

//           this.NotificationData.forEach(obj => {
// // for(let i = 0; i<obj.zones.length; i++){
//             let localUrl = "user/"+obj.user;
//             //let localUrl = "brand/";
//             let url = this.server_url + localUrl;

//             this.http.get(url,this.httpOptions)
//               .subscribe(
//                 (res: Response) => {
//                   var xyz=res;
//                   obj.first_name = xyz["first_name"];
//                   obj.last_name = xyz["last_name"];
//                   obj.mobile_no = xyz["mobile_no"];
//                   //this.zonename.push(obj.zonename);
//                   // this.spinnerService.hide();
//                 },
//                 //err => { alert("Error occoured"); this.spinnerService.hide(); }
//               );
//             // }
//         });

//     //     this.UserData.forEach(objj => {
//     //       for(let i = 0; i<objj.wards.length; i++){
//     //       let localUrl = "ward/"+objj.wards[i];
//     //       //let localUrl = "brand/";
//     //       let url = this.server_url + localUrl;

//     //       this.http.get(url,this.httpOptions)
//     //         .subscribe(
//     //           (res: Response) => {
//     //             var xyz=res;
//     //             objj.wardname=xyz["name"];
//     //             // this.spinnerService.hide();
//     //           },
//     //           //err => { alert("Error occoured"); this.spinnerService.hide(); }
//     //         );
//     //       }
//     //   });
//         // this.spinnerService.hide();
//       },
//       err => {
//         alert("Error occoured");
//         // this.spinnerService.hide();
//       }
//     );
  }

  ngOnInit() {
    this.getNotificationData();
  }

}
