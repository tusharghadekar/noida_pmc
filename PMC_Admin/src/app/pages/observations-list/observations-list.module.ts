import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ObservationsListComponent } from './observations-list.component';
import { ObservationsListRoutingModule } from './observations-list-routing.module';
import {ModalModule} from "ngx-modal";
import { FormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AngularMultiSelectModule } from 'angular4-multiselect-dropdown/angular4-multiselect-dropdown';
import {NgxPaginationModule} from 'ngx-pagination';
import { FormModule } from '../form/form.module';

@NgModule({
  imports: [
    CommonModule,
    ObservationsListRoutingModule,
    ModalModule,
    FormsModule,
    Ng2SearchPipeModule,
    AngularMultiSelectModule,
    NgxPaginationModule,
    FormModule
  ],
  declarations: [ObservationsListComponent]
})
export class ObservationsListModule { }
