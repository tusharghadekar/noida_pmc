import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IssuesListComponent } from './issues-list.component';
import { IssuesListRoutingModule } from './issues-list-routing.module';

@NgModule({
  imports: [
    CommonModule,
    IssuesListRoutingModule
  ],
  declarations: [IssuesListComponent]
})
export class IssuesListModule { }
