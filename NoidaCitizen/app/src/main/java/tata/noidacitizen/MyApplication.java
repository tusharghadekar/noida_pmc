package tata.noidacitizen;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OneSignal;


public class MyApplication extends Application {
    public static Context context;

    public static String getUserToken() {

        SharedPreferences sharedPref = context.getSharedPreferences(
                context.getString(R.string.preference_file_key), context.MODE_PRIVATE);
        String token = sharedPref.getString(context.getString(R.string.user_login_token), "");
        return token;

    }

    public static void saveUserToken(String token) {

        SharedPreferences sharedPref =
                context.getSharedPreferences(context.getString(R.string.preference_file_key),
                        context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(context.getString(R.string.user_login_token), "JWT " + token);
        editor.commit();
    }

    public static void saveEmptyUserToken(String token) {

        SharedPreferences sharedPref =
                context.getSharedPreferences(context.getString(R.string.preference_file_key),
                        context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(context.getString(R.string.user_login_token), token);
        editor.commit();
    }

    public static void saveLoginUserID(int userId) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                context.getString(R.string.preference_file_key), context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(context.getString(R.string.login_user_id), userId);
        editor.commit();
    }

    public static int getUserID() {
        SharedPreferences sharedPref = context.getSharedPreferences(
                context.getString(R.string.preference_file_key), context.MODE_PRIVATE);
        int userId = sharedPref.getInt(context.getString(R.string.login_user_id), 0);
        return userId;

    }

    public static void saveOneSignalId(String userId) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                context.getString(R.string.preference_file_key), context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(context.getString(R.string.one_signal_id), userId);
        editor.commit();
    }

    public static String getOneSignalId() {
        SharedPreferences sharedPref = context.getSharedPreferences(
                context.getString(R.string.preference_file_key), context.MODE_PRIVATE);
        String userId = sharedPref.getString(context.getString(R.string.one_signal_id), "");
        return userId;

    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();

        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();


        OSPermissionSubscriptionState status = OneSignal.getPermissionSubscriptionState();
        status.getSubscriptionStatus().getUserId();

        saveOneSignalId(status.getSubscriptionStatus().getUserId());

        // Toast.makeText(context, getOneSignalId(), Toast.LENGTH_SHORT).show();
    }
}