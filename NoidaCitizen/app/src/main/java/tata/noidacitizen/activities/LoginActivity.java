package tata.noidacitizen.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Selection;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.javiersantos.bottomdialogs.BottomDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OneSignal;

import net.grandcentrix.tray.AppPreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tata.noidacitizen.MyApplication;
import tata.noidacitizen.R;
import tata.noidacitizen.Utility;
import tata.noidacitizen.models.LoginRequest;
import tata.noidacitizen.models.LoginResponse;
import tata.noidacitizen.models.RequestUpdateProfile;
import tata.noidacitizen.models.ResponseUpdateProfile;
import tata.noidacitizen.models.UserResponse;
import tata.noidacitizen.models.UserResult;
import tata.noidacitizen.networkcommunication.ApiClient;
import tata.noidacitizen.networkcommunication.ApiInterface;
import tata.noidacitizen.networkcommunication.DialogUtils;

import static tata.noidacitizen.MyApplication.saveOneSignalId;
import static tata.noidacitizen.SharedPreferences.MOBILE_NO;

public class LoginActivity extends AppCompatActivity {

    private CountDownTimer countDownTimer;
    private Button mButtonVerify, mBtnLogin;
    private TextInputLayout input_layout_mobile;

    private String phoneNumber, otp;
    private FirebaseAuth auth;
    private String verificationCode;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallback;
    private BottomDialog bottomDialog;
    private String str_name, str_mob_no, str_email, str_flat_no, str_society_name, str_address,
            str_password, str_password_confirm;
    private AppPreferences appPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        appPreferences = new AppPreferences(this);

        startFireBaseLogin();

        mBtnLogin = findViewById(R.id.mbLogin);

        input_layout_mobile = findViewById(R.id.input_layout_mobile);

        final EditText editText = input_layout_mobile.getEditText();
        if (editText != null) {
            Objects.requireNonNull(input_layout_mobile.getEditText())
                    .setFilters(
                            new InputFilter[]{getPhoneFilter(editText), new InputFilter.LengthFilter(13)});

            editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        if (editText.getText().toString().isEmpty()) {
                            editText.setText("+91");
                            Selection.setSelection(editText.getText(), editText.getText().length());
                        }
                    } else {
                        if (editText.getText().toString().equalsIgnoreCase("+91")) {
                            editText.setFilters(new InputFilter[]{});
                            editText.setText("");
                            editText.setFilters(new InputFilter[]{
                                    getPhoneFilter(editText), new InputFilter.LengthFilter(13)
                            });
                        }
                    }
                }
            });
        }

        mBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                str_mob_no = Objects.requireNonNull(input_layout_mobile.getEditText()).getText().toString();
                if (!str_mob_no.equalsIgnoreCase("") && str_mob_no.length() >= 13) {

                    phoneNumber = str_mob_no;

                    if (!phoneNumber.equalsIgnoreCase("") && phoneNumber.length() >= 10) {
                        PhoneAuthProvider.getInstance()
                                .verifyPhoneNumber(phoneNumber,                     // Phone number to verify
                                        60,                           // Timeout duration
                                        TimeUnit.SECONDS,                // Unit of timeout
                                        Objects.requireNonNull(LoginActivity.this),
                                        // Activity (for callback binding)
                                        mCallback);                      // OnVerificationStateChangedCallbacks

                        LayoutInflater inflater =
                                (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View customView = null;

                        if (inflater != null) {
                            customView = inflater.inflate(R.layout.enter_otp_view, null);
                            final EditText inputOTP = (EditText) customView.findViewById(R.id.inputOtp);
                            final TextView txt_retry = (TextView) customView.findViewById(R.id.txt_retry);
                            final TextView txt_mob = (TextView) customView.findViewById(R.id.txt_mob);

                            txt_retry.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    bottomDialog.dismiss();
                                }
                            });

                            countDownTimer = new CountDownTimer(30000, 1000) { // adjust the milli seconds here

                                @SuppressLint({"SetTextI18n", "DefaultLocale"})
                                public void onTick(long millisUntilFinished) {
                                    (txt_retry).setText("" + String.format("%d sec",
                                            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)
                                                    - TimeUnit.MINUTES.toSeconds(
                                                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                                }

                                public void onFinish() {
                                    (txt_retry).setEnabled(true);
                                    (txt_retry).setText("Retry");
                                }
                            }.start();

                            (txt_mob).setText(phoneNumber);

                            bottomDialog = new BottomDialog.Builder(LoginActivity.this).setCustomView(customView)
                                    .setCancelable(false)
                                    .show();

                            customView.findViewById(R.id.btn_submit_otp)
                                    .setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {

                                            if (TextUtils.isEmpty(inputOTP.getText().toString().trim())
                                                    || inputOTP.getText().toString().trim().length() != 6) {
                                                Toast.makeText(LoginActivity.this, "Enter Valid OTP", Toast.LENGTH_SHORT)
                                                        .show();
                                            } else {

                                                otp = inputOTP.getText().toString();

                                                PhoneAuthCredential credential =
                                                        PhoneAuthProvider.getCredential(verificationCode, otp);
                                                signInWithPhone(credential);

                                                //Toast.makeText(mContext, "Please Enter Valid OTP", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                        }
                    }
                } else {
                    Utility.showFailureAlert(LoginActivity.this, "Please Enter Valid Mobile Number");
                }
            }
        });
    }

    private boolean validateInput() {

        boolean result = true;

        str_mob_no = Objects.requireNonNull(input_layout_mobile.getEditText()).getText().toString();

        if (str_mob_no.equalsIgnoreCase("")) {
            Utility.showFailureAlert(this, getString(R.string.please_enter_valid_inputs));
            result = false;
        } else if (!Utility.isPhoneNumber(str_mob_no)) {
            Utility.showFailureAlert(this, getString(R.string.error_invalid_phn));
            result = false;
        }

        return result;
    }

    private InputFilter getPhoneFilter(final EditText edtPhone) {

        Selection.setSelection(edtPhone.getText(), edtPhone.getText().length());

        edtPhone.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0) {
                    if (s.equals(phoneNumber)) {
                    } else {
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().startsWith("+91")) {
                    if (edtPhone.getFilters() != null && edtPhone.getFilters().length > 0) {
                        edtPhone.setText("+91");
                        Selection.setSelection(edtPhone.getText(), edtPhone.getText().length());
                    }
                }
            }
        });

        // Input filter to restrict user to enter only digits..
        InputFilter filter = new InputFilter() {

            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart,
                                       int dend) {

                for (int i = start; i < end; i++) {

                    if (!String.valueOf(getString(R.string.digits_number))
                            .contains(String.valueOf(source.charAt(i)))) {
                        return "";
                    }
                }
                return null;
            }
        };
        return filter;
    }

    private void signInWithPhone(PhoneAuthCredential credential) {
        auth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            //Toast.makeText(LoginActivity.this, "Number Verified", Toast.LENGTH_SHORT).show();
                            //                            mUser.setPhoneVerified(true);
                            bottomDialog.dismiss();

                            doLogin(str_mob_no.substring(3), UUID.randomUUID().toString());
                        } else {
                            Toast.makeText(LoginActivity.this, "Incorrect OTP", Toast.LENGTH_SHORT).show();
                            bottomDialog.dismiss();
                        }
                    }
                });
    }

    private void startFireBaseLogin() {

        auth = FirebaseAuth.getInstance();
        // Access a Cloud FireStore instance from your Activity
        mCallback = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
//        Toast.makeText(LoginActivity.this, "Verification Completed", Toast.LENGTH_SHORT).show();


                if (bottomDialog != null) bottomDialog.dismiss();

                doLogin(str_mob_no.substring(3), UUID.randomUUID().toString());
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Toast.makeText(LoginActivity.this, "Verification Failed" + e.toString(), Toast.LENGTH_SHORT)
                        .show();
                Log.d("Signup", "onVerificationFailed: " + e.toString());
            }

            @Override
            public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);
                verificationCode = s;
                // Toast.makeText(LoginActivity.this, "Code sent", Toast.LENGTH_SHORT).show();
            }
        };
    }


    private void doLogin(String username, String password) {

        // username= "7898989878";

        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(LoginActivity.this, "");

        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername((username));
        loginRequest.setPassword(password);

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<LoginResponse> call = apiService.doLogin(loginRequest);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                LoginResponse loginResponse = response.body();

                if (response.code() == 400) {

                    Utility.showFailureAlert(LoginActivity.this, getString(R.string.sr_jr_login));

                } else {
                    if (loginResponse != null && loginResponse.getToken() != null && (!loginResponse.getToken()
                            .isEmpty())) {

                        MyApplication.saveUserToken(loginResponse.getToken());

                        getUserInfoAPI();


                    } else {
                        Utility.showFailureAlert(LoginActivity.this, getString(R.string.login_failed));
                        progressDialog.dismiss();
                    }
                }


            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                // Log error here since request failed
                Utility.showFailureAlert(LoginActivity.this, getString(R.string.internal_error));
                progressDialog.dismiss();
            }
        });
    }

    public void getUserInfoAPI() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");
        Call<UserResponse> call = apiService.getUser(headerMap);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {

                UserResponse userReponse = response.body();
                ArrayList<UserResult> userArrayList = null;
                if (userReponse != null) {
                    userArrayList = (ArrayList<UserResult>) userReponse.getResults();
                    if (userArrayList != null && userArrayList.size() > 0) {

                        UserResult user = userArrayList.get(0);
                        MyApplication.saveLoginUserID(user.getId());

                        updateProfile();

                    }
                }


            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
            }
        });
    }

    public void updateProfile() {


        OSPermissionSubscriptionState status = OneSignal.getPermissionSubscriptionState();
        status.getSubscriptionStatus().getUserId();

        saveOneSignalId(status.getSubscriptionStatus().getUserId());
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        String token = MyApplication.getUserToken();

        final RequestUpdateProfile user = new RequestUpdateProfile();
        user.setPushToken(MyApplication.getOneSignalId());

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");
        Call<ResponseUpdateProfile> call = apiService.updateProfile(headerMap, user, MyApplication.getUserID());
        call.enqueue(new Callback<ResponseUpdateProfile>() {
            @Override
            public void onResponse(Call<ResponseUpdateProfile> call, Response<ResponseUpdateProfile> response) {

                ResponseUpdateProfile userReponse = response.body();
                ArrayList<UserResult> userArrayList = null;
                if (userReponse != null) {

                    appPreferences.put(MOBILE_NO, str_mob_no);
                    startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                    finish();
                }


            }

            @Override
            public void onFailure(Call<ResponseUpdateProfile> call, Throwable t) {
            }
        });
    }
}
