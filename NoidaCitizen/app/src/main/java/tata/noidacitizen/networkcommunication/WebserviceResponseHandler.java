package tata.noidacitizen.networkcommunication;

public interface WebserviceResponseHandler {

    void onResponseSuccess(Object o);

    void onResponseFailure();


}
