package tata.noidacitizen.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseNotificationsResult {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user")
    @Expose
    private Integer user;
    @SerializedName("ward_name")
    @Expose
    private String wardName;
    @SerializedName("zone_name")
    @Expose
    private String zoneName;
    @SerializedName("ticket_type")
    @Expose
    private String ticketType;
    @SerializedName("ticket_status")
    @Expose
    private Integer ticketStatus;
    @SerializedName("engineer")
    @Expose
    private String engineer;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;

    @SerializedName("mCreatedAt")
    @Expose
    private Double mCreatedAt;
    @SerializedName("mUpdatedAt")
    @Expose
    private Double mUpdatedAt;

    @SerializedName("ticket_id")
    @Expose
    private Integer ticketId;

    @SerializedName("read")
    @Expose
    private boolean read;

    public Integer getTicketId() {
        return ticketId;
    }

    public void setTicketId(Integer ticketId) {
        this.ticketId = ticketId;
    }

    public boolean isRead() {
        return read;
    }
    public void setRead(boolean read) {
        this.read = read;
    }

    public Double getmCreatedAt() {
        return mCreatedAt;
    }

    public void setmCreatedAt(Double mCreatedAt) {
        this.mCreatedAt = mCreatedAt;
    }

    public Double getmUpdatedAt() {
        return mUpdatedAt;
    }

    public void setmUpdatedAt(Double mUpdatedAt) {
        this.mUpdatedAt = mUpdatedAt;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUser() {
        return user;
    }

    public void setUser(Integer user) {
        this.user = user;
    }

    public String getWardName() {
        return wardName;
    }

    public void setWardName(String wardName) {
        this.wardName = wardName;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public String getTicketType() {
        return ticketType;
    }

    public void setTicketType(String ticketType) {
        this.ticketType = ticketType;
    }

    public Integer getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(Integer ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    public String getEngineer() {
        return engineer;
    }

    public void setEngineer(String engineer) {
        this.engineer = engineer;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


}