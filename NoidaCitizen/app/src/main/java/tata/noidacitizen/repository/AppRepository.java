package tata.noidacitizen.repository;

public class AppRepository {
    private static final String TAG = "AppRepository";
    private static AppRepository appRepository;

    private AppRepository() {
    }

    public static AppRepository getInstance() {
        if (appRepository == null) {
            appRepository = new AppRepository();
        }
        return appRepository;
    }


}
