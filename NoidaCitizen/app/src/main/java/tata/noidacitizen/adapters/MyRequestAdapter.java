package tata.noidacitizen.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import tata.noidacitizen.R;
import tata.noidacitizen.models.ResponseMyRequestResult;

import static tata.noidacitizen.SharedPreferences.COMPLETED;
import static tata.noidacitizen.SharedPreferences.IN_PROGRESS;
import static tata.noidacitizen.SharedPreferences.PENDING;


public class MyRequestAdapter extends RecyclerView.Adapter<MyRequestAdapter.MyViewHolder> {

    private Context mContext;
    private MyRequestAdapter.OnItemClickListener mItemClickListener;
    private List<ResponseMyRequestResult> listItems;

    public MyRequestAdapter(Context context, List<ResponseMyRequestResult> listItems) {
        this.listItems = listItems;
        this.mContext = context;
    }

    public void setOnItemClickListener(final MyRequestAdapter.OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    @NonNull
    @Override
    public MyRequestAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_requests_list, parent, false);

        return new MyRequestAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyRequestAdapter.MyViewHolder holder, int position) {

        if (listItems.get(position).getTitle() != null && !listItems.get(position)
                .getTitle()
                .equals("null")) {
            holder.txtRequest.setText(listItems.get(position).getTitle());
        }

        if (listItems.get(position).getStatus() != null && !listItems.get(position)
                .getStatus()
                .equals("null")) {

            if (listItems.get(position).getStatus() == COMPLETED) {

                holder.txtStatus.setTextColor(ContextCompat.getColor(mContext, R.color.green));
                holder.txtStatus.setText("Completed");
            }

            if (listItems.get(position).getStatus() == PENDING) {

                holder.txtStatus.setTextColor(ContextCompat.getColor(mContext, R.color.red));
                holder.txtStatus.setText("Pending");
            }

            if (listItems.get(position).getStatus() == IN_PROGRESS) {

                holder.txtStatus.setTextColor(ContextCompat.getColor(mContext, R.color.orange));
                holder.txtStatus.setText("In Progress");
            }


        }

        if (listItems.get(position).getAddress() != null && !listItems.get(position)
                .getAddress()
                .equals("null")) {
            holder.txtLocation.setText(listItems.get(position).getAddress());
        }
        if (listItems.get(position).getmUpdatedAt() != null) {

            double l = listItems.get(position).getmUpdatedAt();

            Date date = new Date((long) l * 1000);
            @SuppressLint("SimpleDateFormat") SimpleDateFormat fmtOut = new SimpleDateFormat("EEE, d MMM yyyy hh:mm a");

            //  String date = String.format("%.10s", listItems.get(position).getUpdatedAt());
            holder.txtDate.setText(fmtOut.format(date));
        }


    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtRequest;
        TextView txtStatus, txtLocation, txtDate;
        LinearLayout llItemDetails;

        MyViewHolder(View itemView) {
            super(itemView);
            txtRequest = (TextView) itemView.findViewById(R.id.txtRequest);
            txtStatus = (TextView) itemView.findViewById(R.id.txtStatus);
            txtLocation = (TextView) itemView.findViewById(R.id.txtLocation);
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);
            llItemDetails = (LinearLayout) itemView.findViewById(R.id.llItemDetails);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(view, getPosition());

//        if (llItemDetails.getVisibility()==View.GONE){
//          txtRequest.setTypeface(Typeface.DEFAULT_BOLD);
//          llItemDetails.setVisibility(View.VISIBLE);
//        }else {
//          txtRequest.setTypeface(Typeface.DEFAULT);
//          llItemDetails.setVisibility(View.GONE);
//        }
            }
        }
    }

    /*
    method to set listener to the adapter ViewHolder item
     */
}
