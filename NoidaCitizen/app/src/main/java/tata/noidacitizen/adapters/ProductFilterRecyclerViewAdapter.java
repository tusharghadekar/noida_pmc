package tata.noidacitizen.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import tata.noidacitizen.R;
import tata.noidacitizen.models.Complaint;

public class ProductFilterRecyclerViewAdapter extends
        RecyclerView.Adapter<ProductFilterRecyclerViewAdapter.ViewHolder> {

    private List<Complaint> complaintList;
    private Context context;

    public ProductFilterRecyclerViewAdapter(List<Complaint> ComplaintList
            , Context ctx) {
        complaintList = ComplaintList;
        context = ctx;
    }

    @Override
    public ProductFilterRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                          int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_complaint_list, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Complaint filterM = complaintList.get(position);
        holder.txtComplaint.setText(filterM.getComplaint());
        if (filterM.isSelected()) {
            holder.selectionState.setChecked(true);
        } else {
            holder.selectionState.setChecked(false);
        }

    }

    @Override
    public int getItemCount() {
        return complaintList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView txtComplaint;
        public CheckBox selectionState;

        public ViewHolder(View view) {
            super(view);
            txtComplaint = (TextView) view.findViewById(R.id.txtComplaint);
//            selectionState = (CheckBox) view.findViewById(R.id.selectionState);

            //item click event listener
            view.setOnClickListener(this);

            //checkbox click event handling
            selectionState.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView,
                                             boolean isChecked) {
                    if (isChecked) {
                        selectionState.setChecked(true);
                        Toast.makeText(ProductFilterRecyclerViewAdapter.this.context,
                                "selected complaint is " + txtComplaint.getText(),
                                Toast.LENGTH_LONG).show();
                    } else {
                        selectionState.setChecked(false);
                    }
                }
            });
        }

        @Override
        public void onClick(View v) {
            selectionState.setChecked(true);
        }
    }
}