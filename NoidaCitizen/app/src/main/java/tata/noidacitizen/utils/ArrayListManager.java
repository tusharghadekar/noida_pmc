package tata.noidacitizen.utils;


import java.util.ArrayList;

import tata.noidacitizen.models.MyRequests;
import tata.noidacitizen.models.Notification;

public class ArrayListManager {
    private static ArrayListManager arrayListManager;
    private static ArrayList<MyRequests> myRequestsArrayList = new ArrayList<>();
    private static ArrayList<Notification> notificationArrayList = new ArrayList<>();

    private ArrayListManager() {

    }

    public static ArrayListManager getInstance() {
        if (arrayListManager == null) {
            arrayListManager = new ArrayListManager();
        }
        return arrayListManager;
    }

    public static void reset() {
        myRequestsArrayList = new ArrayList<>();
    }

    public static void remove(int position) {
        myRequestsArrayList.remove(position);
    }

    public ArrayList<MyRequests> getMyRequestsArrayList() {
        return myRequestsArrayList;

    }

    public ArrayList<Notification> getNotificationArrayList() {
        return notificationArrayList;

    }
}
