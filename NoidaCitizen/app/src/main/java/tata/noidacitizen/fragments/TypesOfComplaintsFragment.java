package tata.noidacitizen.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tata.noidacitizen.MyApplication;
import tata.noidacitizen.R;
import tata.noidacitizen.activities.HomeActivity;
import tata.noidacitizen.adapters.TypesOfComplaintAdapter;
import tata.noidacitizen.models.ResponseTicketType;
import tata.noidacitizen.models.ResponseTicketTypeResult;
import tata.noidacitizen.networkcommunication.ApiClient;
import tata.noidacitizen.networkcommunication.ApiInterface;
import tata.noidacitizen.networkcommunication.DialogUtils;

/*
 * Created by ${KEDAR_LABDE} on 12-12-2017.
 */

public class TypesOfComplaintsFragment extends Fragment {
    private Context mContext;
    private TypesOfComplaintAdapter typesOfComplaintAdapter;
    private ArrayList<ResponseTicketTypeResult> complaintArrayList = new ArrayList<>();

    private ArrayList<ResponseTicketTypeResult> checkedComplaintList = new ArrayList<>();
    private RecyclerView mRecyclerView;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }


    private void init(View view) {
        setHasOptionsMenu(false);

        mRecyclerView = view.findViewById(R.id.recyclerView);
        typesOfComplaintAdapter = new TypesOfComplaintAdapter(mContext, complaintArrayList);
        LinearLayoutManager mLayoutManager =
                new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(typesOfComplaintAdapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_types_of_complaints, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init(view);

//        complaintArrayList.add(new ResponseTicketTypeResult("Street Light OFF", false));
//        complaintArrayList.add(new ResponseTicketTypeResult("Street Light Flickering", false));
//        complaintArrayList.add(new ResponseTicketTypeResult("Feeder OFF", false));
//        complaintArrayList.add(new ResponseTicketTypeResult("Shock on Pole.", false));
//        complaintArrayList.add(new ResponseTicketTypeResult("Shock on Feeder.", false));
//        complaintArrayList.add(new ResponseTicketTypeResult("Electric Cable hanging.", false));
//        complaintArrayList.add(new ResponseTicketTypeResult("Cable Cut.", false));
//        complaintArrayList.add(new ResponseTicketTypeResult("Short-Circuit.", false));
//        complaintArrayList.add(new ResponseTicketTypeResult("Cabinet Door not Locked/Open.", false));
//        complaintArrayList.add(new ResponseTicketTypeResult("Earthing on Pole Not Available.", false));
//        complaintArrayList.add(new ResponseTicketTypeResult("Earthing on Feeder Not Available.", false));
//        complaintArrayList.add(new ResponseTicketTypeResult("Pole Damage.", false));
//        complaintArrayList.add(new ResponseTicketTypeResult("Shock on Pole.", false));
//        complaintArrayList.add(new ResponseTicketTypeResult("Pole Arm Damage.", false));
//        complaintArrayList.add(new ResponseTicketTypeResult("Insulation Damage on Joints.", false));
//        complaintArrayList.add(new ResponseTicketTypeResult("Others", false));


        typesOfComplaintAdapter.setOnItemClickListener(new TypesOfComplaintAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Bundle bundle = new Bundle();

                bundle.putSerializable("str", complaintArrayList.get(position));

                Fragment selected = new RegisterComplaintFragment();
                selected.setArguments(bundle);
                ((HomeActivity) mContext).getSupportActionBar().setTitle("Register Complaint");
                FragmentManager fragmentManager = getChildFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.childContainer, selected, "RegisterComplaintFragment")
                        .addToBackStack(null)
                        .commit();
            }
        });


        getTicketTypes();

    }

    private void getTicketTypes() {

        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(mContext, "");

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseTicketType> call = apiService.getTicketTypes(headerMap);
        call.enqueue(new Callback<ResponseTicketType>() {
            @Override
            public void onResponse(Call<ResponseTicketType> call, Response<ResponseTicketType> response) {
                ResponseTicketType responseTicketType = response.body();


                if (responseTicketType != null && responseTicketType.getResults() != null) {

                    progressDialog.dismiss();
                    complaintArrayList.addAll(responseTicketType.getResults());
                    typesOfComplaintAdapter.notifyDataSetChanged();

                } else {
                    ((HomeActivity) mContext).showFailAlert("Complaints Fetch Failed");

                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseTicketType> call, Throwable t) {
                // Log error here since request failed

                ((HomeActivity) mContext).showFailAlert(getString(R.string.internal_error));
                progressDialog.dismiss();
            }
        });
    }


}


