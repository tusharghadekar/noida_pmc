package tata.noidacitizen.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tata.noidacitizen.MyApplication;
import tata.noidacitizen.R;
import tata.noidacitizen.activities.HomeActivity;
import tata.noidacitizen.adapters.ImageDownloadAdapter;
import tata.noidacitizen.models.ResponseDownloadImage;
import tata.noidacitizen.models.ResponseDownloadImageResult;
import tata.noidacitizen.models.ResponseMyRequestResult;
import tata.noidacitizen.models.ResponseTicketTypeResult;
import tata.noidacitizen.networkcommunication.ApiClient;
import tata.noidacitizen.networkcommunication.ApiInterface;
import tata.noidacitizen.networkcommunication.DialogUtils;

import static tata.noidacitizen.SharedPreferences.COMPLETED;
import static tata.noidacitizen.SharedPreferences.IN_PROGRESS;
import static tata.noidacitizen.SharedPreferences.PENDING;

/*
 * Created by ${KEDAR_LABDE} on 12-12-2017.
 */

public class ObservationFragment extends Fragment {
    TextView txtIssueId, txtStatus, txtIssue, txtObservation, txtDescription, txtWard, txtDate, txtAddress;
    EditText edtObservation;
    private Context mContext;
    private ArrayList<Uri> imagesUriList = new ArrayList<>();
    private ArrayList<ResponseDownloadImageResult> imagesList = new ArrayList<>();
    private ImageDownloadAdapter imageDownloadAdapter;
    private RecyclerView mRecyclerViewDownload;
    private int imageUploadCounter = 0;
    private int spinnerPos = 0;
    private int spinnerPosTicketType = 0;
    private ResponseMyRequestResult responseMyRequestResult;
    private ArrayList<ResponseTicketTypeResult> arraylistTicketType = new ArrayList<>();

    ;
    private Activity mActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
        mContext = activity;
    }

    private void init(View view) {

        txtWard = view.findViewById(R.id.txtWard);
        txtIssue = view.findViewById(R.id.txtIssue);
        txtDescription = view.findViewById(R.id.txtDescription);
        txtStatus = view.findViewById(R.id.txtStatus);
        txtObservation = view.findViewById(R.id.txtObservation);
        txtDate = view.findViewById(R.id.txtDate);
        txtIssueId = view.findViewById(R.id.txtIssueId);
        txtAddress = view.findViewById(R.id.txtAddress);


        imageDownloadAdapter = new ImageDownloadAdapter(mContext, imagesList);
        mRecyclerViewDownload = view.findViewById(R.id.recyclerViewDownload);
        mRecyclerViewDownload.setNestedScrollingEnabled(false);
        LinearLayoutManager mLayoutManager =
                new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerViewDownload.setLayoutManager(mLayoutManager);
        mRecyclerViewDownload.setAdapter(imageDownloadAdapter);

        Objects.requireNonNull(((HomeActivity) mContext).getSupportActionBar()).setTitle("Issue Observation");


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_observation, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(false);

        init(view);

        if (getArguments() != null) {

            responseMyRequestResult = (ResponseMyRequestResult) getArguments().getSerializable("str");

            if (responseMyRequestResult != null) {

                if (responseMyRequestResult.getTitle() != null) {
                    txtIssue.setText(responseMyRequestResult.getTitle());
                }

                if (responseMyRequestResult.getId() != null) {
                    txtIssueId.setText("id_" + responseMyRequestResult.getId() + "_" + responseMyRequestResult.getTitle());
                }

                if (responseMyRequestResult.getObservation() != null) {
                    txtObservation.setText(responseMyRequestResult.getObservation());
                }

                if (responseMyRequestResult.getDescription() != null) {
                    txtDescription.setText(responseMyRequestResult.getDescription());
                }

                if (responseMyRequestResult.getWardName() != null) {
                    txtWard.setText(responseMyRequestResult.getWardName());
                }

                if (responseMyRequestResult.getmUpdatedAt() != null) {

                    double l = responseMyRequestResult.getmUpdatedAt();

                    Date date = new Date((long) l * 1000);
                    @SuppressLint("SimpleDateFormat") SimpleDateFormat fmtOut = new SimpleDateFormat("EEE, d MMM yyyy hh:mm a");
                    txtDate.setText(fmtOut.format(date));
                }

                if (responseMyRequestResult.getAddress() != null) {
                    txtAddress.setText(responseMyRequestResult.getAddress());
                }


                switch (responseMyRequestResult.getStatus()) {
                    case PENDING:
                        txtStatus.setText("Pending");
                        break;
                    case IN_PROGRESS:
                        txtStatus.setText("In Progress");
                        break;
                    case COMPLETED:
                        txtStatus.setText("Resolved");
                        break;
                }

                //Download images
                downloadImages(responseMyRequestResult.getId());

                imageDownloadAdapter.setOnItemClickListener(new ImageDownloadAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        Dialog builder = new Dialog(mContext);
                        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        builder.getWindow().setBackgroundDrawable(
                                new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialogInterface) {
                                //nothing;
                            }
                        });

                        ImageView imageView = new ImageView(mContext);

                        Picasso.with(mContext)
                                .load(imagesList.get(position).getImage())
                                .placeholder(R.drawable.loading)
                                .into(imageView);

                        builder.addContentView(imageView, new RelativeLayout.LayoutParams(
                                ViewGroup.LayoutParams.WRAP_CONTENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT));
                        builder.show();
                    }
                });


            }

        }

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    // downloading Image/Video
    private void downloadImages(int ticketId) {


        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(mContext, "Images...");

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseDownloadImage> call = apiService.downloadImages(headerMap, ticketId);
        call.enqueue(new Callback<ResponseDownloadImage>() {
            @Override
            public void onResponse(@NonNull Call<ResponseDownloadImage> call, @NonNull Response<ResponseDownloadImage> response) {

                ResponseDownloadImage serverResponse = response.body();
                if (serverResponse != null) {

                    imagesList.addAll(serverResponse.getResults());
                    imageDownloadAdapter.notifyDataSetChanged();
                    if (imagesList.size() > 0) {
                        mRecyclerViewDownload.setVisibility(View.VISIBLE);
                    }


                } else {

                    if (serverResponse != null) {
                        Log.d("uploadFile", serverResponse.toString());
                    }
                }

                if (progressDialog != null) {
                    progressDialog.dismiss();
                }

            }

            @Override
            public void onFailure(Call<ResponseDownloadImage> call, Throwable t) {
                Log.d("uploadFile", t.toString());
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
            }
        });


    }


}


