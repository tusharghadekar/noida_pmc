package tata.noidacitizen.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tata.noidacitizen.MyApplication;
import tata.noidacitizen.R;
import tata.noidacitizen.activities.HomeActivity;
import tata.noidacitizen.adapters.MyRequestAdapter;
import tata.noidacitizen.models.ResponseMyRequest;
import tata.noidacitizen.models.ResponseMyRequestResult;
import tata.noidacitizen.networkcommunication.ApiClient;
import tata.noidacitizen.networkcommunication.ApiInterface;
import tata.noidacitizen.networkcommunication.DialogUtils;

/*
 * Created by ${KEDAR_LABDE} on 12-12-2017.
 */

public class MyRequestsFragment extends Fragment {
    private Context mContext;
    private MyRequestAdapter myRequestAdapter;
    private ArrayList<ResponseMyRequestResult> myRequestsArrayList = new ArrayList<>();
    private RecyclerView mRecyclerView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }

    private void init(View view) {
        setHasOptionsMenu(false);

        mRecyclerView = view.findViewById(R.id.recyclerView);
        myRequestAdapter = new MyRequestAdapter(mContext, myRequestsArrayList);
        LinearLayoutManager mLayoutManager =
                new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(myRequestAdapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_requests, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init(view);

//    myRequestsArrayList.add(new MyRequests("LED Street Light Not Glowing","Completed"));
//    myRequestsArrayList.add(new MyRequests("LED Street Light Missing","Completed"));
//    myRequestsArrayList.add(new MyRequests("Whole Sector / Area LED Street Light Not Glowing","Pending"));
//    myRequestsArrayList.add(new MyRequests("HM LED Light Not Glowing","In Progress"));

        getMyRequests();

        myRequestAdapter.setOnItemClickListener(new MyRequestAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {


                Bundle bundle = new Bundle();
                bundle.putSerializable("str", myRequestsArrayList.get(position));
                Fragment selected = new ObservationFragment();
                selected.setArguments(bundle);

                FragmentManager fragmentManager = ((HomeActivity) mContext).getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, selected, "227")
                        .commitAllowingStateLoss();

            }
        });
    }

    private void getMyRequests() {

        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(mContext, "");

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseMyRequest> call = apiService.getMyRequests(headerMap);
        call.enqueue(new Callback<ResponseMyRequest>() {
            @Override
            public void onResponse(Call<ResponseMyRequest> call, Response<ResponseMyRequest> response) {

                ResponseMyRequest responseMyRequest = response.body();

                if (responseMyRequest != null && responseMyRequest.getResults() != null) {

                    progressDialog.dismiss();
                    myRequestsArrayList.addAll(responseMyRequest.getResults());
                    myRequestAdapter.notifyDataSetChanged();

                } else {

                    ((HomeActivity) mContext).showFailAlert("Request Fetch Failed");
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseMyRequest> call, Throwable t) {
                // Log error here since request failed

                ((HomeActivity) mContext).showFailAlert(getString(R.string.internal_error));
                progressDialog.dismiss();
            }
        });
    }
}


