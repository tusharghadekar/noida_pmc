package tata.noidacitizen;

public interface SharedPreferences {
    String DEFAULT_VALUE = "";
    String EMAIL = "email";
    String MOBILE_NO = "mobile_no";

    //roll
    int ADMIN = 1;
    int SR_ENGINEER = 2;
    int JR_ENGINEER = 3;
    int CITIZEN = 4;

    //status
    int PENDING = 1;
    int IN_PROGRESS = 2;
    int COMPLETED = 3;

    int ISSUE = 1;
    int OBSERVATION = 2;

}
