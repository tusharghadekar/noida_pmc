package mobicloud.pmc_ledgrievance;

public interface SharedPreferences {
    String DEFAULT_VALUE = "";
    String EMAIL = "email";
    String USER_TYPE = "user_type";
    String MOBILE_NO = "mobile_no";


    //roll
    int ADMIN = 1;
    int SR_ENGINEER = 2;
    int JR_ENGINEER = 3;
    int CITIZEN = 4;
    int SR_ADMIN = 5;
    int MAIN_ADMIN = 6;


    //status
    int PENDING = 1;
    int IN_PROGRESS = 2;
    int COMPLETED = 3;
    int PMC_ISSUE = 4;
    int TATA_POWER_ISSUE = 5;
    Boolean ESCALATED = false;


    int ISSUE = 1;
    int OBSERVATION = 2;

    //WORKING_FROM
    int PMC = 0;
    int TATAPOWER = 1;
    int MSEB = 2;


}
