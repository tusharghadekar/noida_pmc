package mobicloud.pmc_ledgrievance.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;

import mobicloud.pmc_ledgrievance.R;

import net.grandcentrix.tray.AppPreferences;

import java.util.Objects;

import static mobicloud.pmc_ledgrievance.SharedPreferences.DEFAULT_VALUE;
import static mobicloud.pmc_ledgrievance.SharedPreferences.JR_ENGINEER;
import static mobicloud.pmc_ledgrievance.SharedPreferences.SR_ENGINEER;
import static mobicloud.pmc_ledgrievance.SharedPreferences.USER_TYPE;

public class SplashActivity extends AppCompatActivity {

    private AppPreferences appPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        appPreferences = new AppPreferences(this);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (Objects.requireNonNull(appPreferences.getString(USER_TYPE, DEFAULT_VALUE))
                        .equals(DEFAULT_VALUE)) {

                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                } else {

                    if (Objects.equals(appPreferences.getString(USER_TYPE, DEFAULT_VALUE), SR_ENGINEER + "")) {
                        startActivity(new Intent(SplashActivity.this, SrHomeActivity.class));
                    } else if (Objects.equals(appPreferences.getString(USER_TYPE, DEFAULT_VALUE), JR_ENGINEER + "")) {
                        startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                    }
                    finish();

                }
                finish();
            }
        }, 2000);
    }


    @RequiresApi(28)
    private static class OnUnhandledKeyEventListenerWrapper implements
            View.OnUnhandledKeyEventListener {
        private ViewCompat.OnUnhandledKeyEventListenerCompat mCompatListener;

        OnUnhandledKeyEventListenerWrapper(ViewCompat.OnUnhandledKeyEventListenerCompat listener) {
            this.mCompatListener = listener;
        }

        public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
            return this.mCompatListener.onUnhandledKeyEvent(v, event);
        }
    }
}
