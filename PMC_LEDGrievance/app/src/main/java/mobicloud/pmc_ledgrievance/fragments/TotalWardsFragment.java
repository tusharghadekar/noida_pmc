package mobicloud.pmc_ledgrievance.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import mobicloud.pmc_ledgrievance.MyApplication;
import mobicloud.pmc_ledgrievance.R;
import mobicloud.pmc_ledgrievance.activities.HomeActivity;
import mobicloud.pmc_ledgrievance.activities.SrHomeActivity;
import mobicloud.pmc_ledgrievance.adapters.WardsAdapter;
import mobicloud.pmc_ledgrievance.models.ResponseWard;
import mobicloud.pmc_ledgrievance.models.ResponseWardResult;
import mobicloud.pmc_ledgrievance.networkcommunication.ApiClient;
import mobicloud.pmc_ledgrievance.networkcommunication.ApiInterface;
import mobicloud.pmc_ledgrievance.networkcommunication.DialogUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*
 * Created by ${KEDAR_LABDE} on 12-12-2017.
 */

public class TotalWardsFragment extends Fragment {
    private Context mContext;
    private WardsAdapter wardsAdapter;
    private ArrayList<ResponseWardResult> responseWardResults = new ArrayList<>();
    private RecyclerView mRecyclerView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }

    private void init(View view) {

        mRecyclerView = view.findViewById(R.id.recyclerView);
        wardsAdapter = new WardsAdapter(mContext, responseWardResults);
        LinearLayoutManager mLayoutManager =
                new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(wardsAdapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frament_wards, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(false);
        ((SrHomeActivity) mContext).setTitle("Wards");

        init(view);

        getWards();

        wardsAdapter.setOnItemClickListener(new WardsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                startActivity(new Intent(mContext, HomeActivity.class).putExtra("exit", "exit").putExtra("ward_id", responseWardResults.get(position).getId() + "").putExtra("ward_name", responseWardResults.get(position).getName() + ""));
            }
        });

    }

    private void getWards() {

        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(mContext, "");

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseWard> call = apiService.getWards(headerMap);
        call.enqueue(new Callback<ResponseWard>() {
            @Override
            public void onResponse(Call<ResponseWard> call, Response<ResponseWard> response) {
                ResponseWard responseWard = response.body();


                if (responseWard != null && responseWard.getResults() != null) {

                    progressDialog.dismiss();
                    responseWardResults.addAll(responseWard.getResults());
                    wardsAdapter.notifyDataSetChanged();

                } else {

                    ((SrHomeActivity) mContext).showFailAlert("Wards Fetch Failed");


                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseWard> call, Throwable t) {
                // Log error here since request failed

                ((SrHomeActivity) mContext).showFailAlert(getString(R.string.internal_error));
                progressDialog.dismiss();
            }
        });
    }


}


