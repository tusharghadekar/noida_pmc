package mobicloud.pmc_ledgrievance.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import mobicloud.pmc_ledgrievance.MyApplication;
import mobicloud.pmc_ledgrievance.R;
import mobicloud.pmc_ledgrievance.Utility;
import mobicloud.pmc_ledgrievance.activities.HomeActivity;
import mobicloud.pmc_ledgrievance.activities.SrHomeActivity;
import mobicloud.pmc_ledgrievance.adapters.RequestsAdapter;
import mobicloud.pmc_ledgrievance.models.ResponseMyRequest;
import mobicloud.pmc_ledgrievance.models.ResponseMyRequestResult;
import mobicloud.pmc_ledgrievance.models.ResponseWard;
import mobicloud.pmc_ledgrievance.models.ResponseWardResult;
import mobicloud.pmc_ledgrievance.networkcommunication.ApiClient;
import mobicloud.pmc_ledgrievance.networkcommunication.ApiInterface;
import mobicloud.pmc_ledgrievance.networkcommunication.DialogUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*
 * Created by ${KEDAR_LABDE} on 12-12-2017.
 */

public class ResolvedRequestsFragment extends Fragment {
    private Context mContext;
    private RequestsAdapter myRequestAdapter;
    private ArrayList<ResponseMyRequestResult> myRequestsArrayList = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private int ward_id = 0;
    private Activity mActivity;
    private View mView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
        mContext = activity;
    }

    private void init(View view) {

        mView= view;

        mRecyclerView = view.findViewById(R.id.recyclerView);
        myRequestAdapter = new RequestsAdapter(mContext, myRequestsArrayList, this, getActivity());
        LinearLayoutManager mLayoutManager =
                new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(myRequestAdapter);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_requests, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(false);
        init(view);

        if (getArguments() != null) {

            ward_id = Integer.parseInt(Objects.requireNonNull(getArguments().getString("ward_id")));

            if (ward_id != 0) {
                getWardById(ward_id);
            }

        } else {

            getWards();

        }

        myRequestAdapter.setOnItemClickListener(new RequestsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                // Toast.makeText(mContext, "Clicked", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getMyRequests(final int ward_id) {

        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(mContext, "");

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseMyRequest> call = apiService.getCompletedRequests(headerMap);
        call.enqueue(new Callback<ResponseMyRequest>() {
            @Override
            public void onResponse(Call<ResponseMyRequest> call, Response<ResponseMyRequest> response) {

                ResponseMyRequest responseMyRequest = response.body();

                if (responseMyRequest != null && responseMyRequest.getResults() != null) {

                    progressDialog.dismiss();
                    for (ResponseMyRequestResult request : responseMyRequest.getResults()) {

                        if (ward_id == request.getWard()/*&&!request.getEscalated()*/) {
                            myRequestsArrayList.add(request);
                        }
                    }

                    TextView txtCount = mView.findViewById(R.id.txtCount);
                    txtCount.setText("Complaints Count on this Screen is : " + myRequestsArrayList.size());

                    myRequestAdapter.notifyDataSetChanged();

                } else {

                    Utility.showFailureAlert(ResolvedRequestsFragment.this, getString(R.string.internal_error));
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseMyRequest> call, Throwable t) {
                // Log error here since request failed

                Utility.showFailureAlert(ResolvedRequestsFragment.this, getString(R.string.internal_error));
                progressDialog.dismiss();
            }
        });
    }


    private void getWardById(final int ward_id) {

        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(mContext, "");

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseWardResult> call = apiService.getWardById(headerMap, ward_id);
        call.enqueue(new Callback<ResponseWardResult>() {
            @Override
            public void onResponse(Call<ResponseWardResult> call, Response<ResponseWardResult> response) {
                ResponseWardResult responseWardResult = response.body();


                if (responseWardResult != null) {

                    progressDialog.dismiss();

                    getMyRequests(ward_id);

                } else {

                    if (mActivity instanceof HomeActivity) {
                        ((HomeActivity) mContext).showFailAlert("Ward Fetch Failed");
                    } else {
                        ((SrHomeActivity) mContext).showFailAlert("Ward Fetch Failed");
                    }

                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseWardResult> call, Throwable t) {
                // Log error here since request failed
                if (mActivity instanceof HomeActivity) {
                    ((HomeActivity) mContext).showFailAlert(getString(R.string.internal_error));
                } else {
                    ((SrHomeActivity) mContext).showFailAlert(getString(R.string.internal_error));
                }

                progressDialog.dismiss();
            }
        });
    }

    private void getWards() {

        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(mContext, "");

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseWard> call = apiService.getWards(headerMap);
        call.enqueue(new Callback<ResponseWard>() {
            @Override
            public void onResponse(Call<ResponseWard> call, Response<ResponseWard> response) {
                ResponseWard responseWard = response.body();


                if (responseWard != null && responseWard.getResults() != null) {

                    progressDialog.dismiss();

                    getMyRequests(responseWard.getResults().get(0).getId());

                } else {

                    ((HomeActivity) mContext).showFailAlert("Wards Fetch Failed");


                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseWard> call, Throwable t) {
                // Log error here since request failed

                ((HomeActivity) mContext).showFailAlert(getString(R.string.internal_error));
                progressDialog.dismiss();
            }
        });
    }
}

