package mobicloud.pmc_ledgrievance.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.button.MaterialButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import mobicloud.pmc_ledgrievance.MyApplication;
import mobicloud.pmc_ledgrievance.R;
import mobicloud.pmc_ledgrievance.Utility;
import mobicloud.pmc_ledgrievance.models.UserResponse;
import mobicloud.pmc_ledgrievance.models.UserResult;
import mobicloud.pmc_ledgrievance.networkcommunication.ApiClient;
import mobicloud.pmc_ledgrievance.networkcommunication.ApiInterface;
import mobicloud.pmc_ledgrievance.networkcommunication.DialogUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*
 * Created by ${KEDAR_LABDE} on 12-12-2017.
 */

public class ProfileFragment extends Fragment {
    private Context mContext;
    private MaterialButton mbSubmit;
    private ImageButton mbUpload;
    private TextInputLayout input_layout_address, input_layout_name, input_layout_house_no, input_layout_Sector, input_layout_mobile;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }

    private void init(View view) {

        mbSubmit = view.findViewById(R.id.mbSubmit);
        mbUpload = view.findViewById(R.id.mbUpload);
        input_layout_address = view.findViewById(R.id.input_layout_address);
        input_layout_mobile = view.findViewById(R.id.input_layout_mobile);
        input_layout_name = view.findViewById(R.id.input_layout_name);
        input_layout_house_no = view.findViewById(R.id.input_layout_house_no);
        input_layout_Sector = view.findViewById(R.id.input_layout_Sector);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(false);
        init(view);

        getUserInfoAPI();

        mbSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                updateProfile();

            }
        });
    }

    public void getUserInfoAPI() {

        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(mContext, "");

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");
        Call<UserResponse> call = apiService.getUser(headerMap);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {

                progressDialog.dismiss();
                UserResponse userReponse = response.body();
                ArrayList<UserResult> userArrayList = null;
                if (userReponse != null) {

                    userArrayList = (ArrayList<UserResult>) userReponse.getResults();
                    if (userArrayList != null && userArrayList.size() > 0) {

                        UserResult user = userArrayList.get(0);

                        if (user.getUsername() != null) {
                            Objects.requireNonNull(input_layout_mobile.getEditText()).setText(user.getUsername());
                            Objects.requireNonNull(input_layout_mobile.getEditText()).setEnabled(false);
                        }
                        if (user.getAddress() != null) {
                            Objects.requireNonNull(input_layout_house_no.getEditText()).setText(user.getAddress().toString());
                        }

                        if (user.getFirstName() != null) {
                            Objects.requireNonNull(input_layout_name.getEditText()).setText(user.getAddress().toString());
                        }
                        if (user.getAddress() != null) {
                            Objects.requireNonNull(input_layout_Sector.getEditText()).setText(user.getAddress().toString());
                        }
                        if (user.getAddress() != null) {
                            Objects.requireNonNull(input_layout_address.getEditText()).setText(user.getAddress().toString());
                        }
                    }
                }


            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {

                Utility.showFailureAlert(ProfileFragment.this, getString(R.string.internal_error));
                progressDialog.dismiss();
            }
        });
    }

    public void updateProfile() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        String token = MyApplication.getUserToken();

        UserResult user = new UserResult();
        user.setFirstName(Objects.requireNonNull(input_layout_name.getEditText()).getText().toString());
        user.setAddress(Objects.requireNonNull(input_layout_address.getEditText()).getText().toString());

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");
        Call<UserResponse> call = apiService.updateProfile(headerMap, user, MyApplication.getUserID());
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {

                UserResponse userReponse = response.body();
                ArrayList<UserResult> userArrayList = null;
                if (userReponse != null) {
                    userArrayList = (ArrayList<UserResult>) userReponse.getResults();
                    if (userArrayList != null && userArrayList.size() > 0) {

//            UserResult user = userArrayList.get(0);
//            MyApplication.saveLoginUserID(user.getId());

                        Utility.showSuccessAlert(ProfileFragment.this,
                                "Profile has been updated successfully.");
                    }
                }


            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
            }
        });
    }
}


