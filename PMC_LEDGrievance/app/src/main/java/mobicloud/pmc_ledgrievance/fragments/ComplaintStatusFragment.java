package mobicloud.pmc_ledgrievance.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.button.MaterialButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import mobicloud.pmc_ledgrievance.MyApplication;
import mobicloud.pmc_ledgrievance.R;
import mobicloud.pmc_ledgrievance.Utility;
import mobicloud.pmc_ledgrievance.activities.HomeActivity;
import mobicloud.pmc_ledgrievance.activities.SrHomeActivity;
import mobicloud.pmc_ledgrievance.models.ResponseMyRequest;
import mobicloud.pmc_ledgrievance.models.ResponseMyRequestResult;
import mobicloud.pmc_ledgrievance.models.ResponseWardResult;
import mobicloud.pmc_ledgrievance.networkcommunication.ApiClient;
import mobicloud.pmc_ledgrievance.networkcommunication.ApiInterface;
import mobicloud.pmc_ledgrievance.networkcommunication.DialogUtils;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static mobicloud.pmc_ledgrievance.SharedPreferences.COMPLETED;
import static mobicloud.pmc_ledgrievance.SharedPreferences.IN_PROGRESS;
import static mobicloud.pmc_ledgrievance.SharedPreferences.PENDING;
import static mobicloud.pmc_ledgrievance.SharedPreferences.PMC;
import static mobicloud.pmc_ledgrievance.SharedPreferences.PMC_ISSUE;
import static mobicloud.pmc_ledgrievance.SharedPreferences.TATAPOWER;
import static mobicloud.pmc_ledgrievance.SharedPreferences.TATA_POWER_ISSUE;

/*
 * Created by ${KEDAR_LABDE} on 12-12-2017.
 */

public class ComplaintStatusFragment extends Fragment {
    TextView txtCountPending, txtCountResolved, txtCountInPro, txtCountEscalated,txtIssueOwner;
    String ward_id = "";
    int countCompleted = 0;
    int countPending = 0;
    int countInProgress = 0;
    int countPMCIssues = 0;
    int countTATAIssues = 0;
    private Context mContext;
    private MaterialButton mbSubmit;
    private ImageButton mbUpload;
    private LinearLayout ll_pending, ll_resolved, ll_inPro, ll_escalated;
    private Activity mActivity;
    String title = "";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
        mContext = activity;
    }

    private void init(View view) {

        ll_pending = view.findViewById(R.id.ll_pending);
        ll_resolved = view.findViewById(R.id.ll_resolved);
        ll_inPro = view.findViewById(R.id.ll_inPro);
        ll_escalated =view.findViewById(R.id.ll_escalated);

        txtCountPending = view.findViewById(R.id.txtCountPending);
        txtCountResolved = view.findViewById(R.id.txtCountResolved);
        txtCountInPro = view.findViewById(R.id.txtCountInPro);
        txtCountEscalated = view.findViewById(R.id.txtCountEscalated);
        txtIssueOwner= view.findViewById(R.id.txtIssueOwner);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_complaint_status, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(false);
        init(view);



//        if (MyApplication.getEngineerTypeWorkingFrom() == PMC) {
//
//            title = "PMC Issues";
//
//        } else if (MyApplication.getEngineerTypeWorkingFrom() == TATAPOWER) {
//
//            title = "TATA Issues";
//
//        }
//        txtIssueOwner.setText(title);

        if (getArguments() != null) {

            ward_id = (getArguments().getString("ward_id"));

            if (ward_id != null && !ward_id.equalsIgnoreCase("")) {
//                getMyRequests(Integer.parseInt(ward_id));

                getWards();
            }

        } else {
            getWards();
        }

        ll_pending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Bundle bundle = new Bundle();
                bundle.putSerializable("ward_id", ward_id);
                Fragment selected = new PendingRequestsFragment();

                if (mActivity instanceof HomeActivity) {

                    if (!ward_id.equalsIgnoreCase("")) {


                        ((HomeActivity) mContext).getSupportActionBar().setTitle("Pending Complaints");
                        selected.setArguments(bundle);
                        FragmentManager fragmentManager = ((HomeActivity) mContext).getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, selected, "5")
                                .commitAllowingStateLoss();


                    } else {

                        ((HomeActivity) mContext).getSupportActionBar().setTitle("Pending Complaints");
                        FragmentManager fragmentManager = ((HomeActivity) mContext).getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, selected, "5")
                                .commitAllowingStateLoss();

                    }
                } else if (mActivity instanceof SrHomeActivity) {

                    if (!ward_id.equalsIgnoreCase("")) {

                        ((SrHomeActivity) mContext).getSupportActionBar().setTitle("Pending Complaints");
                        selected.setArguments(bundle);
                        FragmentManager fragmentManager = ((SrHomeActivity) mContext).getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, selected, "5")
                                .commitAllowingStateLoss();


                    } else {
                        ((SrHomeActivity) mContext).getSupportActionBar().setTitle("Pending Complaints");
                        FragmentManager fragmentManager = ((SrHomeActivity) mContext).getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, selected, "5")
                                .commitAllowingStateLoss();

                    }
                }


            }
        });

        ll_resolved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment selected = new ResolvedRequestsFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("ward_id", ward_id);

                if (mActivity instanceof HomeActivity) {

                    if (!ward_id.equalsIgnoreCase("")) {

                        ((HomeActivity) mContext).getSupportActionBar().setTitle("Resolved Complaints");
                        selected.setArguments(bundle);
                        FragmentManager fragmentManager = ((HomeActivity) mContext).getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, selected, "5")
                                .commitAllowingStateLoss();


                    } else {

                        ((HomeActivity) mContext).getSupportActionBar().setTitle("Resolved Complaints");
                        FragmentManager fragmentManager = ((HomeActivity) mContext).getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, selected, "5")
                                .commitAllowingStateLoss();

                    }
                } else if (mActivity instanceof SrHomeActivity) {

                    if (!ward_id.equalsIgnoreCase("")) {

                        ((SrHomeActivity) mContext).getSupportActionBar().setTitle("Resolved Complaints");
                        selected.setArguments(bundle);
                        FragmentManager fragmentManager = ((SrHomeActivity) mContext).getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, selected, "5")
                                .commitAllowingStateLoss();


                    } else {

                        ((SrHomeActivity) mContext).getSupportActionBar().setTitle("Resolved Complaints");
                        FragmentManager fragmentManager = ((SrHomeActivity) mContext).getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, selected, "5")
                                .commitAllowingStateLoss();

                    }
                }


            }
        });

        ll_inPro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle = new Bundle();
                bundle.putSerializable("ward_id", ward_id);
                bundle.putSerializable("engineerCallFrom", "PMC");
                Fragment selected = new PMCTATARequestsFragment();

                if (mActivity instanceof HomeActivity) {

                    if (!ward_id.equalsIgnoreCase("")) {


                        ((HomeActivity) mContext).getSupportActionBar().setTitle(title);
                        selected.setArguments(bundle);
                        FragmentManager fragmentManager = ((HomeActivity) mContext).getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, selected, "5")
                                .commitAllowingStateLoss();


                    } else {
                        ((HomeActivity) mContext).getSupportActionBar().setTitle(title);
                        FragmentManager fragmentManager = ((HomeActivity) mContext).getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, selected, "5")
                                .commitAllowingStateLoss();

                    }
                } else if (mActivity instanceof SrHomeActivity) {

                    if (!ward_id.equalsIgnoreCase("")) {

                        ((SrHomeActivity) mContext).getSupportActionBar().setTitle(title);
                        selected.setArguments(bundle);
                        FragmentManager fragmentManager = ((SrHomeActivity) mContext).getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, selected, "5")
                                .commitAllowingStateLoss();

                    } else {

                        ((SrHomeActivity) mContext).getSupportActionBar().setTitle(title);
                        FragmentManager fragmentManager = ((SrHomeActivity) mContext).getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, selected, "5")
                                .commitAllowingStateLoss();

                    }
                }

            }
        });

        ll_escalated.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle = new Bundle();
                bundle.putSerializable("ward_id", ward_id);
                bundle.putSerializable("engineerCallFrom", "TATA");
                Fragment selected = new PMCTATARequestsFragment();

                if (mActivity instanceof HomeActivity) {

                    if (!ward_id.equalsIgnoreCase("")) {


                        ((HomeActivity) mContext).getSupportActionBar().setTitle(title);
                        selected.setArguments(bundle);
                        FragmentManager fragmentManager = ((HomeActivity) mContext).getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, selected, "5")
                                .commitAllowingStateLoss();


                    } else {
                        ((HomeActivity) mContext).getSupportActionBar().setTitle(title);
                        FragmentManager fragmentManager = ((HomeActivity) mContext).getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, selected, "5")
                                .commitAllowingStateLoss();

                    }
                } else if (mActivity instanceof SrHomeActivity) {

                    if (!ward_id.equalsIgnoreCase("")) {

                        ((SrHomeActivity) mContext).getSupportActionBar().setTitle(title);
                        selected.setArguments(bundle);
                        FragmentManager fragmentManager = ((SrHomeActivity) mContext).getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, selected, "5")
                                .commitAllowingStateLoss();

                    } else {

                        ((SrHomeActivity) mContext).getSupportActionBar().setTitle(title);
                        FragmentManager fragmentManager = ((SrHomeActivity) mContext).getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, selected, "5")
                                .commitAllowingStateLoss();

                    }
                }


            }
        });
    }


    private void getWards() {

        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(mContext, "");

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseWardResult> call = apiService.getWardById(headerMap, Integer.parseInt(ward_id));
        call.enqueue(new Callback<ResponseWardResult>() {
            @Override
            public void onResponse(Call<ResponseWardResult> call, Response<ResponseWardResult> response) {
                ResponseWardResult ResponseWardResult = response.body();


                if (ResponseWardResult != null ) {

                    progressDialog.dismiss();
//                    getMyRequests(ResponseWardResult.getResults().get(0).getId());


                    txtCountPending.setText(ResponseWardResult.getPending() + "");
                    txtCountResolved.setText(ResponseWardResult.getResolved() + "");
                    txtCountInPro.setText(ResponseWardResult.getPmc() + "");
                    txtCountEscalated.setText(ResponseWardResult.getTata() + "");

                } else {

                    if (mActivity instanceof HomeActivity) {
                        ((HomeActivity) mContext).showFailAlert("Ward Fetch Failed");
                    } else {
                        ((SrHomeActivity) mContext).showFailAlert("Ward Fetch Failed");
                    }


                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseWardResult> call, Throwable t) {
                // Log error here since request failed

                if (mActivity instanceof HomeActivity) {
                    ((HomeActivity) mContext).showFailAlert(getString(R.string.internal_error));
                } else {
                    ((SrHomeActivity) mContext).showFailAlert(getString(R.string.internal_error));
                }
                progressDialog.dismiss();
            }
        });
    }




}


