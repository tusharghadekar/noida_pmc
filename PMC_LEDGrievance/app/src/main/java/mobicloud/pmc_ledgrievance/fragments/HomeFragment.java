package mobicloud.pmc_ledgrievance.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;
import mobicloud.pmc_ledgrievance.MyApplication;
import mobicloud.pmc_ledgrievance.R;
import mobicloud.pmc_ledgrievance.Utility;
import mobicloud.pmc_ledgrievance.activities.HomeActivity;
import mobicloud.pmc_ledgrievance.activities.SrHomeActivity;
import mobicloud.pmc_ledgrievance.adapters.MyInfoWindowAdapter;
import mobicloud.pmc_ledgrievance.models.Datum;
import mobicloud.pmc_ledgrievance.models.FeederList;
import mobicloud.pmc_ledgrievance.models.ResponseFeederList;
import mobicloud.pmc_ledgrievance.models.ResponseMyRequest;
import mobicloud.pmc_ledgrievance.models.ResponseMyRequestResult;
import mobicloud.pmc_ledgrievance.models.ResponseWard;
import mobicloud.pmc_ledgrievance.models.ResponseWardResult;
import mobicloud.pmc_ledgrievance.networkcommunication.ApiClient;
import mobicloud.pmc_ledgrievance.networkcommunication.ApiInterface;
import mobicloud.pmc_ledgrievance.networkcommunication.DialogUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static mobicloud.pmc_ledgrievance.SharedPreferences.COMPLETED;
import static mobicloud.pmc_ledgrievance.SharedPreferences.IN_PROGRESS;
import static mobicloud.pmc_ledgrievance.SharedPreferences.PENDING;
import static mobicloud.pmc_ledgrievance.SharedPreferences.PMC;
import static mobicloud.pmc_ledgrievance.SharedPreferences.PMC_ISSUE;
import static mobicloud.pmc_ledgrievance.SharedPreferences.TATAPOWER;
import static mobicloud.pmc_ledgrievance.SharedPreferences.TATA_POWER_ISSUE;


/*
 * Created by ${KEDAR_LABDE} on 12-12-2017.
 */

public class HomeFragment extends Fragment {
    int countTotal = 0;
    int countCompleted = 0;
    int countPending = 0;
    int countPMCIssues = 0;
    int countTATAIssues = 0;
    int countInProgress = 0;
    int countEscalated = 0;
    int countOnFeeders = 0;
    int countOffFeeders = 0;
    int countOnFeeders0 = 0;
    private View mView;

    TextView txtTotal, txtStatus, txtWard, txtPending, txtEscalated;
    CardView cardTotal, cardStatus, cardInPro, cardPending, cardEscalated, cardNotification;
    private Context mContext;
    private String ward_id = "";
    private Activity mActivity;
    private GoogleMap mMap;
    private SupportMapFragment mMapView;
    private ArrayList<FeederList> responseFeederLists = new ArrayList<>();
    private LinearLayout llOffFeeders, llOnFeeders, llSingleFeeder;
    private ImageButton ibSearch, ibRefresh;
    private TextView txtOnFeeders, txtOFFFeeders, txtFeederName, txtFeederStatus, txtOnFeeders0;
    private TextView txtFeeder;

    ProgressDialog progressDialog;



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
        mContext = activity;
    }

    private void init(View view) {

        mView = view;

        mMapView = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.homeMap);
        //  mMapView = (MapFragment) Objects.requireNonNull(getActivity()).getFragmentManager().findFragmentById(R.id.homeMap);
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap map) {
                mMap = map;
                LatLng sydney = new LatLng(18.516726, 73.856255);
                //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 13));


            }
        });

        progressDialog = DialogUtils.startProgressDialog(mContext, "");
        txtTotal = view.findViewById(R.id.txtTotal);
        txtStatus = view.findViewById(R.id.txtStatus);
        txtEscalated = view.findViewById(R.id.txtEscalated);
        //txtWard = view.findViewById(R.id.txtWard);
        ibSearch = view.findViewById(R.id.ibSearch);
        ibRefresh = view.findViewById(R.id.ibRefresh);
        txtOnFeeders = view.findViewById(R.id.txtOnFeeders);
        txtOFFFeeders = view.findViewById(R.id.txtOffFeeders);
        txtFeederName = view.findViewById(R.id.txtFeederName);
        txtFeederStatus = view.findViewById(R.id.txtFeederStatus);
        txtFeeder = view.findViewById(R.id.txtFeeder);
        txtOnFeeders0 = view.findViewById(R.id.txtOnFeeders0);


        cardTotal = view.findViewById(R.id.cardTotal);
        cardStatus = view.findViewById(R.id.cardStatus);
        cardEscalated = view.findViewById(R.id.cardEscalated);
        llOffFeeders = view.findViewById(R.id.llOffFeeders);
        llOnFeeders = view.findViewById(R.id.llOnFeeders);
        llSingleFeeder = view.findViewById(R.id.llSingleFeeder);

        llOnFeeders.setVisibility(View.VISIBLE);
        llOffFeeders.setVisibility(View.VISIBLE);


        cardTotal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((HomeActivity) mContext).openFragmentTotal();
            }
        });

        cardStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Bundle bundle = new Bundle();
                bundle.putSerializable("ward_id", ward_id);
                Fragment selected = new ComplaintStatusFragment();

                FragmentManager fragmentManager = ((HomeActivity) mContext).getSupportFragmentManager();
                ((HomeActivity) mContext).getSupportActionBar().setTitle("Complaints Status");


                if (!ward_id.equalsIgnoreCase("")) {

                    selected.setArguments(bundle);

                }

                fragmentManager.beginTransaction()
                        .replace(R.id.container, selected, "66")
                        .commitAllowingStateLoss();

            }
        });

        cardEscalated.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment selected = new EscalatedRequestsFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("ward_id", ward_id);
                if (!ward_id.equalsIgnoreCase("")) {

                    selected.setArguments(bundle);

                }

                FragmentManager fragmentManager = ((HomeActivity) mContext).getSupportFragmentManager();
                ((HomeActivity) mContext).getSupportActionBar().setTitle("Complaints Escalated");

                fragmentManager.beginTransaction()
                        .replace(R.id.container, selected, "34")
                        .commitAllowingStateLoss();
            }
        });


        ibRefresh.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                Toast.makeText(mContext, "Refresh Feeders", Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        ibRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                llOnFeeders.setVisibility(View.VISIBLE);
                llOffFeeders.setVisibility(View.VISIBLE);
                llSingleFeeder.setVisibility(View.GONE);

                getFeeders();

            }
        });
        ibSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (responseFeederLists.size() > 0) {

                    if (llSingleFeeder.getVisibility() == View.GONE) {

                        new SimpleSearchDialogCompat(mActivity, getString(R.string.app_name),
                                "Enter Feeder Name Here..", null, responseFeederLists,
                                new SearchResultListener<FeederList>() {
                                    @Override
                                    public void onSelected(BaseSearchDialogCompat dialog,
                                                           FeederList feeder, int position) {

                                        mMap.clear();
                                        countOnFeeders = 0;
                                        countOffFeeders = 0;
                                        countOnFeeders0 = 0;
                                        llOnFeeders.setVisibility(View.GONE);
                                        llOffFeeders.setVisibility(View.GONE);
                                        llSingleFeeder.setVisibility(View.VISIBLE);


                                        ibSearch.setImageResource(R.drawable.ic_clear);

                                        txtFeederName.setText(feeder.getDetail().getNodename());
                                        txtFeederStatus.setText(feeder.getDetail().getStatus());

                                        LatLng pune = new LatLng(18.516726, 73.856255);
                                        //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
                                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pune, 10));

                                        MarkerOptions optionsYou = new MarkerOptions();
                                        if (feeder.getDetail().getLatitude() != null && !feeder.getDetail().getLatitude().equalsIgnoreCase("")) {
                                            LatLng inActiveFeeder = new LatLng((float) Double.parseDouble(feeder.getDetail().getLatitude()), (float) Double.parseDouble(feeder.getDetail().getLongitude()));


                                            String strData = "";

                                            for (Datum datum : feeder.getDetail().getData()) {

                                                try {
                                                    if (strData.equalsIgnoreCase("")) {
                                                        strData = strData.concat( "LastUpdated : " + datum.getTimestamp() + "\nResourceId : " + datum.getResourceId() + "\nResourceType : " + datum.getResourceType() + "\nFormattedValue : " + datum.getFormattedValue());

                                                    } else {
                                                        strData = strData.concat("\nResourceId : " + datum.getResourceId() + "\nResourceType : " + datum.getResourceType() + "\nFormattedValue : " + datum.getFormattedValue());

                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                            if (feeder.getDetail().getStatus().equalsIgnoreCase("Ok")) {


                                                if (feeder.getDetail().getState().equalsIgnoreCase("1")) {

                                                    if (feeder.getDetail().getData().size() == 0) {

                                                        strData = "Feeder Data Not Available";
                                                        // Setting the position of the marker
                                                    }


                                                    optionsYou.position(inActiveFeeder);
                                                    optionsYou.title(feeder.getDetail().getNodename() + "(Online ON)");
                                                    optionsYou.snippet(strData);
                                                    optionsYou.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_on));
                                                    mMap.addMarker(optionsYou);

                                                    countOnFeeders++;


                                                } else if (feeder.getDetail().getState().equalsIgnoreCase("0")) {

                                                    if (feeder.getDetail().getData().size() == 0) {

                                                        strData = "Feeder Data Not Available";
                                                        // Setting the position of the marker
                                                    }


                                                    // Setting the position of the marker
                                                    optionsYou.position(inActiveFeeder);
                                                    optionsYou.title(feeder.getDetail().getNodename() + "(Online OFF)");
                                                    optionsYou.snippet(feeder.getDetail().getStatus() + ", " + strData);
                                                    optionsYou.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_down));
                                                    mMap.addMarker(optionsYou);

                                                    countOnFeeders0++;

                                                }


                                            } else if (feeder.getDetail().getStatus().equalsIgnoreCase("DOWN")) {

                                                strData = "Data Not Available";

                                                optionsYou.position(inActiveFeeder);
                                                optionsYou.title(feeder.getDetail().getNodename() + "(Offline)");
                                                optionsYou.snippet(strData);
                                                optionsYou.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_yellow));
                                                mMap.addMarker(optionsYou);


                                                countOffFeeders++;

                                            }


                                        }


                                        dialog.dismiss();
                                    }
                                }).show();

                    } else {

                        llOnFeeders.setVisibility(View.VISIBLE);
                        llOffFeeders.setVisibility(View.VISIBLE);
                        llSingleFeeder.setVisibility(View.GONE);

                        getFeeders();

                    }

                } else {

                    Utility.showFailureAlert(mActivity, "Feeders Not Available.");

                }

            }
        });


    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(false);
        init(view);


        if (getArguments() != null) {

            ward_id = (getArguments().getString("ward_id"));

            if (ward_id != null && !ward_id.equalsIgnoreCase("")) {
                getWardById(Integer.parseInt(ward_id));
            }

        } else {

            getWards();

        }

    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }


    private void getMyRequests(final int ward_id) {

        progressDialog.show();
        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseMyRequest> call = apiService.getTotalRequestById(headerMap, String.valueOf(ward_id));
        call.enqueue(new Callback<ResponseMyRequest>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<ResponseMyRequest> call, Response<ResponseMyRequest> response) {

                ResponseMyRequest responseMyRequest = response.body();

                if (responseMyRequest != null && responseMyRequest.getResults() != null) {


                    getFeeders();

//
                    txtStatus.setText(responseMyRequest.getResults().size()  + "");
                    txtTotal.setText(responseMyRequest.getResults().size() + "");

                    getTotalEscalatedRequestById(ward_id);


                } else {

                    Utility.showFailureAlert(HomeFragment.this, getString(R.string.internal_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseMyRequest> call, Throwable t) {
                // Log error here since request failed

                Utility.showFailureAlert(HomeFragment.this, getString(R.string.internal_error));
                progressDialog.dismiss();
            }
        });
    }

    private void getTotalEscalatedRequestById(final int ward_id) {

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseMyRequest> call = apiService.getTotalEscalatedRequestById(headerMap, String.valueOf(ward_id));
        call.enqueue(new Callback<ResponseMyRequest>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<ResponseMyRequest> call, Response<ResponseMyRequest> response) {

                ResponseMyRequest responseMyRequest = response.body();

                if (responseMyRequest != null && responseMyRequest.getResults() != null) {



                    txtEscalated.setText(responseMyRequest.getResults().size() + "");

                    progressDialog.dismiss();


                } else {

                    Utility.showFailureAlert(HomeFragment.this, getString(R.string.internal_error));
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseMyRequest> call, Throwable t) {
                // Log error here since request failed

                Utility.showFailureAlert(HomeFragment.this, getString(R.string.internal_error));
                progressDialog.dismiss();
            }
        });
    }


    private void getWardById(final int ward_id) {



        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseWardResult> call = apiService.getWardById(headerMap, ward_id);
        call.enqueue(new Callback<ResponseWardResult>() {
            @Override
            public void onResponse(Call<ResponseWardResult> call, Response<ResponseWardResult> response) {
                ResponseWardResult responseWardResult = response.body();


                if (responseWardResult != null) {



                    // txtWard.setText(responseWardResult.getName());

                    if (mActivity instanceof HomeActivity) {
                        Objects.requireNonNull(((HomeActivity) mContext).getSupportActionBar()).setTitle(responseWardResult.getName());
                    } else {
                        Objects.requireNonNull(((HomeActivity) mContext).getSupportActionBar()).setTitle(responseWardResult.getName());
                    }

                    getMyRequests(ward_id);

                } else {

                    if (mActivity instanceof HomeActivity) {
                        ((HomeActivity) mContext).showFailAlert("Ward Fetch Failed");
                    } else {
                        ((SrHomeActivity) mContext).showFailAlert("Ward Fetch Failed");
                    }


                }
            }

            @Override
            public void onFailure(Call<ResponseWardResult> call, Throwable t) {
                // Log error here since request failed
                if (mActivity instanceof HomeActivity) {
                    ((HomeActivity) mContext).showFailAlert(getString(R.string.internal_error));
                } else {
                    ((SrHomeActivity) mContext).showFailAlert(getString(R.string.internal_error));
                }

                progressDialog.dismiss();
            }
        });
    }

    private void getWards() {

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseWard> call = apiService.getWards(headerMap);
        call.enqueue(new Callback<ResponseWard>() {
            @Override
            public void onResponse(Call<ResponseWard> call, Response<ResponseWard> response) {
                ResponseWard responseWard = response.body();


                if (responseWard != null && responseWard.getResults() != null) {

                    //  txtWard.setText(responseWard.getResults().get(0).getName());
                    Objects.requireNonNull(((HomeActivity) mContext).getSupportActionBar()).setTitle((responseWard.getResults().get(0).getName()));


                    ward_id = String.valueOf(responseWard.getResults().get(0).getId());
                    ((HomeActivity) Objects.requireNonNull(getActivity())).setWardId(ward_id);
                    getMyRequests(responseWard.getResults().get(0).getId());


                } else {

                    ((HomeActivity) mContext).showFailAlert("Wards Fetch Failed");

                }
            }

            @Override
            public void onFailure(Call<ResponseWard> call, Throwable t) {
                // Log error here since request failed

                ((HomeActivity) mContext).showFailAlert(getString(R.string.internal_error));
                progressDialog.dismiss();
            }
        });
    }

    private void getFeeders() {

        responseFeederLists.clear();
        countOnFeeders = 0;
        countOffFeeders = 0;
        countOnFeeders0  = 0;
        txtOnFeeders.setText(countOnFeeders + "");
        txtOFFFeeders.setText(countOffFeeders + "");
        txtOnFeeders0.setText(countOnFeeders0 + "");
        txtFeeder.setText(countOffFeeders + countOnFeeders + countOnFeeders0 + "");

        ibSearch.setImageResource(R.drawable.ic_search_feeder);



        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseFeederList> call = apiService.getFeedersByWard(headerMap, ward_id);
        call.enqueue(new Callback<ResponseFeederList>() {
            @Override
            public void onResponse(Call<ResponseFeederList> call, Response<ResponseFeederList> response) {


                if (response.body() != null) {


                    for (FeederList feederList : response.body().getFeederList()) {

                        if (feederList.getDetail().getData() != null) {

                            responseFeederLists.add(feederList);
                        }
                    }
                    //responseFeederLists.addAll(response.body().getFeederList());

                    mMapView.getMapAsync(new OnMapReadyCallback() {
                        @Override
                        public void onMapReady(GoogleMap map) {
                            mMap = map;
                            LatLng pune = new LatLng(18.516726, 73.856255);
                            //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pune, 10));

                            mMap.setInfoWindowAdapter(new MyInfoWindowAdapter(HomeFragment.this));
//                            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
//
//                                @Override
//                                public View getInfoWindow(Marker arg0) {
//                                    return null;
//                                }
//
//                                @Override
//                                public View getInfoContents(Marker marker) {
//
//                                    LinearLayout info = new LinearLayout(mContext);
//                                    info.setOrientation(LinearLayout.VERTICAL);
////                                    info.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT));
//
//                                    TextView title = new TextView(mContext);
//                                    title.setTextColor(Color.BLACK);
//                                    title.setGravity(Gravity.CENTER);
//                                    title.setTextSize(10);
//                                    title.setTypeface(null, Typeface.BOLD);
//                                    title.setText(marker.getTitle());
//
//                                    TextView snippet = new TextView(mContext);
//                                    snippet.setTextColor(Color.GRAY);
//                                    title.setTextSize(10);
//                                    snippet.setText(marker.getSnippet());
//
//                                    info.addView(title);
//                                    info.addView(snippet);
//
//                                    return info;
//                                }
//                            });

                            markPoints();
                            // Adding new item to the ArrayList
                            // markerPoints.add(complaint);

                            // markerPoints.add(engineer);


                        }
                    });


                }

            }

            @Override
            public void onFailure(Call<ResponseFeederList> call, Throwable t) {
                // Log error here since request failed

                ((HomeActivity) mContext).showFailAlert(getString(R.string.internal_error));
                progressDialog.dismiss();
            }
        });
    }

    private void markPoints() {


        for (FeederList feeder : responseFeederLists) {


            try {
                MarkerOptions optionsYou = new MarkerOptions();
                if (feeder.getDetail().getLatitude() != null && !feeder.getDetail().getLatitude().equalsIgnoreCase("")) {
                    LatLng inActiveFeeder = new LatLng((float) Double.parseDouble(feeder.getDetail().getLatitude()), (float) Double.parseDouble(feeder.getDetail().getLongitude()));


                    String strData = "";

                    for (Datum datum : feeder.getDetail().getData()) {

                        try {
                            if (strData.equalsIgnoreCase("")) {
                                strData = strData.concat( "LastUpdated : " + datum.getTimestamp() + "\nResourceId : " + datum.getResourceId() + "\nResourceType : " + datum.getResourceType() + "\nFormattedValue : " + datum.getFormattedValue());

                            } else {
                                strData = strData.concat("\nResourceId : " + datum.getResourceId() + "\nResourceType : " + datum.getResourceType() + "\nFormattedValue : " + datum.getFormattedValue());

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (feeder.getDetail().getStatus().equalsIgnoreCase("Ok")) {


                        if (feeder.getDetail().getState().equalsIgnoreCase("1")) {

                            if (feeder.getDetail().getData().size() == 0) {

                                strData = "Feeder Data Not Available";
                                // Setting the position of the marker
                            }


                            optionsYou.position(inActiveFeeder);
                            optionsYou.title(feeder.getDetail().getNodename() + "(Online ON)");
                            optionsYou.snippet(strData);
                            optionsYou.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_on));
                            mMap.addMarker(optionsYou);

                            countOnFeeders++;


                        } else if (feeder.getDetail().getState().equalsIgnoreCase("0")) {

                            if (feeder.getDetail().getData().size() == 0) {

                                strData = "Feeder Data Not Available";
                                // Setting the position of the marker
                            }


                            // Setting the position of the marker
                            optionsYou.position(inActiveFeeder);
                            optionsYou.title(feeder.getDetail().getNodename() + "(Online OFF)");
                            optionsYou.snippet(feeder.getDetail().getStatus() + ", " + strData);
                            optionsYou.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_down));
                            mMap.addMarker(optionsYou);


                            countOnFeeders0++;

                        }


                    } else {

                        strData = "Data Not Available";

                        optionsYou.position(inActiveFeeder);
                        optionsYou.title(feeder.getDetail().getNodename() + "(Offline)");
                        optionsYou.snippet(strData);
                        optionsYou.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_yellow));
                        mMap.addMarker(optionsYou);


                        countOffFeeders++;

                    }

                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

        }

        txtOnFeeders.setText(countOnFeeders + "");
        txtOFFFeeders.setText(countOffFeeders + "");
        txtOnFeeders0.setText(countOnFeeders0 + "");
        txtFeeder.setText(countOffFeeders + countOnFeeders + countOnFeeders0 + "");


    }


}


