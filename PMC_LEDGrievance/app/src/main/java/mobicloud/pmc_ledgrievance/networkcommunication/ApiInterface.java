package mobicloud.pmc_ledgrievance.networkcommunication;


import mobicloud.pmc_ledgrievance.models.LoginRequest;
import mobicloud.pmc_ledgrievance.models.LoginResponse;
import mobicloud.pmc_ledgrievance.models.RequestRegisterComplaint;
import mobicloud.pmc_ledgrievance.models.ResponseDownloadImage;
import mobicloud.pmc_ledgrievance.models.ResponseFeeder;
import mobicloud.pmc_ledgrievance.models.ResponseFeederDetails;
import mobicloud.pmc_ledgrievance.models.ResponseFeederList;
import mobicloud.pmc_ledgrievance.models.ResponseMyRequest;
import mobicloud.pmc_ledgrievance.models.ResponseMyRequestResult;
import mobicloud.pmc_ledgrievance.models.ResponseNotifications;
import mobicloud.pmc_ledgrievance.models.ResponseNotificationsResult;
import mobicloud.pmc_ledgrievance.models.ResponseObservation;
import mobicloud.pmc_ledgrievance.models.ResponseTicketType;
import mobicloud.pmc_ledgrievance.models.ResponseUploadImage;
import mobicloud.pmc_ledgrievance.models.ResponseWard;
import mobicloud.pmc_ledgrievance.models.ResponseWardResult;
import mobicloud.pmc_ledgrievance.models.UserResponse;
import mobicloud.pmc_ledgrievance.models.UserResult;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("tickettype/?ticket_type=2&limit=1000")
    Call<ResponseObservation> getObservationTypes(@HeaderMap Map<String, String> headers);

    @GET("ward/?&limit=1000")
    Call<ResponseWard> getWards(@HeaderMap Map<String, String> headers);

    @POST("ticket/")
    Call<RequestRegisterComplaint> postRegisterComplaint(@HeaderMap Map<String, String> headers, @Body RequestRegisterComplaint registerComplaint);

    @POST("login/")
    Call<LoginResponse> doLogin(@Body LoginRequest loginRequest);

    @GET("user/")
    Call<UserResponse> getUser(@HeaderMap Map<String, String> headers);

    @GET("ticket/?&limit=1000")
    Call<ResponseMyRequest> getMyRequests(@HeaderMap Map<String, String> headers);

    @GET("ticket/?&limit=1000")
    Call<ResponseMyRequest> getTotalRequestById(@HeaderMap Map<String, String> headers, @Query("ward") String ward_id);

    @GET("ticket/?escalated=1&limit=1000")
    Call<ResponseMyRequest> getTotalEscalatedRequestById(@HeaderMap Map<String, String> headers, @Query("ward") String ward_id);

    @GET("ticket/?escalated=1&limit=1000")
    Call<ResponseMyRequest> getTotalEscalatedRequest(@HeaderMap Map<String, String> headers);
//    http://139.59.55.102/api/5&limit=1000

    @GET("ticket/?status=1&limit=1000")
    Call<ResponseMyRequest> getPendingRequests(@HeaderMap Map<String, String> headers);

    @GET("ticket/?status=2&limit=1000")
    Call<ResponseMyRequest> getInProgressRequests(@HeaderMap Map<String, String> headers);

    @GET("ticket/?status=3&limit=1000")
    Call<ResponseMyRequest> getCompletedRequests(@HeaderMap Map<String, String> headers);

    @GET("ticket/?status=4&limit=1000")
    Call<ResponseMyRequest> getPMCRequests(@HeaderMap Map<String, String> headers, @Query("ward") int ward_id);

    @GET("ticket/?status=5&limit=1000")
    Call<ResponseMyRequest> getTATARequests(@HeaderMap Map<String, String> headers, @Query("ward") int ward_id);

    @GET("notification/")
    Call<ResponseNotifications> getNotifications(@HeaderMap Map<String, String> headers);

    @Multipart
    @POST("ticketimage/")
    Call<ResponseUploadImage> uploadFile(@HeaderMap Map<String, String> headers, @Part MultipartBody.Part file, @Part("ticket") RequestBody ticket_id);

    @PUT("ticket/{id}/")
    Call<ResponseMyRequest> patchObservation(@HeaderMap Map<String, String> headers, @Body ResponseMyRequestResult registerObservation, @Path("id") int ticket_id);

    @PATCH("user/{id}/")
    Call<UserResponse> updateProfile(@HeaderMap Map<String, String> headers, @Body UserResult user, @Path("id") int user_id);

    @GET("ward/{id}/")
    Call<ResponseWardResult> getWardById(@HeaderMap Map<String, String> headers, @Path("id") int user_id);

    @GET("ticketimage/?")
    Call<ResponseDownloadImage> downloadImages(@HeaderMap Map<String, String> headers, @Query("ticket") int ticket_id);

    @GET("feeder/")
    Call<ResponseFeederList> getFeeders(@HeaderMap Map<String, String> headers);

    @GET("feeder/?")
    Call<ResponseFeederList> getFeedersByWard(@HeaderMap Map<String, String> headers,@Query("ward") String ward_id);

    @GET("feeder/?node_id={id}/")
    Call<ResponseFeederDetails> getFeederDetail(@HeaderMap Map<String, String> headers, @Path("id") int user_id);

    @PATCH("notification/{id}/")
    Call<ResponseNotificationsResult> readNotification(@HeaderMap Map<String, String> headers, @Body ResponseNotificationsResult notificationsResult, @Path("id") int user_id);

}
