package mobicloud.pmc_ledgrievance.networkcommunication;

public interface WebserviceResponseHandler {

    void onResponseSuccess(Object o);

    void onResponseFailure();


}
