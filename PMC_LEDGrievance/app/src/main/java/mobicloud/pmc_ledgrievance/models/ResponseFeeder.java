package mobicloud.pmc_ledgrievance.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseFeeder {

    @SerializedName("updated")
    @Expose
    private String updated;
    @SerializedName("results")
    @Expose
    private List<ResultFeaders> results = null;

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public List<ResultFeaders> getResults() {
        return results;
    }

    public void setResults(List<ResultFeaders> results) {
        this.results = results;
    }

}