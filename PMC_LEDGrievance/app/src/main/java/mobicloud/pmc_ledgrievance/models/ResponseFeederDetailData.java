package mobicloud.pmc_ledgrievance.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseFeederDetailData {

@SerializedName("formattedValue")
@Expose
private String formattedValue;
@SerializedName("resource_type")
@Expose
private String resourceType;
@SerializedName("resourceId")
@Expose
private String resourceId;

public String getFormattedValue() {
return formattedValue;
}

public void setFormattedValue(String formattedValue) {
this.formattedValue = formattedValue;
}

public String getResourceType() {
return resourceType;
}

public void setResourceType(String resourceType) {
this.resourceType = resourceType;
}

public String getResourceId() {
return resourceId;
}

public void setResourceId(String resourceId) {
this.resourceId = resourceId;
}

}