package mobicloud.pmc_ledgrievance.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseFeederDetails {

@SerializedName("data")
@Expose
private List<ResponseFeederDetailData> data = null;

public List<ResponseFeederDetailData> getData() {
return data;
}

public void setData(List<ResponseFeederDetailData> data) {
this.data = data;
}

}