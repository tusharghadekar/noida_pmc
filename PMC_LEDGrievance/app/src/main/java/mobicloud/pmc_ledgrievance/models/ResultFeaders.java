package mobicloud.pmc_ledgrievance.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ir.mirrajabi.searchdialog.core.Searchable;

public class ResultFeaders implements Searchable {



    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("nodeid")
    @Expose
    private Integer nodeid;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("nodename")
    @Expose
    private String nodename;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getNodeid() {
        return nodeid;
    }

    public void setNodeid(Integer nodeid) {
        this.nodeid = nodeid;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getNodename() {
        return nodename;
    }

    public void setNodename(String nodename) {
        this.nodename = nodename;
    }

    @Override
    public String getTitle() {
        return nodename;
    }
}
