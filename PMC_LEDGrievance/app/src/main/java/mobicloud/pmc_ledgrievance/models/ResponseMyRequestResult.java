package mobicloud.pmc_ledgrievance.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ResponseMyRequestResult implements Serializable {


    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("ticket_type")
    @Expose
    private Integer ticketType;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("type_group_title")
    @Expose
    private String typeGroupTitle;
    @SerializedName("type_group")
    @Expose
    private String typeGroup;
    @SerializedName("ward_name")
    @Expose
    private String wardName;
    @SerializedName("zone_name")
    @Expose
    private String zoneName;
    @SerializedName("engineer_name")
    @Expose
    private String engineerName;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("zone")
    @Expose
    private Integer zone;
    @SerializedName("ward")
    @Expose
    private Integer ward;
    @SerializedName("engineer")
    @Expose
    private Integer engineer;
    @SerializedName("user")
    @Expose
    private Integer user;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("mCreatedAt")
    @Expose
    private Double mCreatedAt;
    @SerializedName("issue_for")
    @Expose
    private Integer issueFor;
    @SerializedName("mUpdatedAt")
    @Expose
    private Double mUpdatedAt;
    @SerializedName("observation")
    @Expose
    private String observation;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("escalated")
    @Expose
    private Boolean escalated;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;

    @SerializedName("mobile_no")
    @Expose
    private String mobileNo;

    @SerializedName("citizen_name")
    @Expose
    private String citizenName;

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getCitizenName() {
        return citizenName;
    }

    public void setCitizenName(String citizenName) {
        this.citizenName = citizenName;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTicketType() {
        return ticketType;
    }

    public void setTicketType(Integer ticketType) {
        this.ticketType = ticketType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTypeGroupTitle() {
        return typeGroupTitle;
    }

    public void setTypeGroupTitle(String typeGroupTitle) {
        this.typeGroupTitle = typeGroupTitle;
    }

    public String getTypeGroup() {
        return typeGroup;
    }

    public void setTypeGroup(String typeGroup) {
        this.typeGroup = typeGroup;
    }

    public String getWardName() {
        return wardName;
    }

    public void setWardName(String wardName) {
        this.wardName = wardName;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public String getEngineerName() {
        return engineerName;
    }

    public void setEngineerName(String engineerName) {
        this.engineerName = engineerName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getZone() {
        return zone;
    }

    public void setZone(Integer zone) {
        this.zone = zone;
    }

    public Integer getWard() {
        return ward;
    }

    public void setWard(Integer ward) {
        this.ward = ward;
    }

    public Integer getEngineer() {
        return engineer;
    }

    public void setEngineer(Integer engineer) {
        this.engineer = engineer;
    }

    public Integer getUser() {
        return user;
    }

    public void setUser(Integer user) {
        this.user = user;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Double getMCreatedAt() {
        return mCreatedAt;
    }

    public void setMCreatedAt(Double mCreatedAt) {
        this.mCreatedAt = mCreatedAt;
    }

    public Integer getIssueFor() {
        return issueFor;
    }

    public void setIssueFor(Integer issueFor) {
        this.issueFor = issueFor;
    }

    public Double getMUpdatedAt() {
        return mUpdatedAt;
    }

    public void setMUpdatedAt(Double mUpdatedAt) {
        this.mUpdatedAt = mUpdatedAt;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getEscalated() {
        return escalated;
    }

    public void setEscalated(Boolean escalated) {
        this.escalated = escalated;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public ResponseMyRequestResult(Integer id, Integer ticketType, String title, String description, Integer zone,
                                   Integer ward, Integer engineer, Integer user, String latitude, String longitude,
                                   Integer status,Boolean escalated,String typeGroup) {
        this.id = id;
        this.ticketType = ticketType;
        this.title = title;
        this.description = description;
        this.zone = zone;
        this.ward = ward;
        this.engineer = engineer;
        this.user = user;
        this.latitude = latitude;
        this.longitude = longitude;
        this.status = status;
        this.escalated= escalated;
        this.observation= description;
        this.typeGroup= typeGroup;
    }

    public Double getmCreatedAt() {
        return mCreatedAt;
    }

    public void setmCreatedAt(Double mCreatedAt) {
        this.mCreatedAt = mCreatedAt;
    }

    public Double getmUpdatedAt() {
        return mUpdatedAt;
    }

    public void setmUpdatedAt(Double mUpdatedAt) {
        this.mUpdatedAt = mUpdatedAt;
    }


}