package mobicloud.pmc_ledgrievance.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseFeederList {

    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("feeder_list")
    @Expose
    private List<FeederList> feederList = null;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<FeederList> getFeederList() {
        return feederList;
    }

    public void setFeederList(List<FeederList> feederList) {
        this.feederList = feederList;
    }


}