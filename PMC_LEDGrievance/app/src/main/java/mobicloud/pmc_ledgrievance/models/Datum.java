package mobicloud.pmc_ledgrievance.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("formattedValue")
    @Expose
    private String formattedValue;
    @SerializedName("resourceId")
    @Expose
    private String resourceId;
    @SerializedName("resource_type")
    @Expose
    private String resourceType;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;

    public String getFormattedValue() {
        return formattedValue;
    }

    public void setFormattedValue(String formattedValue) {
        this.formattedValue = formattedValue;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}