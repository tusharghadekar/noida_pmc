package mobicloud.pmc_ledgrievance.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ResponseTicketTypeResult implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("ticket_type")
    @Expose
    private Integer ticketType;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("type_group")
    @Expose
    private Integer typeGroup;
    @SerializedName("type_group_title")
    @Expose
    private String typeGroupTitle;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTicketType() {
        return ticketType;
    }

    public void setTicketType(Integer ticketType) {
        this.ticketType = ticketType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getTypeGroup() {
        return typeGroup;
    }

    public void setTypeGroup(Integer typeGroup) {
        this.typeGroup = typeGroup;
    }

    public String getTypeGroupTitle() {
        return typeGroupTitle;
    }

    public void setTypeGroupTitle(String typeGroupTitle) {
        this.typeGroupTitle = typeGroupTitle;
    }

}