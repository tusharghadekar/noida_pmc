package mobicloud.pmc_ledgrievance.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import mobicloud.pmc_ledgrievance.R;
import mobicloud.pmc_ledgrievance.models.ResponseNotificationsResult;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static mobicloud.pmc_ledgrievance.SharedPreferences.COMPLETED;
import static mobicloud.pmc_ledgrievance.SharedPreferences.IN_PROGRESS;
import static mobicloud.pmc_ledgrievance.SharedPreferences.PENDING;


public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.MyViewHolder> {

    private Context mContext;
    private OnItemClickListener mItemClickListener;
    private List<ResponseNotificationsResult> listItems;

    public NotificationsAdapter(Context context, List<ResponseNotificationsResult> listItems) {
        this.listItems = listItems;
        this.mContext = context;
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notifications_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        if (listItems.get(position).getmCreatedAt() != null) {

            double l = listItems.get(position).getmCreatedAt();

            Date date = new Date((long) l * 1000);
            @SuppressLint("SimpleDateFormat") SimpleDateFormat fmtOut = new SimpleDateFormat("EEE, d MMM yyyy hh:mm a");

            holder.txtNotificationDesc.setText("Ward Name : " + listItems.get(position).getWardName() + "\n Date : " + fmtOut.format(date));
        }

        if (!listItems.get(position).isRead()){

            holder.txtNotification.setTypeface(null, Typeface.BOLD);        // for Bold only

        }else {
            holder.txtNotification.setTypeface(null, Typeface.NORMAL);
        }

        switch (listItems.get(position).getTicketStatus()) {

            case PENDING:

                holder.txtNotification.setText("Complaint id_" + listItems.get(position).getTicketId() + " " + listItems.get(position).getTicketType() + " has been booked.");

                break;

            case IN_PROGRESS:

                holder.txtNotification.setText("Complaint id_" + listItems.get(position).getTicketId() + " " + listItems.get(position).getTicketType() + " has been changed to In Progress.");

                break;

            case COMPLETED:

                holder.txtNotification.setText("Complaint id_" + listItems.get(position).getTicketId() + " " + listItems.get(position).getTicketType() + " has been resolved and submitted.");

                break;
        }

    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtNotification, txtNotificationDesc;

        MyViewHolder(View itemView) {
            super(itemView);
            txtNotification = (TextView) itemView.findViewById(R.id.txtNotification);
            txtNotificationDesc = (TextView) itemView.findViewById(R.id.txtNotificationDesc);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(view, getPosition());

            }
        }
    }

    /*
    method to set listener to the adapter ViewHolder item
     */
}
