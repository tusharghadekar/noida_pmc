import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  avatarImgSrc: string = 'assets/images/pmc_logo.png';
  userName: string = 'PMC';
  userPost: string = 'Admin';
  
  constructor() { }

  ngOnInit() {
  }

}
