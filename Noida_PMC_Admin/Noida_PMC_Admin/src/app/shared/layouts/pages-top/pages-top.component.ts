import { Component, Input } from '@angular/core';
import { GlobalService } from '../../services/global.service';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'pages-top',
  templateUrl: './pages-top.component.html',
  styleUrls: ['./pages-top.component.scss'],
})
export class PagesTopComponent {
  avatarImgSrc: string = 'assets/images/Noida.jpg';
  TataImgSrc: string = 'assets/images/Tata.jpg';
  userName: string = 'NOIDA';
  userPost: string = 'Admin';
  private readonly notifier: NotifierService;

  sidebarToggle: boolean = true;
  tip = { ring: true, email: true };

  constructor(private _globalService: GlobalService,notifierService: NotifierService) { }

  public _sidebarToggle() {
    /* this._globalService.sidebarToggle$.subscribe(sidebarToggle => {
      this.sidebarToggle = sidebarToggle;
    }, error => {
      console.log('Error: ' + error);
    }); */

    this._globalService.data$.subscribe(data => {
      if (data.ev === 'sidebarToggle') {
        this.sidebarToggle = data.value;
      }
    }, error => {
      console.log('Error: ' + error);
    });
    this._globalService.dataBusChanged('sidebarToggle', !this.sidebarToggle);

    this.notifier.notify( 'success', 'You are awesome! I mean it!' );
    //this._globalService._sidebarToggleState(!this.sidebarToggle);
  }
}
