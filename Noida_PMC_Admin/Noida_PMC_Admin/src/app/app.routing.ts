import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages/pages.component';
import { ZonesComponent } from './pages/zones/zones.component';
const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'login'
  },
//   {
//     path: 'zones',
//     redirectTo: 'zones'
    
// },
];

export const routing = RouterModule.forRoot(appRoutes,{ useHash: true });
