import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { PagesModule } from './pages/pages.module';
import { routing } from './app.routing';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ModalModule} from "ngx-modal";
import { HttpClientModule } from '@angular/common/http';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {NgxPaginationModule} from 'ngx-pagination';
import { AngularMultiSelectModule } from 'angular4-multiselect-dropdown/angular4-multiselect-dropdown';
// import { NgxLoadingModule } from 'ngx-loading';
import { MyDateRangePickerModule } from 'mydaterangepicker';
import { NotifierModule } from 'angular-notifier';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    PagesModule,
    ModalModule,
    routing,
    HttpClientModule,
    FilterPipeModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    AngularMultiSelectModule,
    MyDateRangePickerModule,
    // NgxLoadingModule.forRoot({})
    NotifierModule
  ],
  declarations: [
    AppComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
