import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ObservationsListComponent } from './observations-list.component';
const routes: Routes = [{
  path: '',
  component: ObservationsListComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ObservationsListRoutingModule { }
