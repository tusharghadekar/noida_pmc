import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Location } from '@angular/common';
import { Http, Response } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';


import { Router, ActivatedRoute, Params } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-issues',
  templateUrl: './issues.component.html',
  styleUrls: ['./issues.component.scss']
})
export class IssuesComponent implements OnInit {

  public viewStore: string;

  server_url: string = environment.devServer_url;
  tokens: any = localStorage.getItem('token');
  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'JWT ' + this.tokens
    })
  };

  constructor(private _router: Router, private route: ActivatedRoute, private http: HttpClient, private location: Location) { }


  public addIssue(e): void {
    e.preventDefault();
      ;

    let ticket_type = e.target.elements[1].value;
    let title = e.target.elements[3].value;
    // let category = e.target.elements[2].value;

    const addIssueObject = {
      ticket_type: ticket_type,
      title: title,
      //  category : category,
    }
      ;
    let localUrl = "tickettype/";
    let url = this.server_url + localUrl;
    this.http.post(url, JSON.stringify(addIssueObject), this.httpOptions)
      .subscribe(
        (res) => {
          if (res) {
            alert("You added successfully..");
            // this.viewStore = '';
            //  this.getBrandData();
            this.getIssueData(1);
          this.getObservetionData(2);
          } else {
            alert("Issue NOT Added");
          }
        },
        err => {
          alert("Error occoured");
          // this.viewAuditor ='';
        }
      );

  }

  userFilter: any = { name: '' };
  id: any;
  IssueData;
  IssueDataCount;
  getIssueData(type) {
      
    // this.id = this.route.snapshot.paramMap.get('id');
    // this.spinnerService.show();
    let localUrl = "tickettype/?ticket_type="+type;
    //let localUrl = "brand/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.IssueData = res['results'];
          // .filter(
          //   issue => issue.ticket_type === type);
          this.IssueDataCount = res['count'];

          //   this.BrandData.forEach(obj => {

          //     let localUrl = "category/"+obj.category;
          //     //let localUrl = "brand/";
          //     let url = this.server_url + localUrl;

          //     this.http.get(url,this.httpOptions)
          //       .subscribe(
          //         (res: Response) => {
          //           var xyz=res;
          //           obj.catname=xyz["name"];
          //           // this.spinnerService.hide();
          //         },
          //         //err => { alert("Error occoured"); this.spinnerService.hide(); }
          //       );

          // });
          // this.spinnerService.hide();
        },
        err => {
          alert("Error occoured");
          // this.spinnerService.hide();
        }
      );
  }
  oservationData;
  oservationDataCount;
  getObservetionData(type) {
      
    // this.id = this.route.snapshot.paramMap.get('id');
    // this.spinnerService.show();
    let localUrl = "tickettype/?ticket_type="+type;
    //let localUrl = "brand/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.oservationData = res['results'];
          // .filter(
          //   issue => issue.ticket_type === type);
          this.oservationDataCount = res['count'];

          //   this.BrandData.forEach(obj => {

          //     let localUrl = "category/"+obj.category;
          //     //let localUrl = "brand/";
          //     let url = this.server_url + localUrl;

          //     this.http.get(url,this.httpOptions)
          //       .subscribe(
          //         (res: Response) => {
          //           var xyz=res;
          //           obj.catname=xyz["name"];
          //           // this.spinnerService.hide();
          //         },
          //         //err => { alert("Error occoured"); this.spinnerService.hide(); }
          //       );

          // });
          // this.spinnerService.hide();
        },
        err => {
          alert("Error occoured");
          // this.spinnerService.hide();
        }
      );
  }


  updateIssue(beforeUpdateIssueObj) {
      
    this.beforeUpdateIssueObj = {
      "ticket_type": beforeUpdateIssueObj.ticket_type,
      "title": beforeUpdateIssueObj.title
    };
    let localUrl = "tickettype/" + beforeUpdateIssueObj.id + "/";
    let url = this.server_url + localUrl;

    this.http.put(url, JSON.stringify(this.beforeUpdateIssueObj), this.httpOptions).subscribe(
      (res) => {
        if (res) {
          this.beforeUpdateIssueObj = {
            "id": res['id'],
            "ticket_type": res['ticket_type'],
            "title": res['title'],
          };
          alert("Successfully Updated");
          this.viewStore = '';
          this.getIssueData(1);
          this.getObservetionData(2);
        } else { alert("error Occured") }
      },
    );


  } // Update Auditor functionality close
  beforeUpdateIssueObj;
  // Editing Auditor function 
  editIssue(IssueData) {

    if (confirm("You want to edit ?")) {
        ;
      // this.viewStore = 'editStore';
      let localUrl = "tickettype/" + IssueData.id + "/";
      let url = this.server_url + localUrl;
      const checkcatObject = { id: IssueData.id }

      this.http.patch(url, JSON.stringify(checkcatObject), this.httpOptions).subscribe(
        (res) => {
          if (res)
            this.beforeUpdateIssueObj = {
              "id": res['id'],
              "ticket_type": res['ticket_type'],
              "title": res['title'],
              

            };

        }, err => { alert("You made no change"); }
      );
    } else { }

  }

  deleteIssue(IssueData) {
    if (confirm("You want to Delete?")) {
        ;
      let localUrl = "tickettype/" + IssueData.id + "/";
      let url = this.server_url + localUrl;

      this.http.delete(url, this.httpOptions).subscribe(
        (res) => {
          alert("You Deleted Zone");
          //this.viewStore="";
          //this.getIssueData();
        }, err => { alert("You made no change"); }
      );
    } else { alert("error occured"); }

  }

  issuepageNo: number = 1;
  pageChange(pageNo: any) {
    // this.zoneName=[];
      
    this.issuepageNo = pageNo;
    console.log("pageNo: " + this.issuepageNo);
    let localUrl = "tickettype/?page="+this.issuepageNo+"&ticket_type=1";
    //let localUrl = "brand/";
    let url = this.server_url + localUrl;
    // this.spinnerService.show();
    // let localUrl = "user/?page="+this.userpageNo;
    // let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.IssueData = res['results'];
          // .filter(
          //   issue => issue.ticket_type === 1);
          this.IssueDataCount = res['count'];

          //   this.BrandData.forEach(obj => {

          //     let localUrl = "category/"+obj.category;
          //     //let localUrl = "brand/";
          //     let url = this.server_url + localUrl;

          //     this.http.get(url,this.httpOptions)
          //       .subscribe(
          //         (res: Response) => {
          //           var xyz=res;
          //           obj.catname=xyz["name"];
          //           // this.spinnerService.hide();
          //         },
          //         //err => { alert("Error occoured"); this.spinnerService.hide(); }
          //       );

          // });
          // this.spinnerService.hide();
        },
        err => {
          alert("Error occoured");
          // this.spinnerService.hide();
        }
      );
  }
  issuepageN: number = 1;
  pageChangen(pageN: any) {
    // this.zoneName=[];
      
    this.issuepageN = pageN;
    console.log("pageNo: " + this.issuepageN);
    let localUrl = "tickettype/?page="+this.issuepageN+"&ticket_type=2";
    //let localUrl = "brand/";
    let url = this.server_url + localUrl;
    // this.spinnerService.show();
    // let localUrl = "user/?page="+this.userpageNo;
    // let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.oservationData = res['results'];
          // .filter(
          //   issue => issue.ticket_type === 2);
          this.oservationDataCount = res['count'];

          //   this.BrandData.forEach(obj => {

          //     let localUrl = "category/"+obj.category;
          //     //let localUrl = "brand/";
          //     let url = this.server_url + localUrl;

          //     this.http.get(url,this.httpOptions)
          //       .subscribe(
          //         (res: Response) => {
          //           var xyz=res;
          //           obj.catname=xyz["name"];
          //           // this.spinnerService.hide();
          //         },
          //         //err => { alert("Error occoured"); this.spinnerService.hide(); }
          //       );

          // });
          // this.spinnerService.hide();
        },
        err => {
          alert("Error occoured");
          // this.spinnerService.hide();
        }
      );
  }
  ngOnInit() {
    //this.getIssueData();
    // this.viewStore = 'addStore';

  }

}
