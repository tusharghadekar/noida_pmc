import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IssuesComponent } from './issues.component';
import { IssuesRoutingModule } from './issues-routing.module';
import {ModalModule} from "ngx-modal";
import { FormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AngularMultiSelectModule } from 'angular4-multiselect-dropdown/angular4-multiselect-dropdown';
import {NgxPaginationModule} from 'ngx-pagination';
import { FormModule } from '../form/form.module';
@NgModule({
  imports: [
    CommonModule,
    ModalModule,
    IssuesRoutingModule,
    FormModule,
    NgxPaginationModule,
    AngularMultiSelectModule,
    Ng2SearchPipeModule,
    FormsModule

  ],
  declarations: [IssuesComponent]
})
export class IssuesModule { }
