import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EngineerComponent } from './engineer.component';
import { EngineerRoutingModule } from './engineer-routing.module';
import {ModalModule} from "ngx-modal";
import { FilterPipeModule } from 'ngx-filter-pipe';
import { FormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AngularMultiSelectModule } from 'angular4-multiselect-dropdown/angular4-multiselect-dropdown';
import {NgxPaginationModule} from 'ngx-pagination';

@NgModule({
  imports: [
    CommonModule,
    EngineerRoutingModule,
    ModalModule,
    FilterPipeModule,
    FormsModule,
    Ng2SearchPipeModule,
    AngularMultiSelectModule,
    NgxPaginationModule
  ],
  declarations: [EngineerComponent]
})
export class EngineerModule { }
