import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Location } from '@angular/common';
import { Http, Response } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
declare var $: any;

@Component({
  selector: 'app-engineer',
  templateUrl: './engineer.component.html',
  styleUrls: ['./engineer.component.scss']
})
export class EngineerComponent implements OnInit {

  public viewStore: string;

  server_url: string = environment.devServer_url;
  tokens: any = localStorage.getItem('token');
  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'JWT ' + this.tokens
    })
  };

  constructor(private _router: Router, private route: ActivatedRoute, private http: HttpClient, private location: Location) { }

  // @ViewChild('myModal')
  dropdownList = [];
  selectedItemsZone = [];
  dropdownSettings = {};
  selectedItemsWard = [];

  selectedId:any=[];
  selectedIdForPass:any=[];

  WardsArray=[];
  ZonesArray=[];
  
  public addUser(e): void {
    e.preventDefault();
      ;
    this.WardsArray=[];
    this.ZonesArray=[];
    let user_type = e.target.elements[1].value;
    let first_name = e.target.elements[3].value;
    let last_name = e.target.elements[5].value;
    let address = e.target.elements[7].value;
    let email = e.target.elements[9].value;
    let mobileno = e.target.elements[11].value;
    let password = e.target.elements[13].value;
    let latitude = e.target.elements[15].value;
    // let wards = e.target.elements[17].value;
    // let zones = e.target.elements[19].value;
    let username = e.target.elements[17].value;
    for (let i = 0; i < this.selectedItemsZone.length; i++) {
         this.ZonesArray.push(this.selectedItemsZone[i].id);
    }

    for (let i = 0; i < this.selectedItemsWard.length; i++) {
      this.WardsArray.push(this.selectedItemsWard[i].id);
 }

 if(user_type=="" || user_type==undefined || user_type=="null"){
alert("please select engineers category.");
 }else if(email=="" || email==undefined || email==null){
  alert("please insert email address");
 }else if((mobileno=="" || mobileno==undefined || mobileno==null)){
  alert("please insert valid mobile number");
 }else{


    
    const addUserObject = {
      user_type:user_type,
      first_name: first_name,
      last_name: last_name,
      address:address,
      email: email,
      mobile_no: mobileno,
      password: password,
      wards: this.WardsArray,
      zones:this.ZonesArray,
      username:username
    }
      ;
    let localUrl = "user/";
    let url = this.server_url + localUrl;
    this.http.post(url, JSON.stringify(addUserObject), this.httpOptions)
      .subscribe(
        (res) => {
          if (res) {
            alert("You added new User successfully..");
            this.viewStore = '';
            //  this.getBrandData();
          } else {
            alert("User NOT Added");
          }
        },
        err => {
          alert("Error occoured");
          // this.viewAuditor ='';
        }
      );
    }
  }
  
  ZoneData;
  ZoneDataCount;
  getZoneData() {
      
    // this.id = this.route.snapshot.paramMap.get('id');
    // this.spinnerService.show();
    let localUrl = "zone/";
    //let localUrl = "brand/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.ZoneData = res['results'];
          this.ZoneDataCount = res['count'];

          //   this.BrandData.forEach(obj => {

          //     let localUrl = "category/"+obj.category;
          //     //let localUrl = "brand/";
          //     let url = this.server_url + localUrl;

          //     this.http.get(url,this.httpOptions)
          //       .subscribe(
          //         (res: Response) => {
          //           var xyz=res;
          //           obj.catname=xyz["name"];
          //           // this.spinnerService.hide();
          //         },
          //         //err => { alert("Error occoured"); this.spinnerService.hide(); }
          //       );

          // });
          // this.spinnerService.hide();
        },
        err => {
          alert("Error occoured");
          // this.spinnerService.hide();
        }
      );
  }

  AllZoneDataCount;
  AllZoneData=[];
  getAllzoneData(userpageNo) {
      
    // this.spinnerService.show();
    let localUrl = "zone/?page="+userpageNo;
    let url = this.server_url + localUrl;
    
    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          //  
          //  this.categoryData.push(res['results'])
          // this.categoryData = this.categoryData.concat(res['results']);
          this.AllZoneDataCount = res['count'];
          var xyz = res;
          this.AllZoneData = this.AllZoneData.concat(res['results']);
          //this.allcategoryDataDropdown.push(this.allcategoryData);
          if(this.AllZoneData.length < this.AllZoneDataCount) {
            this.getAllzoneData(userpageNo +1);
          }
          
          //this.allcategoryDataDropdown.push(this.allcategoryData);
          for (let i = 0; i < this.AllZoneData.length; i++) {
            this.AllZoneData[i].id = this.AllZoneData[i].id;
            this.AllZoneData[i].itemName = this.AllZoneData[i].name;
          }

         

         
          // this.spinnerService.hide();
        },
        err => { alert("Error occoured"); 
        // this.spinnerService.hide(); 
      }
      );
  }





  userFilter: any = { email: '' };
  UserDataSr;
  UserDataSrCount;
  zonename=[];
  UserDataTPL
  UserDataTPLCount
  getUserDataTPL() {
      
    this.wardName=[];
    // this.id = this.route.snapshot.paramMap.get('id');
    // this.spinnerService.show();
    let localUrl = "user/?user_type=2";
    // let localUrl = "user/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.UserDataTPL = res['results'];
          this.UserDataTPLCount = res['count'];
  
          for(var i = 0;i<this.UserDataTPL.length;i++){
            let obj = this.UserDataTPL[i];
            this.UserDataTPL[i]['wardss'] = [];
            for(let j=0; j<obj.wards.length; j++ ){
              let wardId = obj.wards[j];
              let wardName = this.getwardName(wardId);
              if (this.UserDataTPL[i]['wardss']){
              this.UserDataTPL[i]['wardss'].push({
                'wardId':wardId,
                'wardName':wardName
              });
            }else{
              this.UserDataTPL[i]['wardss'] = [{}];
              // this.spinnerService.hide();
            }
            // this.spinnerService.hide();
            }
            // this.spinnerService.hide();
          }

          for(var i = 0;i<this.UserDataTPL.length;i++){
            let obj = this.UserDataTPL[i];
            this.UserDataTPL[i]['zoness'] = [];
            for(let j=0; j<obj.zones.length; j++ ){
              let zoneId = obj.zones[j];
              let zoneName = this.getzoneName(zoneId);
              if (this.UserDataTPL[i]['zoness']){
              this.UserDataTPL[i]['zoness'].push({
                'zoneId':zoneId,
                'zoneName':zoneName
              });
            }else{
              this.UserDataTPL[i]['zoness'] = [{}];
              // this.spinnerService.hide();
            }
            // this.spinnerService.hide();
            }
            // this.spinnerService.hide();
          }

          console.log(this.UserDataTPL);

//             this.UserData.forEach(obj => {
// for(let i = 0; i<obj.zones.length; i++){
//               let localUrl = "zone/"+obj.zones[i];
//               //let localUrl = "brand/";
//               let url = this.server_url + localUrl;

//               this.http.get(url,this.httpOptions)
//                 .subscribe(
//                   (res: Response) => {
//                     var xyz=res;
//                     obj.zonename = xyz["name"];
//                     //this.zonename.push(obj.zonename);
//                     // this.spinnerService.hide();
//                   },
//                   //err => { alert("Error occoured"); this.spinnerService.hide(); }
//                 );
//               }
//           });

//           this.UserData.forEach(objj => {
//             for(let i = 0; i<objj.wards.length; i++){
//             let localUrl = "ward/"+objj.wards[i];
//             //let localUrl = "brand/";
//             let url = this.server_url + localUrl;

//             this.http.get(url,this.httpOptions)
//               .subscribe(
//                 (res: Response) => {
//                   var xyz=res;
//                   objj.wardname=xyz["name"];
//                   // this.spinnerService.hide();
//                 },
//                 //err => { alert("Error occoured"); this.spinnerService.hide(); }
//               );
//             }
//         });
          // this.spinnerService.hide();
        },
        err => {
          alert("Error occoured");
          // this.spinnerService.hide();
        }
      );

      
  }

  getUserDataSr() {
      
    this.wardName=[];
    // this.id = this.route.snapshot.paramMap.get('id');
    // this.spinnerService.show();
    let localUrl = "user/?user_type=5";
    // let localUrl = "user/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.UserDataSr = res['results'];
          this.UserDataSrCount = res['count'];
  
          for(var i = 0;i<this.UserDataSr.length;i++){
            let obj = this.UserDataSr[i];
            this.UserDataSr[i]['wardss'] = [];
            for(let j=0; j<obj.wards.length; j++ ){
              let wardId = obj.wards[j];
              let wardName = this.getwardName(wardId);
              if (this.UserDataSr[i]['wardss']){
              this.UserDataSr[i]['wardss'].push({
                'wardId':wardId,
                'wardName':wardName
              });
            }else{
              this.UserDataSr[i]['wardss'] = [{}];
              // this.spinnerService.hide();
            }
            // this.spinnerService.hide();
            }
            // this.spinnerService.hide();
          }

          for(var i = 0;i<this.UserDataSr.length;i++){
            let obj = this.UserDataSr[i];
            this.UserDataSr[i]['zoness'] = [];
            for(let j=0; j<obj.zones.length; j++ ){
              let zoneId = obj.zones[j];
              let zoneName = this.getzoneName(zoneId);
              if (this.UserDataSr[i]['zoness']){
              this.UserDataSr[i]['zoness'].push({
                'zoneId':zoneId,
                'zoneName':zoneName
              });
            }else{
              this.UserDataSr[i]['zoness'] = [{}];
              // this.spinnerService.hide();
            }
            // this.spinnerService.hide();
            }
            // this.spinnerService.hide();
          }

          console.log(this.UserDataSr);

//             this.UserData.forEach(obj => {
// for(let i = 0; i<obj.zones.length; i++){
//               let localUrl = "zone/"+obj.zones[i];
//               //let localUrl = "brand/";
//               let url = this.server_url + localUrl;

//               this.http.get(url,this.httpOptions)
//                 .subscribe(
//                   (res: Response) => {
//                     var xyz=res;
//                     obj.zonename = xyz["name"];
//                     //this.zonename.push(obj.zonename);
//                     // this.spinnerService.hide();
//                   },
//                   //err => { alert("Error occoured"); this.spinnerService.hide(); }
//                 );
//               }
//           });

//           this.UserData.forEach(objj => {
//             for(let i = 0; i<objj.wards.length; i++){
//             let localUrl = "ward/"+objj.wards[i];
//             //let localUrl = "brand/";
//             let url = this.server_url + localUrl;

//             this.http.get(url,this.httpOptions)
//               .subscribe(
//                 (res: Response) => {
//                   var xyz=res;
//                   objj.wardname=xyz["name"];
//                   // this.spinnerService.hide();
//                 },
//                 //err => { alert("Error occoured"); this.spinnerService.hide(); }
//               );
//             }
//         });
          // this.spinnerService.hide();
        },
        err => {
          alert("Error occoured");
          // this.spinnerService.hide();
        }
      );

      
  }
  UserDataZonal;
  UserDataZonalCount
  getUserDataZonal() {
      
    this.wardName=[];
    // this.id = this.route.snapshot.paramMap.get('id');
    // this.spinnerService.show();
    let localUrl = "user/?user_type=6";
    // let localUrl = "user/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.UserDataZonal = res['results'];
          this.UserDataZonalCount = res['count'];
  
          for(var i = 0;i<this.UserDataZonal.length;i++){
            let obj = this.UserDataZonal[i];
            this.UserDataZonal[i]['wardss'] = [];
            for(let j=0; j<obj.wards.length; j++ ){
              let wardId = obj.wards[j];
              let wardName = this.getwardName(wardId);
              if (this.UserDataZonal[i]['wardss']){
              this.UserDataZonal[i]['wardss'].push({
                'wardId':wardId,
                'wardName':wardName
              });
            }else{
              this.UserDataZonal[i]['wardss'] = [{}];
              // this.spinnerService.hide();
            }
            // this.spinnerService.hide();
            }
            // this.spinnerService.hide();
          }

          for(var i = 0;i<this.UserDataZonal.length;i++){
            let obj = this.UserDataZonal[i];
            this.UserDataZonal[i]['zoness'] = [];
            for(let j=0; j<obj.zones.length; j++ ){
              let zoneId = obj.zones[j];
              let zoneName = this.getzoneName(zoneId);
              if (this.UserDataZonal[i]['zoness']){
              this.UserDataZonal[i]['zoness'].push({
                'zoneId':zoneId,
                'zoneName':zoneName
              });
            }else{
              this.UserDataZonal[i]['zoness'] = [{}];
              // this.spinnerService.hide();
            }
            // this.spinnerService.hide();
            }
            // this.spinnerService.hide();
          }

          console.log(this.UserDataZonal);

//             this.UserData.forEach(obj => {
// for(let i = 0; i<obj.zones.length; i++){
//               let localUrl = "zone/"+obj.zones[i];
//               //let localUrl = "brand/";
//               let url = this.server_url + localUrl;

//               this.http.get(url,this.httpOptions)
//                 .subscribe(
//                   (res: Response) => {
//                     var xyz=res;
//                     obj.zonename = xyz["name"];
//                     //this.zonename.push(obj.zonename);
//                     // this.spinnerService.hide();
//                   },
//                   //err => { alert("Error occoured"); this.spinnerService.hide(); }
//                 );
//               }
//           });

//           this.UserData.forEach(objj => {
//             for(let i = 0; i<objj.wards.length; i++){
//             let localUrl = "ward/"+objj.wards[i];
//             //let localUrl = "brand/";
//             let url = this.server_url + localUrl;

//             this.http.get(url,this.httpOptions)
//               .subscribe(
//                 (res: Response) => {
//                   var xyz=res;
//                   objj.wardname=xyz["name"];
//                   // this.spinnerService.hide();
//                 },
//                 //err => { alert("Error occoured"); this.spinnerService.hide(); }
//               );
//             }
//         });
          // this.spinnerService.hide();
        },
        err => {
          alert("Error occoured");
          // this.spinnerService.hide();
        }
      );

      
  }

  UserDataJr;
  UserDataJrCount;
  getUserDataJr(){
      
    this.wardName=[];
    // this.id = this.route.snapshot.paramMap.get('id');
    // this.spinnerService.show();
    let localUrl = "user/?user_type=3";
    // let localUrl = "user/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.UserDataJr = res['results'];
          this.UserDataJrCount = res['count'];
  
          for(var i = 0;i<this.UserDataJr.length;i++){
            let obj = this.UserDataJr[i];
            this.UserDataJr[i]['wardss'] = [];
            for(let j=0; j<obj.wards.length; j++ ){
              let wardId = obj.wards[j];
              let wardName = this.getwardName(wardId);
              if (this.UserDataJr[i]['wardss']){
              this.UserDataJr[i]['wardss'].push({
                'wardId':wardId,
                'wardName':wardName
              });
            }else{
              this.UserDataJr[i]['wardss'] = [{}];
              // this.spinnerService.hide();
            }
            // this.spinnerService.hide();
            }
            // this.spinnerService.hide();
          }

          for(var i = 0;i<this.UserDataJr.length;i++){
            let obj = this.UserDataJr[i];
            this.UserDataJr[i]['zoness'] = [];
            for(let j=0; j<obj.zones.length; j++ ){
              let zoneId = obj.zones[j];
              let zoneName = this.getzoneName(zoneId);
              if (this.UserDataJr[i]['zoness']){
              this.UserDataJr[i]['zoness'].push({
                'zoneId':zoneId,
                'zoneName':zoneName
              });
            }else{
              this.UserDataJr[i]['zoness'] = [{}];
              // this.spinnerService.hide();
            }
            // this.spinnerService.hide();
            }
            // this.spinnerService.hide();
          }
          console.log(this.UserDataJr);

//             this.UserData.forEach(obj => {
// for(let i = 0; i<obj.zones.length; i++){
//               let localUrl = "zone/"+obj.zones[i];
//               //let localUrl = "brand/";
//               let url = this.server_url + localUrl;

//               this.http.get(url,this.httpOptions)
//                 .subscribe(
//                   (res: Response) => {
//                     var xyz=res;
//                     obj.zonename = xyz["name"];
//                     //this.zonename.push(obj.zonename);
//                     // this.spinnerService.hide();
//                   },
//                   //err => { alert("Error occoured"); this.spinnerService.hide(); }
//                 );
//               }
//           });

//           this.UserData.forEach(objj => {
//             for(let i = 0; i<objj.wards.length; i++){
//             let localUrl = "ward/"+objj.wards[i];
//             //let localUrl = "brand/";
//             let url = this.server_url + localUrl;

//             this.http.get(url,this.httpOptions)
//               .subscribe(
//                 (res: Response) => {
//                   var xyz=res;
//                   objj.wardname=xyz["name"];
//                   // this.spinnerService.hide();
//                 },
//                 //err => { alert("Error occoured"); this.spinnerService.hide(); }
//               );
//             }
//         });
          // this.spinnerService.hide();
        },
        err => {
          alert("Error occoured");
          // this.spinnerService.hide();
        }
      );
  }


  UserDataCitizen;
  UserDataCitizenCount;
  getUserDataCitizen() {
      
    this.wardName=[];
    // this.id = this.route.snapshot.paramMap.get('id');
    // this.spinnerService.show();
    // let localUrl = "user/?user_type=2&user_type=3";
    let localUrl = "user/?user_type=4";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.UserDataCitizen = res['results'];
          this.UserDataCitizenCount = res['count'];
  
          for(var i = 0;i<this.UserDataCitizen.length;i++){
            let obj = this.UserDataCitizen[i];
            this.UserDataCitizen[i]['wardss'] = [];
            for(let j=0; j<obj.wards.length; j++ ){
              let wardId = obj.wards[j];
              let wardName = this.getwardName(wardId);
              if (this.UserDataCitizen[i]['wardss']){
              this.UserDataCitizen[i]['wardss'].push({
                'wardId':wardId,
                'wardName':wardName
              });
            }else{
              this.UserDataCitizen[i]['wardss'] = [{}];
              // this.spinnerService.hide();
            }
            // this.spinnerService.hide();
            }
            // this.spinnerService.hide();
          }

          for(var i = 0;i<this.UserDataCitizen.length;i++){
            let obj = this.UserDataCitizen[i];
            this.UserDataCitizen[i]['zoness'] = [];
            for(let j=0; j<obj.zones.length; j++ ){
              let zoneId = obj.zones[j];
              let zoneName = this.getzoneName(zoneId);
              if (this.UserDataCitizen[i]['zoness']){
              this.UserDataCitizen[i]['zoness'].push({
                'zoneId':zoneId,
                'zoneName':zoneName
              });
            }else{
              this.UserDataCitizen[i]['zoness'] = [{}];
              // this.spinnerService.hide();
            }
            // this.spinnerService.hide();
            }
            // this.spinnerService.hide();
          }
          console.log(this.UserDataCitizen);

//             this.UserData.forEach(obj => {
// for(let i = 0; i<obj.zones.length; i++){
//               let localUrl = "zone/"+obj.zones[i];
//               //let localUrl = "brand/";
//               let url = this.server_url + localUrl;

//               this.http.get(url,this.httpOptions)
//                 .subscribe(
//                   (res: Response) => {
//                     var xyz=res;
//                     obj.zonename = xyz["name"];
//                     //this.zonename.push(obj.zonename);
//                     // this.spinnerService.hide();
//                   },
//                   //err => { alert("Error occoured"); this.spinnerService.hide(); }
//                 );
//               }
//           });

//           this.UserData.forEach(objj => {
//             for(let i = 0; i<objj.wards.length; i++){
//             let localUrl = "ward/"+objj.wards[i];
//             //let localUrl = "brand/";
//             let url = this.server_url + localUrl;

//             this.http.get(url,this.httpOptions)
//               .subscribe(
//                 (res: Response) => {
//                   var xyz=res;
//                   objj.wardname=xyz["name"];
//                   // this.spinnerService.hide();
//                 },
//                 //err => { alert("Error occoured"); this.spinnerService.hide(); }
//               );
//             }
//         });
          // this.spinnerService.hide();
        },
        err => {
          alert("Error occoured");
          // this.spinnerService.hide();
        }
      );
  }

  wardName = [];
  wardname:any;
  getwardName(id) {
      
        for(let i=0; i<this.AllWardData.length; i++){
          if(this.AllWardData[i].id === id){
            this.wardname = this.AllWardData[i].name;
            return this.wardname;
            //console.log("catname: " + this.catname[i]);
          }
        }
      }

      zoneName = [];
      zonenamee:any;
      getzoneName(id) {
          
            for(let i=0; i<this.AllZoneData.length; i++){
              if(this.AllZoneData[i].id === id){
                this.zonenamee = this.AllZoneData[i].name;
                return this.zonenamee;
                //console.log("catname: " + this.catname[i]);
              }
            }
          }

  deleteUser(UserData) {
    if (confirm("You want to Delete user?")) {
        ;
      let localUrl = "user/" + UserData.id + "/";
      let url = this.server_url + localUrl;

      this.http.delete(url, this.httpOptions).subscribe(
        (res) => {
          alert("You Deleted user");
          //this.viewStore="";
          this.getUserDataTPL();
          this.getUserDataJr();
          this.getUserDataCitizen();
        }, err => { alert("You made no change"); }
      );
    } else { alert("error occured"); }

  }

  updateUser(beforeUpdateUserObj) {
      

    for (let i = 0; i < this.selectedItemsZone.length; i++) {
      this.ZonesArray.push(this.selectedItemsZone[i].id);
 }

 for (let i = 0; i < this.selectedItemsWard.length; i++) {
   this.WardsArray.push(this.selectedItemsWard[i].id);
}
    this.beforeUpdateUserObj = {
      "user_type":beforeUpdateUserObj.user_type,
      "first_name": beforeUpdateUserObj.first_name,
      "last_name": beforeUpdateUserObj.last_name,
      "address": beforeUpdateUserObj.address,
      "email": beforeUpdateUserObj.email,
      "wards": this.WardsArray,
      "zones": this.ZonesArray,
      "username": beforeUpdateUserObj.username,
      "mobile_no": beforeUpdateUserObj.mobile_no
    };
    let localUrl = "user/" + beforeUpdateUserObj.id + "/";
    let url = this.server_url + localUrl;

    this.http.patch(url, JSON.stringify(this.beforeUpdateUserObj), this.httpOptions).subscribe(
      (res) => {
        if (res) {
          this.beforeUpdateUserObj = {
            "id": res['id'],
            "first_name": res['first_name'],
            "last_name": res['last_name'],
            "address": res['address'],
            "email": res['email'],
            "wards":res['wards'],
            "zones":res['zones'],
            "password": res['password'],
            "username": res['username'],
            "mobile_no": res['mobile_no'],
            "user_type":res['user_type'],
          };
          alert("Successfully Updated");
          this.viewStore = '';
          this.getUserDataTPL();
          this.getUserDataJr();
          this.getUserDataCitizen();
        } else { alert("error Occured") }
      },
    );


  } // Update Auditor functionality close
  beforeUpdateUserObj;
  // Editing Auditor function 
  editUser(UserData) {

    if (confirm("You want to edit User ?")) {
        ;
      this.selectedItemsWard=[];
      this.selectedItemsZone=[];
      this.WardsArray=[];
      this.ZonesArray=[];
      
      this.viewStore = 'editStore';
      let localUrl = "user/" + UserData.id + "/";
      let url = this.server_url + localUrl;
      const checkcatObject = { id: UserData.id }

      this.http.patch(url, JSON.stringify(checkcatObject), this.httpOptions).subscribe(
        (res) => {
          if (res)
            this.beforeUpdateUserObj = {
            "id": res['id'],
            "first_name": res['first_name'],
            "last_name": res['last_name'],
            "address": res['address'],
            "email": res['email'],
            "wards":res['wards'],
            "zones":res['zones'],
            "username": res['username'],
            "mobile_no": res['mobile_no'],
            "user_type":res['user_type'],
            "password": res['password']
            };

        }, err => { alert("You made no change"); }
      );
    } else { }

  }

  WardData = [];
  WardDataCount;
  getWardData(userpageNo) {
    //   
    // this.id = this.route.snapshot.paramMap.get('id');
    // this.spinnerService.show();
    let localUrl = "ward/?page=" + userpageNo;
    //let localUrl = "brand/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          // this.WardData = res['results'];
          this.WardDataCount = res['count'];

          this.WardData = this.WardData.concat(res['results']);
          //this.allcategoryDataDropdown.push(this.allcategoryData);
          if (this.WardData.length < this.WardDataCount) {
            this.getWardData(userpageNo + 1);
          }
          //   this.WardData.forEach(obj => {

          //     let localUrl = "zone/"+obj.zone+ "/";
          //     //let localUrl = "brand/";
          //     let url = this.server_url + localUrl;

          //     this.http.get(url,this.httpOptions)
          //       .subscribe(
          //         (res: Response) => {
          //           var xyz=res;
          //           obj.Zonename=xyz["name"];
          //           // this.spinnerService.hide();
          //         },
          //         //err => { alert("Error occoured"); this.spinnerService.hide(); }
          //       );

          // });
          // this.spinnerService.hide();
        },
        err => {
          alert("Error occoured");
          // this.spinnerService.hide();
        }
      );
  }
  AllWardDataCount;
  AllWardData=[];
  getAllwardData(userpageNo) {
      
    // this.spinnerService.show();
    let localUrl = "ward/?page="+userpageNo;
    let url = this.server_url + localUrl;
    
    this.http.get(url,this.httpOptions)
      .subscribe(
        (res: Response) => {
          //  
          //  this.categoryData.push(res['results'])
          // this.categoryData = this.categoryData.concat(res['results']);
          this.AllWardDataCount = res['count'];
          var xyz = res;
          this.AllWardData = this.AllWardData.concat(res['results']);
          //this.allcategoryDataDropdown.push(this.allcategoryData);
          if(this.AllWardData.length < this.AllWardDataCount) {
            this.getAllwardData(userpageNo +1);
          }
          
          //this.allcategoryDataDropdown.push(this.allcategoryData);
          for (let i = 0; i < this.AllWardData.length; i++) {
            this.AllWardData[i].id = this.AllWardData[i].id;
            this.AllWardData[i].itemName = this.AllWardData[i].name;
          }

         

         
          // this.spinnerService.hide();
        },
        err => { alert("Error occoured"); 
        // this.spinnerService.hide(); 
      }
      );
  }
  
  getUserDataFilterJr(id) {
      
    // this.id = this.route.snapshot.paramMap.get('id');
    // this.spinnerService.show();
    let localUrl = "user/?user_type=3";
    //let localUrl = "brand/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          if(id != "undefined"){
          this.UserDataJr = res['results'].filter(user => user.wards == id);
          this.UserDataJrCount = res['count'];}
          else{
            this.UserDataJr = res['results'];
          this.UserDataJrCount = res['count'];
          }



            
          for(var i = 0;i<this.UserDataJr.length;i++){
            let obj = this.UserDataJr[i];
            this.UserDataJr[i]['wardss'] = [];
            for(let j=0; j<obj.wards.length; j++ ){
              let wardId = obj.wards[j];
              let wardName = this.getwardName(wardId);
              if (this.UserDataJr[i]['wardss']){
              this.UserDataJr[i]['wardss'].push({
                'wardId':wardId,
                'wardName':wardName
              });
            }else{
              this.UserDataJr[i]['wardss'] = [{}];
              // this.spinnerService.hide();
            }
            // this.spinnerService.hide();
            }
            // this.spinnerService.hide();
          }
          console.log(this.UserDataJr);

        //     this.UserData.forEach(obj => {

        //       let localUrl = "zone/"+obj.zones;
        //       //let localUrl = "brand/";
        //       let url = this.server_url + localUrl;

        //       this.http.get(url,this.httpOptions)
        //         .subscribe(
        //           (res: Response) => {
        //             var xyz=res;
        //             obj.zonename=xyz["name"];
        //             // this.spinnerService.hide();
        //           },
        //           //err => { alert("Error occoured"); this.spinnerService.hide(); }
        //         );

        //   });

        //   this.UserData.forEach(objj => {

        //     let localUrl = "ward/"+objj.wards;
        //     //let localUrl = "brand/";
        //     let url = this.server_url + localUrl;

        //     this.http.get(url,this.httpOptions)
        //       .subscribe(
        //         (res: Response) => {
        //           var xyz=res;
        //           objj.wardname=xyz["name"];
        //           // this.spinnerService.hide();
        //         },
        //         //err => { alert("Error occoured"); this.spinnerService.hide(); }
        //       );

        // });
          // this.spinnerService.hide();
        },
        err => {
          alert("Error occoured");
          // this.spinnerService.hide();
        }
      );
  }

  getUserDataFilterSr(id) {
      
    // this.id = this.route.snapshot.paramMap.get('id');
    // this.spinnerService.show();
    let localUrl = "user/?user_type=5";
    //let localUrl = "brand/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          if(id != "undefined"){
          this.UserDataSr = res['results'].filter(user => user.wards == id);
          this.UserDataSrCount = res['count'];}
          else{
            this.UserDataSr = res['results'];
          this.UserDataSrCount = res['count'];
          }



            
          for(var i = 0;i<this.UserDataSr.length;i++){
            let obj = this.UserDataSr[i];
            this.UserDataSr[i]['wardss'] = [];
            for(let j=0; j<obj.wards.length; j++ ){
              let wardId = obj.wards[j];
              let wardName = this.getwardName(wardId);
              if (this.UserDataSr[i]['wardss']){
              this.UserDataSr[i]['wardss'].push({
                'wardId':wardId,
                'wardName':wardName
              });
            }else{
              this.UserDataSr[i]['wardss'] = [{}];
              // this.spinnerService.hide();
            }
            // this.spinnerService.hide();
            }
            // this.spinnerService.hide();
          }
          console.log(this.UserDataSr);

        //     this.UserData.forEach(obj => {

        //       let localUrl = "zone/"+obj.zones;
        //       //let localUrl = "brand/";
        //       let url = this.server_url + localUrl;

        //       this.http.get(url,this.httpOptions)
        //         .subscribe(
        //           (res: Response) => {
        //             var xyz=res;
        //             obj.zonename=xyz["name"];
        //             // this.spinnerService.hide();
        //           },
        //           //err => { alert("Error occoured"); this.spinnerService.hide(); }
        //         );

        //   });

        //   this.UserData.forEach(objj => {

        //     let localUrl = "ward/"+objj.wards;
        //     //let localUrl = "brand/";
        //     let url = this.server_url + localUrl;

        //     this.http.get(url,this.httpOptions)
        //       .subscribe(
        //         (res: Response) => {
        //           var xyz=res;
        //           objj.wardname=xyz["name"];
        //           // this.spinnerService.hide();
        //         },
        //         //err => { alert("Error occoured"); this.spinnerService.hide(); }
        //       );

        // });
          // this.spinnerService.hide();
        },
        err => {
          alert("Error occoured");
          // this.spinnerService.hide();
        }
      );
  }


  getUserDataFilterTPL(id) {
      
    // this.id = this.route.snapshot.paramMap.get('id');
    // this.spinnerService.show();
    let localUrl = "user/?user_type=2";
    //let localUrl = "brand/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          if(id != "undefined"){
          this.UserDataTPL = res['results'].filter(user => user.wards == id);
          this.UserDataTPLCount = res['count'];}
          else{
            this.UserDataTPL = res['results'];
          this.UserDataTPLCount = res['count'];
          }



            
          for(var i = 0;i<this.UserDataTPL.length;i++){
            let obj = this.UserDataTPL[i];
            this.UserDataTPL[i]['wardss'] = [];
            for(let j=0; j<obj.wards.length; j++ ){
              let wardId = obj.wards[j];
              let wardName = this.getwardName(wardId);
              if (this.UserDataTPL[i]['wardss']){
              this.UserDataTPL[i]['wardss'].push({
                'wardId':wardId,
                'wardName':wardName
              });
            }else{
              this.UserDataTPL[i]['wardss'] = [{}];
              // this.spinnerService.hide();
            }
            // this.spinnerService.hide();
            }
            // this.spinnerService.hide();
          }
          console.log(this.UserDataTPL);

        //     this.UserData.forEach(obj => {

        //       let localUrl = "zone/"+obj.zones;
        //       //let localUrl = "brand/";
        //       let url = this.server_url + localUrl;

        //       this.http.get(url,this.httpOptions)
        //         .subscribe(
        //           (res: Response) => {
        //             var xyz=res;
        //             obj.zonename=xyz["name"];
        //             // this.spinnerService.hide();
        //           },
        //           //err => { alert("Error occoured"); this.spinnerService.hide(); }
        //         );

        //   });

        //   this.UserData.forEach(objj => {

        //     let localUrl = "ward/"+objj.wards;
        //     //let localUrl = "brand/";
        //     let url = this.server_url + localUrl;

        //     this.http.get(url,this.httpOptions)
        //       .subscribe(
        //         (res: Response) => {
        //           var xyz=res;
        //           objj.wardname=xyz["name"];
        //           // this.spinnerService.hide();
        //         },
        //         //err => { alert("Error occoured"); this.spinnerService.hide(); }
        //       );

        // });
          // this.spinnerService.hide();
        },
        err => {
          alert("Error occoured");
          // this.spinnerService.hide();
        }
      );
  }

  uploadForm = new FormGroup({
    //file1: new FormControl()
  });
  filedata: File;

  onFileChange(e) {
      
    this.filedata = e.target.files[0];
    //this.filedata.getAsBinary();
    // let formData: FormData = new FormData();
    // let file: File = this.filedata;
    // formData.append("image", file);
    // //formData.append("Key","targetLineFile");
    // //let headers = new Headers();
    // //headers.append('Content-Type', 'multipart/form-data');
    // //headers.append('Authorization', 'JWT ' + this.tokens);
    // let httpOptions= {
    //   headers: new HttpHeaders({
    //     'Content-Type': 'multipart/form-data',
    //     'Authorization': 'JWT ' + this.tokens
    //   })
    // };
    // let localUrl = "user/";
    // let url = this.server_url + localUrl;
    // this.http.post(url, formData,httpOptions)
    //   .subscribe((res) => {
    //     console.log("res" + res);
    //     if (res) {
    //       alert("Your Image uploaded successfully..");
    //       this.spinnerService.hide();
    //       this.viewUser = '';
    //       this.getUserData();
    //     } else {
    //       alert("File NOT Uploaded");
    //       this.spinnerService.hide();
    //     }
    //   });
  }

  updateImage(id,image: any) {

    let formData: FormData = new FormData();
    let file: File = this.filedata;
    formData.append("image", file);
    let httpOptions= {
      headers: new HttpHeaders({
        //'content-type':'multipart/form-data',
        'Authorization': 'JWT ' + this.tokens
      })
    };

    formData.append('image',file.name);

    let localUrl = "user/" +id + "/";
    let url = this.server_url + localUrl;
    
      ;
      this.http.patch(url, formData, httpOptions).subscribe(
        (res) => {
          if (res){
            alert("You have uploaded Image  successfully..");
          // this.viewUser = '';
          this.getUserDataTPL();}
          else { alert("error Occured");
          //  this.viewUser = ''; 
           this.getUserDataTPL(); }
        });
    
  }

  UpdatePassword(id,password) {
  
    this.beforeUpdateUserObj = {
      "password":password
    };

   let localUrl = "user/" +id + "/";
    let url = this.server_url + localUrl;
    
      ;
      this.http.patch(url, this.beforeUpdateUserObj, this.httpOptions).subscribe(
        (res) => {
          if (res){
            alert("You have Changed Password successfully..");
          // this.viewUser = '';
          this.getUserDataTPL();}
          else { alert("error Occured");
          //  this.viewUser = ''; 
           this.getUserDataTPL(); }
        });
    
  }

  userpageNo: number = 1;
  pageChange(pageNo: any) {
    // this.zoneName=[];
      
    this.userpageNo = pageNo;
    console.log("pageNo: " + this.userpageNo);

    // this.spinnerService.show();
    let localUrl = "user/?page="+this.userpageNo+"&user_type=5";
    // let localUrl = "user/?page="+this.userpageNo;
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
    .subscribe(
      (res: Response) => {
        this.UserDataSr = res['results'];
        this.UserDataSrCount = res['count'];
  
        for(var i = 0;i<this.UserDataSr.length;i++){
          let obj = this.UserDataSr[i];
          this.UserDataSr[i]['wardss'] = [];
          for(let j=0; j<obj.wards.length; j++ ){
            let wardId = obj.wards[j];
            let wardName = this.getwardName(wardId);
            if (this.UserDataSr[i]['wardss']){
            this.UserDataSr[i]['wardss'].push({
              'wardId':wardId,
              'wardName':wardName
            });
          }else{
            this.UserDataSr[i]['wardss'] = [{}];
            // this.spinnerService.hide();
          }
          // this.spinnerService.hide();
          }
          // this.spinnerService.hide();
        }

        for(var i = 0;i<this.UserDataSr.length;i++){
          let obj = this.UserDataSr[i];
          this.UserDataSr[i]['zoness'] = [];
          for(let j=0; j<obj.zones.length; j++ ){
            let zoneId = obj.zones[j];
            let zoneName = this.getzoneName(zoneId);
            if (this.UserDataSr[i]['zoness']){
            this.UserDataSr[i]['zoness'].push({
              'zoneId':zoneId,
              'zoneName':zoneName
            });
          }else{
            this.UserDataSr[i]['zoness'] = [{}];
            // this.spinnerService.hide();
          }
          // this.spinnerService.hide();
          }
          // this.spinnerService.hide();
        }
        console.log(this.UserDataSr);

//             this.UserData.forEach(obj => {
// for(let i = 0; i<obj.zones.length; i++){
//               let localUrl = "zone/"+obj.zones[i];
//               //let localUrl = "brand/";
//               let url = this.server_url + localUrl;

//               this.http.get(url,this.httpOptions)
//                 .subscribe(
//                   (res: Response) => {
//                     var xyz=res;
//                     obj.zonename = xyz["name"];
//                     //this.zonename.push(obj.zonename);
//                     // this.spinnerService.hide();
//                   },
//                   //err => { alert("Error occoured"); this.spinnerService.hide(); }
//                 );
//               }
//           });

//           this.UserData.forEach(objj => {
//             for(let i = 0; i<objj.wards.length; i++){
//             let localUrl = "ward/"+objj.wards[i];
//             //let localUrl = "brand/";
//             let url = this.server_url + localUrl;

//             this.http.get(url,this.httpOptions)
//               .subscribe(
//                 (res: Response) => {
//                   var xyz=res;
//                   objj.wardname=xyz["name"];
//                   // this.spinnerService.hide();
//                 },
//                 //err => { alert("Error occoured"); this.spinnerService.hide(); }
//               );
//             }
//         });
        // this.spinnerService.hide();
      },
      err => {
        alert("Error occoured");
        // this.spinnerService.hide();
      }
    );
  }

  userpageNo5: number = 1;
  pageChange5(pageNo: any) {
    // this.zoneName=[];
      
    this.userpageNo5 = pageNo;
    console.log("pageNo: " + this.userpageNo5);

    // this.spinnerService.show();
    let localUrl = "user/?page="+this.userpageNo5+"&user_type=6";
    // let localUrl = "user/?page="+this.userpageNo;
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
    .subscribe(
      (res: Response) => {
        this.UserDataZonal = res['results'];
        this.UserDataZonalCount = res['count'];
  
        for(var i = 0;i<this.UserDataZonal.length;i++){
          let obj = this.UserDataZonal[i];
          this.UserDataZonal[i]['wardss'] = [];
          for(let j=0; j<obj.wards.length; j++ ){
            let wardId = obj.wards[j];
            let wardName = this.getwardName(wardId);
            if (this.UserDataZonal[i]['wardss']){
            this.UserDataZonal[i]['wardss'].push({
              'wardId':wardId,
              'wardName':wardName
            });
          }else{
            this.UserDataZonal[i]['wardss'] = [{}];
            // this.spinnerService.hide();
          }
          // this.spinnerService.hide();
          }
          // this.spinnerService.hide();
        }

        for(var i = 0;i<this.UserDataZonal.length;i++){
          let obj = this.UserDataZonal[i];
          this.UserDataZonal[i]['zoness'] = [];
          for(let j=0; j<obj.zones.length; j++ ){
            let zoneId = obj.zones[j];
            let zoneName = this.getzoneName(zoneId);
            if (this.UserDataZonal[i]['zoness']){
            this.UserDataZonal[i]['zoness'].push({
              'zoneId':zoneId,
              'zoneName':zoneName
            });
          }else{
            this.UserDataZonal[i]['zoness'] = [{}];
            // this.spinnerService.hide();
          }
          // this.spinnerService.hide();
          }
          // this.spinnerService.hide();
        }
        console.log(this.UserDataZonal);

//             this.UserData.forEach(obj => {
// for(let i = 0; i<obj.zones.length; i++){
//               let localUrl = "zone/"+obj.zones[i];
//               //let localUrl = "brand/";
//               let url = this.server_url + localUrl;

//               this.http.get(url,this.httpOptions)
//                 .subscribe(
//                   (res: Response) => {
//                     var xyz=res;
//                     obj.zonename = xyz["name"];
//                     //this.zonename.push(obj.zonename);
//                     // this.spinnerService.hide();
//                   },
//                   //err => { alert("Error occoured"); this.spinnerService.hide(); }
//                 );
//               }
//           });

//           this.UserData.forEach(objj => {
//             for(let i = 0; i<objj.wards.length; i++){
//             let localUrl = "ward/"+objj.wards[i];
//             //let localUrl = "brand/";
//             let url = this.server_url + localUrl;

//             this.http.get(url,this.httpOptions)
//               .subscribe(
//                 (res: Response) => {
//                   var xyz=res;
//                   objj.wardname=xyz["name"];
//                   // this.spinnerService.hide();
//                 },
//                 //err => { alert("Error occoured"); this.spinnerService.hide(); }
//               );
//             }
//         });
        // this.spinnerService.hide();
      },
      err => {
        alert("Error occoured");
        // this.spinnerService.hide();
      }
    );
  }

  userpageNo3: number = 1;
  pageChange3(pageNo: any) {
    // this.zoneName=[];
      
    this.userpageNo3 = pageNo;
    console.log("pageNo: " + this.userpageNo3);

    // this.spinnerService.show();
    let localUrl = "user/?page="+this.userpageNo3+"&user_type=3";
    // let localUrl = "user/?page="+this.userpageNo;
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
    .subscribe(
      (res: Response) => {
        this.UserDataJr = res['results'];
        this.UserDataJrCount = res['count'];
  
        for(var i = 0;i<this.UserDataJr.length;i++){
          let obj = this.UserDataJr[i];
          this.UserDataJr[i]['wardss'] = [];
          for(let j=0; j<obj.wards.length; j++ ){
            let wardId = obj.wards[j];
            let wardName = this.getwardName(wardId);
            if (this.UserDataJr[i]['wardss']){
            this.UserDataJr[i]['wardss'].push({
              'wardId':wardId,
              'wardName':wardName
            });
          }else{
            this.UserDataJr[i]['wardss'] = [{}];
            // this.spinnerService.hide();
          }
          // this.spinnerService.hide();
          }
          // this.spinnerService.hide();
        }

        for(var i = 0;i<this.UserDataJr.length;i++){
          let obj = this.UserDataJr[i];
          this.UserDataJr[i]['zoness'] = [];
          for(let j=0; j<obj.zones.length; j++ ){
            let zoneId = obj.zones[j];
            let zoneName = this.getzoneName(zoneId);
            if (this.UserDataJr[i]['zoness']){
            this.UserDataJr[i]['zoness'].push({
              'zoneId':zoneId,
              'zoneName':zoneName
            });
          }else{
            this.UserDataJr[i]['zoness'] = [{}];
            // this.spinnerService.hide();
          }
          // this.spinnerService.hide();
          }
          // this.spinnerService.hide();
        }
        console.log(this.UserDataJr);

//             this.UserData.forEach(obj => {
// for(let i = 0; i<obj.zones.length; i++){
//               let localUrl = "zone/"+obj.zones[i];
//               //let localUrl = "brand/";
//               let url = this.server_url + localUrl;

//               this.http.get(url,this.httpOptions)
//                 .subscribe(
//                   (res: Response) => {
//                     var xyz=res;
//                     obj.zonename = xyz["name"];
//                     //this.zonename.push(obj.zonename);
//                     // this.spinnerService.hide();
//                   },
//                   //err => { alert("Error occoured"); this.spinnerService.hide(); }
//                 );
//               }
//           });

//           this.UserData.forEach(objj => {
//             for(let i = 0; i<objj.wards.length; i++){
//             let localUrl = "ward/"+objj.wards[i];
//             //let localUrl = "brand/";
//             let url = this.server_url + localUrl;

//             this.http.get(url,this.httpOptions)
//               .subscribe(
//                 (res: Response) => {
//                   var xyz=res;
//                   objj.wardname=xyz["name"];
//                   // this.spinnerService.hide();
//                 },
//                 //err => { alert("Error occoured"); this.spinnerService.hide(); }
//               );
//             }
//         });
        // this.spinnerService.hide();
      },
      err => {
        alert("Error occoured");
        // this.spinnerService.hide();
      }
    );
  }

  userpageNo2
  pageChange2(pageNo: any) {
    // this.zoneName=[];
      
    this.userpageNo2 = pageNo;
    console.log("pageNo: " + this.userpageNo2);

    // this.spinnerService.show();
    // let localUrl = "user/?page="+this.userpageNo+"&user_type=2&user_type=3";
    let localUrl = "user/?page="+this.userpageNo2+"&user_type=4";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
    .subscribe(
      (res: Response) => {
        this.UserDataCitizen = res['results'];
        this.UserDataCitizenCount = res['count'];
  
        for(var i = 0;i<this.UserDataCitizen.length;i++){
          let obj = this.UserDataCitizen[i];
          this.UserDataCitizen[i]['wardss'] = [];
          for(let j=0; j<obj.wards.length; j++ ){
            let wardId = obj.wards[j];
            let wardName = this.getwardName(wardId);
            if (this.UserDataCitizen[i]['wardss']){
            this.UserDataCitizen[i]['wardss'].push({
              'wardId':wardId,
              'wardName':wardName
            });
          }else{
            this.UserDataCitizen[i]['wardss'] = [{}];
            // this.spinnerService.hide();
          }
          // this.spinnerService.hide();
          }
          // this.spinnerService.hide();
        }

        for(var i = 0;i<this.UserDataCitizen.length;i++){
          let obj = this.UserDataCitizen[i];
          this.UserDataCitizen[i]['zoness'] = [];
          for(let j=0; j<obj.zones.length; j++ ){
            let zoneId = obj.zones[j];
            let zoneName = this.getzoneName(zoneId);
            if (this.UserDataCitizen[i]['zoness']){
            this.UserDataCitizen[i]['zoness'].push({
              'zoneId':zoneId,
              'zoneName':zoneName
            });
          }else{
            this.UserDataCitizen[i]['zoness'] = [{}];
            // this.spinnerService.hide();
          }
          // this.spinnerService.hide();
          }
          // this.spinnerService.hide();
        }
        console.log(this.UserDataCitizen);

//             this.UserData.forEach(obj => {
// for(let i = 0; i<obj.zones.length; i++){
//               let localUrl = "zone/"+obj.zones[i];
//               //let localUrl = "brand/";
//               let url = this.server_url + localUrl;

//               this.http.get(url,this.httpOptions)
//                 .subscribe(
//                   (res: Response) => {
//                     var xyz=res;
//                     obj.zonename = xyz["name"];
//                     //this.zonename.push(obj.zonename);
//                     // this.spinnerService.hide();
//                   },
//                   //err => { alert("Error occoured"); this.spinnerService.hide(); }
//                 );
//               }
//           });

//           this.UserData.forEach(objj => {
//             for(let i = 0; i<objj.wards.length; i++){
//             let localUrl = "ward/"+objj.wards[i];
//             //let localUrl = "brand/";
//             let url = this.server_url + localUrl;

//             this.http.get(url,this.httpOptions)
//               .subscribe(
//                 (res: Response) => {
//                   var xyz=res;
//                   objj.wardname=xyz["name"];
//                   // this.spinnerService.hide();
//                 },
//                 //err => { alert("Error occoured"); this.spinnerService.hide(); }
//               );
//             }
//         });
        // this.spinnerService.hide();
      },
      err => {
        alert("Error occoured");
        // this.spinnerService.hide();
      }
    );
  }

  userpageNo4: number = 1;
  pageChange4(pageNo: any) {
    // this.zoneName=[];
      
    this.userpageNo4 = pageNo;
    console.log("pageNo: " + this.userpageNo4);

    // this.spinnerService.show();
    let localUrl = "user/?page="+this.userpageNo4+"&user_type=2";
    // let localUrl = "user/?page="+this.userpageNo;
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
    .subscribe(
      (res: Response) => {
        this.UserDataTPL = res['results'];
        this.UserDataTPLCount = res['count'];
  
        for(var i = 0;i<this.UserDataTPL.length;i++){
          let obj = this.UserDataTPL[i];
          this.UserDataTPL[i]['wardss'] = [];
          for(let j=0; j<obj.wards.length; j++ ){
            let wardId = obj.wards[j];
            let wardName = this.getwardName(wardId);
            if (this.UserDataTPL[i]['wardss']){
            this.UserDataTPL[i]['wardss'].push({
              'wardId':wardId,
              'wardName':wardName
            });
          }else{
            this.UserDataTPL[i]['wardss'] = [{}];
            // this.spinnerService.hide();
          }
          // this.spinnerService.hide();
          }
          // this.spinnerService.hide();
        }

        for(var i = 0;i<this.UserDataTPL.length;i++){
          let obj = this.UserDataTPL[i];
          this.UserDataTPL[i]['zoness'] = [];
          for(let j=0; j<obj.zones.length; j++ ){
            let zoneId = obj.zones[j];
            let zoneName = this.getzoneName(zoneId);
            if (this.UserDataTPL[i]['zoness']){
            this.UserDataTPL[i]['zoness'].push({
              'zoneId':zoneId,
              'zoneName':zoneName
            });
          }else{
            this.UserDataTPL[i]['zoness'] = [{}];
            // this.spinnerService.hide();
          }
          // this.spinnerService.hide();
          }
          // this.spinnerService.hide();
        }
        console.log(this.UserDataTPL);

//             this.UserData.forEach(obj => {
// for(let i = 0; i<obj.zones.length; i++){
//               let localUrl = "zone/"+obj.zones[i];
//               //let localUrl = "brand/";
//               let url = this.server_url + localUrl;

//               this.http.get(url,this.httpOptions)
//                 .subscribe(
//                   (res: Response) => {
//                     var xyz=res;
//                     obj.zonename = xyz["name"];
//                     //this.zonename.push(obj.zonename);
//                     // this.spinnerService.hide();
//                   },
//                   //err => { alert("Error occoured"); this.spinnerService.hide(); }
//                 );
//               }
//           });

//           this.UserData.forEach(objj => {
//             for(let i = 0; i<objj.wards.length; i++){
//             let localUrl = "ward/"+objj.wards[i];
//             //let localUrl = "brand/";
//             let url = this.server_url + localUrl;

//             this.http.get(url,this.httpOptions)
//               .subscribe(
//                 (res: Response) => {
//                   var xyz=res;
//                   objj.wardname=xyz["name"];
//                   // this.spinnerService.hide();
//                 },
//                 //err => { alert("Error occoured"); this.spinnerService.hide(); }
//               );
//             }
//         });
        // this.spinnerService.hide();
      },
      err => {
        alert("Error occoured");
        // this.spinnerService.hide();
      }
    );
  }

 
onItemSelect(item:any){
console.log(item);
// console.log(this.selectedItems);
}
OnItemDeSelect(item:any){
console.log(item);
// console.log(this.selectedItems);
}
onSelectAll(items: any){
console.log(items);
}
onDeSelectAll(items: any){
console.log(items);
}

keyPress(event: any) {
  const pattern = /[0-9\+\-\ ]/;

  let inputChar = String.fromCharCode(event.charCode);
  if (event.keyCode != 8 && !pattern.test(inputChar)) {
    event.preventDefault();
  }
}


dropdownSettingss={}
  ngOnInit() {
    this.getZoneData();
    this.getWardData(1);
    this.getUserDataTPL();
    this.getAllwardData(this.userpageNo);
    this.getAllzoneData(this.userpageNo);

    this.dropdownSettings = {
      singleSelection: false, 
      text:"Select zones",
      selectAllText:'Select All',
      unSelectAllText:'UnSelect All',
      enableSearchFilter: false,
      classes:"myclass custom-class"
    }; 
    
    this.dropdownSettingss = {
      singleSelection: false, 
      text:"Select Area/Sector",
      selectAllText:'Select All',
      unSelectAllText:'UnSelect All',
      enableSearchFilter: false,
      classes:"myclass custom-class"
    }; 
  
  }


}
