import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EngineerComponent } from './engineer.component';
const routes: Routes = [{
  path: '',
  component: EngineerComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EngineerRoutingModule { }
