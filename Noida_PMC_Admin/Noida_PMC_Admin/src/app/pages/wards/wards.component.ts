import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Location } from '@angular/common';
import { Http, Response } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';


import { Router, ActivatedRoute, Params } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-wards',
  templateUrl: './wards.component.html',
  styleUrls: ['./wards.component.scss']
})
export class WardsComponent implements OnInit {

  public viewStore: string;

  server_url: string = environment.devServer_url;
  tokens: any = localStorage.getItem('token');
  public httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'JWT ' + this.tokens
    })
  };

  constructor(private _router: Router, private route: ActivatedRoute, private http: HttpClient, private location: Location) { }


  public addWard(e): void {
    e.preventDefault();
      ;

    let zone = e.target.elements[1].value;
    let name = e.target.elements[3].value;
   let description = e.target.elements[5].value;

    const addWardObject = {
      zone: zone,
      name: name,
      description : description,
    }
      ;
    let localUrl = "ward/";
    let url = this.server_url + localUrl;
    this.http.post(url, JSON.stringify(addWardObject), this.httpOptions)
      .subscribe(
        (res) => {
          if (res) {
            alert("You added new Ward successfully..");
            this.viewStore = 'addStore';
            //  this.getBrandData();
          } else {
            alert("Ward NOT Added");
          }
        },
        err => {
          alert("Error occoured");
          // this.viewAuditor ='';
        }
      );

  }

  userFilter: any = { name: '' };
  id: any;
  WardData;
  WardDataCount;
  getWardData() {
      
    // this.id = this.route.snapshot.paramMap.get('id');
    // this.spinnerService.show();
    let localUrl = "ward/";
    //let localUrl = "brand/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.WardData = res['results'];
          this.WardDataCount = res['count'];

            this.WardData.forEach(obj => {

              let localUrl = "zone/"+obj.zone+ "/";
              //let localUrl = "brand/";
              let url = this.server_url + localUrl;

              this.http.get(url,this.httpOptions)
                .subscribe(
                  (res: Response) => {
                    var xyz=res;
                    obj.Zonename=xyz["name"];
                    // this.spinnerService.hide();
                  },
                  //err => { alert("Error occoured"); this.spinnerService.hide(); }
                );

          });
          // this.spinnerService.hide();
        },
        err => {
          alert("Error occoured");
          // this.spinnerService.hide();
        }
      );
  }


  updateZone(beforeUpdateWardObj) {
      
    this.beforeUpdateWardObj = {
      "zone":beforeUpdateWardObj.zone,
      "name": beforeUpdateWardObj.name,
      "description": beforeUpdateWardObj.description
    };
    let localUrl = "ward/" + beforeUpdateWardObj.id + "/";
    let url = this.server_url + localUrl;

    this.http.put(url, JSON.stringify(this.beforeUpdateWardObj), this.httpOptions).subscribe(
      (res) => {
        if (res) {
          this.beforeUpdateWardObj = {
            "id": res['id'],
            "zone": res['zone'],
            "name": res['name'],
            "description": res['description'],
          };
          alert("Successfully Updated");
          this.viewStore = '';
          this.getWardData();
        } else { alert("error Occured") }
      },
    );


  } // Update Auditor functionality close
  beforeUpdateWardObj;
  // Editing Auditor function 
  editWard(WardData) {

    if (confirm("You want to edit ward ?")) {
        ;
      this.viewStore = 'editStore';
      let localUrl = "ward/" + WardData.id + "/";
      let url = this.server_url + localUrl;
      const checkcatObject = { id: WardData.id }

      this.http.patch(url, JSON.stringify(checkcatObject), this.httpOptions).subscribe(
        (res) => {
          if (res)
            this.beforeUpdateWardObj = {
              "id": res['id'],
              "zone": res['zone'],
              "name": res['name'],
              "description": res['description'],
              

            };

        }, err => { alert("You made no change"); }
      );
    } else { }

  }

  deleteWard(WardData) {
    if (confirm("You want to Delete Ward?")) {
        ;
      let localUrl = "ward/" + WardData.id + "/";
      let url = this.server_url + localUrl;

      this.http.delete(url, this.httpOptions).subscribe(
        (res) => {
          alert("You Deleted ward");
          //this.viewStore="";
          this.getWardData();
        }, err => { alert("You made no change"); }
      );
    } else { alert("error occured"); }

  }

  ZoneData;
  ZoneDataCount;
  getZoneData() {
      
    // this.id = this.route.snapshot.paramMap.get('id');
    // this.spinnerService.show();
    let localUrl = "zone/";
    //let localUrl = "brand/";
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.ZoneData = res['results'];
          this.ZoneDataCount = res['count'];

          //   this.BrandData.forEach(obj => {

          //     let localUrl = "category/"+obj.category;
          //     //let localUrl = "brand/";
          //     let url = this.server_url + localUrl;

          //     this.http.get(url,this.httpOptions)
          //       .subscribe(
          //         (res: Response) => {
          //           var xyz=res;
          //           obj.catname=xyz["name"];
          //           // this.spinnerService.hide();
          //         },
          //         //err => { alert("Error occoured"); this.spinnerService.hide(); }
          //       );

          // });
          // this.spinnerService.hide();
        },
        err => {
          alert("Error occoured");
          // this.spinnerService.hide();
        }
      );
  }

  wardpageNo: number = 1;
  pageChange(pageNo: any) {
    // this.zoneName=[];
      
    this.wardpageNo = pageNo;
    console.log("pageNo: " + this.wardpageNo);

    // this.spinnerService.show();
    let localUrl = "ward/?page="+this.wardpageNo;
    let url = this.server_url + localUrl;

    this.http.get(url, this.httpOptions)
      .subscribe(
        (res: Response) => {
          this.WardData = res['results'];
          this.WardDataCount = res['count'];

            this.WardData.forEach(obj => {

              let localUrl = "zone/"+obj.zone+ "/";
              //let localUrl = "brand/";
              let url = this.server_url + localUrl;

              this.http.get(url,this.httpOptions)
                .subscribe(
                  (res: Response) => {
                    var xyz=res;
                    obj.Zonename=xyz["name"];
                    // this.spinnerService.hide();
                  },
                  //err => { alert("Error occoured"); this.spinnerService.hide(); }
                );

          });
          // this.spinnerService.hide();
        },
        err => {
          alert("Error occoured");
          // this.spinnerService.hide();
        }
      );
  }

  ngOnInit() {
    this.getZoneData();
    this.getWardData();
    this.viewStore = 'addStore';

  }

}
