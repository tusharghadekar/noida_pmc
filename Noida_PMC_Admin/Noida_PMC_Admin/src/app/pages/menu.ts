export let MENU_ITEM = [
    {
        path: 'index',
        title: 'Dashboard',
        icon: 'dashboard'
    },
   
    {
        path: 'engineer',
        title: 'Engineers/Manager',
        icon: 'user'
    },

    {
        path: 'zones',
        title: 'Zones',
        icon: 'table'
    },

    {
        path: 'wards',
        title: 'Area/Sector',
        icon: 'table'
    },

    {
        path: 'issues',
        title: 'Complaints',
        icon: 'pencil'
    },

    {
        path: 'observations-list',
        title: 'Notifications',
        icon: 'bell'
    },

    // {
    //     path: 'profile',
    //     title: 'User Profile',
    //     icon: 'user'
    // },

    
   
];
