// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  mod: 'InDEV',
  devServer_url : "http://34.218.151.6:8000/api/"
  // //devServer_url : "http://118.102.250.138:3034/v1/"
};
