package com.streetlightgrievance.networkcommunication;

public interface WebserviceResponseHandler {

    void onResponseSuccess(Object o);

    void onResponseFailure();


}
