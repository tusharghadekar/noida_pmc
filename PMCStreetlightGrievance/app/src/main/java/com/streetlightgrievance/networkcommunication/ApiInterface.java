package com.streetlightgrievance.networkcommunication;

import com.streetlightgrievance.models.LoginRequest;
import com.streetlightgrievance.models.LoginResponse;
import com.streetlightgrievance.models.RequestRegisterComplaint;
import com.streetlightgrievance.models.RequestUpdateProfile;
import com.streetlightgrievance.models.ResponseDownloadImage;
import com.streetlightgrievance.models.ResponseMyRequest;
import com.streetlightgrievance.models.ResponseNotifications;
import com.streetlightgrievance.models.ResponseNotificationsResult;
import com.streetlightgrievance.models.ResponseTicketType;
import com.streetlightgrievance.models.ResponseUpdateProfile;
import com.streetlightgrievance.models.ResponseUploadImage;
import com.streetlightgrievance.models.ResponseWard;
import com.streetlightgrievance.models.UserResponse;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("tickettypegroup")
    Call<ResponseTicketType> getTicketTypes(@HeaderMap Map<String, String> headers);


    @GET("ward/?&limit=1000")
    Call<ResponseWard> getWards(@HeaderMap Map<String, String> headers);


    @POST("ticket/")
    Call<RequestRegisterComplaint> postRegisterComplaint(@HeaderMap Map<String, String> headers, @Body RequestRegisterComplaint registerComplaint);


    @POST("mobilelogin/")
    Call<LoginResponse> doLogin(@Body LoginRequest loginRequest);

    @GET("user/")
    Call<UserResponse> getUser(@HeaderMap Map<String, String> headers);

    @GET("ticket/")
    Call<ResponseMyRequest> getMyRequests(@HeaderMap Map<String, String> headers);

    @GET("notification/")
    Call<ResponseNotifications> getNotifications(@HeaderMap Map<String, String> headers);

    @Multipart
    @POST("ticketimage/")
    Call<ResponseUploadImage> uploadFile(@HeaderMap Map<String, String> headers, @Part MultipartBody.Part file, @Part("ticket") RequestBody ticket_id);


//    @PUT("ticket/{id}/")
//    Call<UserResponse> patchObservation(@HeaderMap Map<String, String> headers, @Body ResponseMyRequestResult registerObservation, @Path("id") int ticket_id);

    @PATCH("user/{id}/")
    Call<ResponseUpdateProfile> updateProfile(@HeaderMap Map<String, String> headers, @Body RequestUpdateProfile user, @Path("id") int user_id);

    @GET("ticketimage/?")
    Call<ResponseDownloadImage> downloadImages(@HeaderMap Map<String, String> headers, @Query("ticket") int ticket_id);

    @PATCH("notification/{id}/")
    Call<ResponseNotificationsResult> readNotification(@HeaderMap Map<String, String> headers, @Body ResponseNotificationsResult notificationsResult, @Path("id") int user_id);

}
