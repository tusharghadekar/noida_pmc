package com.streetlightgrievance.repository;

public class AppRepository {
    private static AppRepository appRepository;
    private static final String TAG = "AppRepository";

    private AppRepository() {
    }

    public static AppRepository getInstance() {
        if (appRepository == null) {
            appRepository = new AppRepository();
        }
        return appRepository;
    }



}
