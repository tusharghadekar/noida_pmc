package com.streetlightgrievance.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.button.MaterialButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.streetlightgrievance.MyApplication;

import mobicloud.pmc.streetlightgrievance.R;

import com.streetlightgrievance.Utility;
import com.streetlightgrievance.activities.HomeActivity;
import com.streetlightgrievance.adapters.ImageAdapter;
import com.streetlightgrievance.models.RequestRegisterComplaint;
import com.streetlightgrievance.models.ResponseTicketTypeResult;
import com.streetlightgrievance.models.ResponseUploadImage;
import com.streetlightgrievance.models.ResponseWard;
import com.streetlightgrievance.models.ResponseWardResult;
import com.streetlightgrievance.networkcommunication.ApiClient;
import com.streetlightgrievance.networkcommunication.ApiInterface;
import com.streetlightgrievance.networkcommunication.DialogUtils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.streetlightgrievance.SharedPreferences.PENDING;

/*
 * Created by ${KEDAR_LABDE} on 12-12-2017.
 */

public class RegisterComplaintFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    private Context mContext;
    private ArrayList<Uri> imagesUriList = new ArrayList<>();
    private ImageAdapter imageAdapter;
    private RecyclerView mRecyclerView;
    private ArrayList<ResponseWardResult> responseWardResults = new ArrayList<>();

    int wardId, zoneId, engineerId;

    String address, city, state, country, zip, featuredName, premises, subLocality, subAdminArea;

    private MaterialButton mbSubmit;
    private ImageButton mbUpload;
    private TextView txt_image;
    private TextInputLayout input_layout_address, input_layout_name, input_layout_house_no, input_layout_Sector, input_layout_desc;
    private Spinner spinner;
    private Location currentLocation;
    private ResponseTicketTypeResult responseTicketTypeResult;
    private int imageUploadCounter = 0;


    private ArrayList<String> strings;
    private ArrayAdapter<String> dataAdapter;

    double latitude = 0;
    double longitude = 0;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }

    private void init(View view) {
        setHasOptionsMenu(false);

        mbSubmit = view.findViewById(R.id.mbSubmit);
        mbUpload = view.findViewById(R.id.mbUpload);

        imageAdapter = new ImageAdapter(mContext, imagesUriList);
        mRecyclerView = view.findViewById(R.id.recyclerViewImage);
        txt_image = view.findViewById(R.id.txt_image);
        input_layout_address = view.findViewById(R.id.input_layout_address);
        input_layout_name = view.findViewById(R.id.input_layout_name);
        input_layout_house_no = view.findViewById(R.id.input_layout_house_no);
        input_layout_Sector = view.findViewById(R.id.input_layout_Sector);
        input_layout_desc = view.findViewById(R.id.input_layout_desc);


        txt_image.append(" (" + imagesUriList.size() + ")");

        mRecyclerView.setNestedScrollingEnabled(false);

        LinearLayoutManager mLayoutManager =
                new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);

        mRecyclerView.setLayoutManager(mLayoutManager);

        mRecyclerView.setAdapter(imageAdapter);
        // ViewCompat.setNestedScrollingEnabled(mRecyclerView, false);
        spinner = (Spinner) view.findViewById(R.id.spinner);

        strings = new ArrayList<>();

        // Creating adapter for spinner
        dataAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, strings);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);


// Spinner click listener
        spinner.setOnItemSelectedListener(this);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register_complaint, container, false);
    }

    @SuppressLint({"ClickableViewAccessibility", "SetTextI18n"})
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init(view);

        getWards();

        if (getArguments() != null) {

            responseTicketTypeResult = (ResponseTicketTypeResult) getArguments().getSerializable("str");

            if (responseTicketTypeResult != null) {

                Objects.requireNonNull(input_layout_desc.getEditText()).setText(responseTicketTypeResult.getTitle());

                input_layout_desc.getEditText().setOnTouchListener(new View.OnTouchListener() {

                    public boolean onTouch(View v, MotionEvent event) {
                        if (input_layout_desc.getEditText().hasFocus()) {
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                                case MotionEvent.ACTION_SCROLL:
                                    v.getParent().requestDisallowInterceptTouchEvent(false);
                                    return true;
                            }
                        }
                        return false;
                    }
                });
            }


        }

        currentLocation = ((HomeActivity) mContext).getCurrentLocation();

        if (currentLocation != null) {

            latitude = currentLocation.getLatitude();
            longitude = currentLocation.getLongitude();
            getGeoAddress();

        }

        mbSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!Objects.requireNonNull(input_layout_name.getEditText()).getText().toString().equalsIgnoreCase("") && !input_layout_desc.getEditText().getText().toString().equalsIgnoreCase("")) {

                    if (wardId > 0) {

                        if (!Objects.requireNonNull(input_layout_address.getEditText()).getText().toString().equalsIgnoreCase("")){
                            postRegisterComplaint();
                        }else {
                            ((HomeActivity) mContext).showFailAlert("Address Field can not be blank.");
                        }

                    } else {

                        ((HomeActivity) mContext).showFailAlert("Please Select Ward");
                    }

                } else {
                    ((HomeActivity) mContext).showFailAlert("Name and Description Field can not be blank.");

                    currentLocation = ((HomeActivity) mContext).getCurrentLocation();

                    if (currentLocation != null) {
                        latitude = currentLocation.getLatitude();
                        longitude = currentLocation.getLongitude();
                        getGeoAddress();
                    }
                }
            }
        });

        mbUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CropImage.activity()
                        .setFixAspectRatio(true)
                        .setAspectRatio(4, 4)
                        .setOutputCompressQuality(60)
                        .setAllowFlipping(true)
                        .setCropShape(CropImageView.CropShape.RECTANGLE)
                        .start(mContext, RegisterComplaintFragment.this);
            }
        });
    }

    private void getGeoAddress() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(mContext, Locale.getDefault());

                try {
                    addresses = geocoder.getFromLocation(currentLocation.getLatitude(),
                            currentLocation.getLongitude(), 1);

                    if (addresses != null && addresses.size() > 0) {
                        int maxAddressLines = addresses.get(0).getMaxAddressLineIndex();
                        StringBuffer buffer = new StringBuffer();
                        for (int i = 0; i < maxAddressLines; i++) {
                            String comma = i == maxAddressLines - 1 ? "" : ", ";
                            buffer.append(addresses.get(0).getAddressLine(i) + comma);
                        }
                        address = buffer.toString();
                        city = addresses.get(0).getLocality();
                        state = addresses.get(0).getAdminArea();
                        country = addresses.get(0).getCountryName();
                        zip = addresses.get(0).getPostalCode();
                        featuredName =
                                addresses.get(0).getFeatureName(); // Only if available else return NULL
                        premises = addresses.get(0).getPremises();
                        subLocality = addresses.get(0).getSubLocality();
                        subAdminArea = addresses.get(0).getSubAdminArea();


                        Objects.requireNonNull(input_layout_house_no.getEditText()).setText(featuredName);
                        Objects.requireNonNull(input_layout_Sector.getEditText()).setText(address);


                        List<String> words = Arrays.asList(address, premises, subLocality, subAdminArea, city, state, country, zip);
                        StringBuilder sb = new StringBuilder();
                        boolean first = true;
                        for (String word : words) {
                            if (word != null && (word = word.trim()).length() > 0) {
                                if (first) {
                                    first = false;
                                } else {
                                    sb.append(',' + " ");
                                }
                                sb.append(word);
                            }
                        }

                        Objects.requireNonNull(input_layout_address.getEditText()).setText(sb.toString());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, 1000);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                imagesUriList.add(resultUri);
                imageAdapter.notifyDataSetChanged();

                if (imagesUriList.size() > 0) {

                    txt_image.setText("Upload Photos (" + imagesUriList.size() + ")");
                    mRecyclerView.setVisibility(View.VISIBLE);
                } else {
                    mRecyclerView.setVisibility(View.GONE);
                }

                // Toast.makeText(mContext, resultUri.toString() + "", Toast.LENGTH_SHORT).show();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }


    private void getWards() {

        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(mContext, "");

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseWard> call = apiService.getWards(headerMap);
        call.enqueue(new Callback<ResponseWard>() {
            @Override
            public void onResponse(Call<ResponseWard> call, Response<ResponseWard> response) {
                ResponseWard responseWard = response.body();


                if (responseWard != null && responseWard.getResults() != null) {

                    progressDialog.dismiss();
                    responseWardResults.addAll(responseWard.getResults());

                    for (ResponseWardResult ward : responseWardResults) {

                        strings.add(ward.getName());
                    }

                    dataAdapter.notifyDataSetChanged();




                } else {

                    ((HomeActivity) mContext).showFailAlert("Wards Fetch Failed");


                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseWard> call, Throwable t) {
                // Log error here since request failed

                ((HomeActivity) mContext).showFailAlert(getString(R.string.internal_error));
                progressDialog.dismiss();
            }
        });
    }

    private void postRegisterComplaint() {

        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(mContext, "");

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");

        RequestRegisterComplaint requestRegisterComplaint = new RequestRegisterComplaint(responseTicketTypeResult.getId(), responseTicketTypeResult.getTitle(), Objects.requireNonNull(input_layout_desc.getEditText()).getText().toString(), zoneId, wardId, MyApplication.getUserID(), engineerId, latitude, longitude, PENDING, Objects.requireNonNull(input_layout_address.getEditText()).getText().toString());


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<RequestRegisterComplaint> call = apiService.postRegisterComplaint(headerMap, requestRegisterComplaint);
        call.enqueue(new Callback<RequestRegisterComplaint>() {
            @Override
            public void onResponse(Call<RequestRegisterComplaint> call, @NonNull Response<RequestRegisterComplaint> response) {
                RequestRegisterComplaint responseWard = response.body();

                progressDialog.dismiss();
                if (response.code() == 201 || response.code() == 200) {

                    if (imagesUriList.size() == 0) {
                        Utility.showSuccessAlert(getActivity(), "Complaint has been booked. You will receive a confirmation shortly.");

                        ((HomeActivity) mContext).openDrawer();

                    } else {
                        for (Uri uri : imagesUriList) {

                            if (responseWard != null) {
                                uploadFile(uri, String.valueOf(responseWard.getId()));
                            }
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<RequestRegisterComplaint> call, Throwable t) {
                // Log error here since request failed

                ((HomeActivity) mContext).showFailAlert(getString(R.string.internal_error));
                progressDialog.dismiss();
            }
        });
    }


    // Uploading Image/Video
    private void uploadFile(Uri mediaPath, String ticketId) {


        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(mContext, "Uploading Images...");

        File file = null;
        try {
            String path = Utility.getPath(mContext, mediaPath); // "/mnt/sdcard/FileName.mp3"

            file = new File(path);

            String token = MyApplication.getUserToken();

            Map<String, String> headerMap = new HashMap<>();
            headerMap.put("Authorization", token);

            // Parsing any Media type file
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
            RequestBody id = RequestBody.create(MediaType.parse("text/plain"), ticketId);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<ResponseUploadImage> call = apiService.uploadFile(headerMap, fileToUpload, id);
            call.enqueue(new Callback<ResponseUploadImage>() {
                @Override
                public void onResponse(@NonNull Call<ResponseUploadImage> call, @NonNull Response<ResponseUploadImage> response) {

                    imageUploadCounter++;
                    ResponseUploadImage serverResponse = response.body();
                    if (serverResponse != null) {


                    } else {

                        if (serverResponse != null) {
                            Log.d("uploadFile", serverResponse.toString());
                        }
                    }

                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }

                    if (imageUploadCounter == imagesUriList.size()) {
                        Utility.showSuccessAlert(getActivity(), "Complaint has been booked. You will receive a confirmation shortly.");

                        ((HomeActivity) mContext).openDrawer();

                    }
                }

                @Override
                public void onFailure(Call<ResponseUploadImage> call, Throwable t) {
                    Log.d("uploadFile", t.toString());
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }
                }
            });
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

        wardId = responseWardResults.get(position).getId();
        zoneId = responseWardResults.get(position).getZone();

        if (responseWardResults.get(position).getUsers().size() >= 0) {
            engineerId = responseWardResults.get(position).getUsers().get(0);
        } else {
            Utility.showSuccessAlert(RegisterComplaintFragment.this, "Please Contact to admin to assign Engineer for this ward.");
        }


    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}


