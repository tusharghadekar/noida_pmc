package com.streetlightgrievance.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.button.MaterialButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.streetlightgrievance.MyApplication;
import mobicloud.pmc.streetlightgrievance.R;
import com.streetlightgrievance.Utility;
import com.streetlightgrievance.activities.HomeActivity;
import com.streetlightgrievance.models.RequestUpdateProfile;
import com.streetlightgrievance.models.ResponseUpdateProfile;
import com.streetlightgrievance.models.UserResponse;
import com.streetlightgrievance.models.UserResult;
import com.streetlightgrievance.networkcommunication.ApiClient;
import com.streetlightgrievance.networkcommunication.ApiInterface;
import com.streetlightgrievance.networkcommunication.DialogUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*
 * Created by ${KEDAR_LABDE} on 12-12-2017.
 */

public class ProfileFragment extends Fragment {
    private Context mContext;
    private MaterialButton mbSubmit;

    private TextInputLayout input_layout_address, input_layout_name, input_layout_house_no, input_layout_Sector, input_layout_mobile;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }

    private void init(View view) {

        mbSubmit = view.findViewById(R.id.mbSubmit);
        input_layout_address = view.findViewById(R.id.input_layout_address);
        input_layout_mobile = view.findViewById(R.id.input_layout_mobile);
        input_layout_name = view.findViewById(R.id.input_layout_name);
        input_layout_house_no = view.findViewById(R.id.input_layout_house_no);
        input_layout_Sector = view.findViewById(R.id.input_layout_Sector);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.home, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit :
                Log.i("item id ", item.getItemId() + "");

                mbSubmit.setVisibility(View.VISIBLE);

                input_layout_address.setEnabled(true);
                input_layout_name.setEnabled(true);
                input_layout_house_no.setEnabled(true);
                input_layout_Sector.setEnabled(true);


            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);

        init(view);

        getUserInfoAPI();

        input_layout_address.setEnabled(false);
        input_layout_mobile.setEnabled(false);
        input_layout_name.setEnabled(false);
        input_layout_house_no.setEnabled(false);
        input_layout_Sector.setEnabled(false);

        mbSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mbSubmit.setVisibility(View.GONE);


                input_layout_address.setEnabled(false);
                input_layout_mobile.setEnabled(false);
                input_layout_name.setEnabled(false);
                input_layout_house_no.setEnabled(false);
                input_layout_Sector.setEnabled(false);

                updateProfile();
            }
        });

    }

    public void getUserInfoAPI() {

        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(mContext, "");

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");
        Call<UserResponse> call = apiService.getUser(headerMap);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {

                progressDialog.dismiss();
                UserResponse userReponse = response.body();
                ArrayList<UserResult> userArrayList = null;
                if (userReponse != null) {

                    userArrayList = (ArrayList<UserResult>) userReponse.getResults();
                    if (userArrayList != null && userArrayList.size() > 0) {

                        UserResult user = userArrayList.get(0);

                        if (user.getUsername() != null){
                            Objects.requireNonNull(input_layout_mobile.getEditText()).setText(user.getUsername());
                            Objects.requireNonNull(input_layout_mobile.getEditText()).setEnabled(false);
                        }
                        if (user.getAddress() != null){
                            Objects.requireNonNull(input_layout_house_no.getEditText()).setText(user.getAddress().toString());
                        }

                        if (user.getFirstName() != null){
                            Objects.requireNonNull(input_layout_name.getEditText()).setText(user.getFirstName().toString());
                        }
                        if (user.getAddress() != null){
                            Objects.requireNonNull(input_layout_Sector.getEditText()).setText(user.getAddress().toString());
                        }
                        if (user.getAddress() != null){
                            Objects.requireNonNull(input_layout_address.getEditText()).setText(user.getAddress().toString());
                        }
                    }
                }


            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {

                ((HomeActivity) mContext).showFailAlert(getString(R.string.internal_error));
                progressDialog.dismiss();
            }
        });
    }

    public void updateProfile() {

        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(mContext, "");

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        String token = MyApplication.getUserToken();

        RequestUpdateProfile user = new RequestUpdateProfile();
        user.setFirstName(Objects.requireNonNull(input_layout_name.getEditText()).getText().toString());
        user.setAddress(Objects.requireNonNull(input_layout_address.getEditText()).getText().toString());

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");
        Call<ResponseUpdateProfile> call = apiService.updateProfile(headerMap, user ,MyApplication.getUserID() );
        call.enqueue(new Callback<ResponseUpdateProfile>() {
            @Override
            public void onResponse(Call<ResponseUpdateProfile> call, Response<ResponseUpdateProfile> response) {

                progressDialog.dismiss();
                ResponseUpdateProfile userReponse = response.body();

                if (response.code() == 200){
                    Utility.showSuccessAlert(ProfileFragment.this,
                            "Profile has been updated successfully.");
                }else {

                    Toast.makeText(mContext, "Failed to Update", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseUpdateProfile> call, Throwable t) {
                progressDialog.dismiss();

                //Toast.makeText(mContext, "Internal Server Error", Toast.LENGTH_SHORT).show();
            }
        });
    }
}


