package com.streetlightgrievance.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.streetlightgrievance.MyApplication;
import com.streetlightgrievance.Utility;
import com.streetlightgrievance.fragments.MyRequestsFragment;
import com.streetlightgrievance.fragments.NotificationsFragment;
import com.streetlightgrievance.fragments.ProfileFragment;
import com.streetlightgrievance.fragments.TypesOfComplaintsFragment;
import com.streetlightgrievance.models.UserResponse;
import com.streetlightgrievance.models.UserResult;
import com.streetlightgrievance.networkcommunication.ApiClient;
import com.streetlightgrievance.networkcommunication.ApiInterface;
import com.streetlightgrievance.utils.MyLocationManager;

import net.grandcentrix.tray.AppPreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import mobicloud.pmc.streetlightgrievance.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.streetlightgrievance.SharedPreferences.DEFAULT_VALUE;
import static com.streetlightgrievance.SharedPreferences.MOBILE_NO;

public class HomeActivity extends AppCompatActivity
    implements NavigationView.OnNavigationItemSelectedListener {

  private static final int UPDATE_SLIDING_INDEX = 0;
  private DrawerLayout drawer;
  private View frame;
  private float lastTranslate = 0.0f;

  private MyLocationManager myLocationManager;
  private Location currentLocation;

  private ActionBarDrawerToggle toggle;
  private NavigationView navigationView;
  private AppCompatTextView notificationTextView;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_home);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    drawer.setScrimColor(Color.TRANSPARENT);
    toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    frame = findViewById(R.id.app_bar_home);
    toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open,
        R.string.navigation_drawer_close) {

      @Override public void onDrawerStateChanged(int newState) {
        super.onDrawerStateChanged(newState);
      }

      @Override public void onDrawerClosed(View drawerView) {
        // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
        super.onDrawerClosed(drawerView);
        drawer.requestFocus();
        InputMethodManager inputMethodManager =
            (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        assert inputMethodManager != null;
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
      }

      @Override public void onDrawerSlide(final View drawerView, final float slideOffset) {
        super.onDrawerSlide(drawerView, slideOffset);
        float moveFactor = (drawerView.getWidth() * slideOffset);

        drawer.requestFocus();
        InputMethodManager inputMethodManager =
            (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

        TranslateAnimation anim = new TranslateAnimation(lastTranslate, moveFactor, 0.0f, 0.0f);
        anim.setDuration(0);
        anim.setFillAfter(true);
        frame.startAnimation(anim);

        lastTranslate = moveFactor;
      }

      @Override public void onDrawerOpened(View drawerView) {
        // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
        super.onDrawerOpened(drawerView);

        drawer.requestFocus();
        getUserInfoAPI();
        InputMethodManager inputMethodManager =
            (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
      }
    };
    drawer.addDrawerListener(toggle);

    toggle.syncState();

    navigationView = (NavigationView) findViewById(R.id.nav_view);
    navigationView.setNavigationItemSelectedListener(this);
    navigationView.setCheckedItem(R.id.nav_profile);
    onNavigationItemSelected(navigationView.getMenu().findItem(R.id.nav_profile));

    final TextView tollfreenumber = navigationView.findViewById(R.id.tollfreenumber);

    tollfreenumber.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:"+tollfreenumber.getText().toString().trim()));
        startActivity(callIntent);
      }
    });

    myLocationManager = MyLocationManager.getInstance(this);
    myLocationManager.getCurrentLocationListener(new MyLocationManager.GetLocationUpdate() {
      @Override public void foundLocation(Location location) {

        System.out.println(
            "************\tCurrent location received\t*************\n"
                + "\t Latitude = "
                + location.getLatitude()
                + "\n\tLongitude = "
                + location.getLongitude()
                + "\n\tSpeed = "
                + location.getSpeed()
                + "\n************************************************");

        currentLocation = location;

        myLocationManager.stop_lr_Current();
      }
    });

    getCurrentLocation();
    getUserInfoAPI();
  }



  private void initializeCountDrawer(String count) {
    //Inititalise items to add count value/badge value
    notificationTextView = (AppCompatTextView) MenuItemCompat.getActionView(navigationView.getMenu().findItem(R.id.nav_notifications));


    notificationTextView.setGravity(Gravity.CENTER_VERTICAL);
    notificationTextView.setTypeface(null, Typeface.BOLD);
    notificationTextView.setTextColor(getResources().getColor(R.color.colorAccent));
    notificationTextView.setText(count);
  }

  public void getUserInfoAPI() {
    ApiInterface apiService =
            ApiClient.getClient().create(ApiInterface.class);

    String token = MyApplication.getUserToken();

    Map<String, String> headerMap = new HashMap<>();
    headerMap.put("Authorization", token);
    headerMap.put("Content-Type", "application/json");
    Call<UserResponse> call = apiService.getUser(headerMap);
    call.enqueue(new Callback<UserResponse>() {
      @Override
      public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {

        UserResponse userReponse = response.body();
        ArrayList<UserResult> userArrayList = null;
        if (userReponse != null) {
          userArrayList = (ArrayList<UserResult>) userReponse.getResults();
          if (userArrayList != null && userArrayList.size() > 0) {

            UserResult user = userArrayList.get(0);

            if (user.getNotification()!= null){
              initializeCountDrawer(user.getNotification() +"");
            }else {
              initializeCountDrawer("0");
            }



          }
        }


      }

      @Override
      public void onFailure(Call<UserResponse> call, Throwable t) {
      }
    });
  }

  public Location getCurrentLocation() {

    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      if (checkAndRequestPermissions()) {
        if (!isLocationAvailable()) {
          locationPermissionsPop(HomeActivity.this, LOCATION_SETTING);
        } else {

          if (myLocationManager.googleApiClientConnected()) {
            myLocationManager.start_lr_Current();
          }
        }
      }
    } else {
      if (!isLocationAvailable()) {
        locationPermissionsPop(HomeActivity.this, LOCATION_SETTING);
      } else {
        if (myLocationManager.googleApiClientConnected()) {
          myLocationManager.start_lr_Current();
        }
      }
    }

    return currentLocation;
  }

  @Override public void onBackPressed() {
    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    if (drawer.isDrawerOpen(GravityCompat.START)) {
      drawer.closeDrawer(GravityCompat.START);
    } else {
      int count = getSupportFragmentManager().getBackStackEntryCount();
      if (count == 0) {
        showExitAlert();
      } else {
        super.onBackPressed();
      }
    }
  }

//  @Override public boolean onCreateOptionsMenu(Menu menu) {
//    // Inflate the menu; this adds items to the action bar if it is present.
//    getMenuInflater().inflate(R.menu.home, menu);
//    return true;
//  }
//
//  @Override public boolean onOptionsItemSelected(MenuItem item) {
//    // Handle action bar item clicks here. The action bar will
//    // automatically handle clicks on the Home/Up button, so long
//    // as you specify a parent activity in AndroidManifest.xml.
//    int id = item.getItemId();
//
//    //noinspection SimplifiableIfStatement
//    //if (id == R.id.action_settings) {
//    //  return true;
//    //}
//
//    return super.onOptionsItemSelected(item);
//  }

  @SuppressWarnings("StatementWithEmptyBody") @Override
  public boolean onNavigationItemSelected(@NonNull MenuItem item) {
    // Handle navigation view item clicks here.
    int id = item.getItemId();

    Fragment selected = null;

    if (id == R.id.nav_my_request) {

      selected = new MyRequestsFragment();
      getSupportActionBar().setTitle("My Requests");
    } else if (id == R.id.nav_type_of_complaint) {
      getSupportActionBar().setTitle("Type Of Complaint");
      selected = new TypesOfComplaintsFragment();
    } else if (id == R.id.nav_profile) {
      getSupportActionBar().setTitle("Profile");
      selected = new ProfileFragment();
    } else if (id == R.id.nav_notifications) {
      getSupportActionBar().setTitle("Notifications");
      selected = new NotificationsFragment();
    } else if (id == R.id.nav_logout) {
      showLogoutAlert();
    }

    if (selected != null) {
      FragmentManager fragmentManager = getSupportFragmentManager();

      fragmentManager.beginTransaction()
          .replace(R.id.container, selected, String.valueOf(item.getItemId()))
          .commitAllowingStateLoss();
    }

    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    drawer.closeDrawer(GravityCompat.START);
    return true;
  }

  private void showExitAlert() {

    SweetAlertDialog sweetAlertDialog =
        new SweetAlertDialog(this, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
    sweetAlertDialog.setTitleText(getString(R.string.do_u_want_to_exit));
    sweetAlertDialog.setConfirmText("Yes");
    sweetAlertDialog.setCancelText("No");
    sweetAlertDialog.showCancelButton(true);
    sweetAlertDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
      @Override public void onClick(SweetAlertDialog sweetAlertDialog) {

        sweetAlertDialog.dismiss();
        openDrawer();
      }
    });
    sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
      @Override

      public void onClick(SweetAlertDialog sDialog) {
        sDialog.dismiss();
        finish();
      }
    });
    sweetAlertDialog.show();
  }

  private void showLogoutAlert() {

    SweetAlertDialog sweetAlertDialog =
        new SweetAlertDialog(this, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
    sweetAlertDialog.setTitleText(getString(R.string.do_u_want_to_logout));
    sweetAlertDialog.setConfirmText("Yes");
    sweetAlertDialog.setCancelText("No");
    sweetAlertDialog.showCancelButton(true);
    sweetAlertDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
      @Override public void onClick(SweetAlertDialog sweetAlertDialog) {

        sweetAlertDialog.dismiss();
        openDrawer();
      }
    });
    sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
      @Override

      public void onClick(SweetAlertDialog sDialog) {
        sDialog.dismiss();
        AppPreferences appPreferences = new AppPreferences(HomeActivity.this);
        appPreferences.put(MOBILE_NO, DEFAULT_VALUE);
        MyApplication.saveEmptyUserToken("");
        MyApplication.saveLoginUserID(0);
        startActivity(new Intent(HomeActivity.this, LoginActivity.class));
        finish();
      }
    });
    sweetAlertDialog.show();
  }

  public void openDrawer() {
    navigationView.setCheckedItem(R.id.nav_my_request);
    onNavigationItemSelected(navigationView.getMenu().findItem(R.id.nav_my_request));
  }

  public boolean isLocationAvailable() {

    LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
    boolean gps_enabled = false;
    boolean network_enabled = false;

    try {
      gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
    } catch (Exception e) {
      e.printStackTrace();
    }

    try {
      network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    } catch (Exception e) {
      e.printStackTrace();
    }

    if (!gps_enabled && !network_enabled) {
      return false;
    } else {
      return true;
    }
  }

  private boolean checkAndRequestPermissions() {
    int permissionCamera = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
    int permissionCall = ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
    int permissionLocation =
        ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
    int permissionReadExternalStorage =
        ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
    int permissionWriteExternalStorage =
        ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

    List<String> listPermissionsNeeded = new ArrayList<>();

    if (permissionCamera != PackageManager.PERMISSION_GRANTED) {
      listPermissionsNeeded.add(Manifest.permission.CAMERA);
    }
    if (permissionCall != PackageManager.PERMISSION_GRANTED) {
      listPermissionsNeeded.add(Manifest.permission.CALL_PHONE);
    }

    if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
      listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
    }
    if (permissionReadExternalStorage != PackageManager.PERMISSION_GRANTED) {
      listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
    }

    if (permissionWriteExternalStorage != PackageManager.PERMISSION_GRANTED) {
      listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }
    String reqPer[] = listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]);
    if (!listPermissionsNeeded.isEmpty()) {
      ActivityCompat.requestPermissions(HomeActivity.this, reqPer, REQUEST_ID_MULTIPLE_PERMISSIONS);
      return false;
    }

    return true;
  }

  @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    switch (requestCode) {
      case LOCATION_SETTING:
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
          if (checkAndRequestPermissions()) {
            if (!isLocationAvailable()) {
              locationPermissionsPop(HomeActivity.this, LOCATION_SETTING);
            } else {

            }
          }
        } else {
          if (!isLocationAvailable()) {
            locationPermissionsPop(HomeActivity.this, LOCATION_SETTING);
          } else {

          }
        }
        break;
    }
  }

  public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 2;

  @Override public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
      @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    if (requestCode == REQUEST_ID_MULTIPLE_PERMISSIONS) {
      Map<String, Integer> perms = new HashMap<>();
      // Initialize the map with both permissions
      perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
      perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
      perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
      perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);

      // Fill with actual results from user
      if (grantResults.length > 0) {

        for (int i = 0; i < permissions.length; i++)
          perms.put(permissions[i], grantResults[i]);

        // Check for all permissions
        if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
            && perms.get(Manifest.permission.ACCESS_FINE_LOCATION)
            == PackageManager.PERMISSION_GRANTED
            && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE)
            == PackageManager.PERMISSION_GRANTED
            && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            == PackageManager.PERMISSION_GRANTED) {

          if (!isLocationAvailable()) {
            locationPermissionsPop(HomeActivity.this, LOCATION_SETTING);
          } else {

          }
        } else {
          //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
          //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
          if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)
              || ActivityCompat.shouldShowRequestPermissionRationale(this,
              Manifest.permission.ACCESS_FINE_LOCATION)
              || ActivityCompat.shouldShowRequestPermissionRationale(this,
              Manifest.permission.READ_EXTERNAL_STORAGE)
              || ActivityCompat.shouldShowRequestPermissionRationale(this,
              Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            showDialogOK("Camera, Location and Storage Permissions are required for this app",
                new DialogInterface.OnClickListener() {
                  @Override public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                      case DialogInterface.BUTTON_POSITIVE:
                        checkAndRequestPermissions();
                        break;
                      case DialogInterface.BUTTON_NEGATIVE:
                        // proceed with logic by disabling the related features or quit the app.
                        break;
                    }
                  }
                });
          }
          //permission is denied (and never ask again is  checked)
          //shouldShowRequestPermissionRationale will return false
          else {
            //Go to settings and enable permissions
            showAlertToPermissionAccess();
          }
        }
      }
    }
  }

  public static final int LOCATION_SETTING = 1;

  public void locationPermissionsPop(final Activity activity, final int onResultFlag) {
    if (!isLocationAvailable()) {
      AlertDialog.Builder builder = new AlertDialog.Builder(activity);
      builder.setMessage(activity.getResources().getString(R.string.location_permission));
      builder.setCancelable(false);
      builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
        @Override public void onClick(DialogInterface dialog, int which) {
          Intent locationSettingIntent =
              new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
          activity.startActivityForResult(locationSettingIntent, onResultFlag);
        }
      });
      builder.show();
    }
  }

  private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
    new AlertDialog.Builder(this).setMessage(message)
        .setPositiveButton("OK", okListener)
        .setCancelable(false)
        .create()
        .show();
  }

  private void showAlertToPermissionAccess() {
    new AlertDialog.Builder(this).setMessage(
        "Please enable permissions from settings for this app.")
        .setPositiveButton("Go to settings", new DialogInterface.OnClickListener() {
          @Override public void onClick(DialogInterface dialog, int which) {

            Intent intent = new Intent();
            intent.setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", HomeActivity.this.getPackageName(), null);
            intent.setData(uri);
            HomeActivity.this.startActivityForResult(intent, LOCATION_SETTING);
          }
        })
        .setCancelable(false)
        .create()
        .show();
  }

  public void showFailAlert(String msg) {

    Utility.showFailureAlert(this,msg);
  }
}
