package com.streetlightgrievance.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.streetlightgrievance.MyApplication;
import com.streetlightgrievance.adapters.ImageDownloadAdapter;
import com.streetlightgrievance.models.ResponseDownloadImage;
import com.streetlightgrievance.models.ResponseDownloadImageResult;
import com.streetlightgrievance.models.ResponseMyRequestResult;
import com.streetlightgrievance.models.ResponseTicketTypeResult;
import com.streetlightgrievance.networkcommunication.ApiClient;
import com.streetlightgrievance.networkcommunication.ApiInterface;
import com.streetlightgrievance.networkcommunication.DialogUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import mobicloud.pmc.streetlightgrievance.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.streetlightgrievance.SharedPreferences.COMPLETED;
import static com.streetlightgrievance.SharedPreferences.IN_PROGRESS;
import static com.streetlightgrievance.SharedPreferences.PENDING;

public class ObservationActivity extends AppCompatActivity {

    private ArrayList<Uri> imagesUriList = new ArrayList<>();

    private ArrayList<ResponseDownloadImageResult> imagesList = new ArrayList<>();
    private ImageDownloadAdapter imageDownloadAdapter;
    private RecyclerView mRecyclerViewDownload;
    private int imageUploadCounter = 0;

    TextView txtIssueId, txtStatus, txtIssue, txtObservation, txtDescription, txtWard, txtDate, txtAddress;

    EditText edtObservation;

    private int spinnerPos = 0;
    private int spinnerPosTicketType = 0;
    private ResponseMyRequestResult responseMyRequestResult;
    private ArrayList<ResponseTicketTypeResult> arraylistTicketType = new ArrayList<>();

    private void init() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle("Issue Details");

        txtWard = findViewById(R.id.txtWard);
        txtIssue = findViewById(R.id.txtIssue);
        txtDescription = findViewById(R.id.txtDescription);
        txtStatus = findViewById(R.id.txtStatus);
        txtObservation = findViewById(R.id.txtObservation);
        txtDate = findViewById(R.id.txtDate);
        txtIssueId = findViewById(R.id.txtIssueId);
        txtAddress = findViewById(R.id.txtAddress);


        imageDownloadAdapter = new ImageDownloadAdapter(this, imagesList);
        mRecyclerViewDownload = findViewById(R.id.recyclerViewDownload);
        mRecyclerViewDownload.setNestedScrollingEnabled(false);
        LinearLayoutManager mLayoutManager =
                new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerViewDownload.setLayoutManager(mLayoutManager);
        mRecyclerViewDownload.setAdapter(imageDownloadAdapter);


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_observation);

        init();

        if (getIntent().hasExtra("str")) {

            responseMyRequestResult = (ResponseMyRequestResult) getIntent().getSerializableExtra("str");

            if (responseMyRequestResult != null) {

                if (responseMyRequestResult.getTypeGroupTitle() != null) {
                    txtIssue.setText(responseMyRequestResult.getTypeGroupTitle());
                }

                if (responseMyRequestResult.getId() != null) {
                    txtIssueId.setText("id_" + responseMyRequestResult.getId() + "_" + responseMyRequestResult.getTypeGroupTitle().replace(" ","_"));
                }

                if (responseMyRequestResult.getObservation() != null) {
                    txtObservation.setText(responseMyRequestResult.getObservation());
                }

                if (responseMyRequestResult.getDescription() != null) {
                    txtDescription.setText(responseMyRequestResult.getDescription());
                }

                if (responseMyRequestResult.getWardName() != null) {
                    txtWard.setText(responseMyRequestResult.getWardName());
                }

                if (responseMyRequestResult.getmUpdatedAt() != null) {
                    double l = responseMyRequestResult.getmUpdatedAt();

                    Date date = new Date((long) l * 1000);
                    @SuppressLint("SimpleDateFormat") SimpleDateFormat fmtOut = new SimpleDateFormat("EEE, d MMM yyyy hh:mm a");

                    //  String date = String.format("%.10s", listItems.get(position).getUpdatedAt());
                    txtDate.setText(fmtOut.format(date));
                }

                if (responseMyRequestResult.getAddress() != null) {
                    txtAddress.setText(responseMyRequestResult.getAddress());
                }


                switch (responseMyRequestResult.getStatus()) {
                    case PENDING:
                        txtStatus.setText("Pending");
                        break;
                    case IN_PROGRESS:
                        txtStatus.setText("In Progress");
                        break;
                    case COMPLETED:
                        txtStatus.setText("Resolved");
                        break;
                }

                //Download images
                downloadImages(responseMyRequestResult.getId());

                imageDownloadAdapter.setOnItemClickListener(new ImageDownloadAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        Dialog builder = new Dialog(ObservationActivity.this);
                        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        builder.getWindow().setBackgroundDrawable(
                                new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialogInterface) {
                                //nothing;
                            }
                        });

                        ImageView imageView = new ImageView(ObservationActivity.this);

                        Picasso.with(ObservationActivity.this)
                                .load(imagesList.get(position).getImage())
                                .placeholder(R.drawable.loading)
                                .into(imageView);

                        builder.addContentView(imageView, new RelativeLayout.LayoutParams(
                                ViewGroup.LayoutParams.WRAP_CONTENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT));
                        builder.show();
                    }
                });


            }

        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    // downloading Image/Video
    private void downloadImages(int ticketId) {


        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(this, "Images...");

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseDownloadImage> call = apiService.downloadImages(headerMap, ticketId);
        call.enqueue(new Callback<ResponseDownloadImage>() {
            @Override
            public void onResponse(@NonNull Call<ResponseDownloadImage> call, @NonNull Response<ResponseDownloadImage> response) {

                ResponseDownloadImage serverResponse = response.body();
                if (serverResponse != null) {

                    imagesList.addAll(serverResponse.getResults());
                    imageDownloadAdapter.notifyDataSetChanged();
                    if (imagesList.size() > 0) {
                        mRecyclerViewDownload.setVisibility(View.VISIBLE);
                    }


                } else {

                    if (serverResponse != null) {
                        Log.d("uploadFile", serverResponse.toString());
                    }
                }

                if (progressDialog != null) {
                    progressDialog.dismiss();
                }

            }

            @Override
            public void onFailure(Call<ResponseDownloadImage> call, Throwable t) {
                Log.d("uploadFile", t.toString());
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
            }
        });


    }
}
