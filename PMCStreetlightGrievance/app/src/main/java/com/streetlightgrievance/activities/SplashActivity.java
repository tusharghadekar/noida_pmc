package com.streetlightgrievance.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OneSignal;
import com.streetlightgrievance.MyApplication;
import com.streetlightgrievance.networkcommunication.ApiClient;
import com.streetlightgrievance.networkcommunication.ApiInterface;

import net.grandcentrix.tray.AppPreferences;

import java.util.Objects;

import mobicloud.pmc.streetlightgrievance.R;

import static com.streetlightgrievance.MyApplication.saveOneSignalId;
import static com.streetlightgrievance.SharedPreferences.DEFAULT_VALUE;
import static com.streetlightgrievance.SharedPreferences.MOBILE_NO;

public class SplashActivity extends AppCompatActivity {

  private AppPreferences appPreferences;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_splash);

    appPreferences = new AppPreferences(this);

    OSPermissionSubscriptionState status = OneSignal.getPermissionSubscriptionState();
    status.getSubscriptionStatus().getUserId();

    saveOneSignalId(status.getSubscriptionStatus().getUserId());

    //Toast.makeText(this, MyApplication.getOneSignalId(), Toast.LENGTH_SHORT).show();

    new Handler().postDelayed(new Runnable() {
      @Override public void run() {

        if (Objects.requireNonNull(appPreferences.getString(MOBILE_NO, DEFAULT_VALUE))
            .equals(DEFAULT_VALUE)) {
          startActivity(new Intent(SplashActivity.this, LoginActivity.class));
        } else {
          startActivity(new Intent(SplashActivity.this, HomeActivity.class));
        }
        finish();
      }
    }, 2000);
  }


  @RequiresApi(28)
  private static class OnUnhandledKeyEventListenerWrapper implements
      View.OnUnhandledKeyEventListener {
    private ViewCompat.OnUnhandledKeyEventListenerCompat mCompatListener;

    OnUnhandledKeyEventListenerWrapper(ViewCompat.OnUnhandledKeyEventListenerCompat listener) {
      this.mCompatListener = listener;
    }

    public boolean onUnhandledKeyEvent(View v, KeyEvent event) {
      return this.mCompatListener.onUnhandledKeyEvent(v, event);
    }
  }
}
