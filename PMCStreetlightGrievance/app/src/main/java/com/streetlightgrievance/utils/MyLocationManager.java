package com.streetlightgrievance.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MyLocationManager
    implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

  Context context;

  public static MyLocationManager myLocationManager ;
  double latitude, longitude;
  Location trackingLocation;
  static GetLocationUpdate getLocationUpdate;

  private GoogleApiClient mGoogleApiClient;

  //Current Location Listener
  LocationRequest lr_Current;
  CurrentLocationListener ll_Current;

  private MyLocationManager(Context context) {

    this.context = context;

    if (checkPlayServices()) {
      buildGoogleApiClient();
      connectGoogleApiClient();
    } else {
      Toast.makeText(context,
          "Google Play services is not available on your device, without this service this application is not working.",
          Toast.LENGTH_SHORT).show();
    }
  }

  public static MyLocationManager getInstance(Context context) {
    if (myLocationManager == null) {
      myLocationManager = new MyLocationManager(context);
    }
    return myLocationManager;
  }

  public boolean googleApiClientConnected() {
    if (mGoogleApiClient != null) if (mGoogleApiClient.isConnected()) return true;

    return false;
  }

  public void connectGoogleApiClient() {
    if (mGoogleApiClient != null) {
      mGoogleApiClient.connect();
    }
  }

  @Override public void onConnected(Bundle bundle) {
    start_lr_Current();
  }

  @Override public void onConnectionSuspended(int i) {
    connectGoogleApiClient();
  }

  @Override public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
  }

  private void displayLocation() {

    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
        != PackageManager.PERMISSION_GRANTED
        && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
        != PackageManager.PERMISSION_GRANTED) {
      return;
    }

    //        getLocationUpdate.foundLocation(trackingLocation);

    latitude = trackingLocation.getLatitude();
    longitude = trackingLocation.getLongitude();

    new Thread(new Runnable() {
      @Override public void run() {
        // TODO need to update current location based on working hrs setting
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(context, Locale.getDefault());

        try {
          addresses = geocoder.getFromLocation(latitude, longitude, 1);
          if (addresses != null && addresses.size() > 0) {
            int maxAddressLines = addresses.get(0).getMaxAddressLineIndex();
            StringBuffer buffer = new StringBuffer();
            for (int i = 0; i < maxAddressLines; i++) {
              String comma = i == maxAddressLines - 1 ? "" : ", ";
              buffer.append(addresses.get(0).getAddressLine(i) + comma);
            }
            String address = buffer.toString();
            if (address == null) address = "N/A";
            String zipcode = addresses.get(0).getPostalCode();
            if (zipcode == null) zipcode = "0";
          }
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }).start();
  }

  protected synchronized void buildGoogleApiClient() {
    mGoogleApiClient = new GoogleApiClient.Builder(context).addConnectionCallbacks(this)
        .addOnConnectionFailedListener(this)
        .addApi(LocationServices.API)
        .build();
  }

  private boolean checkPlayServices() {
    int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);

    if (resultCode != ConnectionResult.SUCCESS) {
      //if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
      //    GooglePlayServicesUtil.getErrorDialog(resultCode, (Activity) context, PLAY_SERVICES_RESOLUTION_REQUEST).show();
      //} else {
      //Toast.makeText(context, "This device is not supported.", Toast.LENGTH_LONG).show();
      //
      //}
      return false;
    }
    return true;
  }

  public interface GetLocationUpdate {
    void foundLocation(Location location);
  }

  public void start_lr_Current() {
    System.out.println("* Started Current location updates...");

    lr_Current = new LocationRequest();
    lr_Current.setInterval(1000);
    lr_Current.setFastestInterval(1000);
    lr_Current.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    ll_Current = new CurrentLocationListener();

    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
        != PackageManager.PERMISSION_GRANTED
        && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
        != PackageManager.PERMISSION_GRANTED) {
      return;
    }
    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, lr_Current,
        ll_Current);
  }

  public void stop_lr_Current() {
    System.out.println("* Stopped Current location updates...");
    LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, ll_Current);
  }

  public class CurrentLocationListener implements LocationListener {

    @Override public void onLocationChanged(Location currentLocation) {

      if (currentLocation != null && getLocationUpdate != null) {


        getLocationUpdate.foundLocation(currentLocation);
      }
    }
  }

  public void getCurrentLocationListener(GetLocationUpdate getLocationUpdate) {
    MyLocationManager.getLocationUpdate = getLocationUpdate;
  }
}
