package com.streetlightgrievance.utils;


import com.streetlightgrievance.models.MyRequests;
import com.streetlightgrievance.models.Notification;

import java.util.ArrayList;

public class ArrayListManager {
    private static ArrayListManager arrayListManager;
    private static ArrayList<MyRequests> myRequestsArrayList = new ArrayList<>();
    private static ArrayList<Notification> notificationArrayList = new ArrayList<>();

    private ArrayListManager() {

    }

    public static ArrayListManager getInstance() {
        if (arrayListManager == null) {
            arrayListManager = new ArrayListManager();
        }
        return arrayListManager;
    }


    public ArrayList<MyRequests> getMyRequestsArrayList() {
        return myRequestsArrayList;

    }

    public ArrayList<Notification> getNotificationArrayList() {
        return notificationArrayList;

    }



    public static void reset() {
        myRequestsArrayList = new ArrayList<>();
    }

    public static void remove(int position) {
        myRequestsArrayList.remove(position);
    }
}
