package com.streetlightgrievance.models;

/**
 * Created by Kedar Labde @ Benchmark IT Solutions LCC on 20-11-2018.
 */
public class Notification {

  private String notification;


  public Notification(String notification) {
    this.notification = notification;

  }

  public String getNotification() {
    return notification;
  }

  public void setNotification(String notification) {
    this.notification = notification;
  }

}
