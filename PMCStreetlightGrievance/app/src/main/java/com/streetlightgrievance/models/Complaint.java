package com.streetlightgrievance.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Kedar Labde @ Benchmark IT Solutions LCC on 20-11-2018.
 */
public class Complaint implements Parcelable {

  private String complaint;
  boolean selected;

  public Complaint(String complaint, boolean selected) {
    this.complaint = complaint;
    this.selected = selected;
  }

  public String getComplaint() {
    return complaint;
  }

  public void setComplaint(String complaint) {
    this.complaint = complaint;
  }

  public boolean isSelected() {
    return selected;
  }

  public void setSelected(boolean selected) {
    this.selected = selected;
  }

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel parcel, int i) {

  }
}
