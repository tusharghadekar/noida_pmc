package com.streetlightgrievance.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

import mobicloud.pmc.streetlightgrievance.R;

/**
 * Created by Ashish on 12/15/2017.
 */

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.MyViewHolder> {

  private Context mContext;
  private ImageAdapter.OnItemClickListener mItemClickListener;
  private List<Uri> listItems;

  public ImageAdapter(Context context, List<Uri> listItems) {
    this.listItems = listItems;
    this.mContext = context;
  }

  public void setOnItemClickListener(final ImageAdapter.OnItemClickListener mItemClickListener) {
    this.mItemClickListener = mItemClickListener;
  }

  @Override public ImageAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View itemView =
        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_uri_list, parent, false);

    return new ImageAdapter.MyViewHolder(itemView);
  }

  @Override public void onBindViewHolder(ImageAdapter.MyViewHolder holder, int position) {

    if (listItems.get(position) != null) {


      Picasso.with(mContext)
          .load(listItems.get(position))
          .resize(250, 250)
          .centerCrop()
          .placeholder(R.drawable.loading)
          .into(holder.imageView);
    }
  }

  @Override public int getItemCount() {
    return listItems.size();
  }

  public interface OnItemClickListener {
    void onItemClick(View view, int position);
  }

  public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    ImageView imageView;

    public MyViewHolder(View itemView) {
      super(itemView);
      imageView = (ImageView) itemView.findViewById(R.id.image);
      itemView.setOnClickListener(this);
    }

    @Override public void onClick(View view) {
      if (mItemClickListener != null) {
        mItemClickListener.onItemClick(view, getPosition());
      }
    }
  }

    /*
    method to set listener to the adapter ViewHolder item
     */
}
