package com.streetlightgrievance.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.streetlightgrievance.models.ResponseTicketTypeResult;

import java.util.List;

import mobicloud.pmc.streetlightgrievance.R;


public class TypesOfComplaintAdapter extends
    RecyclerView.Adapter<TypesOfComplaintAdapter.ViewHolder> {

  private List<ResponseTicketTypeResult> complaintList;
  private Context context;
  private OnItemClickListener mItemClickListener;

  public TypesOfComplaintAdapter(Context ctx, List<ResponseTicketTypeResult> ComplaintList) {
    complaintList = ComplaintList;
    context = ctx;
  }

  public void setOnItemClickListener(final TypesOfComplaintAdapter.OnItemClickListener mItemClickListener) {
    this.mItemClickListener = mItemClickListener;
  }


  @NonNull @Override
  public TypesOfComplaintAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
      int viewType) {

    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.item_complaint_list, parent, false);

    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(TypesOfComplaintAdapter.ViewHolder holder, int position) {
    ResponseTicketTypeResult filterM = complaintList.get(position);
    holder.txtComplaint.setText(filterM.getTitle());

  }

  @Override
  public int getItemCount() {
    return complaintList.size();
  }

  public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView txtComplaint;


    public ViewHolder(final View view) {
      super(view);
      txtComplaint = (TextView) view.findViewById(R.id.txtComplaint);

      //item click event listener
      view.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

      if (mItemClickListener != null) {

        mItemClickListener.onItemClick(v, getPosition());

      }
    }
  }


  public interface OnItemClickListener {
    void onItemClick(View view, int position);
  }
}