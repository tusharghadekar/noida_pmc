package tata.noidaengineer.networkcommunication;

import android.support.v4.app.Fragment;

public interface WebserviceResponseHandler {

    void onResponseSuccess(Fragment fragment,String count);

    void onResponseFailure();


}
