package tata.noidaengineer.networkcommunication;


import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import tata.noidaengineer.models.LoginRequest;
import tata.noidaengineer.models.LoginResponse;
import tata.noidaengineer.models.RequestRegisterComplaint;
import tata.noidaengineer.models.ResponseDownloadImage;
import tata.noidaengineer.models.ResponseMyRequest;
import tata.noidaengineer.models.ResponseMyRequestResult;
import tata.noidaengineer.models.ResponseNotifications;
import tata.noidaengineer.models.ResponseNotificationsResult;
import tata.noidaengineer.models.ResponseTicketType;
import tata.noidaengineer.models.ResponseUploadImage;
import tata.noidaengineer.models.ResponseWard;
import tata.noidaengineer.models.ResponseWardResult;
import tata.noidaengineer.models.UserResponse;
import tata.noidaengineer.models.UserResult;

public interface ApiInterface {

    @GET("tickettype/?ticket_type=2&limit=1000")
    Call<ResponseTicketType> getObservationTypes(@HeaderMap Map<String, String> headers);

    @GET("ward/")
    Call<ResponseWard> getWards(@HeaderMap Map<String, String> headers);

    @POST("ticket/")
    Call<RequestRegisterComplaint> postRegisterComplaint(@HeaderMap Map<String, String> headers, @Body RequestRegisterComplaint registerComplaint);

    @POST("login/")
    Call<LoginResponse> doLogin(@Body LoginRequest loginRequest);

    @GET("user/")
    Call<UserResponse> getUser(@HeaderMap Map<String, String> headers);

    @GET("ticket/?&status=1")
    Call<ResponseMyRequest> getMyRequests(@HeaderMap Map<String, String> headers);

    @GET("ticket/?&status=1&limit=10000")
    Call<ResponseMyRequest> getSectorRequests(@HeaderMap Map<String, String> headers, @Query("ward") int ward_id);

    @GET("ticket/?&status=3&limit=10000")
    Call<ResponseMyRequest> getSectorResolvedRequests(@HeaderMap Map<String, String> headers, @Query("ward") int ward_id);

    @GET("ticket/?")
    Call<ResponseMyRequest> getSectorComplaintStatusRequests(@HeaderMap Map<String, String> headers, @Query("ward") int ward_id);

    @GET("ticket/?&status=1&escalated=1&limit=10000")
    Call<ResponseMyRequest> getEscalatedRequests(@HeaderMap Map<String, String> headers);

    @GET("ticket/?&status=1&escalated=1&limit=10000")
    Call<ResponseMyRequest> getSectorEscalatedRequests(@HeaderMap Map<String, String> headers, @Query("ward") int ward_id);

    @GET("ticket/?status=1&limit=1000")
    Call<ResponseMyRequest> getPendingRequests(@HeaderMap Map<String, String> headers);

    @GET("ticket/?status=2&limit=1000")
    Call<ResponseMyRequest> getInProgressRequests(@HeaderMap Map<String, String> headers);

    @GET("ticket/?status=3&limit=1000")
    Call<ResponseMyRequest> getCompletedRequests(@HeaderMap Map<String, String> headers);

    @GET("notification/")
    Call<ResponseNotifications> getNotifications(@HeaderMap Map<String, String> headers);

    @Multipart
    @POST("ticketimage/")
    Call<ResponseUploadImage> uploadFile(@HeaderMap Map<String, String> headers, @Part MultipartBody.Part file, @Part("ticket") RequestBody ticket_id);

    @PUT("ticket/{id}/")
    Call<ResponseMyRequest> patchObservation(@HeaderMap Map<String, String> headers, @Body ResponseMyRequestResult registerObservation, @Path("id") int ticket_id);

    @PATCH("user/{id}/")
    Call<UserResponse> updateProfile(@HeaderMap Map<String, String> headers, @Body UserResult user, @Path("id") int user_id);

    @GET("ward/{id}/")
    Call<ResponseWardResult> getWardById(@HeaderMap Map<String, String> headers, @Path("id") int user_id);

    @GET("ticketimage/?")
    Call<ResponseDownloadImage> downloadImages(@HeaderMap Map<String, String> headers, @Query("ticket") int ticket_id);


    @PATCH("notification/{id}/")
    Call<ResponseNotificationsResult> readNotification(@HeaderMap Map<String, String> headers, @Body ResponseNotificationsResult notificationsResult, @Path("id") int user_id);


}
