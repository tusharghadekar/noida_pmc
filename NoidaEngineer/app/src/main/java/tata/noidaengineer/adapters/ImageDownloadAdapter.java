package tata.noidaengineer.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

import tata.noidaengineer.R;
import tata.noidaengineer.models.ResponseDownloadImageResult;


public class ImageDownloadAdapter extends RecyclerView.Adapter<ImageDownloadAdapter.MyViewHolder> {

    private Context mContext;
    private ImageDownloadAdapter.OnItemClickListener mItemClickListener;
    private List<ResponseDownloadImageResult> listItems;

    public ImageDownloadAdapter(Context context, List<ResponseDownloadImageResult> listItems) {
        this.listItems = listItems;
        this.mContext = context;
    }

    public void setOnItemClickListener(final ImageDownloadAdapter.OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    @Override
    public ImageDownloadAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_uri_list, parent, false);

        return new ImageDownloadAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ImageDownloadAdapter.MyViewHolder holder, int position) {

        if (listItems.get(position) != null) {


            Picasso.with(mContext)
                    .load(listItems.get(position).getImage())
                    .resize(250, 250)
                    .centerCrop()
                    .placeholder(R.drawable.loading)
                    .into(holder.imageView);
        }
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imageView;


        public MyViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.image);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(view, getPosition());

            }
        }
    }

    /*
    method to set listener to the adapter ViewHolder item
     */
}
