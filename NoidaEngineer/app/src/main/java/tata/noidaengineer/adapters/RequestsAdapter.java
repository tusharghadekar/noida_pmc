package tata.noidaengineer.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.javiersantos.bottomdialogs.BottomDialog;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import tata.noidaengineer.R;
import tata.noidaengineer.activities.ObservationActivity;
import tata.noidaengineer.models.ResponseMyRequestResult;

import static tata.noidaengineer.SharedPreferences.COMPLETED;
import static tata.noidaengineer.SharedPreferences.IN_PROGRESS;
import static tata.noidaengineer.SharedPreferences.PENDING;


public class RequestsAdapter extends RecyclerView.Adapter<RequestsAdapter.MyViewHolder> {

    int spinnerPos;
    Activity activity;
    private Context mContext;
    private RequestsAdapter.OnItemClickListener mItemClickListener;
    private List<ResponseMyRequestResult> listItems;
    private Fragment fragment;
    private BottomDialog bottomDialog;

    public RequestsAdapter(Context context, List<ResponseMyRequestResult> listItems, Fragment fragment, Activity activity) {
        this.listItems = listItems;
        this.mContext = context;
        this.fragment = fragment;
        this.activity = activity;
    }

    public void setOnItemClickListener(final RequestsAdapter.OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    @NonNull
    @Override
    public RequestsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_requests_list, parent, false);

        return new RequestsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RequestsAdapter.MyViewHolder holder, int position) {

//        if (fragment instanceof TotalRequestsFragment) {
//            holder.txtStatus.setVisibility(View.GONE);
//        } else {
//            holder.txtStatus.setVisibility(View.VISIBLE);
//        }

        if (listItems.get(position).getTitle() != null && !listItems.get(position)
                .getTitle()
                .equals("null")) {
            holder.txtRequest.setText(listItems.get(position).getTitle());
        }

        if (listItems.get(position).getCitizenName() != null && !listItems.get(position)
                .getCitizenName()
                .equals("null")) {
            holder.txtCitizenName.setText(listItems.get(position).getCitizenName());
        }else {
            holder.txtCitizenName.setText("Not Available");

        }

        if (listItems.get(position).getMobileNo() != null && !listItems.get(position)
                .getMobileNo()
                .equals("null")) {
            holder.txtCitizenMobNo.setText(listItems.get(position).getMobileNo());
        }else {
            holder.txtCitizenName.setText("Not Available");

        }

        if (listItems.get(position).getStatus() != null && !listItems.get(position)
                .getStatus()
                .equals("null")) {

            if (listItems.get(position).getStatus() == COMPLETED) {

                if (listItems.get(position).getEscalated()) {
                    holder.txtStatus.setTextColor(ContextCompat.getColor(mContext, R.color.red));
                    holder.txtRequest.setTextColor(ContextCompat.getColor(mContext, R.color.red));
                    holder.txtStatus.setText("Resolved & Escalated");
                } else {
                    holder.txtStatus.setTextColor(ContextCompat.getColor(mContext, R.color.green));
                    holder.txtStatus.setText("Resolved");
                    holder.txtRequest.setTextColor(ContextCompat.getColor(mContext, R.color.green));

                }

            }

            if (listItems.get(position).getStatus() == PENDING) {

                if (listItems.get(position).getEscalated()) {
                    holder.txtStatus.setTextColor(ContextCompat.getColor(mContext, R.color.red));
                    holder.txtRequest.setTextColor(ContextCompat.getColor(mContext, R.color.red));
                    holder.txtStatus.setText("Pending & Escalated");
                } else {
                    holder.txtStatus.setTextColor(ContextCompat.getColor(mContext, R.color.orange));
                    holder.txtRequest.setTextColor(ContextCompat.getColor(mContext, R.color.orange));
                    holder.txtStatus.setText("Pending");
                }
            }

            if (listItems.get(position).getStatus() == IN_PROGRESS) {

                if (listItems.get(position).getEscalated()) {
                    holder.txtStatus.setTextColor(ContextCompat.getColor(mContext, R.color.red));
                    holder.txtRequest.setTextColor(ContextCompat.getColor(mContext, R.color.red));
                    holder.txtStatus.setText("In Progress & Escalated");
                } else {
                    holder.txtStatus.setTextColor(ContextCompat.getColor(mContext, R.color.olive));
                    holder.txtRequest.setTextColor(ContextCompat.getColor(mContext, R.color.olive));

                    holder.txtStatus.setText("In Progress");
                }
            }


            if (listItems.get(position).getAddress() != null && !listItems.get(position)
                    .getAddress()
                    .equals("null")) {
                holder.txtLocation.setText(listItems.get(position).getAddress());
            }

            if (listItems.get(position).getmUpdatedAt() != null) {

                double l = listItems.get(position).getmUpdatedAt();

                Date date = new Date((long) l * 1000);
                @SuppressLint("SimpleDateFormat") SimpleDateFormat fmtOut = new SimpleDateFormat("EEE, d MMM yyyy hh:mm a");

                //  String date = String.format("%.10s", listItems.get(position).getUpdatedAt());
                holder.txtDate.setText(fmtOut.format(date));
            }

            if (listItems.get(position).getmUpdatedAt() != null) {

                double l = listItems.get(position).getmUpdatedAt();

                Date date = new Date((long) l * 1000);
                @SuppressLint("SimpleDateFormat") SimpleDateFormat fmtOut = new SimpleDateFormat("EEE, d MMM yyyy hh:mm a");

                //  String date = String.format("%.10s", listItems.get(position).getUpdatedAt());


                if (listItems.get(position).getStatus() == COMPLETED) {

                    holder.txtClosedTime.setText(fmtOut.format(date));
                }else {
                    holder.txtClosedTime.setText("N.A.");
                }

            }


        }

    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    private void openBottomSheet() {


        mContext.startActivity(new Intent(mContext, ObservationActivity.class)
                .putExtra("str", listItems.get(spinnerPos)));

//        Bundle bundle = new Bundle();
//        bundle.putSerializable("str", listItems.get(spinnerPos));
//        Fragment selected = new ObservationFragment();
//        selected.setArguments(bundle);
//
//        if (activity instanceof HomeActivity) {
//            FragmentManager fragmentManager = ((HomeActivity) mContext).getSupportFragmentManager();
//            fragmentManager.beginTransaction()
//                    .replace(R.id.container, selected, "227")
//                    .commitAllowingStateLoss();
//        } else if (activity instanceof SrHomeActivity) {
//            FragmentManager fragmentManager = ((SrHomeActivity) mContext).getSupportFragmentManager();
//            fragmentManager.beginTransaction()
//                    .replace(R.id.container, selected, "227")
//                    .commitAllowingStateLoss();
//        }


    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtRequest,txtCitizenMobNo,txtCitizenName;
        TextView txtStatus, txtLocation, txtDate,txtClosedTime;
        LinearLayout llItemDetails;


        MyViewHolder(View itemView) {
            super(itemView);
            txtRequest = (TextView) itemView.findViewById(R.id.txtRequest);
            txtStatus = (TextView) itemView.findViewById(R.id.txtStatus);
            txtLocation = (TextView) itemView.findViewById(R.id.txtLocation);
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);
            llItemDetails = (LinearLayout) itemView.findViewById(R.id.llItemDetails);
            txtCitizenMobNo = (TextView) itemView.findViewById(R.id.txtCitizenMobNo);
            txtCitizenName = (TextView) itemView.findViewById(R.id.txtCitizenName);
            txtClosedTime = (TextView) itemView.findViewById(R.id.txtClosedTime);



            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(view, getPosition());

//                if (llItemDetails.getVisibility() == View.GONE) {
//                    txtRequest.setTypeface(Typeface.DEFAULT_BOLD);
//                    llItemDetails.setVisibility(View.VISIBLE);
//                } else {
//                    txtRequest.setTypeface(Typeface.DEFAULT);
//                    llItemDetails.setVisibility(View.GONE);
//                }


                spinnerPos = getPosition();

                openBottomSheet();


            }
        }
    }

}
