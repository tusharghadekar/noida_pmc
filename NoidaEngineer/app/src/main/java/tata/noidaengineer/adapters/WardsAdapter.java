package tata.noidaengineer.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import tata.noidaengineer.R;
import tata.noidaengineer.models.ResponseWardResult;


public class WardsAdapter extends RecyclerView.Adapter<WardsAdapter.MyViewHolder> {

    private Context mContext;
    private WardsAdapter.OnItemClickListener mItemClickListener;
    private List<ResponseWardResult> listItems;

    public WardsAdapter(Context context, List<ResponseWardResult> listItems) {
        this.listItems = listItems;
        this.mContext = context;
    }

    public void setOnItemClickListener(final WardsAdapter.OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    @NonNull
    @Override
    public WardsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_wards, parent, false);

        return new WardsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull WardsAdapter.MyViewHolder holder, int position) {

        if (listItems.get(position).getName() != null && !listItems.get(position)
                .getName()
                .equals("null")) {
            holder.txtWard.setText(listItems.get(position).getName());
        }

        if (listItems.get(position).getTotalRequests() != null) {
            holder.txtRequestCount.setText(listItems.get(position).getTotalRequests() + "");
        }

    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtWard, txtRequestCount;

        MyViewHolder(View itemView) {
            super(itemView);
            txtWard = (TextView) itemView.findViewById(R.id.txtWard);
            txtRequestCount = (TextView) itemView.findViewById(R.id.txtRequestCount);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(view, getPosition());

            }
        }
    }

    /*
    method to set listener to the adapter ViewHolder item
     */
}
