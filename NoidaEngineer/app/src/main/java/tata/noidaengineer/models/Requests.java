package tata.noidaengineer.models;

/**
 * Created by Kedar Labde @ Benchmark IT Solutions LCC on 20-11-2018.
 */
public class Requests {


    private String request, status;

    public Requests(String request, String status) {
        this.request = request;
        this.status = status;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
