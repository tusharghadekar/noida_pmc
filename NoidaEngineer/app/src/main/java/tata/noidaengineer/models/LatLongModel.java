package tata.noidaengineer.models;

import java.io.Serializable;

public class LatLongModel implements Serializable {

    double engineerLatitude, engineerLongitude, complaintLatitude, complaintLongitude;

    public double getEngineerLatitude() {
        return engineerLatitude;
    }

    public void setEngineerLatitude(double engineerLatitude) {
        this.engineerLatitude = engineerLatitude;
    }

    public double getEngineerLongitude() {
        return engineerLongitude;
    }

    public void setEngineerLongitude(double engineerLongitude) {
        this.engineerLongitude = engineerLongitude;
    }

    public double getComplaintLatitude() {
        return complaintLatitude;
    }

    public void setComplaintLatitude(double complaintLatitude) {
        this.complaintLatitude = complaintLatitude;
    }

    public double getComplaintLongitude() {
        return complaintLongitude;
    }

    public void setComplaintLongitude(double complaintLongitude) {
        this.complaintLongitude = complaintLongitude;
    }
}
