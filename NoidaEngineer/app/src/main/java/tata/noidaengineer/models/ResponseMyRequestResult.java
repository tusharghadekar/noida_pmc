package tata.noidaengineer.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ResponseMyRequestResult implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("ticket_type")
    @Expose
    private Integer ticketType;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("ward_name")
    @Expose
    private String wardName;
    @SerializedName("zone_name")
    @Expose
    private String zoneName;
    @SerializedName("engineer_name")
    @Expose
    private String engineerName;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("zone")
    @Expose
    private Integer zone;
    @SerializedName("ward")
    @Expose
    private Integer ward;
    @SerializedName("engineer")
    @Expose
    private Integer engineer;
    @SerializedName("user")
    @Expose
    private Integer user;
    @SerializedName("latitude")
    @Expose
    private Object latitude;
    @SerializedName("longitude")
    @Expose
    private Object longitude;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("observation")
    @Expose
    private String observation;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("escalated")
    @Expose
    private Boolean escalated;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;

    @SerializedName("mCreatedAt")
    @Expose
    private Double mCreatedAt;
    @SerializedName("mUpdatedAt")
    @Expose
    private Double mUpdatedAt;

    @SerializedName("assigned")
    @Expose
    private boolean assigned;


    @SerializedName("mobile_no")
    @Expose
    private String mobileNo;

    @SerializedName("citizen_name")
    @Expose
    private String citizenName;

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getCitizenName() {
        return citizenName;
    }

    public void setCitizenName(String citizenName) {
        this.citizenName = citizenName;
    }


    public ResponseMyRequestResult(Integer id, Integer ticketType, String title, String description, Integer zone,
                                   Integer ward, Integer engineer, Integer user, Object latitude, Object longitude,
                                   Integer status, boolean assigned, Boolean escalated) {
        this.id = id;
        this.ticketType = ticketType;
        this.title = title;
        this.description = description;
        this.zone = zone;
        this.ward = ward;
        this.engineer = engineer;
        this.user = user;
        this.latitude = latitude;
        this.longitude = longitude;
        this.status = status;
        this.assigned = assigned;
        this.escalated= escalated;
        this.observation= description;
    }

    public boolean isAssigned() {
        return assigned;
    }

    public void setAssigned(boolean assigned) {
        this.assigned = assigned;
    }

    public Double getmCreatedAt() {
        return mCreatedAt;
    }

    public void setmCreatedAt(Double mCreatedAt) {
        this.mCreatedAt = mCreatedAt;
    }

    public Double getmUpdatedAt() {
        return mUpdatedAt;
    }

    public void setmUpdatedAt(Double mUpdatedAt) {
        this.mUpdatedAt = mUpdatedAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTicketType() {
        return ticketType;
    }

    public void setTicketType(Integer ticketType) {
        this.ticketType = ticketType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getWardName() {
        return wardName;
    }

    public void setWardName(String wardName) {
        this.wardName = wardName;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public String getEngineerName() {
        return engineerName;
    }

    public void setEngineerName(String engineerName) {
        this.engineerName = engineerName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getZone() {
        return zone;
    }

    public void setZone(Integer zone) {
        this.zone = zone;
    }

    public Integer getWard() {
        return ward;
    }

    public void setWard(Integer ward) {
        this.ward = ward;
    }

    public Integer getEngineer() {
        return engineer;
    }

    public void setEngineer(Integer engineer) {
        this.engineer = engineer;
    }

    public Integer getUser() {
        return user;
    }

    public void setUser(Integer user) {
        this.user = user;
    }

    public Object getLatitude() {
        return latitude;
    }

    public void setLatitude(Object latitude) {
        this.latitude = latitude;
    }

    public Object getLongitude() {
        return longitude;
    }

    public void setLongitude(Object longitude) {
        this.longitude = longitude;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getEscalated() {
        return escalated;
    }

    public void setEscalated(Boolean escalated) {
        this.escalated = escalated;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


}