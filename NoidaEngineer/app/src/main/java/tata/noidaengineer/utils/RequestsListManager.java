package tata.noidaengineer.utils;

import java.util.ArrayList;

import tata.noidaengineer.models.Requests;

public class RequestsListManager {
    private static RequestsListManager billingManager;
    private static ArrayList<Requests> requestsArrayList = new ArrayList<>();

    private RequestsListManager() {

    }

    public static RequestsListManager getInstance() {
        if (billingManager == null) {
            billingManager = new RequestsListManager();
        }
        return billingManager;
    }

    public static void reset() {
        requestsArrayList = new ArrayList<>();
    }

    public static void remove(int position) {
        requestsArrayList.remove(position);
    }

    public ArrayList<Requests> getRequestsList() {
        return requestsArrayList;

    }
}
