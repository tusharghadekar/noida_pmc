package tata.noidaengineer.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tata.noidaengineer.MyApplication;
import tata.noidaengineer.R;
import tata.noidaengineer.Utility;
import tata.noidaengineer.activities.HomeActivity;
import tata.noidaengineer.adapters.NotificationsAdapter;
import tata.noidaengineer.models.ResponseNotifications;
import tata.noidaengineer.models.ResponseNotificationsResult;
import tata.noidaengineer.networkcommunication.ApiClient;
import tata.noidaengineer.networkcommunication.ApiInterface;
import tata.noidaengineer.networkcommunication.DialogUtils;

/*
 * Created by ${KEDAR_LABDE} on 12-12-2017.
 */


public class NotificationsFragment extends Fragment {
    private Context mContext;
    private NotificationsAdapter notificationsAdapter;
    private ArrayList<ResponseNotificationsResult> notificationArrayList = new ArrayList<>();
    private RecyclerView mRecyclerView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }

    private void init(View view) {

        mRecyclerView = view.findViewById(R.id.recyclerView);
        notificationsAdapter = new NotificationsAdapter(mContext, notificationArrayList);
        LinearLayoutManager mLayoutManager =
                new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(notificationsAdapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_notifications, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(false);
        init(view);

        getNotifications();

        notificationsAdapter.setOnItemClickListener(new NotificationsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                if (!notificationArrayList.get(position).isRead()){
                    setNotificationRead(notificationArrayList.get(position).getId());
                }

            }
        });


    }

    public void setNotificationRead(int id) {

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        String token = MyApplication.getUserToken();

        final ResponseNotificationsResult notificationsResult = new ResponseNotificationsResult();
        notificationsResult.setRead(true);

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");
        Call<ResponseNotificationsResult> call = apiService.readNotification(headerMap, notificationsResult, id);
        call.enqueue(new Callback<ResponseNotificationsResult>() {
            @Override
            public void onResponse(Call<ResponseNotificationsResult> call, Response<ResponseNotificationsResult> response) {

                ResponseNotificationsResult body = response.body();

                getNotifications();
            }

            @Override
            public void onFailure(Call<ResponseNotificationsResult> call, Throwable t) {
            }
        });
    }


    private void getNotifications() {

        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(mContext, "");

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseNotifications> call = apiService.getNotifications(headerMap);
        call.enqueue(new Callback<ResponseNotifications>() {
            @Override
            public void onResponse(Call<ResponseNotifications> call, Response<ResponseNotifications> response) {
                ResponseNotifications responseNotifications = response.body();

                if (responseNotifications != null && responseNotifications.getResults() != null) {

                    progressDialog.dismiss();
                    notificationArrayList.clear();
                    notificationArrayList.addAll(responseNotifications.getResults());
                    notificationsAdapter.notifyDataSetChanged();

                } else {

                    ((HomeActivity) mContext).showFailAlert("Fetch Failed");

                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseNotifications> call, Throwable t) {
                // Log error here since request failed

                Utility.showFailureAlert(NotificationsFragment.this, getString(R.string.internal_error));
                progressDialog.dismiss();
            }
        });
    }
}





