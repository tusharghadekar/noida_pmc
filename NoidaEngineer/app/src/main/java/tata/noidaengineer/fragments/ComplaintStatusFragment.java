package tata.noidaengineer.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.button.MaterialButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tata.noidaengineer.MyApplication;
import tata.noidaengineer.R;
import tata.noidaengineer.Utility;
import tata.noidaengineer.activities.HomeActivity;
import tata.noidaengineer.activities.SrHomeActivity;
import tata.noidaengineer.models.ResponseMyRequest;
import tata.noidaengineer.models.ResponseWard;
import tata.noidaengineer.networkcommunication.ApiClient;
import tata.noidaengineer.networkcommunication.ApiInterface;
import tata.noidaengineer.networkcommunication.DialogUtils;

/*
 * Created by ${KEDAR_LABDE} on 12-12-2017.
 */

public class ComplaintStatusFragment extends Fragment {
    TextView txtCountPending, txtCountResolved, txtCountInPro;
    String ward_id = "";
    int countCompleted = 0;
    int countPending = 0;
    int countInProgress = 0;
    int countEscalated = 0;
    private Context mContext;
    private MaterialButton mbSubmit;
    private ImageButton mbUpload;
    private LinearLayout ll_pending, ll_resolved, ll_inPro;
    private Activity mActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
        mContext = activity;
    }

    private void init(View view) {

        ll_pending = view.findViewById(R.id.ll_pending);
        ll_resolved = view.findViewById(R.id.ll_resolved);
        ll_inPro = view.findViewById(R.id.ll_inPro);

        txtCountPending = view.findViewById(R.id.txtCountPending);
        txtCountResolved = view.findViewById(R.id.txtCountResolved);
        txtCountInPro = view.findViewById(R.id.txtCountInPro);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_complaint_status, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(false);
        init(view);

        if (getArguments() != null) {

            ward_id = (getArguments().getString("ward_id"));

            if (ward_id != null && !ward_id.equalsIgnoreCase("")) {
                getSectorRequests(Integer.parseInt(ward_id));
            }

        } else {
            getWards();
        }

        ll_pending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Bundle bundle = new Bundle();
                bundle.putSerializable("ward_id", ward_id);
                Fragment selected = new PendingRequestsFragment();

                if (mActivity instanceof HomeActivity) {

                    if (!ward_id.equalsIgnoreCase("")) {


                        ((HomeActivity) mContext).getSupportActionBar().setTitle("Pending Complaints");
                        selected.setArguments(bundle);
                        FragmentManager fragmentManager = ((HomeActivity) mContext).getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, selected, "5")
                                .commitAllowingStateLoss();


                    } else {

                        ((HomeActivity) mContext).getSupportActionBar().setTitle("Pending Complaints");
                        FragmentManager fragmentManager = ((HomeActivity) mContext).getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, selected, "5")
                                .commitAllowingStateLoss();

                    }
                } else if (mActivity instanceof SrHomeActivity) {

                    if (!ward_id.equalsIgnoreCase("")) {

                        ((SrHomeActivity) mContext).getSupportActionBar().setTitle("Pending Complaints");
                        selected.setArguments(bundle);
                        FragmentManager fragmentManager = ((SrHomeActivity) mContext).getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, selected, "5")
                                .commitAllowingStateLoss();


                    } else {
                        ((SrHomeActivity) mContext).getSupportActionBar().setTitle("Pending Complaints");
                        FragmentManager fragmentManager = ((SrHomeActivity) mContext).getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, selected, "5")
                                .commitAllowingStateLoss();

                    }
                }


            }
        });

        ll_resolved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment selected = new ResolvedRequestsFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("ward_id", ward_id);

                if (mActivity instanceof HomeActivity) {

                    if (!ward_id.equalsIgnoreCase("")) {

                        ((HomeActivity) mContext).getSupportActionBar().setTitle("Resolved Complaints");
                        selected.setArguments(bundle);
                        FragmentManager fragmentManager = ((HomeActivity) mContext).getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, selected, "5")
                                .commitAllowingStateLoss();


                    } else {

                        ((HomeActivity) mContext).getSupportActionBar().setTitle("Resolved Complaints");
                        FragmentManager fragmentManager = ((HomeActivity) mContext).getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, selected, "5")
                                .commitAllowingStateLoss();

                    }
                } else if (mActivity instanceof SrHomeActivity) {

                    if (!ward_id.equalsIgnoreCase("")) {

                        ((SrHomeActivity) mContext).getSupportActionBar().setTitle("Resolved Complaints");
                        selected.setArguments(bundle);
                        FragmentManager fragmentManager = ((SrHomeActivity) mContext).getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, selected, "5")
                                .commitAllowingStateLoss();


                    } else {

                        ((SrHomeActivity) mContext).getSupportActionBar().setTitle("Resolved Complaints");
                        FragmentManager fragmentManager = ((SrHomeActivity) mContext).getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, selected, "5")
                                .commitAllowingStateLoss();

                    }
                }


            }
        });

        ll_inPro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle = new Bundle();
                bundle.putSerializable("ward_id", ward_id);
                Fragment selected = new AttednedRequestsFragment();

                if (mActivity instanceof HomeActivity) {

                    if (!ward_id.equalsIgnoreCase("")) {


                        ((HomeActivity) mContext).getSupportActionBar().setTitle("In Progress Complaints");
                        selected.setArguments(bundle);
                        FragmentManager fragmentManager = ((HomeActivity) mContext).getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, selected, "5")
                                .commitAllowingStateLoss();


                    } else {
                        ((HomeActivity) mContext).getSupportActionBar().setTitle("In Progress Complaints");
                        FragmentManager fragmentManager = ((HomeActivity) mContext).getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, selected, "5")
                                .commitAllowingStateLoss();

                    }
                } else if (mActivity instanceof SrHomeActivity) {

                    if (!ward_id.equalsIgnoreCase("")) {

                        ((SrHomeActivity) mContext).getSupportActionBar().setTitle("In Progress Complaints");
                        selected.setArguments(bundle);
                        FragmentManager fragmentManager = ((SrHomeActivity) mContext).getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, selected, "5")
                                .commitAllowingStateLoss();

                    } else {
                        ((SrHomeActivity) mContext).getSupportActionBar().setTitle("In Progress Complaints");
                        FragmentManager fragmentManager = ((SrHomeActivity) mContext).getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, selected, "5")
                                .commitAllowingStateLoss();

                    }
                }


            }
        });


    }


    private void getWards() {

        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(mContext, "");

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseWard> call = apiService.getWards(headerMap);
        call.enqueue(new Callback<ResponseWard>() {
            @Override
            public void onResponse(Call<ResponseWard> call, Response<ResponseWard> response) {
                ResponseWard responseWard = response.body();


                if (responseWard != null && responseWard.getResults() != null) {

                    progressDialog.dismiss();
                    getSectorRequests(responseWard.getResults().get(0).getId());

                } else {

                    if (mActivity instanceof HomeActivity) {
                        ((HomeActivity) mContext).showFailAlert(getString(R.string.ward_fetch_failed));
                    } else {
                        ((SrHomeActivity) mContext).showFailAlert(getString(R.string.ward_fetch_failed));
                    }


                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseWard> call, Throwable t) {
                // Log error here since request failed

                ((HomeActivity) mContext).showFailAlert(getString(R.string.internal_error));
                progressDialog.dismiss();
            }
        });
    }


    private void getSectorRequests(final int ward_id) {

        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(mContext, "");

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseMyRequest> call = apiService.getSectorRequests(headerMap,ward_id);
        call.enqueue(new Callback<ResponseMyRequest>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<ResponseMyRequest> call, Response<ResponseMyRequest> response) {

                ResponseMyRequest responseMyRequest = response.body();

                if (responseMyRequest != null && responseMyRequest.getResults() != null) {

                    countPending = responseMyRequest.getCount();
                    txtCountPending.setText(countPending + "");


                    progressDialog.dismiss();

                    getSectorResolvedRequests(ward_id);


                } else {

                    Utility.showFailureAlert(ComplaintStatusFragment.this, getString(R.string.internal_error));
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseMyRequest> call, Throwable t) {
                // Log error here since request failed

                Utility.showFailureAlert(ComplaintStatusFragment.this, getString(R.string.internal_error));
                progressDialog.dismiss();
            }
        });
    }


    private void getSectorResolvedRequests(final int ward_id) {

        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(mContext, "");

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseMyRequest> call = apiService.getSectorResolvedRequests(headerMap, ward_id);
        call.enqueue(new Callback<ResponseMyRequest>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<ResponseMyRequest> call, Response<ResponseMyRequest> response) {

                ResponseMyRequest responseMyRequest = response.body();

                if (responseMyRequest != null && responseMyRequest.getResults() != null) {

                    countCompleted = responseMyRequest.getCount();
                    txtCountResolved.setText(countCompleted + "");


                    progressDialog.dismiss();


                } else {

                    Utility.showFailureAlert(ComplaintStatusFragment.this, getString(R.string.internal_error));
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseMyRequest> call, Throwable t) {
                // Log error here since request failed

                Utility.showFailureAlert(ComplaintStatusFragment.this, getString(R.string.internal_error));
                progressDialog.dismiss();
            }
        });
    }

}


