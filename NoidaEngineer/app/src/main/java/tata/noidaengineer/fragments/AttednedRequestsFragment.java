package tata.noidaengineer.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tata.noidaengineer.MyApplication;
import tata.noidaengineer.R;
import tata.noidaengineer.Utility;
import tata.noidaengineer.activities.HomeActivity;
import tata.noidaengineer.activities.SrHomeActivity;
import tata.noidaengineer.adapters.RequestsAdapter;
import tata.noidaengineer.models.ResponseMyRequest;
import tata.noidaengineer.models.ResponseMyRequestResult;
import tata.noidaengineer.models.ResponseWard;
import tata.noidaengineer.models.ResponseWardResult;
import tata.noidaengineer.networkcommunication.ApiClient;
import tata.noidaengineer.networkcommunication.ApiInterface;
import tata.noidaengineer.networkcommunication.DialogUtils;
import tata.noidaengineer.networkcommunication.WebserviceResponseHandler;

/*
 * Created by ${KEDAR_LABDE} on 12-12-2017.
 */

public class AttednedRequestsFragment extends Fragment {
    private Context mContext;
    private RequestsAdapter myRequestAdapter;
    private ArrayList<ResponseMyRequestResult> myRequestsArrayList = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private Activity mActivity;
    private int ward_id = 0;
    private View mView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
        mActivity = activity;
    }

    private void init(View view) {

        mView = view;
        mRecyclerView = view.findViewById(R.id.recyclerView);
        myRequestAdapter = new RequestsAdapter(mContext, myRequestsArrayList, this, getActivity());
        LinearLayoutManager mLayoutManager =
                new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(myRequestAdapter);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_requests, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(false);
        init(view);

        if (getArguments() != null) {

            ward_id = Integer.parseInt(Objects.requireNonNull(getArguments().getString("ward_id")));

            if (ward_id != 0) {
                getWardById(ward_id);
            }

        } else {

            getWards();

        }

        myRequestAdapter.setOnItemClickListener(new RequestsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                // Toast.makeText(mContext, "Clicked", Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void getMyRequests(final int ward_id) {

        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(mContext, "");

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseMyRequest> call = apiService.getInProgressRequests(headerMap);
        call.enqueue(new Callback<ResponseMyRequest>() {
            @Override
            public void onResponse(Call<ResponseMyRequest> call, Response<ResponseMyRequest> response) {

                ResponseMyRequest responseMyRequest = response.body();

                if (responseMyRequest != null && responseMyRequest.getResults() != null) {

                    progressDialog.dismiss();
                    for (ResponseMyRequestResult request : responseMyRequest.getResults()) {

                        if (ward_id == request.getWard() && !request.getEscalated()) {
                            myRequestsArrayList.add(request);
                        }
                    }
                    TextView txtCount = mView.findViewById(R.id.txtCount);
                    txtCount.setText("Complaints Count on this Screen is : "+myRequestsArrayList.size());
                    myRequestAdapter.notifyDataSetChanged();

                } else {

                    Utility.showFailureAlert(AttednedRequestsFragment.this, getString(R.string.internal_error));
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseMyRequest> call, Throwable t) {
                // Log error here since request failed

                Utility.showFailureAlert(AttednedRequestsFragment.this, getString(R.string.internal_error));
                progressDialog.dismiss();
            }
        });
    }


    private void getWardById(final int ward_id) {

        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(mContext, "");

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseWardResult> call = apiService.getWardById(headerMap, ward_id);
        call.enqueue(new Callback<ResponseWardResult>() {
            @Override
            public void onResponse(Call<ResponseWardResult> call, Response<ResponseWardResult> response) {
                ResponseWardResult responseWardResult = response.body();


                if (responseWardResult != null) {

                    progressDialog.dismiss();

                    getMyRequests(ward_id);

                } else {

                    if (mActivity instanceof HomeActivity) {
                        ((HomeActivity) mContext).showFailAlert(getString(R.string.ward_fetch_failed));
                    } else {
                        ((SrHomeActivity) mContext).showFailAlert(getString(R.string.ward_fetch_failed));
                    }

                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseWardResult> call, Throwable t) {
                // Log error here since request failed
                if (mActivity instanceof HomeActivity) {
                    ((HomeActivity) mContext).showFailAlert(getString(R.string.internal_error));
                } else {
                    ((SrHomeActivity) mContext).showFailAlert(getString(R.string.internal_error));
                }

                progressDialog.dismiss();
            }
        });
    }

    private void getWards() {

        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(mContext, "");

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseWard> call = apiService.getWards(headerMap);
        call.enqueue(new Callback<ResponseWard>() {
            @Override
            public void onResponse(Call<ResponseWard> call, Response<ResponseWard> response) {
                ResponseWard responseWard = response.body();


                if (responseWard != null && responseWard.getResults() != null) {

                    progressDialog.dismiss();

                    getMyRequests(responseWard.getResults().get(0).getId());

                } else {

                    ((HomeActivity) mContext).showFailAlert(getString(R.string.ward_fetch_failed));


                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseWard> call, Throwable t) {
                // Log error here since request failed

                ((HomeActivity) mContext).showFailAlert(getString(R.string.internal_error));
                progressDialog.dismiss();
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            // Refresh your fragment here
            if (getFragmentManager() != null) {
                getFragmentManager().beginTransaction().detach(this).attach(this).commit();
            }
            Log.i("IsRefresh", "Yes");
        }
    }
}