package tata.noidaengineer.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tata.noidaengineer.MyApplication;
import tata.noidaengineer.R;
import tata.noidaengineer.Utility;
import tata.noidaengineer.activities.SrHomeActivity;
import tata.noidaengineer.models.ResponseMyRequest;
import tata.noidaengineer.models.ResponseMyRequestResult;
import tata.noidaengineer.models.ResponseWard;
import tata.noidaengineer.networkcommunication.ApiClient;
import tata.noidaengineer.networkcommunication.ApiInterface;
import tata.noidaengineer.networkcommunication.DialogUtils;

import static tata.noidaengineer.SharedPreferences.COMPLETED;


/*
 * Created by ${KEDAR_LABDE} on 12-12-2017.
 */

public class SrHomeFragment extends Fragment {
    int countTotal = 0;
    int countCompleted = 0;
    int countPending = 0;
    int countInProgress = 0;
    int countEscalated = 0;
    TextView txtTotal, txtWards, txtInProgress, txtPending, txtEscalated;
    CardView cardTotal, cardStatus, cardInPro, cardPending, cardEscalated, cardNotification;
    private Context mContext;
    private GoogleMap mMap;
    private SupportMapFragment mMapView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }

    private void init(View view) {

        mMapView = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.homeMap);
        //  mMapView = (MapFragment) Objects.requireNonNull(getActivity()).getFragmentManager().findFragmentById(R.id.homeMap);


        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap map) {
                mMap = map;
                LatLng sydney = new LatLng(28.5355, 77.3910);
                //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 10));


            }
        });

        txtTotal = view.findViewById(R.id.txtTotal);
        txtWards = view.findViewById(R.id.txtWards);
        txtEscalated = view.findViewById(R.id.txtEscalated);

        cardTotal = view.findViewById(R.id.cardTotal);
        cardStatus = view.findViewById(R.id.cardStatus);
        cardEscalated = view.findViewById(R.id.cardEscalated);


        cardTotal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SrHomeActivity) mContext).openFragmentTotal();
            }
        });

        cardStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment selected = new TotalWardsFragment();
                ((SrHomeActivity) mContext).getSupportActionBar().setTitle("Total Sectors");


                FragmentManager fragmentManager = ((SrHomeActivity) mContext).getSupportFragmentManager();

                fragmentManager.beginTransaction()
                        .replace(R.id.container, selected, "66")
                        .commitAllowingStateLoss();

            }
        });

        cardEscalated.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment selected = new EscalatedRequestsFragment();
                ((SrHomeActivity) mContext).getSupportActionBar().setTitle("Complaints Escalated");


                FragmentManager fragmentManager = ((SrHomeActivity) mContext).getSupportFragmentManager();

                fragmentManager.beginTransaction()
                        .replace(R.id.container, selected, "34")
                        .commitAllowingStateLoss();
            }
        });


    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sr_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(false);
        init(view);

        getMyRequests();

    }


    private void getMyRequests() {

        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(mContext, "");

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseMyRequest> call = apiService.getMyRequests(headerMap);
        call.enqueue(new Callback<ResponseMyRequest>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<ResponseMyRequest> call, Response<ResponseMyRequest> response) {

                ResponseMyRequest responseMyRequest = response.body();

                if (responseMyRequest != null && responseMyRequest.getResults() != null) {

                    ((SrHomeActivity) mContext).getUserInfoAPI();

                    //txtTotal.setText(responseMyRequest.getResults().size() + "");

//                    for (ResponseMyRequestResult request : responseMyRequest.getResults()) {
//
//                        if (request.getStatus() != COMPLETED) {
//                            countTotal++;
//                        }
//                    }

                    countTotal = responseMyRequest.getCount();
                    txtTotal.setText(countTotal + "");

//                    for (ResponseMyRequestResult request : responseMyRequest.getResults()) {
//
//                        if (request.getEscalated()) {
//                            countEscalated++;
//                        }
//                    }
//                    txtEscalated.setText(countEscalated + "");

                    progressDialog.dismiss();

                    getEscalatedRequests();

                    getWards();


                } else {

                    Utility.showFailureAlert(SrHomeFragment.this, getString(R.string.internal_error));
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseMyRequest> call, Throwable t) {
                // Log error here since request failed

                Utility.showFailureAlert(SrHomeFragment.this, getString(R.string.internal_error));
                progressDialog.dismiss();
            }
        });
    }


    private void getWards() {

        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(mContext, "");

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseWard> call = apiService.getWards(headerMap);
        call.enqueue(new Callback<ResponseWard>() {
            @Override
            public void onResponse(Call<ResponseWard> call, Response<ResponseWard> response) {
                ResponseWard responseWard = response.body();


                if (responseWard != null && responseWard.getResults() != null) {

                    progressDialog.dismiss();

                    txtWards.setText(responseWard.getCount() + "");

                } else {

                    ((SrHomeActivity) mContext).showFailAlert(getString(R.string.ward_fetch_failed));


                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseWard> call, Throwable t) {
                // Log error here since request failed

                ((SrHomeActivity) mContext).showFailAlert(getString(R.string.internal_error));
                progressDialog.dismiss();
            }
        });
    }

    private void getEscalatedRequests() {

        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(mContext, "");

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseMyRequest> call = apiService.getEscalatedRequests(headerMap);
        call.enqueue(new Callback<ResponseMyRequest>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<ResponseMyRequest> call, Response<ResponseMyRequest> response) {

                ResponseMyRequest responseMyRequest = response.body();

                if (responseMyRequest != null && responseMyRequest.getResults() != null) {


//                    for (ResponseMyRequestResult request : responseMyRequest.getResults()) {
//
//                        if (ward_id == request.getWard()&& request.getStatus() != COMPLETED) {
//                            countTotal++;
//                        }
//
//                    }

                    countEscalated = responseMyRequest.getCount();
                    txtEscalated.setText(countEscalated + "");

//                    for (ResponseMyRequestResult request : responseMyRequest.getResults()) {
//
//                        if (request.getStatus() == COMPLETED && ward_id == request.getWard()) {
//
//                            if (!request.getEscalated()){
//                                countCompleted++;
//                            }
//                        }
//
//                    }

//                    for (ResponseMyRequestResult request : responseMyRequest.getResults()) {
//
//                        if (request.getStatus() == IN_PROGRESS && ward_id == request.getWard()) {
//                            if (!request.getEscalated()) {
//                                countInProgress++;
//                            }
//                        }
//                    }
//
//                    for (ResponseMyRequestResult request : responseMyRequest.getResults()) {
//
//                        if (request.getStatus() == PENDING && ward_id == request.getWard()) {
//                            if (!request.getEscalated()) {
//                                countPending++;
//                            }
//
//                        }
//                    }
//
//                    txtStatus.setText(countCompleted + countPending + countInProgress + "");
//
//                    for (ResponseMyRequestResult request : responseMyRequest.getResults()) {
//
//                        if (request.getEscalated() && ward_id == request.getWard()) {
//                            countEscalated++;
//                        }
//                    }
//                    txtEscalated.setText(countEscalated + "");

                    progressDialog.dismiss();


                } else {

                    Utility.showFailureAlert(SrHomeFragment.this, getString(R.string.internal_error));
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseMyRequest> call, Throwable t) {
                // Log error here since request failed

                Utility.showFailureAlert(SrHomeFragment.this, getString(R.string.internal_error));
                progressDialog.dismiss();
            }
        });
    }

}


