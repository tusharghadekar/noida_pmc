package tata.noidaengineer.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tata.noidaengineer.MyApplication;
import tata.noidaengineer.R;
import tata.noidaengineer.activities.HomeActivity;
import tata.noidaengineer.activities.SrHomeActivity;
import tata.noidaengineer.adapters.WardsAdapter;
import tata.noidaengineer.models.ResponseWard;
import tata.noidaengineer.models.ResponseWardResult;
import tata.noidaengineer.networkcommunication.ApiClient;
import tata.noidaengineer.networkcommunication.ApiInterface;
import tata.noidaengineer.networkcommunication.DialogUtils;

import static android.app.Activity.RESULT_OK;

/*
 * Created by ${KEDAR_LABDE} on 12-12-2017.
 */

public class TotalWardsFragment extends Fragment {
    private Context mContext;
    private WardsAdapter wardsAdapter;
    private ArrayList<ResponseWardResult> responseWardResults = new ArrayList<>();
    private RecyclerView mRecyclerView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }

    private void init(View view) {

        mRecyclerView = view.findViewById(R.id.recyclerView);
        wardsAdapter = new WardsAdapter(mContext, responseWardResults);
        LinearLayoutManager mLayoutManager =
                new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(wardsAdapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frament_wards, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(false);
        ((SrHomeActivity) mContext).setTitle("Wards");

        init(view);

        getWards();

        wardsAdapter.setOnItemClickListener(new WardsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                startActivityForResult(new Intent(mContext, HomeActivity.class)
                                .putExtra("exit", "exit")
                                .putExtra("ward_id", responseWardResults.get(position).getId() + "")
                                .putExtra("ward_name", responseWardResults.get(position).getName() + "")
                        , REQUEST_CODE);
            }
        });

    }

    public static final int REQUEST_CODE = 111;

    private void getWards() {

        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(mContext, "");

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseWard> call = apiService.getWards(headerMap);
        call.enqueue(new Callback<ResponseWard>() {
            @Override
            public void onResponse(Call<ResponseWard> call, Response<ResponseWard> response) {
                ResponseWard responseWard = response.body();


                if (responseWard != null && responseWard.getResults() != null) {

                    progressDialog.dismiss();
                    responseWardResults.clear();

                    for (ResponseWardResult responseWardResult : responseWard.getResults()) {

                        if (responseWardResult.getTotalRequests() > 0) {
                            responseWardResults.add(responseWardResult);
                        }
                    }
                    //responseWardResults.addAll(responseWard.getResults());
                    wardsAdapter.notifyDataSetChanged();

                } else {

                    ((SrHomeActivity) mContext).showFailAlert(getString(R.string.ward_fetch_failed));


                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseWard> call, Throwable t) {
                // Log error here since request failed

                ((SrHomeActivity) mContext).showFailAlert(getString(R.string.internal_error));
                progressDialog.dismiss();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {

                getWards();
            }
        } catch (Exception ex) {
            Toast.makeText(mContext, ex.toString(),
                    Toast.LENGTH_SHORT).show();
        }

    }
}


