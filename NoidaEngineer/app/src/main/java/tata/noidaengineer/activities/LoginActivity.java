package tata.noidaengineer.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OneSignal;

import net.grandcentrix.tray.AppPreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tata.noidaengineer.MyApplication;
import tata.noidaengineer.R;
import tata.noidaengineer.Utility;
import tata.noidaengineer.models.LoginRequest;
import tata.noidaengineer.models.LoginResponse;
import tata.noidaengineer.models.UserResponse;
import tata.noidaengineer.models.UserResult;
import tata.noidaengineer.networkcommunication.ApiClient;
import tata.noidaengineer.networkcommunication.ApiInterface;
import tata.noidaengineer.networkcommunication.DialogUtils;

import static tata.noidaengineer.MyApplication.saveOneSignalId;
import static tata.noidaengineer.SharedPreferences.USER_TYPE;

public class LoginActivity extends AppCompatActivity {

    private Button mBtnLogin;
    private TextInputLayout input_layout_email, input_layout_password;

    private String str_email, str_password;
    private AppPreferences appPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        appPreferences = new AppPreferences(this);

        mBtnLogin = findViewById(R.id.mbLogin);

        input_layout_email = findViewById(R.id.input_layout_email);
        input_layout_password = findViewById(R.id.input_layout_password);


        mBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (validateInput()) {

                    doLogin(str_email, str_password);

                }


            }
        });
    }

    private boolean validateInput() {

        boolean result = true;

        str_email = Objects.requireNonNull(input_layout_email.getEditText()).getText().toString();
        str_password = Objects.requireNonNull(input_layout_password.getEditText()).getText().toString();

        if (str_email.equalsIgnoreCase("") || str_password.equalsIgnoreCase("")) {
            Utility.showFailureAlert(this, getString(R.string.please_enter_valid_inputs));
            result = false;
        }

        return result;
    }


    private void doLogin(String username, String password) {

        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(LoginActivity.this, "");

        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername((username));
        loginRequest.setPassword(password);

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<LoginResponse> call = apiService.doLogin(loginRequest);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                LoginResponse loginResponse = response.body();

                if (loginResponse != null && loginResponse.getToken() != null && (!loginResponse.getToken()
                        .isEmpty())) {

                    MyApplication.saveUserToken(loginResponse.getToken());

                    getUserInfoAPI();


                } else {
                    Utility.showFailureAlert(LoginActivity.this, response.message());
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                // Log error here since request failed
                Utility.showFailureAlert(LoginActivity.this, getString(R.string.internal_error));
                progressDialog.dismiss();
            }
        });
    }

    public void getUserInfoAPI() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");
        Call<UserResponse> call = apiService.getUser(headerMap);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {

                UserResponse userReponse = response.body();
                ArrayList<UserResult> userArrayList = null;
                if (userReponse != null) {
                    userArrayList = (ArrayList<UserResult>) userReponse.getResults();
                    if (userArrayList != null && userArrayList.size() > 0) {

                        UserResult user = userArrayList.get(0);
                        MyApplication.saveLoginUserID(user.getId());
                        MyApplication.saveEngineerType(user.getUserType());
//                        MyApplication.saveEngineerTypeWorkingFrom(user.getWorkingFrom());


                        appPreferences.put(USER_TYPE, user.getUserType());
//                        if (Objects.equals(appPreferences.getString(USER_TYPE, DEFAULT_VALUE), SR_ENGINEER + "")) {
//                            startActivity(new Intent(LoginActivity.this, SrHomeActivity.class));
//                        } else if (Objects.equals(appPreferences.getString(USER_TYPE, DEFAULT_VALUE), JR_ENGINEER + "")) {
//                            startActivity(new Intent(LoginActivity.this, HomeActivity.class));
//                        }
                        updateProfile();
                    }
                }


            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
            }
        });
    }

    public void updateProfile() {


        OSPermissionSubscriptionState status = OneSignal.getPermissionSubscriptionState();
        status.getSubscriptionStatus().getUserId();

        saveOneSignalId(status.getSubscriptionStatus().getUserId());
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        String token = MyApplication.getUserToken();

        final UserResult user = new UserResult();
        user.setPushToken(MyApplication.getOneSignalId());

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");
        Call<UserResponse> call = apiService.updateProfile(headerMap, user, MyApplication.getUserID());
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {


                if (response.code() == 200){
                    startActivity(new Intent(LoginActivity.this, SrHomeActivity.class));
                    finish();
                }
//                UserResponse userReponse = response.body();
//                ArrayList<UserResult> userArrayList = null;
//                if (userReponse != null) {
//                    userArrayList = (ArrayList<UserResult>) userReponse.getResults();
//                    if (userArrayList != null && userArrayList.size() > 0) {
//
//
//
//                    }
//                }


            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
            }
        });
    }


}
