package tata.noidaengineer.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.button.MaterialButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tata.noidaengineer.MyApplication;
import tata.noidaengineer.R;
import tata.noidaengineer.Utility;
import tata.noidaengineer.adapters.ImageAdapter;
import tata.noidaengineer.adapters.ImageDownloadAdapter;
import tata.noidaengineer.models.LatLongModel;
import tata.noidaengineer.models.ResponseDownloadImage;
import tata.noidaengineer.models.ResponseDownloadImageResult;
import tata.noidaengineer.models.ResponseMyRequest;
import tata.noidaengineer.models.ResponseMyRequestResult;
import tata.noidaengineer.models.ResponseTicketType;
import tata.noidaengineer.models.ResponseTicketTypeResult;
import tata.noidaengineer.models.ResponseUploadImage;
import tata.noidaengineer.models.ResponseWardResult;
import tata.noidaengineer.networkcommunication.ApiClient;
import tata.noidaengineer.networkcommunication.ApiInterface;
import tata.noidaengineer.networkcommunication.DialogUtils;

import static tata.noidaengineer.SharedPreferences.COMPLETED;
import static tata.noidaengineer.SharedPreferences.IN_PROGRESS;
import static tata.noidaengineer.SharedPreferences.JR_ENGINEER;
import static tata.noidaengineer.SharedPreferences.PENDING;
import static tata.noidaengineer.SharedPreferences.SR_ENGINEER;

public class ObservationActivity extends AppCompatActivity {


    TextView txt_image, txtStatus, txtIssue, txtObservation, txtDescription, txtIssueId, txtWard, txtDate, txtAddress, txtCitizenMobNo, txtCitizenName;
    EditText edtObservation;
    LinearLayout ll_jr_eng_view, ll_sr_observation_field, ll_uploadPhotos, ll_SpinnerStatus, ll_SpinnerObservation, ll_sr_eng_view;
    LatLng complaintLocation;
    private MaterialButton mbSubmit;
    private ImageButton mbUpload;
    private ArrayList<Uri> imagesUriList = new ArrayList<>();
    private ArrayList<ResponseDownloadImageResult> imagesList = new ArrayList<>();
    private ImageAdapter imageAdapter;
    private RecyclerView mRecyclerView;
    private ImageDownloadAdapter imageDownloadAdapter;
    private RecyclerView mRecyclerViewDownload;
    private int imageUploadCounter = 0;
    private ImageButton copyText;
    private MaterialSpinner spinner, spinnerObservation;
    private int spinnerPos = 0, observationPos = 0;
    private int spinnerPosTicketType = 0;
    private ResponseMyRequestResult responseMyRequestResult;
    private ArrayList<ResponseTicketTypeResult> arraylistTicketType = new ArrayList<>();
    private TextInputLayout input_layout_email;
    private Location engineerLocation;
    private double lat = 0.0, longitude = 0.0;
    private LatLongModel latLongModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_observation);


        init();

        if (getIntent().hasExtra("str")) {

            latLongModel = new LatLongModel();

            responseMyRequestResult = (ResponseMyRequestResult) getIntent().getSerializableExtra("str");

            if (responseMyRequestResult != null) {

                getObservationTypes();

//                getWardById(view);

                if (responseMyRequestResult.getLatitude() != null) {
                    lat = Double.parseDouble(String.valueOf(responseMyRequestResult.getLatitude()));
                    latLongModel.setComplaintLatitude(lat);
                }
                if (responseMyRequestResult.getLongitude() != null) {
                    longitude = Double.parseDouble(String.valueOf(responseMyRequestResult.getLongitude()));
                    latLongModel.setComplaintLongitude(longitude);
                }

                if (responseMyRequestResult.getTitle() != null) {
                    txtIssue.setText(responseMyRequestResult.getTitle());
                }

                if (responseMyRequestResult.getMobileNo() != null) {
                    txtCitizenMobNo.setText(responseMyRequestResult.getMobileNo());
                } else {
                    txtCitizenMobNo.setText("Not Available");

                }

                if (responseMyRequestResult.getCitizenName() != null) {
                    txtCitizenName.setText(responseMyRequestResult.getCitizenName());
                } else {
                    txtCitizenName.setText("Not Available");

                }

                if (responseMyRequestResult.getId() != null) {
                    txtIssueId.setText("id_" + responseMyRequestResult.getId() + "_" + responseMyRequestResult.getTitle());
                }

                if (responseMyRequestResult.getObservation() != null) {
                    txtObservation.setText(responseMyRequestResult.getObservation());
                }

                if (responseMyRequestResult.getDescription() != null) {
                    txtDescription.setText(responseMyRequestResult.getDescription());
                }

                if (responseMyRequestResult.getWardName() != null) {
                    txtWard.setText(responseMyRequestResult.getWardName());
                }

                if (responseMyRequestResult.getAddress() != null) {
                    txtAddress.setText(responseMyRequestResult.getAddress());
                }

                if (responseMyRequestResult.getmUpdatedAt() != null) {
                    double l = responseMyRequestResult.getmUpdatedAt();

                    Date date = new Date((long) l * 1000);
                    @SuppressLint("SimpleDateFormat") SimpleDateFormat fmtOut = new SimpleDateFormat("EEE, d MMM yyyy hh:mm a");

                    txtDate.setText(fmtOut.format(date));
                }

                switch (MyApplication.getEngineerType()) {

                    case JR_ENGINEER:

                        ll_sr_observation_field.setVisibility(View.GONE);
                        ll_jr_eng_view.setVisibility(View.VISIBLE);
                        mbSubmit.setVisibility(View.VISIBLE);

                        break;

                    case SR_ENGINEER:

                        ll_sr_observation_field.setVisibility(View.GONE);
                        ll_jr_eng_view.setVisibility(View.VISIBLE);
                        mbSubmit.setVisibility(View.VISIBLE);

                        break;


                    default:

                        ll_sr_observation_field.setVisibility(View.VISIBLE);
                        ll_jr_eng_view.setVisibility(View.GONE);
                        mbSubmit.setVisibility(View.GONE);

                        break;
                }


                mbSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if (spinnerPosTicketType != 0) {

                            if (spinnerPos != 0) {

                                if (imagesUriList.size() > 0) {
                                    patchObservationComplaint();
                                } else {
                                    Utility.showFailureAlert(ObservationActivity.this, "Please Upload Observation Images.");
                                }
                            } else {
                                Utility.showFailureAlert(ObservationActivity.this, "Please Select the Status.");
                            }
                        } else {
                            Utility.showFailureAlert(ObservationActivity.this, "Please Select the Observation.");
                        }
                    }
                });

                mbUpload.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CropImage.activity()
                                .setFixAspectRatio(true)
                                .setAspectRatio(4, 4)
                                .setOutputCompressQuality(60)
                                .setAllowFlipping(true)
                                .setCropShape(CropImageView.CropShape.RECTANGLE)
                                .start(ObservationActivity.this);
                    }
                });


                switch (responseMyRequestResult.getStatus()) {
                    case PENDING:
                        txtStatus.setText("Pending");
                        break;
                    case IN_PROGRESS:
                        txtStatus.setText("In Progress");
                        break;
                    case COMPLETED:
                        ll_sr_observation_field.setVisibility(View.VISIBLE);
                        ll_jr_eng_view.setVisibility(View.GONE);
                        mbSubmit.setVisibility(View.GONE);
                        txtStatus.setText("Resolved");
                        break;
                }


                spinner.setSelectedIndex(spinnerPos);
                spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

                    @Override
                    public void onItemSelected(MaterialSpinner view, int position, long id, String item) {

                        if (item.equalsIgnoreCase("Pending")) {
                            spinnerPos = 1;
                        }
                        if (item.equalsIgnoreCase("In Progress")) {
                            spinnerPos = 2;
                        }
                        if (item.equalsIgnoreCase("Resolved")) {
                            spinnerPos = 3;
                        }

                    }
                });


                spinnerObservation.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

                    @Override
                    public void onItemSelected(MaterialSpinner view, int position, long id, String item) {

                        edtObservation.setText(item);
                        spinnerPosTicketType = arraylistTicketType.get(position).getId();

                        observationPos = position;

                        if (responseMyRequestResult.getStatus() == PENDING) {

                            if (MyApplication.getEngineerType() == SR_ENGINEER) {

                                if (observationPos == 0 || item.equalsIgnoreCase("Other")) {
                                    spinner.setItems("Select", "Resolved");
                                } else {
                                    spinner.setItems("Select", "Pending");
                                }

                                if (item.equalsIgnoreCase("Repeated Complaints")) {
                                    spinner.setItems("Select", "Pending", "Resolved");
                                }
                            } else {

                                if (observationPos == 0) {
                                    spinner.setItems("Select", "Pending");
                                } else {
                                    spinner.setItems("Select", "Pending", "Resolved");
                                }

                                if (item.equalsIgnoreCase("Repeated Complaints")) {
                                    spinner.setItems("Select", "Pending", "Resolved");
                                }

                            }


                        }
//                        else if (responseMyRequestResult.getStatus() == IN_PROGRESS) {
//
//                            if (MyApplication.getEngineerType() == SR_ENGINEER) {
//
//                                if (observationPos == 0) {
//                                    spinner.setItems("Select", "Resolved");
//                                } else {
//                                    spinner.setItems("Select", "Pending");
//                                }
//
//                            } else {
//
//                                if (observationPos == 0) {
//                                    spinner.setItems("Select", "Pending");
//                                } else {
//                                    spinner.setItems("Select", "Resolved");
//                                }
//
//                            }
//
//                        } else if (responseMyRequestResult.getStatus() == COMPLETED) {
//
//                            spinner.setItems("Select");
//
//                        }
                    }
                });

                downloadImages(responseMyRequestResult.getId());

                imageDownloadAdapter.setOnItemClickListener(new ImageDownloadAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        Dialog builder = new Dialog(ObservationActivity.this);
                        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        builder.getWindow().setBackgroundDrawable(
                                new ColorDrawable(android.graphics.Color.TRANSPARENT));
                        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialogInterface) {
                                //nothing;
                            }
                        });

                        ImageView imageView = new ImageView(ObservationActivity.this);

                        Picasso.with(ObservationActivity.this)
                                .load(imagesList.get(position).getImage())
                                .placeholder(R.drawable.loading)
                                .into(imageView);

                        builder.addContentView(imageView, new RelativeLayout.LayoutParams(
                                ViewGroup.LayoutParams.WRAP_CONTENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT));
                        builder.show();
                    }
                });


            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void init() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle("Complaint Observation");

//        if (mActivity instanceof HomeActivity) {
//            ((HomeActivity) mContext).getSupportActionBar().setTitle("Complaint Observation");
//            engineerLocation = ((HomeActivity) mContext).getCurrentLocation();
//
//        } else {
//            ((SrHomeActivity) mContext).getSupportActionBar().setTitle("Complaint Observation");
//
//            engineerLocation = ((SrHomeActivity) mContext).getCurrentLocation();
//        }

        copyText = findViewById(R.id.copyText);
        edtObservation = findViewById(R.id.edtObservation);
        spinnerObservation = findViewById(R.id.spinnerObservation);
        spinner = findViewById(R.id.spinner);
        mbUpload = findViewById(R.id.mbUpload);
        mbSubmit = findViewById(R.id.mbSubmit);

        txt_image = findViewById(R.id.txt_image);
        txtWard = findViewById(R.id.txtWard);
        txtIssue = findViewById(R.id.txtIssue);
        txtDescription = findViewById(R.id.txtDescription);
        txtStatus = findViewById(R.id.txtStatus);
        txtObservation = findViewById(R.id.txtObservation);
        txtDate = findViewById(R.id.txtDate);
        txtIssueId = findViewById(R.id.txtIssueId);
        txtAddress = findViewById(R.id.txtAddress);
        txtCitizenMobNo = findViewById(R.id.txtCitizenMobNo);
        txtCitizenName = findViewById(R.id.txtCitizenName);


        ll_jr_eng_view = findViewById(R.id.ll_jr_eng_view);
        ll_sr_observation_field = findViewById(R.id.ll_sr_observation_field);

        ll_sr_eng_view = findViewById(R.id.ll_sr_eng_view);
        input_layout_email = findViewById(R.id.input_layout_email);


        imageDownloadAdapter = new ImageDownloadAdapter(ObservationActivity.this, imagesList);
        mRecyclerViewDownload = findViewById(R.id.recyclerViewDownload);
        mRecyclerViewDownload.setNestedScrollingEnabled(false);
        LinearLayoutManager mLayoutManager =
                new LinearLayoutManager(ObservationActivity.this, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerViewDownload.setLayoutManager(mLayoutManager);
        mRecyclerViewDownload.setAdapter(imageDownloadAdapter);


        imageAdapter = new ImageAdapter(ObservationActivity.this, imagesUriList);
        mRecyclerView = findViewById(R.id.recyclerViewImage);
        mRecyclerView.setNestedScrollingEnabled(false);
        LinearLayoutManager mLayoutManager1 =
                new LinearLayoutManager(ObservationActivity.this, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager1);
        mRecyclerView.setAdapter(imageAdapter);

        copyText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                copyToClipBoard(txtAddress);
            }
        });

    }

    private void copyToClipBoard(TextView s) {
        android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        android.content.ClipData clip = android.content.ClipData
                .newPlainText(
                        getResources().getString(
                                R.string.message), s.getText());
        clipboard.setPrimaryClip(clip);
        Toast.makeText(ObservationActivity.this, "Address Copied To ClipBoard.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_map:
                Log.i("item id ", item.getItemId() + "");

                if (engineerLocation != null && lat != 0.0 || longitude != 0.0) {
                    if (engineerLocation != null) {
                        latLongModel.setEngineerLatitude(engineerLocation.getLatitude());
                        latLongModel.setEngineerLongitude(engineerLocation.getLongitude());
                    }
                    startActivity(new Intent(ObservationActivity.this, MapsActivity.class).putExtra("latLongModel", latLongModel));
                } else {
                    Utility.showFailureAlert(this, "Complaint Location Not Available.");
                }


            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void getObservationTypes() {

        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(ObservationActivity.this, "");

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseTicketType> call = apiService.getObservationTypes(headerMap);
        call.enqueue(new Callback<ResponseTicketType>() {
            @Override
            public void onResponse(Call<ResponseTicketType> call, Response<ResponseTicketType> response) {
                ResponseTicketType responseTicketType = response.body();


                if (responseTicketType != null && responseTicketType.getResults() != null) {

                    progressDialog.dismiss();

                    arraylistTicketType.addAll(responseTicketType.getResults());
                    ArrayList<String> strings = new ArrayList<>();

                    for (ResponseTicketTypeResult wResultard : responseTicketType.getResults()) {

                        strings.add(wResultard.getTitle());
                    }

                    spinnerObservation.setItems(strings);

                } else {

                    Utility.showFailureAlert(ObservationActivity.this, getString(R.string.complaint_fetch_failed));


                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseTicketType> call, Throwable t) {
                Utility.showFailureAlert(ObservationActivity.this, getString(R.string.internal_error));
                progressDialog.dismiss();
            }
        });
    }


    private void getWardById(final View view) {

        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(ObservationActivity.this, "");

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseWardResult> call = apiService.getWardById(headerMap, responseMyRequestResult.getWard());
        call.enqueue(new Callback<ResponseWardResult>() {
            @Override
            public void onResponse(Call<ResponseWardResult> call, Response<ResponseWardResult> response) {
                ResponseWardResult responseWardResult = response.body();


                if (responseWardResult != null) {

                    progressDialog.dismiss();

                    TextView txtWard = view.findViewById(R.id.txtWard);
                    txtWard.setText(responseWardResult.getName());

                } else {

                    Utility.showFailureAlert(ObservationActivity.this, getString(R.string.ward_fetch_failed));


                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseWardResult> call, Throwable t) {
                Utility.showFailureAlert(ObservationActivity.this, getString(R.string.internal_error));

                progressDialog.dismiss();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                imagesUriList.add(resultUri);
                imageAdapter.notifyDataSetChanged();

                if (imagesUriList.size() > 0) {

                    txt_image.setText("Upload Photos (" + imagesUriList.size() + ")");
                    mRecyclerView.setVisibility(View.VISIBLE);
                } else {
                    mRecyclerView.setVisibility(View.GONE);
                }

                // Toast.makeText(mContext, resultUri.toString() + "", Toast.LENGTH_SHORT).show();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }


    private void patchObservationComplaint() {

        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(ObservationActivity.this, "");

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);
        headerMap.put("Content-Type", "application/json");

        ResponseMyRequestResult requestRegisterComplaint =
                new ResponseMyRequestResult(responseMyRequestResult.getId(),
                        spinnerPosTicketType,
                        responseMyRequestResult.getTitle(),
                        edtObservation.getText().toString(),
                        responseMyRequestResult.getZone(),
                        responseMyRequestResult.getWard(),
                        responseMyRequestResult.getEngineer(),
                        MyApplication.getUserID(),
                        responseMyRequestResult.getLatitude(),
                        responseMyRequestResult.getLongitude(),
                        spinnerPos, true, false);


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseMyRequest> call = apiService.patchObservation(headerMap, requestRegisterComplaint, responseMyRequestResult.getId());
        call.enqueue(new Callback<ResponseMyRequest>() {
            @Override
            public void onResponse(Call<ResponseMyRequest> call, @NonNull Response<ResponseMyRequest> response) {
                ResponseMyRequest responseWard = response.body();

                progressDialog.dismiss();
                if (response.code() == 201 || response.code() == 200) {

                    if (imagesUriList.size() == 0) {
                        Utility.showSuccessAlert(ObservationActivity.this, "Complaint has been observed. You will receive a confirmation shortly.");

                    } else {
                        for (Uri uri : imagesUriList) {

                            if (responseWard != null) {
                                uploadFile(uri, String.valueOf(responseMyRequestResult.getId()));
                            }
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<ResponseMyRequest> call, Throwable t) {
                // Log error here since request failed
                Utility.showFailureAlert(ObservationActivity.this, getString(R.string.internal_error));

                progressDialog.dismiss();
            }
        });
    }


    // Uploading Image/Video
    private void uploadFile(Uri mediaPath, String ticketId) {


        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(ObservationActivity.this, "Uploading Images...");

        File file = null;
        try {
            String path = Utility.getPath(ObservationActivity.this, mediaPath); // "/mnt/sdcard/FileName.mp3"

            file = new File(path);

            String token = MyApplication.getUserToken();

            Map<String, String> headerMap = new HashMap<>();
            headerMap.put("Authorization", token);

            // Parsing any Media type file
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("image", file.getName(), requestBody);
            RequestBody id = RequestBody.create(MediaType.parse("text/plain"), ticketId);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<ResponseUploadImage> call = apiService.uploadFile(headerMap, fileToUpload, id);
            call.enqueue(new Callback<ResponseUploadImage>() {
                @Override
                public void onResponse(@NonNull Call<ResponseUploadImage> call, @NonNull Response<ResponseUploadImage> response) {

                    imageUploadCounter++;
                    ResponseUploadImage serverResponse = response.body();
                    if (serverResponse != null) {


                    } else {

                        if (serverResponse != null) {
                            Log.d("uploadFile", serverResponse.toString());
                        }
                    }

                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }

                    if (imageUploadCounter == imagesUriList.size()) {
                        Utility.showSuccessAlert(ObservationActivity.this, "Complaint has been observed. You will receive a confirmation shortly.");

                        finish();

                    }
                }

                @Override
                public void onFailure(Call<ResponseUploadImage> call, Throwable t) {
                    Log.d("uploadFile", t.toString());
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }
                }
            });
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }


    }

    // downloading Image/Video
    private void downloadImages(int ticketId) {


        final ProgressDialog progressDialog = DialogUtils.startProgressDialog(ObservationActivity.this, "Images...");

        String token = MyApplication.getUserToken();

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", token);


        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseDownloadImage> call = apiService.downloadImages(headerMap, ticketId);
        call.enqueue(new Callback<ResponseDownloadImage>() {
            @Override
            public void onResponse(@NonNull Call<ResponseDownloadImage> call, @NonNull Response<ResponseDownloadImage> response) {

                ResponseDownloadImage serverResponse = response.body();
                if (serverResponse != null) {

                    imagesList.addAll(serverResponse.getResults());
                    imageDownloadAdapter.notifyDataSetChanged();
                    if (imagesList.size() > 0) {
                        mRecyclerViewDownload.setVisibility(View.VISIBLE);
                    }


                } else {

                    if (serverResponse != null) {
                        Log.d("uploadFile", serverResponse.toString());
                    }
                }

                if (progressDialog != null) {
                    progressDialog.dismiss();
                }

            }

            @Override
            public void onFailure(Call<ResponseDownloadImage> call, Throwable t) {
                Log.d("uploadFile", t.toString());
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
            }
        });


    }

}
