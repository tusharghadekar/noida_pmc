package tata.noidaengineer;

public interface SharedPreferences {
    String DEFAULT_VALUE = "";
    String EMAIL = "email";
    String USER_TYPE = "user_type";
    String MOBILE_NO = "mobile_no";


    //roll
    int ADMIN = 1;
    int SR_ENGINEER = 2;
    int JR_ENGINEER = 3;
    int CITIZEN = 4;
    int MANAGER = 5;
    int ZONAL_HEAD = 6;


    //status
    int PENDING = 1;
    int IN_PROGRESS = 2;
    int COMPLETED = 3;
    Boolean ESCALATED = false;


    int ISSUE = 1;
    int OBSERVATION = 2;

    //WORKING_FROM
    int PMC = 0;
    int TATAPOWER = 1;
    int MSEB = 2;


}
