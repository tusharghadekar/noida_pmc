package tata.noidaengineer;
/*
 * Created by Kedar Labde on 12/30/2016.
 */

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.tapadoo.alerter.Alerter;

import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utility {

    private static final int INTERNET_CONNECTION_SUCCESSFUL = 0;
    /*
     * Constant date format for date use for  */
    public static SimpleDateFormat SIMPLE_DATE_FORMAT =
            new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());

    public static String setMaskXXX(String maskXXX) {
        StringBuilder str = new StringBuilder(maskXXX);
        if (maskXXX.length() > 3) {

            return (str.substring(0, 3)) + "XXX";
        }
        return str.toString();
    }

    private static void removeAllFragments(FragmentManager fragmentManager) {
        while (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStackImmediate();
        }
    }

    /**
     * This method is use to check the device internet connectivity.
     *
     * @return true :if your device is connected to internet.
     * false :if your device is not connected to internet.
     */
    public static boolean isConnected(Context context) {
        ConnectivityManager manager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = manager.getActiveNetworkInfo();

        if (info == null) return false;
        if (info.getState() != NetworkInfo.State.CONNECTED) return false;

        return true;
    }

    public static boolean isValidEmail(CharSequence inputStr) {

        String expression =
                "^[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\\.)+[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?$";
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            return true;
        }
        return false;
    }

    public static boolean isPhoneNumber(CharSequence inputStr) {

        String expression = "^[0-9]{13}$";
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            return true;
        }
        return false;
    }

    public static void getSettingsListViewSize(ListView myListView) {
        ListAdapter myListAdapter = myListView.getAdapter();
        if (myListAdapter == null) {
            //do nothing return null
            return;
        }
        //set listAdapter in loop for getting final size
        int totalHeight = 0;
        for (int size = 0; size < myListAdapter.getCount(); size++) {
            View listItem = myListAdapter.getView(size, null, myListView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        //setting listview item in adapter
        ViewGroup.LayoutParams params = myListView.getLayoutParams();
        params.height = totalHeight + (myListView.getDividerHeight() * (myListAdapter.getCount() - 1));
        myListView.setLayoutParams(params);
    }

    public static void getListViewSize(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int desiredWidth =
                View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0) {
                view.setLayoutParams(
                        new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));
            }
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public static boolean passwordValidator(String password) {
        String PASSWORD_PATTERN = "^(?!.* ).{6,25}$";
        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public static boolean passwordMatch(String password, String conformPassword) {
        return password.equals(conformPassword);
    }

    public static String fromHtml(String string) {
        return Html.fromHtml(string).toString();
    }

    public static String ToHtml(String string) {
        return TextUtils.htmlEncode(string);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm =
                (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        assert imm != null;
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }

    public static void setKeyboardOnlyOnEdittext(View view, final Context context) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof TextInputEditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                @SuppressLint("ClickableViewAccessibility")
                public boolean onTouch(View v, MotionEvent event) {
                    hideKeyboard((Activity) context);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setKeyboardOnlyOnEdittext(innerView, context);
            }
        }
    }

    /***
     *
     * @param mString this will setup to your textView
     * @param colorId  text will fill with this color.
     * @return string with color, it will append to textView.
     */
    public static Spannable getColoredString(String mString, int colorId) {
        Spannable spannable = new SpannableString(mString);
        spannable.setSpan(new ForegroundColorSpan(colorId), 0, spannable.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannable;
    }

    public static void showFailureAlert(Activity activity, String message) {
        if (activity != null) {
            Alerter.create(Objects.requireNonNull(activity))
                    .setText(message)
                    .setDuration(2000)
                    .enableSwipeToDismiss()
                    .setTextAppearance(R.style.AlertTextAppearance_Title)
                    .setIcon(R.drawable.ic_error)
                    .setBackgroundColorRes(R.color.colorAccent)
                    .show();
        }
    }

    public static void showFailureAlert(Fragment activity, String message) {
        if (activity != null) {
            Alerter.create(Objects.requireNonNull(activity.getActivity()))
                    .setText(message)
                    .setDuration(2000)
                    .setTextAppearance(R.style.AlertTextAppearance_Title)
                    .enableSwipeToDismiss()
                    .setIcon(R.drawable.ic_error)
                    .setBackgroundColorRes(R.color.colorAccent)
                    .show();
        }
    }

    public static void showSuccessAlert(Fragment activity, String message) {
        if (activity != null) {
            Alerter.create(Objects.requireNonNull(activity.getActivity()))
                    .setText(message)
                    .setDuration(2000)
                    .setTextAppearance(R.style.AlertTextAppearance_Title)
                    .enableSwipeToDismiss()
                    .setBackgroundColorRes(R.color.green)
                    .show();
        }
    }

    public static void showSuccessAlert(Activity activity, String message) {
        if (activity != null) {
            Alerter.create(Objects.requireNonNull(activity))
                    .setText(message)
                    .setDuration(2000)
                    .setTextAppearance(R.style.AlertTextAppearance_Title)
                    .enableSwipeToDismiss()
                    .setBackgroundColorRes(R.color.green)
                    .show();
        }
    }

    //  public static void showNoInternetAlert(final Fragment activity, String message) {
    //    if (activity != null) {
    //      Alerter.create(Objects.requireNonNull(activity.getActivity()))
    //              .setTitle("No Internet Connection")
    //              .setText(message)
    //              .disableOutsideTouch()
    //              .enableInfiniteDuration(true)
    //              .setIcon(R.drawable.ic_signal)
    //              .setBackgroundColorRes(R.color.colorAccent)
    //              .setOnClickListener(new View.OnClickListener() {
    //                @Override public void onClick(View view) {
    //                  activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS),
    //                          INTERNET_CONNECTION_SUCCESSFUL);
    //                }
    //              })
    //              .show();
    //    }
    //  }

    //  public static void showNoInternetAlertFragment(final Fragment fragment, String message) {
    //    if (fragment != null) {
    //      Alerter.create(fragment.getActivity())
    //              .setTitle("No Internet Connection")
    //              .setText(message)
    //              .disableOutsideTouch()
    //              .enableInfiniteDuration(true)
    //              .setIcon(R.drawable.ic_signal)
    //              .setBackgroundColorRes(R.color.colorAccent)
    //              .setOnClickListener(new View.OnClickListener() {
    //                @Override public void onClick(View view) {
    //                  fragment.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS),
    //                          INTERNET_CONNECTION_SUCCESSFUL);
    //                }
    //              })
    //              .show();
    //    }
    //  }

    @SuppressLint("NewApi")
    public static String getPath(Context context, Uri uri) throws URISyntaxException {
        final boolean needToCheckUri = Build.VERSION.SDK_INT >= 19;
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        // deal with different Uris.
        if (needToCheckUri && DocumentsContract.isDocumentUri(context.getApplicationContext(), uri)) {
            if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{split[1]};
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {MediaStore.Images.Media.DATA};
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static Date fromStringToDate(String stringDate, String pattern) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat(pattern);
        try {
            Date date = format.parse(stringDate);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
